<figure>
    ![Leonardo Feltrin Foletto](imagenes/Leonardo-Feltrin-Foletto.png)
    <figcaption>Foto: Sheila Uberti</figcaption>
</figure>

Leonardo Feltrin Foletto nació en Taquari, interior de Río Grande del
Sur. Formado como periodista en la UFSM, en Santa María (Río Grande del
Sur), obtuvo un máster en Periodismo en la UFSC y un doctorado en
Comunicación en la UFRGS, con investigaciones relacionadas con la
comunicación, la tecnología y el activismo. Como periodista
redactor, ha estado en *A Razão*, de Santa María, y en la Folha de
S.Paulo (Ilustrada). Ha sido profesor visitante en algunas universidades
(PUC-RS, UCS, Unisinos, PUC-SP e Unochapecó) y escribió <cite>Efêmero
revisitado: conversas sobre teatro e cultura digital</cite>, con
una beca de la Fundação Nacional das Artes (Funarte) en 2011. Desde
2007 trabaja en comunicación digital, cultura libre y tecnopolítica en
Brasil y Iberoamérica en proyectos como Casa da Cultura Digital (São
Paulo y Porto Alegre), Ônibus Hacker, Festival BaixoCentro, Fórum
Internacional do Software Livre (FISL), Rede de Produtoras Culturais
Colaborativas, <i lang="en">hackerspace</i> Matehackers, Labhacker,
Creative Commons Brasil e LabCidade (FAU-USP), entre otros. Además de
*BaixaCultura* ([baixacultura.org](https://baixacultura.org/)), espacio
en línea sobre cultura libre y (contra)cultura digital activo desde
2008, laboratorio y extensión de buena parte de las ideas presentadas
aquí.
