Este libro nace de un esfuerzo que data del 2008, año de creación de
BaixaCultura[^1], blog, sitio web, proyecto, laboratorio en línea creado
por mí y por el poeta Reuben da Cunha Rocha, entonces alumnos de máster
de universidades públicas en un Brasil que aún creía en el futuro. Al tratar de
escribir sobre productos culturales que pudiesen ser consumidos
(apreciados, poseídos, gozados) en Internet, en pocos meses nos
cruzamos con la cultura libre, entonces una idea que hablaba de la
principal discusión en el Internet global de aquel momento: la
compartición de archivos en la red y las disputas en torno a la
(i)legalidad de este acto. Luego tiramos del hilo:
<i lang="en">software</i> libre, <i lang="en">copyleft</i>, cultura
digital, jáqueres, ciberactivismo vinieron por un lado;
<i lang="en">remix</i>, plagio, apropiación, arte radical, contracultura,
por otro. Unimos estos dos hilos con la piratería, la compartición
y la discusión tecnopolítica.

[^1]: Disponible en <https://baixacultura.org>.

Once años después, más de trescientos textos publicados y numerosos
debates, proyecciones de películas, talleres, conferencias, charlas y
entrevistas realizadas, BaixaCultura continuaba. Sin Reuben desde 2010,
me tocó a mí, con ayuda de varias personas a lo largo de ese período,
mantener el espacio abierto, ahora en otro Internet y con la agenda de
compartición de archivos y de cultura libre con menos espacio en todos
los lugares. Las promesas de transformación radical de la sociedad a las
que Internet incitaba en muchos de nosotros durante aquella época se
transformaron en algo parecido a una pesadilla. En 2020, no se podía
escapar de una palabra para describirlo: *distopía*. Aún así, la
compartición de archivos en la Red continúa inquebrantable en los guetos
contraculturales y de jáqueres; la cultura libre sigue como movimiento
no solo a favor de una cultura, sino también de un conocimiento libre y
de los bienes comunes; el <i lang="en">copyleft</i> se mantiene como uno
de los mayores <i lang="en">hacks</i> en más de tres siglos de derechos
de autor en Occidente; el <i lang="en">software</i> libre permanece como
una utopía de construcción colaborativa y solidaria de tecnologías que,
por ahora, y por poco, ha perdido la oportunidad de ser la realidad
global; y el <i lang="en">remix</i> se ha convertido en la principal forma
de creación artística en un mundo que, más conectado que nunca, ya no
duda de que solo se crea recreando.

Por todos estos motivos, sigue siendo importante hablar de cultura
libre. A pesar del objetivo debatido por muchas personas en BaixaCultura
en ese período y en otros lugares diversos, lo que este libro pretende
es examinar una idea que comenzó mucho antes que el Internet y
que permanecerá mientras haya seres humanos vivos creando. Sería, sin
embargo, una tarea extensa y titánica dar cuenta de todos los aspectos que
rodean una idea de historia tan extensa. Por eso la elección de tratar
de situar, contextualizar, recuperar y debatir una *base común* sobre un
tema, con ayuda de muchas áreas &mdash;historia, derecho, comunicación,
arte, sociología, antropología, ciencia política, estudios de ciencia y
tecnología, informática&mdash;.

Desarrollada y difundida como idea en la década de los 1990, en los
primeros años de Internet en el mundo, la cultura libre se nutre
directamente del concepto del <i lang="en">software</i> libre y del
<i lang="en">copyleft</i>, ambas creaciones relacionadas con productos
tecnológicos &mdash;el software&mdash; del inicio de los años 80. Su
base, por tanto, está relacionada con el desarrollo de la tecnología
digital, así como su popularización es fruto de un escenario de
expansión del acceso a la información a partir de Internet. Pero la idea
de cultura libre, por lo menos desde la perspectiva con la que la abordo
aquí, tiene una historia que comienza mucho antes que el
<i lang="en">software</i> libre y que Internet. Hablar de formas libres de
creación, uso, modificación, consumo, protección y reproducción de
cultura pasa por entender las maneras de producir y difundir información
y cultura en diferentes períodos históricos, como la Antigüedad, la Edad
Media y la Modernidad; por considerar los mecanismos creados por el
derecho occidental para controlar (y restringir) la creación
intelectual; por entender cómo inventos tecnológicos como la imprenta,
el gramófono, el cine, la radio, la fotografía, los ordenadores y,
principalmente, Internet tienen gran importancia en la alteración de
todos los aspectos de la creación cultural. Hablar de cultura libre es
también echar un vistazo a cómo se estaban creando las ideas de autoría,
propiedad intelectual, original y copia, sin olvidar las nociones de
Extremo Oriente y de los pueblos indígenas de las Américas sobre esos
asuntos; es observar cómo las personas, grupos y movimientos
subvirtieron el <i lang="lt">statu quo</i> de la creación y circulación
de la cultura de sus épocas, en especial a lo largo del siglo XX, y de las
implicaciones políticas de sus acciones.

Pensado durante mucho tiempo y comenzada finalmente su escritura en
2019, este libro investiga la cultura libre también en dos aspectos
conocidos: el de la remuneración de los creadores, que debería
garantizar la continuidad de la producción de sus obras, y el del
acceso, (re)utilización y circulación de las obras, que prometería a la
humanidad el derecho de disfrutarlas y recrearlas. En esos dos polos,
muchos veces contrapuestos, hay matices y cuestionamientos, entre los
cuales la propia concepción de que alguien pueda ser dueño de una idea,
una melodía, una frase, una imagen, una tecnología, y la opinión de que
una obra no puede ser compartida o consumida sin algún pago a quien la
creó. Daré por cumplido el objetivo de este libro si, al final, diera a
entender que hay muchos más matices (y polos) para ver y entender la
cultura libre de los que te puedes imaginar.

<br><p class="right-align">Leonardo Foletto</p>

<p class="right-align">São Paulo, invierno de 2020</p>

