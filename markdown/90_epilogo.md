Después de hablar tanto de creación, reapropiación, propiedad, copia,
procomún, <i lang="en">copyleft</i> y <i lang="en">copyright</i> a través
de tiempos, de lugares y de visiones del mundo diferentes, conviene
preguntar: ¿y este libro? ¿Cuál es la marca del autor para uso (privado
o público), la cita y la reapropiación? ¿Se emplea alguna licencia?
¿Cuál?

Mi &mdash;*nuestra*, porque, a pesar de haber un nombre detrás
de esta obra, ella no deja de ser colectiva, como os habéis dado cuenta
a lo largo de la lectura&mdash; elección es la licencia que representa
el <i lang="en">copyleft</i>: Creative Commons CC BY-SA[^298]&mdash;.

[^298]: Su texto está completamente disponible aquí: <https://creativecommons.org/licenses/by-sa/4.0/>.

<figure>
    ![Licencia Creative Commons BY-SA](imagenes/Creative_Commons_BY-SA.png)
    <figcaption></figcaption>
</figure>

Esta dice que este trabajo puede ser *compartido* &mdash;copiado y
redistribuido&mdash; en cualquier medio o formato y *adaptado*
&mdash;combinado, transformado&mdash; para cualquier propósito; siempre
que haya *atribución* de autoría, lo que significa que cualquier
uso debe mencionar quién escribió esta obra y dónde fue modificada
&mdash;partiendo de que quien quiera compartir, usar y adaptar este
libro lo hará de manera razonable&mdash;, y que cualquier obra derivada
de esta sea compartida bajo la misma licencia aquí descrita, una
garantía que no permite el cierre de esta obra con una licencia que
restrinja todas las indicaciones citadas anteriormente.

El alcance de esta licencia se aplica a las formas materiales con
las que esta obra circula: impresa como libro, en formato de archivo
digital de libro electrónico y publicada en partes en plataformas de
Internet. La elección de esta parte del presupuesto de que este trabajo
solo existe porque muchos otros existieron, y de que fomentar otras obras
será un elogio a las ideas que aquí circulan. Conocemos las
posibilidades de apropiación indebida y descuidada que muchos ya
hicieron con obras similares, pero elegimos correr ese riesgo para
garantizar que este libro sea libre para diferentes fines, incluido el
comercial.

En este aspecto, animamos al uso, la reproducción y la (re)venta de
este trabajo para fortalecer a pequeñas editoriales y sellos
alternativos, siempre que se respeten las orientaciones ya indicadas; si
quisieras hacer eso, nos sentiríamos felices si nos lo haces saber.
Recordamos, no obstante, que el trabajo de editoriales independientes
como esta necesita ser remunerado para que continúe existiendo. Por eso,
considera comprarlo impreso y, así, valorar las elecciones editoriales
y gráficas hechas aquí, así como la inversión financiera realizada
&mdash;es esto lo que hará que otras obras como esta sean
publicadas&mdash;. Finalmente, recordamos que la mejor experiencia de
lectura de este texto &mdash;como muchos otros&mdash; es aquella
proporcionada por esta invención de hace miles de años llamada *libro
impreso*, con el olor del papel penetrando las fosas nasales y animando
a una lectura lenta, de anotaciones y subrayados diversos que incitan a
diálogos y conducen a una experiencia única y singular de conocer.

