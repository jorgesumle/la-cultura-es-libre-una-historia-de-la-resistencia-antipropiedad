
ACOSTA, Alberto. <cite>O bem viver</cite>: uma oportunidade
para imaginar outros mundos. Trad. Tadeu Breda. São
Paulo: Autonomia Literária; Elefante, 2016.

ALFORD, William P. <cite>Steal a Book Is an Elegant Offense</cite>:
Intellectual Property Law in Chinese Civilization. Stanford: Stanford University Press, 1995.

ARMSTRONG, Elizabeth. <cite>Before Copyright</cite>: The
French Book-Privilege System 1498-1526. Cambridge:
Cambridge University Press, 1990.

BAKER, Pam. The Open Source Programa at
Microsoft: How Open Source Thrives. <cite>The Linux
Foundation</cite>, 2 mar. 2018. Disponible en
<https://www.linuxfoundation.org/blog/2018/03/open-source-program-microsoft-open-source-thrives>.

BARBROOK, Richard; CAMERON, Andy. <cite>A ideologia
californiana</cite>: uma crítica ao livre mercado nascido no Vale
do Silício. Trad. Marcelo Träsel. Porto Alegre; União da
Vitória: BaixaCultura; Monstro dos Mares, 2018.

______; ______. The Californian Ideology. <cite>Mute</cite>, v. 1,
n. 3, 1 sep. 1995. Disponible en <https://www.metamute.org/editorial/articles/californian-ideology>.

BARLOW, John Perry. A Declaração de Independência
do ciberespaço. In: FÓRUM ECONÔMICO MUNDIAL,
8 feb. 1996, Davos, Suíça. Disponible en <http://www.dhnet.org.br/ciber/textos/barlow.htm>.

______. The Economy of Ideas. <cite>Wired</cite>, 3 ene.
1994. Disponible en <https://www.wired.com/1994/03/economy-ideas>.

BASTOS, Marcus. A cultura da reciclagem. In: ROSAS, Ricardo; SALGADO, Marcos (orgs.). <cite>Recombinação</cite>. 2002. Disponible en <https://virgulaimagem.redezero.org/rizoma-net>.

BELISÁRIO, Adriano; TARIN, Bruno (orgs.). <cite>Copyfight</cite>.
Rio de Janeiro: Azougue, 2012.

BENJAMIN, Walter. <cite>A obra de arte na era de sua reprodutibilidade técnica</cite>. Trad. Gabriel Valladão Silva. Porto
Alegre: L&PM, 2018.

BENKLER, Yochai. <cite>The Wealth of Networks</cite>: How Social Production Transforms Markets and Freedom. New
Haven: Yale University Press, 2006.

BERARDI, Franco. <cite>Depois do futuro</cite>. Trad. Regina Silva. São Paulo: Ubu, 2019.

______. <cite>Generación post-alfa</cite>: patologías e imaginarios
en el semiocapitalismo. Buenos Aires: Tinta Limón, 2007.

BIDDLE, Sam. Coronavírus traz novos riscos de abuso
de vigilância digital sobre a população. <cite>The Intercept</cite>, 6 abr.
2020. Disponible en
<https://theintercept.com/2020/04/06/coronavirus-covid-19-vigilancia-privacidade>.
Consultado el 29 sep. 2020.

BLISSET, Luther. <cite>Q</cite>: o caçador de hereges. São Paulo:
Conrad, [1999] 2002.

BOLLIER, David. <cite>Pensar desde los comunes</cite>: una breve
introducción. Madri: Traficantes de Sueños, 2016.

______. <cite>Think Like a Commoner</cite>: A Short Introduction
to the Life of the Commons. Gabriola Island (Canadá):
New Society Publishers, 2014.

______. <cite>Viral Spiral</cite>: How the Commoners Built a Digital
Republic of their Own. Nova York: New Press, 2009.

BOUCHERON, Patrick. <cite>Como se revoltar?</cite> Trad. Cecília Ciscato. São Paulo: Editora 34, 2018.

BRIGGS, Asa; BURKE, Peter. <cite>Uma história social da
mídia</cite>: de Gutenberg a Diderot. Trad. Maria Carmelita Pádua Dias; rev. técn. Paulo Vaz. 2.ed. Rio de Janeiro: Zahar,
2006.

BRIGHTMAN, Marc; FAUSTO, Carlos; GROTTI, Vanessa (orgs.). <cite>Ownership and Nurture</cite>: Studies in Native
Amazonian Property Relations. Nova York: Berghahn
Books, 2016.

BURROUGHS, William. O método <i lang="en">cut-up</i>. Trad. Ricardo Rosas; Arquivo Rizoma. In: ROSAS, Ricardo; SALGADO, Marcos (orgs.). <cite>Recombinação</cite>. 2002. En línea. Disponible en <https://virgulaimagem.redezero.org/rizoma-net>.

CAMPOS, Haroldo de. <cite>Serafim</cite>: um grande não-livro.
In: ANDRADE, Oswald de. <cite>Serafim Ponte Grande</cite>. 2.ed. Rio
de Janeiro; Brasília: Instituto Nacional do Livro; MEC, 1971.

CARNEIRO DA CUNHA, Manuela. “Cultura” e cultura: conhecimentos tradicionais e direitos intelectuais. In:
<cite>Cultura com aspas e outros ensaios</cite>. São Paulo: Cosac &
Naify, 2009. pp. 311-373.

COELHO DE SOUZA, Marcela Stockler. The Forgotten Pattern and the Stolen Design: Contract, Exchange and
Creativity among the Kĩsêdjê. In: BRIGHTMAN, Marc;
FAUSTO, Carlos; GROTTI, Vanessa (orgs.). <cite>Ownership
and Nurture</cite>: Studies in Native Amazonian Property
Relations. 1.ed. Nova York: Berghahn Books, 2016.

COLEMAN, Gabriella. <cite>Coding Freedom</cite>: The Ethics and Aesthetics of Hacking. Princeton: Princeton
University Press, 2013.

CONDORCET, Jean-Antoine-Nicolas de Caritat, marquês de. Fragments sur la liberté de la presse. In: <cite>Œuvres
de Condorcet</cite>. T. 11. Paris: Firmin Didot Frères, 1847.

CONFÚCIO. <cite>Os analectos</cite>. Trad. do chinês para o inglês, introdução e notas D. C. Lau. Trad. do inglês Caroline Chang. Porto Alegre: L&PM, 2007.

CONTRERAS, Pau. <cite>Me llamo Kofham</cite>: identidad hacker.
Una aproximación antropológica. Barcelona: Gedisa, 2004.

DE ANGELIS, Massimo. Introduction. <cite>The Commoner</cite>,
n. 11, p. 1, 2006. Disponible en
<http://www.commoner.org.uk/?p=24>. Consultado el 19 mar. 2020.

DEAK, André; FOLETTO, Leonardo. Ambiente digital
de difusão: por onde circula a cultura on-line? <cite>BaixaCultura</cite>, 14 jun. 2019. En línea. Disponible en
<http://baixacultura.org/ambiente-digital-de-difusao-por-onde-circula-a-cultura-online>.

DEBORD, Guy. <cite>A sociedade do espetáculo</cite>. Rio de Janeiro: Contraponto, [1967] 1998.

______; WOLMAN, Gil. <cite>O guia dos usuários do detournamènt</cite>. Porto Alegre: BaixaCultura, [1956] 2015.

DIDEROT, Denis. <cite>Carta sobre o comércio do livro</cite>.
Trad. Bruno Feitler. Rio de Janeiro: Casa da Palavra, 2002.

DIRINGER, David. <cite>The Book before Printing</cite>: Ancient,
Medieval and Oriental. Nova York: Dover, 1982.

EINSENSTEIN, Elizabeth. <cite>The Printing Revolution in
Early Modern Europe</cite>. 2.ed. Nova York: Cambridge University Press, 2005.

ENZENSBERGER, Hans Magnus. <cite>Der kurtze Sommer
der Anarchie</cite>. Berlim: Suhrkamp, 1977. [ed. bras.: <cite>O curto
verão da anarquia</cite>. Trad. Marcio Suzuki. São Paulo: Companhia das Letras, 1987.]

EZE, Michael Onyebuchi. <cite>Intelectual History in Contemporary South Africa</cite>. Nova York: Palgrava MacMillan, 2010.

FENG, Peter; FENG, Xyang. <cite>Intellectual Property in
China</cite>. Hong Kong: Sweet & Maxwell Asia, 2003.

FEDERICI, Silvia. <cite>Revolución en punto cero</cite>: Trabajo
doméstico, reproducción y luchas feministas. Madrid: Traficantes de
Sueños, 2013.

FOLETTO, Leonardo. <cite>Um mosaico de parcialidades na
nuvem coletiva</cite>: rastreando a mídia ninja (2013-2016). Por-
to Alegre, 2017. Tese (Doutorado na área de Comunicação
e Informação) – Faculdade de Biblioteconomia e Comunicação, Universidade Federal do Rio Grande do Sul.

______. Midiativismo, mídia alternativa, radical, livre, tática: um inventário de conceitos semelhantes. In:
BRAIGHI, Antônio Augusto; LESSA, Cláudio; CÂMARA,
Marco Túlio (orgs.). <cite>Interfaces do midiativismo</cite>: do conceito à prática. Belo Horizonte: Cefet-MG, 2018. pp. 95-110.

FOLETTO, Leonardo. Ressaca da internet, espírito do
tempo. <cite>BaixaCultura</cite>, 9 jul. 2018. Disponible en
<http://baixacultura.org/ressaca-da-internet-espirito-do-tempo>.

______. Cultura hacker e jornalismo: práticas jornalísticas <i lang="en">do it yourself</i> na comunidade brasileira, transparência hacker. In: Congreso Internacional de la Unión
Latina de Economía Política de la Información (Ulepicc),
8, 2013, Quiles, Argentina. Disponible en
<http://www.leofoletto.info/wp-content/uploads/2015/09/artigo_etica_hacker_e_jornalismo_ulepicc_2013.pdf>.

______; MARTINS, Beatriz; LUNA, Carlos. Encontro
On-Line Cultura Livre do Sul: a produção cultural comunitária para a construção do comum. <cite>Contratexto</cite>, n. 33, pp.
105-124, jun. 2020.

FOUCAULT, Michel. O que é um autor? In: <cite>Ditos e
escritos</cite>. V. 3: Estética: literatura e pintura, música e cinema. Disponible en
<https://edisciplinas.usp.br/pluginfile.php/179076/mod_resource/content/1/Foucault%20Michel%20-%20O%20que%20%C3%A9%20um%20autor.pdf>.

GARCIA DOS SANTOS, Laymert. <cite>Politizar as novas
tecnologias</cite>: o impacto sociotécnico da informação digital
e genética. São Paulo: Editora 34, 2004.

GEERTZ, Clifford. <cite>A interpretação das culturas</cite>. Rio de
Janeiro: LTC, 2008.

GINZBURG, Carlo. <cite>O queijo e os vermes</cite>: o cotidiano e as ideias de um moleiro perseguido pela Inquisição.
8.reimp. São Paulo: Companhia das Letras, 1996.

______. <cite>Os andarilhos do bem</cite>: feitiçarias e cultos agrários nos séculos XVI e XVII. 1.reimp. São Paulo: Companhia das Letras, 1990.

______. A micro-história e outros ensaios. Rio de Janeiro: Bertrand, 1989.

GUATTARI, Félix; ROLNIK, Suely. <cite>Micropolíticas</cite>: cartografias do desejo. 4.ed. Petrópolis: Vozes, 1996.

GUDYNAS, Eduardo. <cite>Direitos da natureza</cite>: ética biocêntrica e políticas ambientais. São Paulo: Elefante, 2019.

HAN, BYUNG-CHUL. <cite>Shanzai</cite>: el arte de la falsificación y la deconstrucción en China. Buenos Aires: Caja
Negra, 2016.

HARDT, Michael; NEGRI, Antonio. <cite>Multidão</cite>: guerra e
democracia na era do império. Rio de Janeiro: Record, 2005.

HEIDEL, Evelin (Scann). Que se callen las musas: por
qué el feminismo debe oponerse al copyright. <cite>GenderIT</cite>.
org, 14 ago. 2017. Disponible en
<https://www.genderit.org/es/feminist-talk/columna-que-se-callen-las-musas-por-qu%C3%A9-el-feminismo-debe-oponerse-al-copyright>.

HIMANEN, Pekka. <cite>La ética del hacker y el espíritu de la
era de la información</cite>. Trad. Ferran Meler Ortí. Barcelona:
Destino, 2002.

HIRSCH, Eric; STRATHERN, Marilyn. <cite>Transactions
and Creations</cite>: Property Debates and the Stimulus of Melanesia. Nova York: Berghahn Books, 2004.

HOME, Stewart. <cite>Assalto à cultura</cite>: utopia subversão
guerrilha na (anti)arte do século XX. 2.ed. São Paulo:
Conrad, 2005.

HONG, Sungook. <cite>Wireless</cite>: From Marconi’s Black-Box
to the Audion. Massachusetts: MIT Press, 2001.

HUANG, H. On Public Domain in Copyright Law.
<cite>Frontiers of Law in China</cite>, v. 4, n. 2, pp. 178-195, 2009.

HUTCHINSON, Thomas. <cite>History of Massachusetts</cite>:
From the First Settlement Thereof in 1628, until the Year
1750. 3.ed. 2v. Massachusetts: Thomas C. Cushing, 1795.

HYDE, Lewis. <cite>The Gift</cite>: Creativity and the Artist in
the Modern World. Nova York: Random House, 1983.
[ed. bras.: A dádiva: como o espírito criador transforma o
mundo. Trad. Maria Alice Máximo. Rio de Janeiro: Civilização Brasileira, 2010.]

KITTLER, Friedrich A. <cite>Gramofone, filme, typewriter</cite>.
Trad. Guilherme Gontijo Flores; Daniel Martineschen. Belo
Horizonte; Rio de Janeiro: Editora UFMG; EdUerj, 2019.

KLEINER, Dimitry. <cite>The Telekommunist Manifesto</cite>.
Amsterdã: Institute of Network Cultures, 2010. Disponível
em: <http://www.networkcultures.org/networknotebook>.

KRAMER, Florian. O mal-entendido do Creative
Commons. In: BELISÁRIO, Adriano; TARIN, Bruno
(orgs.). <cite>Copyfight</cite>. Rio de Janeiro: Azougue, 2012.

LANIER, Jaron. <cite>Diez razones para borrar tus redes sociales de
inmediato</cite>. Trad. Marcos Pérez Sánchez. Disponible en:
<http://catedradatos.com.ar/media/Lanier-Jaron-Diez-razones-para-borrar-tus-redes-sociales-de-inmediato-XcUiDi-2018.pdf>.

LATOUR, Bruno. <cite>Jamais fomos modernos</cite>. Trad. Carlos
Irineu da Costa. Rio de Janeiro: Editora 34, 1994.

LAUTRÉAMONT, Conde de. <cite>Os cantos de Maldoror</cite>:
poesias, cartas, obra completa. Trad., prefácio e notas
Cláudio Willer. 2.ed. São Paulo: Iluminuras, 2005.

LESSIG, Lawrence. <cite>Code and Other Laws of Cyberspace</cite>.
Nova York: Basic Books, 1999.

_____. <cite>Cultura libre</cite>: Cómo los grandes medios usan la tecnología
y las leyes para encerrar la cultura y controlar la creatividad. Trad.
Antonio Córdoba, Daniel Alvarez Valenzuela. Santiago: LOM Ediciones,
2005.

LEVY, Steven. <cite>Hackers</cite>: Heroes of the Computer Revolution. Nova York: Nerraw Manijaime; Doubleday, 1984.

LIMA, Tânia Stolze. <cite>Um peixe olhou para mim</cite>: o povo
Yudjá e a perspectiva. São Paulo; Rio de Janeiro: Editora
Unesp; ISA; NuTI, 2005.

LOCKE, John. <cite>Dois tratados sobre o governo</cite>. Trad. Júlio Fischer. São Paulo: Martins Fontes, 1998.

LONG, Pamela. <cite>Openness, Secrecy, Authorship</cite>: Technical Arts and the Culture of Knowledge from Antiquity
to the Renaissance. Baltimore: John Hopkins University
Press, 2001.

MACHADO PONTES, Leonardo; SOUSA ALVES,
Marcos. O direito de autor como um direito de propriedade: um estudo histórico da origem do copyright e <i lang="fr">do droit
d’auteur</i>. In: CONGRESSO NACIONAL DO CONPEDI,
18, São Paulo, 2009. Disponible en <http://www.publicadireito.com.br/conpedi/manaus/arquivos/Anais/sao_paulo/2535.pdf>.

MACHADO, Almires Martins; ORTIZ, Rosalvo Ivarra.
Direito e cosmologia Guarani: um diálogo impreterível.
<cite>Revista de Direito: trabalho, sociedade e cidadania</cite>, Brasília: Centro Universitário Iesb, v. 5, n. 5, jul.-dic. 2018.

MACIEL, Lucas da Costa. Perspectivismo ameríndio.
In: <cite>Enciclopédia de antropologia</cite>. São Paulo: Universidade
de São Paulo, Departamento de Antropologia, 2019. Disponible en
<http://ea.fflch.usp.br/conceito/perspectivismo-amer%C3%ADndio>.

MANCE, Euclides André. Filosofia africana: autenticidade e libertação. In: SERRA, Carlos. <cite>O que é filosofia africana?</cite>
Lisboa: Escolar Editora, 2015. [Cadernos de Ciências Sociais].

MANGUEL, Alberto. <cite>Uma história da leitura</cite>. São Paulo: Companhia das Letras, 1996.

MANSOUX, Aymeric. Livre como queijo: confusão artística acerca da abertura. In: BELISÁRIO, Adriano; TARIN, Bruno. <cite>Copyfight</cite>. Rio de Janeiro: Azougue, 2012.

MARCIAL, Marco Valério. <cite>Épigrammes</cite>. Trad. Edouard
Thomas Simon. v. I. Paris: Guitel, 1819. [ed. bras.: <cite>Epigramas</cite>. Tradução, notas e posfácio de Rodrigo Garcia Lopes.
Cotia-SP: Ateliê Editorial, 2018].

MARTINS, Beatriz Cintra. <cite>Autoria em rede</cite>: os novos
processos autorais através das redes eletrônicas. Rio de Janeiro: Mauad, 2014.

MARX, Karl. <cite>Os despossuídos</cite>: debates sobre a lei referente ao furto de madeira. Trad. Nélio Schneider. São
Paulo: Boitempo, 2017.

MAUSS, Marcel. Essai sur le don. <cite>L’Année Sociologique</cite>,
ano I. Paris: Presses Universitaires de France, 1923-1924.
[ed. bras.: <cite>Sociologia e antropologia</cite>. Trad. Paulo Neves.
São Paulo: Ubu, 2017].

MCLUHAN, Marshall. <cite>Understand Media</cite>: The Extensions of Man. Nova York: McGraw-Hill, 1964. [ed. bras.:
<cite>Os meios de comunicação como extensão do homem</cite>. Trad.
Décio Pignatari. São Paulo: Cultrix, 2005].

MILAN, Stefania. When Algorithms Shape Collective
Action: Social Media and the Dynamics of Cloud Protesting.
<cite>Social Media + Society</cite>, Londres: Sage, v. 2, jul.-dic. 2015.

MOREAU, Antoine. Sobre arte livre e cultura livre. In:
BELISÁRIO, Adriano; TARIN, Bruno (orgs.). <cite>Copyfight</cite>.
Rio de Janeiro: Azougue, 2012.

MOROZOV, Evgeny. <cite>Big Tech</cite>: a ascensão dos dados e
a morte da política. Trad. Cláudio Marcondes. São Paulo:
Ubu, 2018.

______. Solucionismo, nova aposta das elites globais.
<cite>Outras Palavras</cite>, 23 abr. 2020. Disponible en
<https://outraspalavras.net/tecnologiaemdisputa/solucionismo-nova-aposta-das-elites-globais>. Consultado el 29 sep. 2020.

MUSMANN, H. G. Genesis of the MP3 Audio Coding
Standard. <cite>IEEE Transactions on Consumer Electronics</cite>, v. 52,
n. 3, pp. 1043-1049, ago. 2006. doi: 10.1109/TCE.2006.1706505. Disponible en
<https://ieeexplore.ieee.org/document/1706505>.

NEGREIROS, Regina Coli Araújo Trindade. Ubuntu:
considerações acerca de uma filosofia africana em contraposição à tradicional filosofia ocidental. <cite>Problemata: R.
Intern. Fil.</cite>, v. 10, n. 2, pp. 111-127, 2019.

NIMUS, Anna. <cite>Copyright, copyleft e os creative anti-commons</cite>. Berlim: Subta, 2006.

ORTELLADO, Pablo. <cite>Porque somos contra a propriedade
intelectual</cite>. In: ROSAS, Ricardo; SALGADO, Marcos (orgs.).
Recombinação. 2002. En línea. Disponible en
<https://virgulaimagem.redezero.org/rizoma-net>.

OXFORD ENGLISH DICTIONARY. 3.ed. Oxford: Oxford University Press, 2005.

PARANAGUÁ, Pedro; BRANCO, Sérgio. <cite>Direitos autorais</cite>. Rio de Janeiro: FGV, 2009.

PASQUINELLI, Matteo. A ideologia da cultura livre e a
gramática da sabotagem. In: BELISÁRIO, Adriano; TARIN,
Bruno (orgs.). <cite>Copyfight</cite>. Rio de Janeiro: Azougue, 2012.

PERROMAT, Kevin. <cite>El plagio en las literaturas hispánicas</cite>: historia, teoría y práctica. Paris, 2010. Tese (Doutorado em Études Romanes: Espagnol) – Université Paris-Sorbonne.

PONTES, Hugo. O que é arte xerox? In: ROSAS, Ricardo; SALGADO, Marcos (orgs.). <cite>Recombinação</cite>. 2002.
En línea. Disponible en <https://virgulaimagem.redezero.org/rizoma-net>.

PUTNAM, G. H. <cite>Authors and their Public in Ancient
Times</cite>. 3.ed. Nova York: Knickerbocker, 1923.

RAYMOND, Eric Steven. <cite>A catedral e o bazar</cite>. Trad.
Erik Kohler. [S.l.: s.n.], 1999.

RENÁ, Paulo. <i lang="fr">Droit d’autor</i> vs. copyright: diferenças
conceituais entre direito de autor e direito de cópia.
<cite>Hiperfície</cite>, 28 mar. 2012. Disponible en
<https://hiperficie.wordpress.com/2012/03/28/droit-dautor-vs-copyright-diferencas-conceituais-entre-direito-de-autor-e-direito-de-copia>. Consultado el 24 sep. 2020.

RENDUELES, César. <cite>Sociofobia</cite>: mudança política na
era da utopia digital. Trad. Sérgio Molina. São Paulo: Edições Sesc São Paulo, 2016.

______; SUBIRATS, Joan. <cite>Los (bienes) comunes</cite>: oportunidad o espejismo? Madri: Icaria, 2017.

ROSAS, Ricardo; SALGADO, Marcos (orgs.).
<cite>Recombinação</cite>. 2002. En línea. Disponible en
<https://virgulaimagem.redezero.org/rizoma-net>.

ROWAN, Jaron. <cite>Cultura libre de estado</cite>. Madrid: Traficantes de Sueños, 2016.

SAHLINS, Marshall. <cite>Cultura e razão prática</cite>. Rio de Janeiro: Jorge Zahar, 2003.

______. <cite>Stone Age Economics</cite>. Chicago: Aldine; Atherton, 1972.

SAVAZONI, Rodrigo. <cite>O comum entre nós</cite>: da cultura
digital à democracia do século XXI. São Paulo: Sesc Edições, 2018. [e-Book].

______; COHN, Sérgio. <cite>Cultura digital.br</cite>. Rio de Janeiro: Azougue, 2009.

SERTÃ, Ana Luísa; ALMEIDA, Sabrina. Ensaio sobre
a dádiva. In: <cite>Enciclopédia de antropologia</cite>. São Paulo:
Universidade de São Paulo, Departamento de Antropologia,
2016. Disponible en <http://ea.fflch.usp.br/obra/ensaiosobre-dádiva>.

SHIRAISHI NETO, Joaquim; TAPAJÓS ARAÚJO,
Marlon Aurélio. “<em>Buen vivir</em>”: notas de um conceito constitucional em disputa. <cite>Pensar</cite>, Fortaleza, v. 20, n. 2, p. 379403, maio-ago. 2015. Disponible en
<https://periodicos.unifor.br/rpen/article/viewFile/2886/pdf>.

SIMON, Imre; VIEIRA, Miguel Said. O rossio não-rival
(The Non-rival Commons). <cite>Revista da USP</cite>, n. 86, pp. 66-77,
2010. Disponible en <https://www.ime.usp.br/~is/papir/RNR_v9.pdf>.

SMIERS, Joost. <cite>Artes sob Pressão</cite>: promovendo a diversidade cultural na era da globalização. São Paulo: Escrituras, Instituto Pensarte, 2003.

______; VAN SCHIJNDEL; Marieke. <cite>Imagine um mundo sem direitos do autor nem monopólios</cite>. Trad. Helena Barradas et al. 2006. En línea. Disponible en <http://baixacultura.org/nao-e-dificil-imaginar-um-mundo-sem-copyright/>.

SOUZA, Joyce; AVELINO, Rodolfo; AMADEU DA
SILVEIRA, Sérgio. <cite>A sociedade de controle</cite>: manipulação
e modulação nas redes digitais. São Paulo: Hedra, 2018.

STALLMAN, Richard. <cite>Free Software, Free Society</cite>: Selectec Essays of Richard M. Stallman. Boston: GNU Press, 2002.
______. <cite>Software libre para una sociedad libre</cite>. Madrid:
Traficantes de Sueños, 2004.

______. O Manifesto GNU. 1985. Disponible en
<https://www.gnu.org/gnu/manifesto.pt-br.html>.

STONE, Charles R. What Plagarism Was not: Some
Preliminary Observations on Classical Chinese Attitudes Toward what the West Calls Intellectual Property.
<cite>Marquette Law Review</cite>, v. 92, n. 1, 2008. Disponible en
<https://core.ac.uk/download/pdf/148687571.pdf>.

STRATHERN, Marilyn. <cite>Property, Substance, and Effect</cite>:
Anthropological Essays on Persons and Things. Oxford:
Athlone, 1999.

______. <cite>The Gender of the Gift</cite>: Problems with Women and Problems with Society in Melanesia. Berkeley:
University of California Press, 1988.

TELES, G. M. <cite>Vanguarda europeia e modernismo brasileiro</cite>. Petrópolis, Rio de Janeiro: Vozes, 2002.

TORREMANS, Paul (org.). <cite>Copyright Law</cite>: A Handbook
of Contemporary Research. Cheltenham: Edward Elgar,
2007.

TORRES, Aracele. <cite>A tecnoutopia do software livre</cite>: uma
história do projeto técnico e político do GNU. São Paulo:
Alameda, 2018.

TSUEN-HSUIN, Tsien; NEEDHAM, Joseph. <cite>Paper
and Printing</cite>: Science and Civilisation in China. v. 5. parte
1. Cambridge: Cambridge University Press, 1985.

VALENTE, Mariana. <cite>Implicações jurídicas e políticas do
direito autoral na internet</cite>. São Paulo, 2013. Dissertação (Mestrado) – Universidade de São Paulo, Faculdade de Direito.

VIEIRA, Miguel Said. <cite>Os bens comuns intelectuais e
a mercantilização</cite>. São Paulo, 2014. Tese (Doutorado em
Educação) – Universidade de São Paulo, Faculdade de
Educação.

VILLA-FORTE, Leonardo. <cite>Escrever sem escrever</cite>: literatura e apropriação no século XXI. Rio de Janeiro; Belo
Horizonte: Editora PUC-RJ; Relicário, 2019.

VIVEIROS DE CASTRO, Eduardo. <cite>Metafísicas canibais</cite>: elementos para uma antropologia pós-estrutural. São
Paulo: Cosac & Naify, 2015.

______. Economia da cultura digital. In: SAVAZONI,
Rodrigo; COHN, Sérgio (orgs.). <cite>Cultura digital.br</cite>. Rio de
Janeiro: Beco do Azougue, 2009.

______. <cite>A inconstância da alma selvagem e outros ensaios de antropologia</cite>. São Paulo: Cosac & Naify, 2002.

WAGNER, Roy. <cite>A invenção da cultura</cite>. São Paulo: Ubu,
2017.

WHITE, Harold O. <cite>Plagiarism and Imitation during the English Renaissance</cite>: A Study in Critical Distinctions. Cambridge
(Massachusetts): Harvard University Press, 1935.

WILLER, Cláudio. Prefácio. In: LAUTRÉAMONT,
Conde de. <cite>Os cantos de Maldoror</cite>: poesias, cartas, obra
completa. Trad., prefácio e notas Cláudio Willer. 2.ed. São
Paulo: Iluminuras, 2005.

WILLIAMS, Sam. <cite>Free as in Freedom</cite>: Richard Stallman
and the Free Software Revolution. Boston: Free Software Foundation, 2002. Disponible en
<https://archive.org/stream/faif-2.0/faif-2.0_djvu.txt>.

WITTENBERG, Philip. <cite>The Protection and Marketing
of Literary Property</cite>. Nova York: J. Messner Inc., 1937.

WOODMANSEE, Martha. <cite>The Author, Art, and the
Market</cite>: Rereading the History of Aesthetics. Nova York:
Columbia University Press, 1994.

WU MING. <cite>Copyright e maremoto</cite>. Trad. Rizoma. 2002.
En línea. Disponible en <http://baixacultura.org/wu-ming-e-um-maremoto-anticopyright>.

______. <cite>Notas inéditas sobre copyright e copyleft</cite>. Trad.
Reuben da Cunha Rocha. In: <cite>La Remezcla</cite>. Porto Alegre:
BaixaCultura, [2005] 2016.

YU, Peter K. <cite>Intellectual Property and Confucianism</cite>:
Diversity in Intellectual Property: Identities, Interests
and Intersections. Org. Irene Calboli; Srividhya Ragavan.
Cambridge: Cambridge University Press, 2015.

ZUMTHOR, Paul. <cite>Essai de poétique médiévale</cite>. Paris:
Seuil, [1972] 2000.

