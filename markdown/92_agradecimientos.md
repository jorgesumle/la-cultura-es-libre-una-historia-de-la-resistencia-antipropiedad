Este libro fue escrito, en su redacción final, entre septiempre de 2019
y septiembre de 2020, período del inicio de la pandemia del nuevo
coronavirus y de una cuarentena que duró mucho más de 40 días. Muchas
personas me permitieron y ayudaron a escribirlo. Sería difícil citar a
todas; algunas de ellas están en la referencias y en las notas a pie de
página.

Puedo dar las gracias a otras aquí. Los intercambios de ideas con Elias
Machado y Leonardo Retamoso Palma fueron importantes para muchas de las
discusiones de este trabajo. En diferentes momentos y de formas
diferentes fueron interlocutores, impulsores o colaboradores de las
ideas aquí trazadas en los últimos años Rodrigo Savazoni, Mariana
Valente, Felipe Fonseca, André Deak, Pedro Markun, Evelyn Gomes, Lívia
Ascava, Sheila Uberti, Janaína Spode, Carolina Dalla Chiesa, Aline
Bueno, Fabrício Solagna, Leonardo Roat, Augusto Paim, Luís Eduardo
Tavares, Aracele Torres, Guilherme Flynn, Pablo Ortellado, Sérgio
Amadeu, Eduardo Viveiros de Castro, Marcelo Träsel, Rubens Velloso,
Gustavo Torrezan, Sávio Lima Lopes, Reuben da Cunha Rocha, Edson
Andrade, Victor Wolfenbüttel, William Araújo, Pedro Jatobá, Rodrigo
Troian, Joel Grigolo, Iuri Martins, Thiago Almeida, Douglas Freitas,
Márcia, Veiga, Angelo Kirst Adami y Tatiana Dias (quien dio valiosas
sugerencias en la fase definitiva del texto). Doy las gracias también a Daniel
Santini y Cauê Seignermartin Ameni, por el apoyo y la confianza en el
proyecto; y a Beatriz Martins, Carlos Lunna, Jorge Gemetto, Mariana
Fossati, Dani Cottilas, Barbi Couto y la red Cultura Livre do Sul, que
fueron como un laboratorio de muchas de las palabras escritas aquí, así
como a la Rede das Produtoras Culturais Colaborativas, colectivos ambos
que ponen en práctica algunas de las maneras de entender la cultura
libre.

