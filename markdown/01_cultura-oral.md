<section class="citas">
<i>Te encomiendo, Quinciano, mis libritos. Si es que puedo llamar míos
a los que recita un poeta amigo tuyo. Si ellos se quejan de su dolorosa
esclavitud, acude en su ayuda por entero. Y cuando aquel se proclame su
dueño, di que son míos y que han sido liberados. Si lo dices bien alto
tres o cuatro veces, harás que se avergüence el plagiario.</i>

<div class="right-align">
Marco Valerio Marcial, <cite>Epigramas</cite>, s. I
</div>

<br>

<i>La imitación es esencial, la fabricación es peligrosa, la materia es
propiedad colectiva.</i>

<div class="right-align">
Alguien, s. I
</div>

<br>

<i>Las mejores ideas son de todos. Por tanto, dado que lo que es de
todos es también de cada uno, toda verdad me pertenece; todo lo que ha
sido bien dicho por alguien también es mío.</i>

<div class="right-align">
Séneca, s. II
</div>

<br>

<i>El fortalecimiento de la atribución divina a la autoría se implanta
como el estándar en la Edad Media a partir de los siglos VII y VIII.
Hay una tendencia progresiva a considerar cada texto como una parte de
un Gran Discurso escrito, una comunidad del lenguaje que diluía lo
particular en una significación general propicia a la antología, la
expresión estereotipada de la (re)combinación de elementos preexistentes.
</i>

<div class="right-align">
Kevin Perromat, <cite>El plagio en las literaturas hispánicas</cite>, 2010
</div>
</section>

<br>

### I.

Las artes producidas en la Antigüedad griega y romana no parecen haber
prestado especial atención a la cuestión de la propiedad ligada a la
cultura. No hay vestigios de referencias a códigos jurídicos,
protecciones, sanciones o derechos sobre la producción y difusión de
obras culturales parecidos a los que tenemos hoy. Como escribe Kevin
Perromat[^5] en su extenso estudio sobre el plagio en la literatura
hispánica, hay diversos factores que nos harían deducir que ambas
civilizaciones, con escritores, dramaturgos y filósofos que más de dos
mil años después son todavía conocidos y leídos en el mundo entero, con
una abundancia de artistas, comerciantes y sobre todo juristas (en
especial los romanos), podrían haber tratado de reglamentar la
producción y difusión de bienes culturales. Pero no lo hicieron &mdash;o
lo hicieron de una forma que no dejó registros hasta hoy&mdash;. ¿Por
qué?

[^5]: Perromat, <cite>El plagio en las literaturas hispánicas</cite>, p.
  24.

La primera razón es que, en las civilizaciones griega y romana, las
narraciones formaban parte de una tradición común, lo que permitía
recreaciones de acuerdo a sus diversos portavoces y a los contextos en
que erran contadas. No era posible rastrear sus orígenes exactos y mucho
menos impedir su libre circulación. La creación poética, por ejemplo,
era de naturaleza fluida, e incluso si el narrador de una determinada
historia pudiera ser reconocido, su contribución no se consideraba fruto
de su individualidad, sino de una cultura colectiva en la cual él estaba
inmerso. Lo que podría habérsele añadido al poema no era documentado
para la posteridad; no había tal preocupación[^6], así como tampoco
había control sobre la producción y la distribución de una obra.

[^6]: Martins, <cite>Autoria em rede: os novos processos autorais através das redes eletrônicas</cite>, p. 28.

En una sociedad predominantemente analfabeta, la finalidad de una obra
cultural &mdash;tanto una obra de teatro como una canción o un poema,
pero también textos políticos&mdash; era la difusión pública en plazas,
teatros, calles, tribunas. Tanto la escritura como la lectura eran para
pocos; las ideas y la cultura se difundían con la voz &mdash;y con la
reapropiación&mdash; de todo el que hablaba. El filósofo griego
Sócrates, uno de los padres de la filosofía occidental y a quien
conocemos solamente a través de textos de otros, llega a decir en
<cite>Fedro</cite>, diálogo compilado por Platón en torno al 370 a.&nbsp;C.,
que la escritura suponía una pérdida en relación al discurso oral,
siendo el segundo más apropiado para mantener vivo el pensamiento.
Sócrates hablaba de la amenaza que la escritura suponía para el
mantenimiento de las funciones de la memoria, que empezaría a
utilizarse cada vez menos y perdería su potencia a medida que los
registros fueran transferidos al papel[^7].

[^7]: <i lang="lt">Ibidem</i>, p. 78.

Tampoco había una noción sólida de autor, por lo menos en el ámbito
jurídico, lo cual solo empezó a ocurrir a partir del siglo XVIII. La
autoría era principalmente colectiva, atribuida a una cultura
determinada o a los dioses, fruto de una inspiración divina o de una
construcción comunitaria en la que importaba más el contenido y lo que
este podría enseñar que su portavoz. Homero, a quien se le atribuye la
redacción de los clásicos la <cite>Ilíada</cite> y la
<cite>Odisea</cite> en torno a los siglos VIII y VII a.&nbsp;C., es un
ejemplo de ese período: no existen evidencias respecto a la fecha de
creación de esas dos obras ni de que hubo una persona llamada Homero que
las escribió. El relato de la Guerra de Troya y de las diversas
dificultades del viaje de Ulises de vuelta a su Ítaca natal, ejes
narrativos centrales de la <cite>Ilíada</cite> y la <cite>Odisea</cite>
respectivamente, son historias que condensan una visión del mundo y
enseñanzas que representan un modo común de pensar de los griegos,
enraizados en la cultura de la época. Homero es un arquetipo, creado
posteriormente, y que cumplía funciones como la de un *horizonte
lingüístico* o «padre y abuelo» de los poetas, una figura casi mística
que se encargó de contar una historia que, conocida y creada por muchos,
era considerada de autoría de dioses, musas, entidades de la
naturaleza[^8].

[^8]: Perromat, <i lang="lt">op.&nbsp;cit.</i>, p. 24.

Con la difusión de la escritura a partir del siglo VII a.&nbsp;C. surgen, en
paralelo a un tipo de creación abierta y colectiva, registros de una
expresión individualizada y de un deseo de reconocimiento de autoría,
que pasaba por la búsqueda de un primer intento de control de la
difusión de una obra[^9]. Uno de los primeros que tratan de controlar la
difusión de su producción en ese período es Teognis, poeta griego que
vivió en el siglo VI a.&nbsp;C. y que planteó, en sus publicaciones
manuscritas, la colocación de un sello: «Estos son versos de Teognis de
Mégara». Su intención con esa <i lang="en">trademark</i> (marca
registrada) antigua no era la de lucrarse con la venta de sus obras,
sino que no las modificaran y pensaran que no eran suyas, lo que
le quitaría el reconocimiento por el trabajo que realizaba[^10].

[^9]: <i lang="lt">Ibidem</i>, p. 30.
[^10]: <i lang="lt">Ibidem</i>, p. 27, citando también a un historiador
  del s. III, Clemente de Alejandría (cuya obra se publicó
  posteriormente, <cite>Omnia quae extant opera</cite>).

En aquella época, los autores comprendían que la mejor forma de ser
financiados por los gobernantes y por la aristocracia griega era que
fueran considerados «especialistas» y tuvieran obras atribuidas a ellos.
Aquí entra una segunda pista que explica el motivo por el que los
griegos y los romanos no hayan prestado especial atención a la cuestión
de la propiedad ligada a la cultura: la ausencia de un mercado para
bienes culturales. Los autores no se sustentaban con la renta directa de
sus obras; vivían prestando servicios de educación para la aristocracia,
consejos para los gobernantes, enseñanza de diversos temas
&mdash;acciones todas ligadas a la presencia y a la comunicación
oral&mdash;. En la tradición discursiva grecorromana, las retribuciones
a los autores eran de orden simbólico, de reconocimiento social de una
actividad que no dejaba de ser elitista y aristocrática[^11] &mdash;a
fin de cuentas, pocos sabían leer&mdash;, pero también de ciertos tipos
de reconocimientos indirectos, como entregas de premios en concursos
oficiales y beneficios materiales derivados de los premios.

[^11]: <i lang="lt">Ibidem</i>, p. 29.

### II.

En Grecia, en el período conocido como helenístico (IV a.&nbsp;C.-II a.&nbsp;C.),
no había un control sobre el destino de un libro después de ser
publicado, sea en relación al número de ejemplares o a la modificación
de su contenido. Además de ser una cultura que valoraba el disfrute y la
creación colectiva y oral, otro factor justificaba esa falta de control:
el formato del libro de la época. Este tenía un formato conocido como
*volumen*, compuesto de una larga tira de papiro o pergamino, que se
leía a medida que era desenrollado por alquien, con ambas manos,
pudiendo tener varios metros de longitud y pesar algunos kilos. Formato
pesado y caro, era de difícil reproducción, cuidado y almacenamiento, lo
que hacía del libro un bien relativamente escaso en la época, como
manifiestan textos de Aristóteles y de Platón[^12].

[^12]: Putnam, <cite>Authors and their Public in Ancient Times</cite>,
  citado en Perromat, <i lang="lt">op.&nbsp;cit.</i>, p. 32.

La aparición de las primeras bibliotecas en el mundo grecorromano, a
partir del siglo IV a.&nbsp;C., supone el comienzo de una política de control
de los textos y manuscritos. Para controlar los textos y obtener una
copia, hay necesidad de garantizar cuál es auténtico y cuál no lo es, lo
que incita a las bibliotecas a investigar para atribuir correctamente la
autoría de las publicaciones[^13]. Creada en ese período, la mítica
Biblioteca de Alejandría, en Egipto, fue una de las principales
iniciativas que aspiraban a organizar el conocimiento y la cultura
producida hasta entonces; hace eso a partir de la selección de un
*canon* de obras de la cultura griega por los sabios y bibliotecarios
identificados como la llamada *Escuela de Alejandría*[^14]. A partir de
un sistema de edición crítica que comparaba las diferentes partes de una
obra buscando su coherencia textual, formaron las versiones que
conocemos de muchos textos de la cultura griega, que serían también la
base para la «rústica Roma», que hereda el «tejido cultural» de los
antiguos griegos y durante los siguientes siglos se apropia de esa
cultura, combinándola con referencias asiáticas y africanas.

[^13]: Long, <cite>Openness, Secrecy, Authorship: Technical Arts and the
  Culture of Knowledge from Antiquity to the Renaissance</cite>, p. 30.
[^14]: Escuela de Alejandría es una designación colectiva para ciertas
  tendencias en literatura, filosofía, medicina y ciencias que se
  desarrollaron en el centro cultural helenístico de la ciudad homónima,
  en Egipto, durante los períodos griego y romano. Calímaco (310-240 a.
  C.) fue el principal bibliotecario, a quien se le atribuye la
  elaboración del catálogo de la Biblioteca de Alejandría.

También en Roma se ve aumentado el poder del nombre del autor como un
sello de credibilidad para los bienes culturales producidos, lo que
todavía no se traduce en protección jurídica ni en control del autor
sobre la difusión de su obra. Un ejemplo de ese momento es la historia
del poeta Virgilio, que, en el siglo I a.&nbsp;C., fue contratado por el
emperador Augusto para crear una epopeya de la fundación de Roma
siguiendo los modelos de la <cite>Ilíada</cite> y la
<cite>Odisea</cite>. Tomando como base los poemas atribuidos a Homero,
recrea el viaje de Ulises en Eneas, guerrero que, tras participar en la
Guerra de Troya, llega a la Península Itálica y allí encara una
serie de aventuras para establecerse. Es conocida la historia de que, al
final de su vida, Virgilio ordenó la destrucción de la
<cite>Eneida</cite> por considerarla una obra de propaganda política de
Augusto y por no tener la perfección poética deseada. Sin embargo, la
<cite>Eneida</cite> no fue destruida; su derecho como autor de controlar
su producción no fue respetado, lo que es representativo del sentimiento
de una época en que el derecho moral y estético de la comunidad de
disfrutar de la obra del artista tenía prioridad sobre la voluntad del
autor[^15].

[^15]: Perromat, <i lang="lt">op.&nbsp;cit.</i>, p. 32.

El pensamiento común de la sociedad romana en relación a la posesión y a
la autoría de los bienes culturales se manifestaba en tres conceptos
identificados por un estudio clásico del inglés Harold Ogen White[^16]:
«La imitación es esencial, la fabricación es peligrosa, la materia es
propiedad colectiva». Esas nociones pueden verse en expresiones como
«<i lang="lt">Oratio publicata, res libera est</i>» («lo publicado pertenece
a todos»), atribuida a Quinto Aurelio Símaco, escritor de la Roma del
siglo IV d.&nbsp;C.; o como la frase «Las mejores ideas son de todos. Por
tanto, dado que lo que es de todos es también de cada uno, toda verdad
me pertenece; todo lo que ha sido bien dicho por alguien también es
mío»[^17], de Séneca, filósofo y escritor del siglo I d.&nbsp;C.

[^16]: White, <cite>Plagiarism and Imitation During the English
  Renaissance: A Study in Critical Distinctions</cite>, citado em
  Perromat, <i lang="lt">op.&nbsp;cit.</i>, p. 44.
[^17]: <i lang="lt">Ibidem</i>.

No obstante, el esfuerzo por el control de la autenticidad de las obras
pasa, a lo largo de los siguientes siglos, a provocar transformaciones
que contrastaban con la cultura recombinante de tradición oral, que
identificaba las ideas y la obras con la propiedad colectiva. Surge
entre algunos filósofos y escritores romanos del período, como Cicerón,
Horacio y Séneca, la discusión en torno a la idea de que únicamente la
imitación por sí sola no es suficiente, es necesario que sea una
imitación creativa[^18]. El plagio, como discusión y como palabra, surge
en ese período a partir de Marco Valerio Marcial (40-104). Protegido por
la aristocracia y por los emperadores, era un poeta bastante popular de
la época, pero, como todos los artistas contemporáneos a él, no vivía de
la venta de su obra. Sus diatribas satíricas y autorreferenciales se
volvieron célebres &mdash;algunas de ellas hablan de restringir el
acceso a sus escritos a cambio de un pago, lo que anticipa la idea de
obra de arte en términos mercantiles[^19]&mdash;. Se puede encontrar un
registro de su posición en uno de sus miles de epigranas, una especie de
comentario &mdash;corto como un <i lang="en">tweet</i> del siglo
XXI&mdash; irónico, muchas veces escatológico y obsceno, sobre alguien o
un suceso: «Corre el rumor de que tú, Fidentino, lees mis versos al
público como si fueran tuyos. Si quieres que se diga que son míos, te
enviaré gratis los poemas; si quieres que se diga que son tuyos,
cómpralos, para que dejen de ser míos»[^20].

[^18]: White, <cite>Plagiarism and Imitation During the English
  Renaissance: A Study in Critical Distinctions</cite>, citado em
  Perromat, <i lang="lt">op.&nbsp;cit.</i>, p. 44.
[^19]: Perromat, <i lang="lt">op.&nbsp;cit.</i>, p. 42.
[^20]: Marcial, <cite>Epigramas</cite>, en trad. de Rodrigo Garcia
  Lopes.

Según Perromat[^21], a Marcial se le atribuye la invención del término
*plagio* en el sentido moderno, ya que antes ese tipo de acción, cuando
se identificaba, era llamada «robo» o «hurto» de ideas. El escritor
romano crea la expresión a partir del verbo latino
<i lang="lt">plagiare</i>, que en latín significa «revender de modo
fraudulento el esclavo o hijo de alguien como propio», delito que en la
época era penado con el azotamiento. Marcial usa el término con el nuevo
sentido en otro de sus epigramas: «Te encomiendo, Quinciano,
mis libritos. Si es que puedo llamar míos a los que recita un poeta
amigo tuyo. Si ellos se quejan de su dolorosa esclavitud, acude en su
ayuda por entero. Y cuando aquel se proclame su dueño, di que son míos y
que han sido liberados. Si lo dices bien alto tres o cuatro veces, harás
que se avergüence el plagiario»[^22].

[^21]: Perromat, <i lang="lt">op.&nbsp;cit.</i>
[^22]: Epigrama LIII, en Marcial,
  <cite>Épigrammes</cite>, pp. 70-71, traducido del francés al español por
  Perromat [le he quitado la tilde a «aquel» y he añadido «a» antes de
  «los que recita»], <i lang="lt">op.&nbsp;cit.</i>, p. 47.

Aun así, la creciente discusión en torno al plagio en la época se
realizaba en el ámbito de la moral y de la estética, no en el sentido
legal o criminal. Muchos de los debates intelectuales celebrados en la
realidad romana de la época revelaban un creciente juicio negativo sobre
la imitación sin creación, la copia simple y pura, pero no llegaron a
ser centrales en la crítica literaria romana[^23]. No hay registro de
leyes que hayan sido promulgadas para regular o penar esas prácticas.
Eso puede haber ocurrido, tanto en Grecia como en Roma, también por la
dificultad material de la reproducción, un obstáculo financiero
considerable para la circulación de obras consideradas plagios y que
probablemente no ayudó a estimular la creación de reglas para eso. Otro
factor es que los sistemas jurídicos de la época no consideraban de
forma separada las obras artísticas y sus soportes. Para los griegos,
por ejemplo, quien adquiriera una obra &mdash;por el coste monetario,
probablemente una biblioteca o un miembro de la aristocracia&mdash;
podía utilizar y modificar la obra libremente, sin que el autor tuviera
ninguna intromisión. La relación colectiva de posesión de los bienes
culturales y la inexistencia de leyes que regularan o castigaran las
prácticas consideradas como robo de ideas y publicaciones perduró hasta
el siglo XVI, cuando, por primera vez, se dio una concesión estatal (un
monopolio) que garantizaba privilegios para imprimir un texto
&mdash;para la Stationer’s Company&mdash;, en Inglaterra&mdash; y se
decretó el Estatuo de Anne, la primera ley de propiedad intelectual, de
1710, también inglesa.

[^23]: Perromat, <i lang="lt">op.&nbsp;cit.</i>, p. 43.

### III.

El período que comprende desde el fin del Imperio Romano hasta la Alta
Edad Media es el del surgimiento del cristianismo y el enfrentamiento y
la posterior asimilación de la civilización grecorromana. Para la
producción cultural, el resultado fue diverso. Perromat y otros
historiadores[^24] afirman que casi todas las versiones de los grandes
escritores y pensadores de Grecia y Roma que hoy conservamos son
adaptaciones y variaciones realizadas en ese período, cuando no
falsificaciones que, después, fueron atribuidas a grandes figuras de la
Antigüedad por razón del prestigio que esta indicación, más tarde,
empezó a dar.

[^24]: <i lang="lt">Ibidem</i>, p. 50.

Otro elemento entonces importante para el cambio de los modos de
producción y circulación de bienes culturales en esa época ocurre a
partir de la transformación del soporte material de las obras. La caída
de un imperio que se extendía por casi toda la Europa que hoy conocemos
implica también la ruptura de rutas comerciales y la escasez de algunos
recursos, es el caso del principal material utilizado para la
fabricación de libros en el mundo grecorromano, el papiro, extraído de
la planta <i lang="lt">Cyperus papyrus</i>, de la familia de las
ciperáceas. Material caro y de poca resistencia, provenía del comercio
con el norte de África (principalmente Egipto) y con Oriente Medio,
disminuyó considerablemente tras la caída del Imperio Romano. Así,
alrededor de los siglos IV y V, los libros pasaron a ser fabricados
principalmente en pergaminos, nombre dado a una piel de animal,
generalmente de cabra, carnero, cordero u oveja, preparada para que
se escriba sobre ella &mdash;material que, aunque perecedero y caro, pasó a
ser el principal utilizado para la fabricación de libros&mdash;. También
en ese período, estos dejan de ser producidos en rulos, produciéndose en formato
de *códice*[^25], cercano al del libro como lo conocemos en el siglo XX.
El historiador Alberto Manguel habla de que, en el siglo XII, la
tecnología para la elaboración de un volumen de la Biblia (Antiguo y
Nuevo Testamento) requería pieles de cerca de doscientos animales[^26].


[^25]: Consiste en cuadernos doblados, cosidos y encuadernados, escritos
  a ambos lados de una hoja numerada, formato hasta hoy estándar
  para la producción de libros.
[^26]: Manguel, <cite>Una historia de la lectura</cite>, p. 198.

La consolidación del cristianismo en Europa en los siguientes siglos
también trae consigo algunos cambios con respecto a la identificación
del autor. En una religión para la que la Biblia era el (único) libro
sagrado y cuya autoría[^27] era, en última instancia, de Dios, de la
voluntad de los escritores y de su supuesta singularidad &mdash;que
llegó a fomentar discusiones iniciales entre griegos y romanos&mdash;
pasó a depender de la verdad de un único «autor», Dios. El
fortalecimiento de la atribución divina a la autoría se consolida como
el estándar en la Edad Media a partir de los siglos VII y VIII; hay una
tendencia progresiva a considerar cada texto parte de un gran discurso,
«una "comunidad de lenguaje" que diluía lo particular en una
significación general propicia a la antología, a la expresión
estereotipada de la (re)combinación de elementos preexistentes»[^28],
una modalidad de escritura y lectura con un carácter eminentemente
colectivo.

[^27]: Conjunto de libros base del cristianismo y del judaísmo, la
  Biblia es motivo de muchas disputas en torno a la autoría. Buscando
  la consolidación de los dogmas judeocristianos, muchos de los primeros
  escritores bíblicos se basaron en otros textos, griegos y de otras
  regiones, compilándolos y adaptándolos según los objetivos de
  evangelización de la población &mdash;en lo que tendrían éxito en los
  siglos siguientes, consiguiendo la difusión de esos dogmas por buena
  parte del mundo&mdash;. Véase: <https://en.wikipedia.org/wiki/Bible>.
[^28]: Perromat, <i lang="lt">op.&nbsp;cit.</i>, p. 60.

Se consolida un discurso en el que diversas personas adaptan, con
libertad extrema, los textos (y aquí podemos decir también música y
teatro) para fines específicos, la mayoría de las veces con fines de
convencer, a partir de una determinada idea, y de establecer un ejemplo
moral y ético a ser seguido. Paul Zumthor, en su <cite>Essai de poétique
médievale</cite>[^29], dice que el texto en ese período funciona
independientemente de sus circunstancias; el oyente (la gran mayoría de
las obras aquí era oral u convertida en oral) esperaba solo su
literariedad, es decir, el significado del «mensaje», y no criticaba las
alteraciones de forma que mantuviesen ese significado &mdash;es más
probable que, ante una mayoría todavía analfabeta, los cambios tampoco
serían percibidos&mdash;. Así, los autores medievales tenían como método
el uso generalizado de materiales de otros por medio de la alusión,
interpolación o paráfrasis y, la mayoría de las veces, no especificaban
su origen. Fragmentos considerables de textos, en ocasiones muy
anteriores, fueron simplemente insertados en nuevas obras, poemas
preexistentes se integraron completamente en composiciones
literarias[^30]. Sin documentación y sin conocimiento del canon de
autores de la época, algunas de esas citas e inserciones jamás serían
reconocidas.

[^29]: Zumthor, <cite>Essai de poétique médievale</cite>.
[^30]: <i lang="lt">Ibidem</i>.

A partir del siglo V, el cristianismo pasó no solo a ser la fe más
difundida en Occidente, sino también a guiar las obras culturales del
período. Una de las consecuencias de ese control fue la desaparición en
Europa occidental, entre los años 500 y 700, de varias obras clásicas
griegas y romanas &mdash;en Bizancio (Constantinopla, actual Estambul,
en Turquía) y territorios de mayor influencia islámica al sur y este del
continente esas obras permanecieron&mdash;. Al sustituir las obras
consideradas prescindibles (paganas en su mayoría) por otras de carácter
religioso, quienes pasaron a poseer tanto los medios de producción como
los conocimientos para publicar y controlar los bienes culturales
&mdash;la Iglesia y los monjes copistas medievales, en el caso de los
libros&mdash; contribuyeron a reducir el canon de los autores de la
Antigüedad[^31]. En ese sentido es en el que se habla hasta el día de hoy
de la Edad Media como un período de apagón cultural, dominado por
intereses cristianos, a pesar de que esa imagen se cuestione desde hace
tiempo por historiadores como el frances Patrick Boucheron, que recuerda
el período como «la adolescencia de la modernidad, su edad ingenua o
revoltosa» y dice que, al observar la llamada «Edad Oscura» más de
cerca, es posible oír el «tintineo estridente de un ruido alegre y
desordenado»[^32]. Solamente a partir de los siglos XIV y XV es cuando
cierta tradición clásica sería reintegrada a la tradición europea,
especialmente a través de regiones comerciales que mantuvieron una
influencia asíatica, islámica y bizantina, es el caso del sur de la
Península Ibérica y de regiones italianas como Sicilia, Nápoles y
Venecia.

[^31]: Para una muestra de obras perdidas en ese período véase Diringer,
<cite>The Book Before Printing: Ancient, Medieval and Oriental</cite>.
[^32]: En Boucheron <cite>Como se revoltar?</cite> Al examinar en
  detalle cómo escribe el historiador francés en este pequeño libro
  sobre el acto de revelarse en la Edad Media, podemos encontrar algunos
  hechos que apuntan a la supervivencia de resquicios de una cierta
  tradición cultura y de revuelta contra los dogmas religiosos en ese
  período. Es el caso también de obras de Carlo Ginzburg como <cite>O
  queijo e os vermes e Os andarilhos do bem</cite> y de lo que por
  convención se llamó microhistoria. Desgraciadamente poca documentación
  de esa época ha sobrevivido, su rescate es un trabajo arduo y lento.

