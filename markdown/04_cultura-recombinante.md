<section class="citas">
<i>Las ideas se perfeccionan. El significado de las palabras participa
del perfeccionamiento. El plagio es necesario. El progreso implica eso.
Este aprovecha una frase de un autor, hace uso de su expresión, quita
una falsa idea y la sustituye por una idea correcta.</i>

<div class="right-align">
Conde de Lautréamont (Isidore Lucien Ducasse), <cite>Poesias</cite>, 1870
</div>

<br>

<div class="right-align">
<i>Derecho a ser traducido, reproducido y deformado en todas las
lenguas.</i>
</div>

<div class="right-align">
Oswald de Andrade, <cite>Serafim Ponte Grande</cite>, 1933
</div>

<br>

<i>En la realidad es necesario eliminar todos los restos de la
noción de propiedad personal en este área. La aparición de las ya
obsoletas nuevas necesidades por obras «inspiradas». Estas se vuelven
obstáculos. No se trata de que te gusten o no. Tenemos que superarlas.
Puede usarse en cualquier momento, no importa de dónde se saquen, para
hacer nuevas combinaciones. Los descubrimientos de la poesía moderna
relativos a la estructura analógica de las imágenes demuestran que
cuando se reúnen dos objetos, no importa cuán distantes puedan estar
de sus contextos originales, siempre se forma una relación. Restringirse
a una disposición personal de palabras es mera convención. La
interferencia mutua de dos mundos de sensaciones, o la reunión de dos
expresiones independientes, sustituye los elementos originales y produce
una organización sintética de mayor eficacia. Se puede usar cualquier
cosa.</i>

<div class="right-align">
Guy Debord; Gil Wolman, <cite>Um guia para os usuários do detournamènt</cite>, 1956
</div>

<br>

<div class="right-align">
<i>El medio es el mensaje.</i>
</div>

<div class="right-align">
Marshall McLuhan, <cite>Understand Media</cite>, 1964
</div>

<br>

<i>En las redes de intermedia de hoy, un flujo de datos formalizados
algorítmicamente puede saltar a todos ellos. De medio a medio, toda
modulación posible se ha vuelto factible: en órganos de luz, señales
acústicas controlan señales ópticas; en la música de ordenador, señales
en lenguaje de máquina controlan señales acústicas; en codificadores de
voz, los mismos datos acústicos controlan otros datos acústicos. Hasta
los pinchadiscos de Nueva York crearon, a partir de imágenes esotéricas
de un Moholy-Nagy, lo cotidiano de la música
<i lang="en" style="font-style: normal;">scratch</i>.</i>

<div class="right-align">
Friedrich A. Kittler,
<cite>Gramofone, filme, typewritter</cite>, 1986
</div>
</section>

### I.

El período que va del inicio de los 1900 hasta la década de 1970 fue el
de consolidación y popularización de las diversas tecnologías de
comunicación y reproducción patentadas por figuras como Graham Bell,
Edison, Marconi, Eastman y Lumière en la segunda mitad del siglo XIX. El
fin del «Imperio de la Impresión», como vimos, da lugar a una forma de
percepción que deja de estar solamente basada en la mediación simbólica
del alfabeto, de las palabras y de las publicaciones impresas, y pasa a
ser en gran medida *sentida*, por ojos y oídos, a partir de la
utilización como medios de los aparatos técnicos de reproducción, como
el gramófono (después gramola, tocadiscos y casetes), la película (y el
cine), la televisión, el vídeo (y el videocasete); y que culminan con el
aparato que une todos estos en un uso, el ordenador, popularizado a
partir del final de los años 70 con la explosión de la nanotecnología
digital y de los PC (<i lang="en">personal computer</i>) producidos en
la región que dictaría el comportamiento mundial ante las nuevas
tecnologías en las próximas décadas, Silicon Valley, en California,
Costa Oeste de Estados Unidos.

Se crean industrias gigantescas basadas en la reproducción de imágenes y
sonidos en el siglo XX. El cine, la radio, la televisión, en ese orden,
se convierten en medios de comunicación y reproducción de masas con
alcance global, influyentes incluso en la periferia del mundo. El
almacenamiento de información se vuelve el estándar de un mundo guiado
por la competencia del mercado; las ideas &mdash;literarias, musicales,
científicas&mdash; son empaquetadas, comercializadas y se convierten en
propiedad, elemento por el cual el capitalismo se organiza y mueve la
rueda del llamado *progreso*, que va a depender cada vez más de las
tecnologías y de los mecanismos jurídicos creados para regular los
intercambios y la publicación de ideas: las patentes y el derecho de
autor (o <i lang="en">copyright</i>).

Ya en las primeras décadas de los años 1900, las bases de las
legislaciones de propiedad intelectual que todavía hoy están en vigor
estaban definidas en acuerdos internacionales como el de Berna o el de
París. Estas normalizaron la idea de creación individual basada en el
mito del «genio» romántico, una imagen representada por aquel sujeto
que, encerrado en su cuarto, generalmente solo, tiene una idea brillante
a partir de sus propias referencias solamente y, con esa idea bien
empaquetada y vendida como mercancía por un intermediario, va a obtener
fama y dinero. Lo que no encajara en ese modelo sería marginado y
reprimido con multas y penas al final de largos y caros procesos
iniciados por aquellos que tenían los medios para contratar y tener
abogados.

La consolidación de las leyes de derecho de autor y de la noción de que
los bienes culturales son propiedades privadas también origina, por otro
lado, un movimiento de resistencia. En el cambio al siglo XX y a lo
largo de las décadas siguientes, van a proliferar movimientos de
protesta contra el derecho de autor en el arte y la cultura cuestionando
que ideas, sonidos, palabras, imágenes y películas puedan ser propiedad
de alguien y usados solamente con la autorización de los llamados
propietarios mediante algún pago financiero. Los análisis propuestos en
las acciones de estos artistas y movimientos traen, como consecuencia
directa o indirecta, la propuesta de dar contestación a la idea de un
bien cultural como mercancía y la construcción de una cultura libre y
común que debería ser de todos, sin necesidad de haber autorización
para ser disfrutada, difundida o reutilizada.

Al apropiarse de ciertas ideas, empaquetarlas y venderlas como obras
cerradas, el régimen de propiedad intelectual trató, por un lado, de
crear un sistema que pudiera remunerar a los creadores (o a sus
representantes) por sus obras &mdash;lo que de hecho hizo al equiparar a
los artistas a otros tantos profesionales con derecho a mantener una
vida digna a partir de sus creaciones&mdash;. Por otro lado, la
propiedad intelectual también restringió la promiscuidad de las ideas y
las encerró en un espacio donde pudiera extraer beneficios exclusivos de
su propiedad y control[^89]. En la mayoría de los casos, el objetivo de
apoderarse de ideas y sacar recursos de ellas fue alcanzado.

[^89]: Nimus, <i lang="lt">op.&nbsp;cit.</i>, p. 27.

Pero existió &mdash;y todavía existe&mdash; una contestación para que el
acceso y la circulación continuaran siendo mayores que la restricción.
En las calles del sur global, la propiedad intelectual empezó a ser
reemplazada por la libre difusión de ideas, a menudo comercializadas al
margen del sistema legal para fomentar nuevas creaciones que se propagan
por todos lados, aunque iniciativas en las legislaciones y en la
propaganda, por Estados y empresas, surjan con el objetivo del control y
la criminalización de esas prácticas. De forma consciente como
contestación al <i lang="lt">statu quo</i> artístico, o espontánea por
ser cotidiana y habitual, la reapropiación de información y de bienes
culturales ya existentes para el desarrollo de nuevas creaciones
proliferó con fuerza en el siglo XX a punto de originar nuevos
movimientos, ritmos y obras en los lugares más diversos. Son incontables
los ejemplos en el arte y en la contracultura, todos presentando alguna
forma de subversión y trasladando los objetos de un sistema de
referencia a otro, con alteración (o no) de significado. Las tecnologías
de comunicación, grabación y reproducción en masa habrían tenido un
papel importante en esa proliferación, en la mayoría de los casos siendo
fundadoras de nuevas prácticas de creación, consumo y difusión de bienes
culturales. El capitalismo habría tenido un papel aún mayor, siendo
tanto el modo de producción en el seno del cual las tecnologías son
generadas y popularizadas como al que se debe tomar como «enemigo», de
manera explícita o no tanto, por las iniciativas culturales de
contestación basadas en la recombinación y en la reapropiación de
significados.

### II.

La contestación a las leyes reguladoras de la propiedad intelectual se
dio en el primer momento, justo después de la consolidación de los
Estados de Occidente. Por motivos muy diferentes y muchas veces
opuestos: anarquistas negando cualquier tipo de propiedad privada,
incluso la intelectual; socialistas en vías de potenciar la propiedad
colectiva, inclusive en las artes, bajo la administración del Estado;
liberales enfatizando el libre mercado, que consideraba que el interés
público de tener acceso a bienes culturales lo más barato posible podría
prevalecer sobre los derechos de los autores; y los artistas, de todas
las ideologías, cuestionando el estatus de la creación romántica y
propietaria y luchando por la posibilidad de usar cualquier tipo de obra
sin necesidad de pedir autorización a quienquiera que sea.

Todavía durante la segunda mitad del siglo XIX, al mismo tiempo que los
múltiples inventos que poco a poco darían fin al libro como principal
modelo de percepción de ideas y de la realidad, uno de los primeros
artistas que abiertamente cuestionaba el derecho de autor y la noción de
genio creador individual fue el conde de Lautréamont, nacido Isidore
Lucien Ducasse en Montevideo, en Uruguay, en 1846, y desde temprano
ciudadano de París, en Francia, donde murió en 1870, a los 24 años.
Lautréamont hizo de su corta obra (y vida) un permanente cuestionamiento
de lo que en la época era institucionalizado en la literatura, tanto en
las temáticas como en el proceso de escritura &mdash;el uso de errores
ortográficos, impropiedades estilísticas, el plagio y repeticiones de
fórmulas, que hacen que a sus obras sean, hasta el día de hoy, admiradas
y no se presten a clasificaciones[^90]. <cite>Los cantos de
Maldoror</cite> (1869), su libro más famoso e influyente, relata, en
seis cantos de una poesía unas veces narrativa y otras lírica y absurda,
sucesivas violencias, depravaciones, crueldades en torno a la cobardía y
la estupidez humana.

Su segundo libro y último trabajo, <cite>Poesías</cite>, «menos
espectacular y todavía más extraño»[^91], publicado en el año de su
muerte, reunió en texto aforismos, máximas, poesías, citas de poetas
griegos y de sus contemporáneos en Francia, como Charles Baudelaire,
Blaise Pascal y Alexandre Dumas. En uno de los fragmentos de la
publicación, dividida en dos partes (fascículos), apelaba a la volver a
una poesía impersonal, escrita por todos, que remitiera a las formas
colectivas de producción de la época medieval, pero con tintes de la
industrialización moderna de la época: «Las ideas se perfeccionan. El
significado de las palabras participa del perfeccionamiento. El plagio
es necesario. El progreso implica eso.  Este aprovecha una frase de un
autor, hace uso de su expresión, quita una falsa idea y la sustituye por
una idea correcta»[^92]. Lautréamont, en especial en este fragmento de
<cite>Poesías</cite>[^93], desafía el mito de la creatividad individual
&mdash;que desde el principio iba de la mano de la justificación de las
relaciones de propiedad intelectual en nombre de un mundo moderno donde,
supuestamente, no se admitirían ideas sin dueño&mdash;. Su gesto señala
a la reapropiación de la cultura como esfera de producción colectiva,
como en la Antigüedad y parte de la Edad Media, pero «sin dejar de
reconocer las limitaciones identificadas por él como artificiales,
atribuidas a la autoría por el régimen ya entonces establecido de
propiedad intelectual»[^94]. Su «la poesía debe hacerse por todos, no
por uno», presente en <cite>Poesías</cite>, anticipa, junto con otro
poeta del mismo período, Stéphane Mallarmé, la atención moderna de la
supremacía del texto sobre el autor-lector, «un traslado de la
intersubjetividad a la intertextualidad, que hace pensar la obra no solo
como un diálogo entre personas, sino entre textos»[^95].

[^90]: En «O astro negro», prefacio de Cláudio Willer a Lautréamont,
  <cite>Os cantos de Maldoror</cite>.
[^91]: <i lang="lt">Ibidem</i>.
[^92]: <i lang="lt">Ibidem</i>.
[^93]: Guy Debord incluso lo cita en en la introducción de <cite>La
  sociedad del espectáculo</cite>, su obra más conocida, de 1967.
[^94]: Nimus, <i lang="lt">op.&nbsp;cit.</i>, p. 27.
[^95]: En Lautréamont, <i lang="lt">op.&nbsp;cit.</i>, p. 33, Willer, que se
  basa en <cite>Los hijos del limo</cite>, del poeta y diplomático
  mejicano Octavio Paz. El foco sobre el texto y no tanto en el autor va
  a ser popular en los estudios llamados de <i lang="fr">écriture</i> en
  Francia a partir de los años 1950, 1960 y 1970, con filósofos como
  Roland Barthes y Jacques Derrida. Barthes, no por casualidad, es autor
  de <cite>La muerte del autor</cite>, un texto en el que señala la
  desaparición de la figura del autor a partir del siglo XIX, siendo el
  lenguaje el verdadero agente de la era escrita, no un individuo.
  Martins, <i lang="lt">op.&nbsp;cit.</i>, p. 21.

Lautréamont también hizo uso del plagio para componer su obra. En
<cite>Cantos</cite> hay diversos fragmentos que hacen referencia a
otros[^96]. En <cite>Poesías</cite> es posible detectar extractos de
<cite>Pensamientos</cite>, del matemático Blaise Pascal, y de
<cite>Máximas</cite>, de François de La Rochefoucauld, además de los
trabajos de escritores y filósofos como Jean de La Bruyère, Luc de
Clapiers, Dante Alighieri, Immanuel Kant y La Fontaine[^97], entre otros
autores que encontraríamos si lo examináramos con más detalle aún. Además
de eso, y de manera todavía más rara en comparación con otros objetores
del <i lang="en">copyright</i> y de la autoría en el siglo XX, también
el modo de difusión de la obra de Lautréamont demostró cierto
cuestionamiento del mercado tradicional de publicaciones: los dos
dípticos de <cite>Poesías</cite> circulaban sin precio por las calles de
París, bajo el modelo de «paga lo que quieras» que sería extendido, casi
un siglo después, por la cultura punk de influencia anarquista y otros
movimientos de contracultura que no reconocen el sistema de propiedad
intelectual como legítimo para sus creaciones.

[^96]: Uno particularmente visible, según Willer, es la muerte del niño
  ante los padres en el canto I, fragmento plagiado del poema «O rei dos
  olmos», de Goethe, con Maldoror en lugar del Gênio da Floresta.
[^97]: Willer, Prefacio, en Lautréamont, <i lang="lt">op.&nbsp;cit.</i>; y
  <https://en.wikipedia.org/wiki/Comte_de_Lautr%C3%A9amont>.

Los libros de Lautréamont solo están citados aquí porque se convirtieron
en una referencia de subversión para muchas de las vanguardias europeas
en el inicio del siglo XX, que así los preservaron y popularizaron. Los
surrealistas franceses Louis Aragon y André Breton lo colocaron en sus
panteones de autores malditos, al lado de Baudelaire y Arthur Rimbaud, y
republicaron <cite>Poesias</cite>, en 1919, después de descubrir uno de
los pocos ejemplares de la obra en la Biblioteca Nacional Francesa. A
Lautrèamont, Aragon y Breton también le dedicaron un número de la revista
surrealista que editaron, <cite>Le Disque Vert</cite>, en 1925, titulada
«Le Cas Lautréamont» («El caso Lautréamont»). Ambas iniciativas
volverían la obra del poeta conocida para nuevos públicos. Antes de la
revista, uno de los pioneros del modernismo en Estados Unidos, Man Ray,
en 1920, hizo una obra clasificada como dadaísta llamada <cite>L’Énigme
d’Isidore Ducasse</cite> (El enigma de Isidore Ducasse). Es una
fotografía en la que se ve alguna cosa escondida en un trozo de fieltro
marrón amarrado con una cuerda de pita, inspirada en un fragmento de
<cite>Los cantos de Maldoror</cite>: «<i lang="en">Beautiful as the
chance meeting, on a dissecting table, of a sewing machine and an
umbrella</i>»[^98].

[^98]: Disponible en
  <https://www.wikiart.org/en/man-ray/the-enigma-of-isidore-ducasse-1920>.
  «Bello como el encuentro fortuito, sobre una mesa de disección, de una
  máquina de coser y un paraguas».

La obra de Man Ray era lo que el artista francés (y su amigo) Marcel
Duchamp llamó en 1913 <i lang="en">ready-made</i>, práctica que consiste
en tomar objetos con los que uno es indiferente y
recontextualizarlos de manera que se torcieran sus significados. En la
década de 1910, los dos produjeron una serie de esas obras, que, de
cierta manera, fueron pioneras en el mundo del arte occidental en usar y
dejar explícita la recombinación de informaciones y otros elementos
para crear una nueva obra. Como cuenta Duchamp:

> En 1913 tuve la alegre idea de fijar una rueda de bicicleta
> a un taburete de cocina y verla girar. Unos pocos meses después compré
> una reproducción barata de un paisaje de una noche de invierno, a la que
> llamé de "Farmacia" después de añadir dos pequeños puntos, uno rojo y
> otro amarillo, en el horizonte. En Nueva York en 1915 compré en una
> ferretería una pala de nieve en la que escribí «Delante del brazo
> roto». Fue por esa época cuando la palabra
> «<i lang="en">ready-made</i>» me vino a la mente para designar esta forma
> de expresión. Un punto que deseo mucho esclarecer es que la elección de
> estos «<i lang="en">ready-mades</i>» nunca fue dictada por el placer
> estético. Esa elección estaba basada en una reacción de indiferencia
> visual con al mismo tiempo una total ausencia de buen o mal gusto... De
> hecho, una completa anestesia. [...] Otro aspecto del
> «<i lang="en">ready-made</i>» lleva el mismo mensaje; de hecho
> casi ninguno de los «<i lang="en">ready-made</i>»
> existentes hoy es un original en el sentido convencional.[^99]

[^99]: Publicado originalmente en el sitio web Iconoclast:
[www.13am.net/iconoclast](https://www.13am.net/iconoclast/), traducción
del inglés por Ricardo Rosas (Arquivo Rizoma) en el libro electrónico
<cite>Recombinação</cite>, p. 17.

En 1917, al sacar un urinario del baño, firmarlo y colocarlo sobre un
pedestal en una galería de arte, su hasta hoy más conocida obra[^100],
Duchamp también rechazó el significado de la interpretación funcional
aparentemente concluida del objeto. Aunque ese significado no hubiera
desaparecido por completo, fue yuxtapuesto a otra posibilidad &mdash;el
significado como objeto de arte&mdash;. El urinario en una galería
instigaba un momento de incertidumbre y reevaluación y cuestionaba una
vez más el esencialismo romántico, que coloca la obra de arte como
producto de una naturaliza divina y que privilegia el trabajo creativo
individual. El urinario y la rueda de bicicleta eran productos
industriales hechos por máquinas, recogidos en lugares diversos y
provistos de un nuevo significado por Duchamp; colocados en lugares de arte
como galerías, no podrían ser patentados como otras obras de la época
(cuadros, esculturas, fotografías), pues el *objeto* en sí no tenía
valor, podía ser descartado y otro ocuparía su lugar en una nueva
exposición sin perjuicio del significado. Lo que valía era la idea
propuesta por la experiencia de que se vea un objeto de uso cotidiano
como un urinario &mdash;o un trozo de fieltro escondiendo otros objetos,
o una rueda de bicicleta&mdash; en una galería de arte. Nacía ahí el
arte conceptual, desde su origen libre, contrario a derechos de autor y
crítico con la propiedad de las ideas, pero también susceptible, en las
décadas siguientes, a transformarse en mercancía y frecuentar museos y
galerías registradas con <i lang="en">copyrights</i> de miles de
dólares.

[^100]: Realizado por Duchamp en 1971 al enviar del objeto a la
  exposición de la Asociación de Artistas Independientes de Nueva York
  bajo el seudónimo R. Mutt.

Duchamp y Man Ray pertenecían a una de las vanguardias europeas de esa
época, el dadaísmo, que, extendido por Suiza, Estados Unidos, Francia y
Holanda, pero también por Georgia, Japón y Rusia[^101], desarrolló buena
parte de sus trabajos a partir del cuestionamiento de la idea de artista
y de la no separación entre arte y vida. El dadaísmo, que es una palabra
con muchos orígenes, pero que en todos ellos significa «nada», debe
mucho a otra noción de Duchamp, el *antiarte*, pensada por él a partir
de los <i lang="en">ready-mades</i> en 1913 y adoptada por muchos
movimientos contraculturales del siglo XX[^102] como un método de
desafiar los pilares de lo que sería un arte tradicional &mdash;entre
ellos temas como la autoría, la belleza y la propiedad
intelectual&mdash;. Varias obras del dadaísmo cuestionaban la idea del
genio creador solitario y manifestaban una rebelión contra los
principios capitalistas que rodeaban los valores artísticos. El rumano
Tristan Tzara, una de las principales figuras del dadaísmo, manifestó un
poco de esa rebelión al utilizar con frecuencia la aleatoriedad, el
<i lang="en">nonsense</i> y el azar para producir obras que chocaran al
<i lang="lt">statu quo</i> del mundo artístico de la época, como en
«Para hacer un poema dadaísta», de 1920:

[^101]: Hay disponibles muchos detalles del dadaísmo en <https://en.wikipedia.org/wiki/Dada>.
[^102]: De los movimientos Cobra e Internacional Letrista, cercanos al
  surrealismo y al dadaísmo de la primera mitad del siglo XX, al Provos
  (Holanda), al punk y al neoísmo, en la segunda mitad, pasando por los
  situacionistas y por la <i lang="en">mail art</i> también citados
  aquí. Esos movimientos *antiartísticos* se explican con detalle en una
  bella y poco conocida obra llamada <cite>El asalto a la cultura:
  movimientos utópicos desde el letrismo a la <span lang="en">class
  war</span></cite>, de Stewart Home.

> Coja unas tijeras.
>
> Escoja en el periódico un artículo de la longitud que desea darle a su poema.
>
> Recorte el artículo.
>
> Recorte en seguida con cuidado cada una de las palabras que forman el artículo y métalas en una bolsa.
>
> Agite suavemente.
>
> Ahora saque cada recorte uno tras otro.
>
> Copie concienzudamente en el orden en que hayan salido de la bolsa.
>
> El poema se parecerá a usted.
>
> Y es usted un escritor infinitamente original y de una sensibilidad
> hechizante, aunque incomprendida por el vulgo.[^103]

[^103]: En el original en francés: «<i lang="fr">Prenez un jornal.
  Prenez des ciseaux.  Choisissez dans ce jornal un article ayant la
  longueur que vou comptez donner à votre poème. Découpez l’article.
  Découpez ensuite avec soin chacun des mots qui forment cet article et
  mettez-le dans um sac. Agitez doucement. Sortez ensuite chaque coupure
  l’une après l’autre dans l’or- dre où eles ont quitté le sac. Copiez
  consciencieusement. Le poème vous ressemblera. Et vous voilà un
  écrivain infiniment original et d’une sen- sibilité charmante, encore
  qu’incromprise du vulgaire</i>».

Presentadas también en muchos manifiestos producidos en la época, las
ideas del dadaísmo[^104] buscaban la destrucción de un sistema artístico
en que la propiedad intelectual, entronada en la noción de autoría, ya
tenía un papel importante. No por casualidad, también aquí la tecnología
empieza a tener mayor relevancia en el arte; los
<i lang="fr">collages</i>, la poesía sonora y el cine son artes,
impulsadas en esa época, en las que los aparatos técnicos juegan un papel
principal, tanto como método de producción (el caso de los
<i lang="fr">collages</i>) como de grabación y presentación al público
(poesía sonora y el cine).

[^104]: Vale también citar como ideas influyentes en el dadaísmo
  aquellas expuestas en los lienzos del alemán Kurt Schwitters, repletos
  de imágenes aleatorias de recortes de periódicos, billetes de tren y
  fotografías, y en las poesías sonoras (sen palabras) de Hugo Ball,
  fundador, junto a su esposa Emmy Hennings, en 1916, del punto de
  encuentro de los dadaístas, el Cabaret Voltaire, en Zúrich, en Suiza.

Los <i lang="fr">collages</i> habían entrado con fuerza en el mundo del
arte con el español Pablo Picasso y el francés Georges Braque, a partir
de 1912, basados en la evolución de las técnicas de impresión, que
hicieron posible la circulación en masa de periódicos y revistas de
donde los pintores recortaban fragmentos y combinaban los diseños y
pinturas en sus cuadros. La poesía sonora ganó repercusión con el
fundador del futurismo italiano, Filippo Tommaso Marinetti, que entre
1912 y 1914 publica <cite>Zang Tumb Tumb</cite>, un poema sonoro y
visual en que las nuevas técnicas modernistas de tipografía y
diagramación del italiano se mezclan con una rica y antigua tradición de
poesía oral para jugar con los sonidos de las palabras &mdash;aunque, en
el caso de Marinetti y del futurismo italiano, las experimentaciones
estuvieran a cargo de una retórica de la velocidad industrial que
derivaría en misoginia y en el fascismo de Mussolini[^105]&mdash;. Los
dadaístas Hugo Ball y Kurt Schwitters también harían poemas sonoros; el
primero fue interpretado por Ball en la apertura del Cabaret Voltaire,
en 1916 &mdash;«<i>gadji beri bimba glandridi lauli lonni
cadori</i>»[^106]&mdash;, que, sin ningún gramófono disponible en la
época, fue registrado muchos años después como una canción pop bailable
en una mezcla de ritmos africanos en «I Zimbra»[^107], del disco
<cite>Fear of Music</cite>, de Talking Heads, en 1979. El alemán
Schwitters tuvo más suerte; en los años 20 recorrió con Tzara, Hans Arp
y Raoul Hausmann diversas exposiciones literarias en Europa recitando (y
provocando) a las audiencias con «Ursonate» (también llamada «Sonata
primal»), un poema sonoro basado en una frase «<i lang="de">Fümms bö wö
tää zää Uu</i>» repetida y aumentada por otros fragmentos durante el
tiempo en que Schwitters lo recitó &mdash;una de esos recitales fue
grabado en una radio de Frankfurt, en Alemania, en 1932, y todavía hoy
está disponible[^108].

[^105]: El primer «Manifiesto futurista», escrito por Marinetti y
  publicado en el periódico francés <cite>Le Figaro</cite> en 1909, «exaltaba la
  velocidad como nuevo valor estético destinado a enriquecer la
  magnificencia del mundo», como escribe Franco «Bifo» Berardi en
  <cite>Después del futuro</cite>, libro donde detalla los conceptos de
  «futuro» a lo largo del siglo XX y hoy. Con la velocidad, viene la
  apología al automóvil, a las virtudes de la guerra, y a la
  desvalorización de todo lo que es femenino, expuesto en este fragmento
  del manifiesto de Marinetti extraído por Bifo: «Queremos alabar al
  hombre que tiene el volante, cuya lanza ideal atraviesa la Tierra,
  lanzada ella misma por el circuito de su órbita» (Berardi,
  <cite>Depois do futuro</cite> [<cite>Después del futuro</cite>], p.
  24).
[^106]: Más información sobre el poema en
  <https://www.nealumphred.com/hugo-ball-sound-poetry-gadji-beri-bimba>.
[^107]: Puede verse en <https://www.youtube.com/watch?v=3tyVn2ZDJ-Y>.
[^108]: Nota del traductor: El vídeo enlazado ya no está disponible en
  YouTube, pero se pueden encontrar representaciones del poema como
  esta: <https://www.youtube.com/watch?v=AAcKb24dVOg>.

La radio, por cierto, pasa a formar parte del día a día mundial a partir
de la década de 1920, lo que impulsa la experimentación sonora. El
registro de los gramófonos, de alcance local y efímero, pasa a tener una
posibilidad de audiencia e interacción con miles de personas. La
influencia de la radio en las décadas de 1930 y 1940 hizo que la mera
lectura de un texto literario para la transmisión pública por un sistema
de radiotransmisión se transformara en otra forma de creación. Se hizo
famoso el pánico de muchos que escuchaban <cite>A guerra dos
mundos</cite>, el 30 de octubre de 1938 en la radio CBS, de Estados
Unidos, una transmisión dirigida por Orson Welles, producida por el The
Mercury Theatre on the Air, basada en un texto de H. G. Wells. Después
de los primeros quince minutos, tras oír relatos de apariciones de
extraterrestres en varias fincas de Estados Unidos, la transmisión
parece fallar, dándoles a muchos la impresión de que la CBS en Nueva
York estaba siendo *realmente* invadida por los extraterrestres de los
que hablaban los relatos escuchados en directo[^109]. Aunque aún hoy
haya discrepancias sobre el impacto real de la transmisión, fue un
episodio que marcó la historia de los medios de comunicación para
ilustrar que una adaptación de un texto impreso para un medio de
comunicación sonoro de alcance público nunca más sería solo una mera
transmisión, sino otra cosa &mdash;«el medio es el mensaje», como
resumiría Marshall McLuhan dos décadas y media después[^110], al
analizar la influencia de la forma de los medios en su contenido&mdash;.

[^109]: Disponible entero en
  <https://en.wikipedia.org/wiki/File:War_of_the_Worlds_1938_Radio_broadcast_full.flac>.
[^110]: En <cite>Understanding Media</cite>, publicado en español como
  <cite>Comprender los medios de comunicación</cite>.

Aún en la década de 1930, la popularización de las técnicas de
comunicación y reproducción como la radio, el gramófono, la fotografía y
el cine motivarían uno de los textos más conocidos del historiador
alemán Walter Benjamin, <cite>La obra de arte en la época de su
reproductibilidad técnica</cite>. Escrito en 1936 y publicado en 1955,
argumenta que la reproducción técnica en la época había alcanzado un
nivel «tal que había comenzado a hacer suya no solo la totalidad de las
obras de arte de épocas anteriores, sino también a hacerse un lugar
propio entre los procedimientos artísticos»[^111]. Benjamin escribe que
esas técnicas de reproducción liberarían el objeto reproducido del
dominio de la tradición y, al multiplicar lo reproducido, pondrían en el
lugar del acontecimiento único el acontecimiento en masa[^112]. Así, el
«aura» singular de las obras de arte se perdería y las copias,
(re)producidas en masa, pasarían a tener valor en sí &mdash;lo que, como
veremos algunas décadas después, sería la norma en las nuevas formas de
expresión artística y cultural a partir de la aparición de tecnologías
electrónicas, después digitales y, finalmente, digitales en red&mdash;.

[^111]: Benjamin, <cite>A obra de arte na era de sua reprodutibilidade
  técnica</cite> [<cite>La obra de arte en la época de su
  reproductibilidad técnica</cite>], p. 24.
[^112]: <i lang="lt">Ibidem</i>.

### II½.

La contestación al <i lang="en">copyright</i> por parte de las
vanguardias europeas también tuvo resonancia en el Brasil de la primera
mitad del siglo XX. Más allá de las prácticas colaborativas y libres de
los pueblos originarios sudamericanos, tema del [capítulo
6](#cultura-colectiva) de este libro, el modernismo brasileño tomó
algunos elementos de los movimientos europeos y los readaptó a las
características de la región, ya acostumbrada a la recombinación de
elementos para la creación de nuevos bienes culturales.

En ese aspecto, el paulista Oswald de Andrade tuvo un papel destacado
como divulgador y adaptador de las ideas de cuestionamiento de la
autoría y del derecho de autor. Para Oswald, la garantía de
supervivencia de la cultura brasileña estaría en la capacidad de entrar
en contacto con otras culturas y absorberlas en un proceso de deglución,
como se expresa en fragmentos del «Manifiesto antropófago», publicado en
la primera edición de la <cite>Revista de Antropofagia</cite>, en 1928:
«Solo me interesa lo que no es mío. Ley del Hombre. Ley del
Antropófago». La antropofagia propuesta por el escritor sería la
inversión del mito del buen salvaje atribuido al ilustrado Rousseau: en
vez de puro e inocente, un indígena inteligente y pícaro, que canibaliza
al extranjero y digiere al colonizador occidental y su cultura.

Otra huella contestataria contra el derecho de autor y también contra
esas nociones de autoría dejada por Oswald es <cite>Serafim Ponte
Grande</cite>, libro publicado en 1933. En el lugar donde se suelen
indicar las «Obras del autor», al inicio de la publicación, él pone la
rúbrica «Obras renegadas», y el propio libro que está por leerse se
incluye entre los títulos «repudiados». La frase que introduce la
portada de la edición parafrasea la típica información de
<i lang="en">copyright</i>: «<em>Derecho a ser traducido, reproducido y
deformado en todas la lenguas.</em>». La forma del libro, como comenta
Haroldo de Campos en el prefacio de la segunda edición (1971), está
hecha a partir del <i lang="fr">collage</i>, la yuxtaposición de
materiales diversos, lo que en las técnicas del cine parece equivaler en
cierto modo al montaje.

> El <i lang="fr">collage</i> &mdash;y también el
> montaje&mdash; siempre que trabajen con un conjunto ya constituido de
> utensilios y materiales, inventariándolos y remanipulando sus
> funciones primitivas, se pueden clasificar en aquel tipo de actividad
> que Lévi-Strauss define como *bricolage* (elaboración de conjuntos
> estructurados, no directamente por medio de otros conjuntos
> estructurados, sino mediante la utilización de restos y fragmentos), la
> cual, si es característica del «pensée sauvage», no deja de tener mucho
> en común con la lógica de tipo concreto, combinatoria, del pensamiento
> poético.[^113]

[^113]: Campos, <cite>Serafim: um grande não livro</cite>, p. 2.

La influencia del <i lang="fr">collage</i> del dadaísmo y del
automatismo del surrealismo en Oswald es considerable, pero hay también
varios artistas a lo largo de la historia que usaron artificios
semejantes de <i lang="fr">collage</i> y de cuestionamiento del propio
libro como objeto narrativo y mercadotécnico. En el prefacio de
<cite>Serafim Ponte Grande</cite> ya citado, Haroldo de Campos cita uno
de ellos, el idiosincrático <cite>La vida y opiniones del caballero
Tristram Shandy</cite>, de Laurence Sterne, escrito entre 1759 y 1767 en
Inglaterra, un «modelo pionero de la revolución del objeto libro que se
proyecta de manera avasalladora e irreversible en nuestro siglo, ahora
teniendo como aliadas las nuevas técnicas de reproducción de
transmisión de información»[^114]. La práctica de Oswald de crear a
partir del <i lang="fr">collage</i> y de la recompilación de diversos
fragmentos de otros artistas o de medios de comunicación de masas,
mezclando géneros y formas diversas para la formación de una obra
específica, se podría ver, como anticipa Haroldo de Campos,
abundantemente en el siglo XX en Brasil, de las artes visuales y
representaciones de Hélio Oiticica, Paulo Bruzcky y Adriana Varejão
hasta la literatura de Valêncio Xavier y, más recientemente, Angélica
Freitas, Verônica Stigger, Cristiane Costa y Leonardo Villa-Forte[^115].

[^114]: <i lang="lt">Ibidem</i>, p. 4.
[^115]: Villa-Forte también publicó <cite>Escrever sem escrever:
  literatura e apropriação no século XXI</cite>, un trabajo que
  reflexiona sobre la literatura en un contexto de reapropiación
  propiciado por las tecnologías digitales e Internet.

La influencia más explícita del dadaísmo a rechazar la originalidad y,
principalmente, de que toda producción artística consiste en el
reciclaje y en el reensamblaje está marcada, sin embargo, tanto en
Oswald como en otros artistas aquí citados, más en la estética que en la
forma en que la obra sea licenciada. Renunciar al derecho de autor
&mdash;o incluso licenciar de formas menos restrictivas&mdash; es, en el
contexto del arte del siglo XX, un distintivo estético que en la
práctica parece manifestarse como radical en exceso incluso para
artistas experimentales.

### III.

La aparición de obras de arte en masa se extendería durante el siglo XX y
empezaría también a incorporar aspectos de las tecnologías de
comunicación, grabación y reproducción a medida que estas se iban
popularizando. También nuevos significados y prácticas artísticas surgen
a partir de la recombinación de un dato (un texto literario grabado en
audio, por ejemplo) transformado a otro registro y recombinado de acuerdo
a diferentes técnicas posibles en otro medio &mdash;las inserciones
sonoras, cortes y pausas de edición que transforman <cite>La guerra de
los mundos</cite> en *otra cosa* al transmitirse en directo, por
ejemplo&mdash;. De la experimentación pura con un invento técnico
también surgen otras recreaciones; el gramófono de Edison en manos del
artista visual húngaro y profesor en la Bauhaus alemana László
Moholy-Nagy podría convertirse en un instrumento productivo, «de forma
que el fenómeno acústico surja por sí solo gracias a la grabación de
marcas necesarias sobre un disco sin registros acústicos previos»[^116].
La propuesta de Moholy-Nagy (y de otros) en esa época de producir música
con el gramófono va a realizarse con la música concreta a partir de
1948, con el francés Pierre Schaefer en experimentaciones sonoras con
micrófonos, voces de actores y otros sonidos posibles de obtenerse en un
estudio de radio de la época. Es en esa década también cuando el
grabador de cinta magnética empieza a ser comercializado y, con la
portabilidad que gradualmente empezó a permitir, a hacer viables nuevas
experimentaciones sonoras &mdash;como las del egipcio Halim El-Dabh,
que, con una grabadora de cinta magnética de un estudio de una radio de
El Cairo, en Egipto, produce en 1944 «The Expression of Zaar»,
considerada una de las primeras canciones electrónicas hechas a partir
de manipulaciones en estudio de sonidos registrados (por la grabadora)
de una ceremonia religiosa de la época[^117]&mdash;.

[^116]: Moholy-Nagy, citado por Kittler, <i lang="en">op.&nbsp;cit.</i>, p.
  79.
[^117]: El Dabh fue por las calles a capturar sonidos externos de una
  antigua ceremonia zaar, un tipo de exorcismo realizado en público.
  Intrigado con las posibilidades de manipular el sonido grabado para
  fines músicales, él creía que podría abrir el contenido de audio bruto
  de la ceremonia zaar para una investigación más en profundidad sobre
  el «sonido interno» contenido en esta. Fuente:
  <https://en.wikipedia.org/wiki/Halim_El-Dabh>.

Siendo desde el principio un corte, del movimiento continuo o de una
historia pasada por la lente[^118], el cine sería un arte que todavía
se prestaba más a recombinaciones. Los datos registrados de dos formas
diferentes (imagen y sonido, en movimiento) ganarían la posibilidad de
diversos efectos especiales en complejos estudios de edición (y también
producción) con las mejores grabadoras, micrófonos y otros equipamientos
sonoros que la más rica de las artes ya permitía en los años 40 y 50,
principalmente en las Costa Oeste de Estados Unidos, en Hollywood. Sería
el arte en que la recombinación podría alcanzar mayor efectividad y
belleza, según los franceses Guy Debord y Gil Wolman, en <cite>Mode
d'emploi du détournement</cite>[^119], de 1956: «los poderes de la
película son tan extensos, y la ausencia de coordinación de esos poderes
es tan evidente, que prácticamente cualquier película que esté sobre la
miserable mediocridad proporciona un tema para infinitas polémicas entre
espectadores o críticos profesionales»[^120]. Herederos de Lautrèamont,
del dadaísmo y de la Bauhaus, Debord y Wolman están vinculados a los
situacionistas, grupo fundado en Francia a partir de 1957 por varios
poetas, arquitectos, cineastas, artistas plásticos que se definían como
«vanguardia artística y política» enfocada en la crítica a la sociedad
de consumo y a la cultura mercantilizada[^121].

[^118]: Kittler, <i lang="lt">op.&nbsp;cit.</i>, p. 177.
[^119]: Publicada por primera vez en el octavo número de la revista
  surrealista belga <cite>Les Lèvres Nues</cite>, en 1956.
[^120]: Este fragmento fue *deturnado* de la introducción a <cite>O guia
  dos usuários do detournamènt</cite> publicada por BaixaCultura en la
  Red y en formato <i lang="en">zine</i>, 2015.
[^121]: «La idea de “situacionismo” se relaciona con la creencia de que
  los individuos deben construir las situaciones de su vida en lo
  cotidiano, cada uno explorando su potencial para romper con la
  alienación reinante y obtener su propio placer». Fuente:
  <https://enciclopedia.itaucultural.org.br/termo3654/situacionismo>.

<cite>Um guia para os usuários do detournamènt</cite> es uno de los
primeros textos que tiene como enfoque el desarrollo de un método
creativo basado en el plagio. Debord y Wolman hablan, por ejemplo, de la
práctica en la literatura, más bien usada en el proceso de escritura que
en el resultado final: «No hay mucho futuro en el <i>deturnamento</i> de
romances enteros, pero durante la fase de transición podría haber un
cierto número de proyectos de este tipo». En la poesía citan la
*metagrafía* &mdash;una técnica de <i lang="fr">collage</i> gráfica
desarrollada por el rumano Isidore Isou y adoptada por el movimiento del
Letrismo[^122], que los inspiró&mdash;. Como «leyes fundamentales del
<i>detournamènt</i>» están 1) la pérdida de importancia de cada elemento
<i>deturnado</i>, que puede llegar tan lejos hasta el punto de perder
completamente su sentido original; y, al mismo tiempo, la 2)
reorganización en otro conjunto de significados que confiere a cada
elemento un nuevo alcance y efecto. Un fragmento:

[^122]: Movimiento cultural ubicado en Francia y creado en los años 40 a
  partir de Isidore Isou, con influencias del dadaísmo y del
  surrealismo.

> No se trata aquí de volver al pasado, lo que es
> reaccionario; incluso los «modernos» objetivos culturales son en última
> instancia reaccionarios en la medida en que dependen de formulaciones
> ideológicas de una sociedad pasada que prolongó su agonía de muerte
> hasta el presente. La única táctica históricamente justificada es la
> innovación extremista. [...] En la realidad es necesario eliminar todos
> los restos de la noción de propiedad personal en este área. La
> aparición de las ya obsoletas nuevas necesidades por obras «inspiradas».
> Estas se vuelven obstáculos. No se trata de que te gusten o no. Tenemos
> que superarlas. Puede usarse en cualquier momento, no importa de dónde
> se saquen, para hacer nuevas combinaciones. Los descubrimientos de la
> poesía moderna relativos a la estructura analógica de las imágenes
> demuestran que cuando se reúnen dos objetos, no importa cuán distantes
> puedan estar de sus contextos originales, siempre se forma una relación.
> Restringirse a una disposición personal de palabras es mera convención.
> La interferencia mutua de dos mundos de sensaciones, o la reunión de dos
> expresiones independientes, sustituye los elementos originales y produce
> una organización sintética de mayor eficacia. Se puede usar cualquier
> cosa.[^123]

[^123]: Debord; Wolman, <i lang="lt">op.&nbsp;cit.</i>

Como práctica, el <i>detournamènt</i> no era un antagonismo a la
tradición, sino acentuación de la reinvención de un nuevo mundo a partir
del pasado, en un momento en que, posguerra, Europa (y Francia) vivían
una explosión artística y de recuperación de las vanguardias del
comienzo de siglo, que en cierta forma enseñaba a todos a «aprender a
vivir de una manera diferente mediante la creación de nuevas prácticas y
formas de comportamiento»[^124]. La idea del desvío propuesta en el
texto de Debord y Wolman parecía funcionar más para revelar que para
ocultar sus orígenes. Sería una forma, entre muchas otras, de entrar
directamente en el amplio diálogo del conocimiento, de exponer
referencias y mostrar a todos lo que se quiere absorber de estas. De la
unión de lo que se aprovecha de un lado con lo que se aprovecha de otro
es de donde, aprendemos pronto, nace algo diferente. En la misma década
de 1950 también nació accidentalmente otra técnica influyente basada en
el plagio, el <i lang="en">cut-up</i>. El pintor y poeta Brion Gysin,
que había sido miembro del grupo de André Breton en el surrealismo
francés, colocó capas de periódicos como una estera para proteger una
mesa mientras cortaba papeles con una cuchilla de afeitar. Al recortar
los periódicos, Gysin notó que las capas cortadas presentaban
yuxtaposiciones interesantes de texto e imagen; comenzó entonces a
dividir artículos de periódico en secciones, reorganizados de forma
aleatoria tal como Tzara proponía en «Para hacer un poema dadaísta».
Muchos poetas tal vez ya hubieran hecho movimientos semejantes para la
creación, pero Gysin conocía al escritor William Burroughs y lo
introdujo a la técnica en 1958, en el Beat Hotel, en París. Nacía una
unión que produciría varias obras en texto y audio, entre ellas el libro
<cite>The Third Mind</cite>, colección de <i lang="en">cut-ups</i>
firmados por los dos. Uno de los escritores más singulares del siglo XX,
Burroughs también usaría la técnica en una trilogía de libros que
incluye <cite>La máquina blanda</cite>, <cite>El tiquet que
explotó</cite> y <cite>Nova Express</cite>, publicados entre 1961 y
1964, todos narraciones en que textos existentes cortados y montados en
pedazos se colocaban de manera aleatoria, combinados con pinturas de
Gysin y sonidos experimentales recortados por Ian Sommervile &mdash;en
grabadoras de cinta magnética, entonces más populares que en el Egipto
de Halim El-Dabh en 1944&mdash;.

[^124]: Nimus, <i lang="en">op.&nbsp;cit.</i>, p. 79.

Burroughs también describiría el <i lang="en">cut-up</i> de forma
didáctica:

> El método es simple. He aquí una manera de hacerlo. Coge una
> página. Como esta página. Ahora corta del centro para abajo. Tienes
> cuatro secciones: 1, 2, 3, 4, ... un dos tres cuatro. Ahora reorganiza
> las secciones colocando sección cuatro con sección uno y sección dos con
> sección tres. Tienes una nueva página. A veces dice lo mismo. A veces
> algo bastante diferente &mdash;cortapegar discursos políticos es un
> ejercicio interesante&mdash;. De cualquier forma, vas a descubrir que
> eso dijo alguna cosa y alguna cosa bien definida. Toma cualquier poeta o
> escritor a quien admiras, digamos, o poemas que hayas leído muchas
> veces. Las palabras perderán su significado y vida por años de
> repetición. Ahora coge el poema y mecanografía pasajes seleccionados.
> Llena una página con extractos. Ahora corta una página. Tienes un nuevo
> poema. Tantos poemas como quieras. Tristan Tzara dijo: «La poesía es
> para todos». Y André Breton lo llamó policía y lo expulsó del
> movimiento. Di de nuevo: «La poesía es para todos». La poesía es un
> lugar y es libre para que todos cortapeguen a Rimbaud, y que te pongas
> en el lugar de Rimbaud.
>
> El método del <i lang="en">cut-up</i> lleva a los escritores al
> <i lang="fr">collage</i>, el cual ha sido usado por pintores durante
> setenta años. Y ha sido usado por las cámaras de fotos y
> cinematográficas. De hecho, todos los cortes de calle del cine o de
> cámaras fotográficas son, por los factores imprevisibles de transeúntes
> y la yuxtaposición, <i lang="en">cut-ups</i>. Y fotógrafos te dirán
> que sus mejores instantáneas son accidentes..., escritores dirán lo
> mismo. Los mejores escritos parecen ser aquellos hechos casi por
> accidente por escritores, hasta que el método de <i lang="en">cut-up</i>
> se volvió explícito &mdash;toda escritura es en realidad
> <i lang="en">cut-ups</i>; volveré a este punto&mdash; no hubo ninguna
> forma de producir el accidente de la espontaneidad. No puedes decidir la
> espontaneidad. Pero puedes introducir el factor imprevisible y
> espontáneo con unas tijeras.[^125]

[^125]: Burroughs, <cite>O método cut-up</cite>, p. 85.

La década de 1950 todavía ve, en un ámbito menos
<i lang="en">underground</i>, el <i lang="en">pop art</i> avanzar en la
recombinación del <i lang="fr">collage</i> modernista y dadaísta con
cada vez más apropiación de los objetos de la cultura de masas, ahora
también incluyendo la televisión, desde 1930 posible en Europa y en
Estados Unidos, pero realmente popularizada y convertida en símbolo en
los hogares occidentales en la década de 1950. Andy Warhol y Roy
Lichtenstein, dos de los nombres (no por casualidad de Estados Unidos)
más conocidos de ese movimiento, se volverían <i lang="en">pop</i> al
usar elementos de historias en viñetas, de la propaganda publicitaria y
de los programas de entretenimiento televisivo para parodiar y comentar
la apatía que el consumo (también de productos mediáticos) puede
producir en las personas. Como en Duchamp, la obra artística en el
<i lang="en">pop art</i> se produce a partir de extractos de otras obras
comercializadas a escala industrial, lo que todavía hoy plantea algunas
cuestiones sobre propiedad intelectual y autoría. Warhol, en concreto,
hizo sus obras más conocidas a partir de imágenes y objetos no
producidos por él, sino reapropiados, es el caso de <cite>Marilyn
Monroe</cite> (1967), 250 serigrafías coloridas hechas en su
<cite>Factory</cite> a partir de una foto publicitaria de la actriz
símbolo de la Edad de Oro de Hollywood. Y, asimismo, de las <cite>Cajas
de Brillo</cite>, un detergente popular en Estados Unidos, traídas por
Warhol al mundo de las artes en 1964 y registrado con
<i lang="en">copyright</i> desde entonces.

En la mayoría de los procesos legales a los que Warhol tuvo que hacer
frente en la época, los tribunales reconocieron su gesto de apropiación
como artístico. Para fundamentar su resolución, evocaron su trayectoria,
el contexto del momento y testimonios de especialistas en el asunto,
como críticos, historiadores y profesores, actores del campo artístico
que podrían dar una «definición» de lo que la sociedad considera un
artista. Las apropiaciones de Warhol pasarían la «prueba estética»
también porque demostraron una «originalidad» en su acción; como antes
lo hiciera Duchamp, en <cite>Cajas de Brillo</cite> Warhol modificaba de
forma significativa e irreversible la esfera (y las reglas) del arte. La
paradoja de la historia es que Warhol (y sus herederos después) se
mostraron inflexibles respecto a modificaciones y usos de las obras del
artista[^126].

[^126]: Como se muestra en Perromat, <i lang="lt">op.&nbsp;cit.</i>, p. 448.

Warhol no sería el primero, ni el último, en actuar de forma
contradictoria al usar todo a la hora de crear, pero no permitir que
otros usaran nada de lo que produjo. Hay un término en psicología para
esa práctica, llamado de «aversión a la pérdida» (<i lang="en">loss
aversion</i>, en inglés), que dice: «no nos gusta perder lo que
tenemos». Habla de una tendencia de dar un valor más alto a las pérdidas
que a las ganancias; los beneficios que obtenemos al copiar el trabajo
de los demás no nos crea una grande impresión, pero, cuando nuestras
ideas son copiadas, lo percibimos como una pérdida y nos volvemos como
perros guardianes sedientos de venganza[^127]. Los estudios de Disney
son tal vez el caso más conocido de esta práctica: usó extensamente el
dominio público para obtener algunas de sus principales obras
&mdash;<cite>Blancanieves y los siete enanitos</cite>,
<cite>Pinocho</cite>, <cite>Alicia en el país de las maravillas</cite>,
<cite>La bella durmiente</cite>, <cite>Aladín</cite>&mdash; y
convertirlas en dibujos animados de éxito comercial[^128]. Pero cuando
llego la hora de que los derechos de autor de las primeras películas de
Disney comenzaran a expirar, hicieron mucha presión para que el plazo de
los derechos de autor fuera prorrogado en Estados Unidos. La última
extensión, Copyright Term Extension Act, de 1998, se volvió conocida
también como «Mickey Mouse Protection Act» y pospuso la entrada en
dominio público de una obra a 70 años tras la muerte del autor, 120 años
después de la creación o 95 años después de la publicación de la obra.
Este último plazo es el caso de Mickey Mouse, publicado en 1928, que
estará en dominio público en 2024 &mdash;a no ser que Disney haga
nuevamente <i lang="en">lobby</i> para postergar esta fecha&mdash;.

[^127]: Como se muestra en una escena de la parte 4 del documental
  <cite>Everything Is a Remix</cite>, de Kirby Ferguson (2015).
  Disponible en <https://vimeo.com/baixacultura>.
[^128]: Más información sobre los casos de historias obtenidas del
  dominio público que se convirtieron en animaciones de éxito en Disney
  en <https://baixacultura.org/a-armadilha-disney>.

### IV.

Como contestación declarada al sistema de propiedad intelectual o para
convertirse en una parte rutinaria del proceso de creación, los casos de
reapropiación de ideas pasarían a ser tan frecuentes como lo quisiéramos
notar a partir de los años 60. Es en esa década cuando los objetos
tecnológicos basados en la grabación y reproducción empiezan a ser
comercializados en la mayor parte del planeta y dejan de ser solo
aparatos de especialistas para volverse cada vez más portátiles y
menores, llegando a muchas casas de clase media del mundo occidental.
Las grabadoras y los reproductores de audio en cinta magnética, por
ejemplo, empiezan a encontrarse fácilmente en los mercados de los
grandes centros urbanos con la producción y comercialización de las
cintas de casete &mdash;tanto «vírgenes», que pueden ser usadas para
grabar, como de música pregrabada&mdash; a partir de 1964. Patente de
Philips de 1963[^129], el formato «compacto» de cinta casete vino a
competir (y más tarde a sustituir) con el Stereo 8 (cartucho de ocho
pistas u ocho <i lang="en">track</i>), creado en Estados Unidos en 1958
y ligeramente mayor que el casete, y a popularizar el hábito de escuchar
y grabar música en cintas en lugar de las grabaciones de voz y dictados,
como era costumbre con las primeras grabadoras. Sería lo que incitaría
el uso de los grabadores portátiles que se propagan como productos
comerciales al final de esa década &mdash;uno de los primeros es el
modelo Typ EL 3302, de Philips, de 1968&mdash; con la posibilidad de
almacenar audios de hasta treinta minutos en cada uno de sus lados.

[^129]: Fuente: <https://en.wikipedia.org/wiki/Cassette_tape>.

Más caro y raro, el <i lang="en">sampler</i> aparece a partir de 1969
como un aparato que graba y permite la manipulación de diferentes
muestras musicales, y después como un modo de cortar y solapar canciones
&mdash;el <i lang="en">sampling</i>&mdash;. Surge a partir de los
sintetizadores, aparatos que, basados en el formato del piano, juntan y
tocan diferentes sonidos, como los primeros desarrollados por el
ingeniero Robert Moog en 1964, todavía analógicos y que popularizan
nombres como osciladores, sobres, generadores de ruido, filtros y
secuenciadores con control de tensión como palabras (y efectos) para
usarse en la modificación sonora. Contemporáneo de Moog es el Mellotron,
de 1963, vendido inicialmente como un teclado con acompañamientos
pregrabados (en cintas magnéticas) para animar los hogares ingleses,
después usado en diversas bandas de rocanrol como The Moody Blues,
Genesis, King Crinsom y The Beatles &mdash;es el sonido que introduce el
clásico «Strawberry Fields Forever», compuesta por John Lennon y Paul
McCartney en 1967&mdash;. Seguirían existiendo el Electronic Music Studios
(EMS), producido en Inglaterra en 1969, los Minimoogs y otros antes del
Fairlight CMI, creado por los australianos Kim Rydie y Peter Vogel en
1979, principal responsable de la popularización del
<i lang="en">sampler</i>, usado tanto en la difusión de la música
electrónica como en el rap &mdash;un estilo musical completamente nacido
y basado en la reapropiación[^130]&mdash;. El <i lang="en">sampler</i>
hizo de la composición, en la música pop, también el arte de combinar
sonidos y fragmentos de canciones[^131].

[^130]: Sobre el origen de los <i lang="en">samplers</i> en el rap,
  véase el documental <cite>Copyright Criminals</cite>, producido y
  dirigido por Benjamin Franzen y publicado en 2009, disponible en
  <https://www.pbs.org/independentlens/copyright-criminals>.
[^131]: Bastos, La cultura del reciclaje, en Rosas; Salgado,
  <cite>Recombinação</cite>, p. 10.

También de esa época, el videocasete aparece comercialmente entre 1959 y
1963 con diversos modelos vendidos por marcas como Toshiba, Philips y
Sony. Basados en el mecanismo de registro y almacenamiento de
información semejante al de las cintas magnéticas de audio, los primeros
todavía eran aparatos pesados, ruidosos y caros, más usados en empresas,
escuelas, hospitales. Pero en la década de 1970, disminuyeron de tamaño
y estarían disponibles en el mercado para, por ejemplo, ser usados para
la grabación casera de programas de televisión. A partir de 1969, con la
comercialización de las cámaras caseras con baterías acopladas (modelo
llamado Portapak), los videocasetes también empezarían a exhibir vídeos
caseros de esas cámaras &mdash;las primeras que usan cintas magnéticas
que pueden reproducirse en esos aparatos son del comienzo de la década
de 1970&mdash;.

Las décadas de 1950, 1960 y 1970 son, ciertamente, las de propagación
del uso de tecnologías de reproducción y grabación de audio y vídeo.
Pero no podemos olvidar que la copia impresa no solo permaneció, sino
también fue impulsada por los inventos técnicos de ese período. Un
nombre se volvió sinónimo de una práctica: Xerox, nacida en 1948 como
marca registrada de fotocopiadoras basadas en el método de la
electrofotografía, práctica que, en 1947, recibió un nuevo nombre de más
fácil definición: xerografía (del griego <i lang="grc">xeros</i>, seco, y
grafía, escritura). Xerox lanzó al mercado su primera máquina de
impresión en 1960, la Xerox 914, y a partir de entonces, igual que con
otras marcas y modelos de fotocopiadoras que la siguieron, se volvió
sinónimo de copia y referencia de una práctica artística &mdash;el
<i lang="en">copy art</i>&mdash;. En Brasil, fue rebautizada como xerografía y
muy practicada, cerca del arte postal (o <i lang="en">mail art</i>), por
artistas como el ya citado Paulo Bruscky, Hudnilson Jr. (Río de Janeiro)
y Hugo Pontes (Minas Gerais). Este último escribió:

> Tal vez el aspecto más importante de la xerografía sea que
> esta ofrece al artista que no tenga habilidad para el diseño condiciones
> para elaborar el montaje de sus proyectos, mezclando planos, líneas y
> sombras, sin ningún instrumento auxiliar que la técnica de diseño exige.
> Mediante este proceso electrónico podemos trasponer a los distintos
> grados de densidad de blanco y negro imágenes cromáticas, reticuladas e
> incluso en relieve (en este caso, objetos transformados en figuras), lo
> que se acerca mucho a los <i lang="fr">collages</i>, haciendo posible
> un regreso a las experiencias de los tachistas.[^132]

[^132]: Pontes, O que é arte xerox?, en Rosas; Salgado,
  <i lang="lt">op.&nbsp;cit.</i>, p. 18.

La cinta y el videocasete, la grabadora de audio y las cámaras de vídeo
portátiles y las fotocopiadoras trajeron un aspecto hasta entonces nuevo
a la cuestión de la propiedad intelectual y de la propagación de la
cultura libre: la reproducción de canciones, vídeos y textos para fines
caseros y no comerciales. Más allá de las copias llamadas piratas, que,
como vimos, siempre acompañaron a las reproducciones legalmente
permitidas, la llegada de las tecnologías de grabación y reproducción a
las casas de la gente popularizó la copia privada, que no pagaba
derechos de autor a nadie. Más que popular, un hábito: grabar una cinta
de casete con músicas escogidas de una o más radios, por ejemplo, se
convirtió en uno de los mejores regalos cuando se quería conquistar a
alguien en los 80.

El potencial recombinante de las tecnologías de grabación y reproducción
desarrolladas en la segunda mitad del siglo XX crearía un problema
también para la industria que, desde la propagación de los derechos de
autor a mediados del siglo XIX, se estableció con base en la propiedad
intelectual. Fue así cuando, en 1964, Phillips sacó al mercado el casete
de audio y la industria fonográfica primero trato de impedir la
publicación del producto y después hizo <i lang="en">lobby</i> en el
Congreso de los Estados Unidos para que se creara un impuesto sobre los
casetes vírgenes para compensar la pérdidas de la industria resultantes
de las copias que los usuarios harían de sus LP a casetes. Lo mismo
sucedió en 1976, cuando Sony publicó el videocasete formato Betamax y
Universal Studios y los estudios de Disney iniciaron un proceso contra
la empresa acusándola de que los productos resultantes de esos aparatos
incitarían a la violación de los derechos de autor[^133].

[^133]: <i lang="lt">Ibidem</i>.

En este último caso, una batalla judicial que duró ocho años acabó
reconociendo que la persona que grababa el último capítulo de la novela
en el videocasete Betamax (o en otros tipos que le siguieron) no
practicaba piratería[^134]. En muchas otras situaciones semejantes
ocurrió lo mismo: ninguna ley conseguiría cohibir de forma eficiente el
uso privado y comunitario de las obras sin el pago de los derechos de
autor correspondientes. No sería posible controlar la reproducción
casera sin fines comerciales cuando las tecnologías de reproducción y
grabación no solo permiten, sino que también emplean la copia para
cualquier fin, incluido el personal, como método básico de
funcionamiento.

[^134]: <i lang="lt">Ibidem</i>.

Como máquina que une grabación y registro de texto, audio e imagen, el
ordenador personal empezaría a ser vendido y popularizado por empresas
creadas en Sillicon Valley a partir de 1975. Dos décadas después, se
combinaría con Internet para volverse, los dos, responsables de hacer que
el proceso de creación estuviera aún más basado en la copia, lo que
ampliaría el debate sobre propiedad intelectual, piratería y cultura
libre a niveles no conocidos hasta entonces.
