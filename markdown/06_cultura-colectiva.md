<section class="citas">
<div class="right-align">
<i>Yo soy porque nosotros somos.</i>

Ubuntu
</div>

<br>

<i>El Maestro dijo: «Yo transmito, pero no innovo; soy verdadero en lo
que digo y estoy dedicado a la Antigüedad».</i>

<div class="right-align">
Confucio, las <cite>Analectas</cite>, aprox. s. IV-II a.&nbsp;C.
</div>

<br>

<i>La visión amerindia trata, por ejemplo, los objetos como registros
menos pasivos de las capacidades de un sujeto que las cosificaciones
personificadas de esas relaciones. De manera que la creación se da
distribuida en la relación entre los múltiples objetos y personas, sin
esta separación entre sujeto y objeto, intelecto y materia, que estamos
acostumbrados a hacer en Occidente. La subjetividad también existe en
los objetos y forma un paisaje animado compuesto de diferentes tipos de
niveles de acciones humanas.</i>

<div class="right-align">
Marcela Stockler Coelho de Souza, <cite>The Forgotten Pattern and the
Stolen Design: Contract, Exchange and Creativity among the
Kĩsêdjê</cite>, 2016
</div>

<br>

<i>Como feministas nos tenemos que oponer al carácter patriarcal del
derecho de autor, generar un paradigma que valore la creación como
práctica social y comunitaria y, sobre todo, intentar cambiar las leyes
que hoy criminalizan o prohíben prácticas fundamentales para la libertad
de expresión, para el intercambio, la distribución y la reapropiación de
la cultura. Tenemos que callar a las musas que inspiran a los genios,
para que puedan finalmente hablar las mujeres.</i>

<div class="right-align">
Evelin Heidel (Scann), <cite>Que se callen las musas</cite>, 2017
</div>
</section>

### I.

El hecho de que nosotros, occidentales, tengamos claro que la
organización de ideas en un objeto determinado que circula para otras
personas nos convierte en autores &mdash;por tanto, supuestamente
legítimos propietarios de derechos de autor sobre nuestra
creación&mdash; es fruto de un recorrido filosófico y de una forma de
ver el mundo que ha sido brevemente presentada hasta aquí en este libro.
Pero esa forma de ver el mundo y las cosas no es la única presente en
este planeta conocido como Tierra, sino solo una *perspectiva*,
destacada genéricamente aquí como occidental. Aunque esa sea la
perspectiva dominante, reguladora de la vida en las sociedades
capitalistas, cabe recordar que existen otras, presentes en muchos
lugares y comunidades tradicionales, que entran en conflicto con ciertas
ideas y modos de actuar arraigados en la sociedad capitalista, en la
cual se creó la propiedad intelectual. Cuidado, solidaridad,
colaboración y colectividad, por ejemplo, son valores todavía
importantes y dominantes en muchas comunidades y culturas milenarias que
han mantenido algunas de sus costumbres basadas en la suficiencia, y no
en la acumulación, y en el cultivo de una sabiduría colaborativa y
colectiva al margen de la búsqueda individualista y propietaria de la
cultura predominante en Occidente.

¿Cómo hablar de original y copia, por ejemplo, si una cultura de dos
milenios de Extremo Oriente incentiva la reproducción y considera más
importante que el origen de una idea su contenido y su continuidad,
incluso si es modificada y reinventada en cada contexto? ¿O cómo decir
que hay un único humano dueño de ideas cuando para muchos pueblos
originarios, entre ellos algunos amerindios, no existe separación entre
sujeto y objeto como conocemos en Occidente, y la subjetividad creadora,
a quien se debería atribuir la «autoría» o la «posesión» de los bienes,
se distribuye en una vasta red que incluye a personas y objetos,
naturaleza y sociedad de modo prácticamente simétrico?

En África, por ejemplo, hace algunos siglos, una filosofía humanista
precolonial conocida como *Ubuntu*[^245] dice: «*yo soy porque nosotros
somos*». Término de origen *nguni banto*, influyente en la lucha contra
el <i lang="en">apartheid</i> en Sudáfrica y conocido de ahí hasta el
África Subsahariana, el *Ubuntu* se caracteriza por la humanidad con sus
semejantes a través de la veneración a sus ancestros, de forma fraternal
y con compasión, incluyendo aquí considerar semejantes todas las formas
de vida &mdash;un biocentrismo que también se opone al antropocentrismo
característico de la sociedad occidental[^246], en la que se crean la
propiedad intelectual y el derecho de autor&mdash;. «La episteme y la
filosofía negra del Ubuntu tienen un sentido de conocimiento distinto al
de Occidente, un conocimiento integrador de la razón y de la emoción, de
forma que el sujeto no solo ve el objeto, sino que también lo siente;
por tanto, “sujeto y objeto se afectan mutuamente en el acto del
conocimiento”»[^247].

[^245]: Considerada una filosofía de difusión oral y no asociada a
  ningún texto específico, es una palabra que tiene variantes en
  varias lenguas africanas: <i lang="ki">umundu</i> en kikuyu y
  <i lang="mer">umuntu</i> en kimeru, dos idiomas hablados en Kenia;
  <i lang="suk">bumuntu</i> en sukuma y haya, hablados en Tanzania;
  <i lang="ts">vumuntu</i> en tsonga y tsua en Mozambique;
  <i lang="bni">bomoto</i> en bobangi, hablado en la República Democrática
  del Congo; <i lang="kg">gimuntu</i> en kikongo y en kwese, hablados en
  el mismo Congo y en Angola, respectivamente. Fuente: Eze,
  <cite>Intelectual History in Contemporary South Africa</cite>, p. 91.
[^246]: Negreiros, Ubuntu: considerações acerca de uma filosofia
  africana em contraposição à tradicional filosofia ocidental,
  <cite>Problemata: R. Intern. Fil.</cite>, v. 10, n. 2, p. 123.
[^247]: Mance, Filosofia africana: autenticidade e libertação, em Serra,
  <cite>O que é filosofia africana?</cite>, p. 75.

Una cosmovisión que tiene valores como respeto, cortesía, compartir,
comunidad, generosidad, confianza y altruismo[^248], en la que el
nosotros prevalece y el yo está incluso en el nosotros, tiene
dificultades para encajar en un sistema filosófico y jurídico como el de
Occidente. ¿Cómo hacer que un producto sea único y que pertenezca a una
persona si este es fruto de un esfuerzo colectivo ancestral y tiene por
fin la transmisión de una idea (o un objeto) creado por muchas manos?
Comunidades que tienen un modo y una práctica de conocer el mundo guiadas
por lo colectivo y por lo comunitario siguen, aunque con dificultades y
muchos choques, preservando sus bienes culturales y sus tradiciones
desde hace mucho tiempo, en el que pese al enfrentamiento a la visión
occidental exclusivista que ve productos ancestrales solo como bienes
que pueden circular en un mercado. Más que mantenernos presos en un pasado
idealizado, examinar un poco más algunas de esas perspectivas puede
mostrarnos caminos alternativos para el presente y permitirnos entender
un poco más cómo la cultura a lo largo de la historia se hizo y se hace
libre.

[^248]: Como apuntaba Nelson Mandela en un vídeo explicativo de Ubuntu
  disponible en
  <https://pt.wikipedia.org/wiki/Ficheiro:Experience_ubuntu.ogv>.

### II.

<i lang="zh">Shanzai</i> es un neologísmo chino creado en los años 2000
para designar lo que es falso, <i lang="en">fake</i>. Abarca desde
literatura hasta premios Nobel, diputados, parques de atracciones,
tenis, canciones, películas, historias de lo más diversas. En principio,
el término se refería a los teléfonos (<i lang="en">smartphones</i>) o a
la falsificación de productos de marcas como Nokia o Samsung que se
comercializan con el nombre de Nokir, Samsing o Anycat. Luego, sin
embargo, se expandieron a todas las áreas, en juegos que, a la manera
del dadaísmo, usaban la creatividad y efectos paródicos y subversivos
con las marcas «originales» para crear otros nombres &mdash;Adidas, por
ejemplo, se convierte en Adidos, Adadas, Adis, Dasida[^249]... Son, no
obstante, más que meras falsificaciones: sus diseños y funcionalidades
no deben nada a los originales, y las modificaciones técnicas o
estéticas realizadas les confieren una identidad propia[^250]. Los
productos <i lang="zh">shanzai</i> se caracterizan sobre todo por su
gran flexibilidad, adaptativos según las necesidades y situaciones
concretas, «algo que no está al alcance de una gran empresa, pues sus
procesos de producción están fijados a largo plazo»[^251].

[^249]: Han, <cite>Shanzai: el arte de la falsificación y la
  deconstrucción en China</cite>, p. 73.
[^250]: <i lang="lt">Ibidem</i>.
[^251]: <i lang="lt">Ibidem</i>.

En <cite>Shanzai: el arte de la falsificación y la deconstrucción en
China</cite>, el filósofo surcoreano radicado en Alemania Byung-Chul Han
analiza varias obras artísticas chinas, <i lang="zh">shanzai</i> o no, y
occidentales para trabajar con la idea de cómo se construyen las
nociones de autoría y originalidad en Extremo Oriente. Para ilustrar la
diferencia en relación a Occidente, cita la idea de
<i lang="grc">ádyton</i>, que en griego antiguo significaba
«inaccesible» o «intransitable». El origen de la palabra remite al
espacio interior de un templo de la Grecia Antigua que estaba
completamente apartado del exterior donde se celebraban los cultos
religiosos. «El aislamiento define lo sagrado», dice Han. La noción de
estar aislado para poder encontrarse con Dios, o consigo mismo, es
diferente en Extremo Oriente, comenzando por la arquitectura de los
espacios llamados sagrados: «El templo budista se caracteriza por la
permeabilidad o por la apertura completa. Algunos templos tienen puertas
y ventanas que no aíslan nada»[^252].

[^252]: <i lang="lt">Ibidem</i>.

En el pensamiento chino no hay <i lang="grc">ádyton</i>, ni como espacio
ni como idea. Nada se separa ni se cierra: la idea de que algo esté
apartado o aislado del todo es ajena a la manera de pensar predominante
en el Extremo Oriente[^253]. Así, no existiría la idea de *original* tal
como se entiende en Occidente, puesto que la originalidad presupone un
comienzo en el sentido estricto, de lo que una parte del pensamiento
chino tradicional reniega al no concebir la creación a partir de un
principio absoluto e individual, pero sí con el proceso continuo, sin
comienzo ni final, sin nacimiento ni muerte, fundamentalmente colectivo.
La desconfianza de los principios inmutables y de los «genios» creativos
individuales remite a la falta de esencia y a un cierto vacío que, a
los ojos de occidentales &mdash;ejemplificados por Han en el pensamiento
crítico con esas nociones orientales del filósofo alemán Hegel, uno de
los pensadores más influyentes de occidente&mdash;, puede ser visto como
hipocresía, astucia o incluso inmoralidad.

[^253]: <i lang="lt">Ibidem</i>.

Hablar de autoría, originalidad y, en consecuencia, de derecho de autor,
<i lang="en">copyright</i> y cultural libre en Extremo Oriente es, por
tanto, diferente de hablar sobre eso en el mundo occidental. Esa manera
de pensar que ignora el <i lang="grc">ádyton</i>, lo «inaccesible», ya
sea como lugar o como idea, viene de una noción de *verdad como
proceso*, es decir, más basada en la *inclusión* continua de varios
elementos que en la exclusión y consecuente unión en torno a un único
elemento separado del todo, como solemos pensar en Occidente. Si la
verdad está en proceso continuo de producción, la noción de donde esta
proviene &mdash;su *originalidad*&mdash; pierde importancia; ya no es
el origen único lo que importa, sino que el contenido de esta verdad
trascienda, circule, sea reorganizado y complementado según contextos,
objetivos y propósitos variados. Cada elemento es importante como parte
de una idea que trasciende de manera colectiva, no como parte aislada
que posee un origen y una autoría.

Teniendo en cuenta eso, el proceso creativo de una obra artística y
cultural en esa región está marcado por la continuidad y por cambios
silenciosos, no por la ruptura que una idea genial aportada por un
artista origina, como se diviniza en la visión occidental a partir del
romanticismo. No se valora tanto la *matriz* de la idea, su origen o su
autor, sino como esta va a &mdash;o debe&mdash; ser continuada. Si la
idea permanece en la copia, entonces es como si la obra continuara, sin
ruptura, sin una «nueva obra». Es como en la naturaleza: células
antiguas son sustituidas por un nuevo material celular. No se pregunta
por la célula original: «lo viejo muere y es sustituido por lo nuevo. La
identidad y la novedad no son excluyentes»[^254].

[^254]: <i lang="lt">Ibidem</i>, p. 64.

Una parte de esa forma de ver la verdad, la originalidad y el proceso de
creación como algo más colectivo que individual remite al confucianismo
(儒學), conjunto de doctrinas morales, éticas, filosóficas y religiosas
creadas por los discípulos de Confucio tras su muerte, en el 479 a.&nbsp;C.,
que tuvo gran influencia en el pensamiento de China y de países como las
Coreas, Japón, Taiwan y Vietnam, principalmente hasta inicios del siglo
XX. Natural de la provincia de Lu, hoy Shantung, este de China, Confucio
venía de una familia noble en decadencia y tuvo diversas ocupaciones en
su vida &mdash;profesor, funcionario público, político, carpintero,
pastor&mdash; hasta alrededor de los 50 años de edad, cuando comenzó a
viajar con frecuencia por las provincias chinas y a reclutar discípulos
en torno a su filosofía basada en la vida simple, en la colectividad y
en el altruismo[^255]. Era una propuesta filosófica que retomaba algunas
costumbres de dinastías chinas más antiguas como Shang (1600-1046 a.&nbsp;C.)
y la propia Zhou (1046-256 a.&nbsp;C.), período en que había una decadencia
moral y ética en la sociedad china.

[^255]: A pesar de su importancia en la tradición china, poca
  información sobre Confucio está realmente comprobada. La aquí citada
  está basada en la entrada de Wikipedia en inglés
  (<https://en.wikipedia.org/wiki/Confucius>) sobre él y en la
  introducción de la edición brasileña de <cite>Las analectas</cite>
  (<cite>Os analectos</cite>), escrita por el también traductor de la
  obra del chino al inglés, D. C. Lau.

A partir de la dinastía Han (de 206 a.&nbsp;C. a 220 d.&nbsp;C.), las enseñanzas
de Confucio empezaron a ejercer gran influencia sobre los gobiernos y la
sociedad china, proporcionando el plano de lo que sería una vida ideal y
la regla por la cual deberían medirse las relaciones humanas[^256].
Reinventadas y reinterpretadas por diversas personas a lo largo de los
siglos, sus ideas darían forma a una serie de costumbres en las áreas de
educación, cultura, política y relaciones sociales del país durante
diferentes momentos de la China imperial. Solo perderían fuerza a
comienzos del siglo XX, cuando termina el período imperial chino y el
confucianismo empieza a ser acusado de ser «excesivamente tradicional»
para convivir con el dinamismo de la entonces sociedad moderna
occidental.

[^256]: Yu, <cite>Intellectual Property and Confucianism: Diversity in Intellectual
Property: Identities, Interests and Intersection</cite>, p. 5.

La influencia de las enseñanzas de Confucio en la forma de ver la
creación en China y en los países de Extremo Oriente empieza a tener
cierta repercusión en los estudios sobre propiedad intelectual a partir
principalmente del libro <cite>To Steal a Book is An Elegant Offense:
Intelectual Property Law in Chinese Civilization</cite>, de William P.
Alford, publicado en 1995. El título del libro, «Robar un libro es una
transgresión elegante», viene de un concepto popular
(<i lang="zh">Qie Shu Bu Suan Tou</i>) chino a partir de
<cite>Kong Yiji</cite>, libro publicado en 1919 por un conocido escritor
de la época llamado Lu Xun. La historia de la obra gira en torno a un
personaje central que da nombre al libro, un intelectual autodidacta
alcohólico y fracasado que frecuenta una taberna en la ciudad de Luzhen
(魯鎮), base de otras ficciones de Xun. Él no aprobó el examen de
<i lang="zh">xiucai</i>, uno de los muchos de la China imperial de la
época, y usa en su discurso frases clásicas confusas, que generan
desprecio entre los otros clientes del local, que lo ridiculizan también
por «hacer trabajillos» y robar para comer y beber.  Una de sus
actividades era copiar manuscritos para clientes ricos; a veces, también
robaba libros de esos clientes para cambiarlos por vino en la taberna.
«Robar un libro es una ofensa elegante» era el argumento que usaba
cuando era insultado por los clientes del bar.

*Kong Yiji*, el personaje principal, fue creado como «arlequín
caricaturesco» que representa a un intelectual autodidacta del período
clásico chino en decadencia[^257] &mdash;al final del libro, el
personaje muere apaleado y es olvidado&mdash;. La obra se produjo en el
contexto del Movimiento del Cuatro de Mayo, del cual Lu Xun formaba
parte, que en 1919 se hizo famoso por protestas en la capital Pekín y
por la crítica antimperialista (la última dinastía imperial, Qing, había
terminado en 1911), en la que el confucionismo y sus maneras
tradicionales, jerárquicas y colectivistas eran consideradas un
obstáculo para la modernización en la «competición con otras naciones
del mundo occidental»[^258]. Desde este punto de vista, la literatura
del Movimiento del Cuatro de Mayo, vinculada también a las ideas
modernizadoras de otros lugares del mundo en esa época, debería intentar
evitar los clichés de la lingüística tradicional china que habían
dificultado y restringido el pensamiento creativo de las personas
durante siglos y apostar por la innovación tanto en el contenido,
considerado anticuado, como en la forma.

[^257]: En Yu, <i lang="lt">op.&nbsp;cit.</i>, que también cita aquí a Feng,
  <cite>Intellectual Property in China</cite>, p. 167.
[^258]: Yu, <i lang="lt">op.&nbsp;cit.</i>, p. 15.

¿Pero cuáles serían esos valores e ideas tradicionales contra los que
los modernistas del Cuatro de Mayo combatían? Para el confucianismo, el
pasado aportaba valores sociales, éticos y morales &mdash;la idea de
familia, por ejemplo, como unidad básica que organiza la
comunidad&mdash; que deberían ser incorporados a la sociedad
contemporánea. La necesidad de conocer el pasado para el crecimiento
personal dictaba que hubiera un amplio acceso a la herencia común de
todos los chinos[^259]. Como tener propiedad sobre esas obras del pasado
permitía a unos pocos monopolizar un conocimiento tan esencial para
todos, había entonces una contradicción entre los derechos de propiedad
intelectual y los valores morales tradicionales de China defendidos por
Confucio. Al enaltecer valores familiares y derechos colectivos, los
chinos no habrían desarrollado el concepto de derechos individual; por
tanto, no considerarían la creatividad y la innovación como propiedad
individual, sino como un beneficio colectivo para la comunidad y la
posteridad. Para aquellos que consideran el individualismo un
prerrequisito esencial para el desarrollo de los derechos de propiedad
intelectual, esa visión del mundo representaría un gran desafío[^260].

[^259]: Alford, <cite>Steal a Book Is an Elegant Offense: Intellectual
  Property Law in Chinese Civilization</cite>, p. 20.
[^260]: Yu, <i lang="lt">op.&nbsp;cit.</i>, p. 4.

Había otro hecho importante instalado en la cultura china y de los
pueblos de Extremo Oriente a partir del confucianismo. En esa filosofía,
desde muy pequeños los niños eran enseñados a pensar a partir de la
memorización y de la copia de los clásicos, procedimiento que, según sus
maestros, inculcaría en los jóvenes valores familiares, compasión filial
y respeto ancestral[^261]. La memorización y la copia, especialmente de
las obras de Confucio, se convertirían en procedimientos necesarios para
garantizar el éxito en los exámenes del Servicio Público Imperial,
realizados durante trece siglos (entre el 605 y 1905, aproximadamente) y
que consistían en una serie de pruebas que servían para seleccionar a
quién, entre la población (masculina y descendiente de la
aristocrática), se le permitiría la entrada en la burocracia estatal
&mdash;lo que daría poder y gloria a los candidatos y honor a sus
familias, distritos y provincias&mdash;.

[^261]: <i lang="lt">Ibidem</i>.

Cuando estos niños crecían, se volvían más *compiladores* que
*creadores*. Memorizaban tantas historias clásicas que empezaban a
construir sus narraciones a partir de un extenso proceso de copiar y
pegar (<i lang="en">cut-and-paste</i>) frases, fragmentos y pasajes de
esos textos antiguos. Si a ojos de un occidental, especialmente de los
siglos XX y XXI, eso se vería como un plagio, para los chinos de la
época se veía como un rasgo distintivo de intelectualidad y conocimiento
cultural. «Cuando autores chinos tradicionales tomaban
fragmentos de un texto prexistente y, principalmente, de un clásico, se
espera que el lector reconozca la fuente del material utilizado
instantáneamente. Si un lector es lo suficientemente desgraciado para
dejar de reconocer ese material citado, es culpa de él, no del
autor»[^262].

[^262]: En Yu, <i lang="lt">op.&nbsp;cit.</i>, y Stone, What Plagarism Was
  Not: Some Preliminary Observations on Classical Chinese Attitudes
  Toward What the West Calls Intellectual Property, <cite>Marquette Law
  Review</cite>, 2008.

En *Lún Yǔ* (論語, en español conocido como las <cite>Analectas</cite>),
principal colección de ensayos e ideas atribuidas a Confucio, está
escrito: «El Maestro dijo: “Yo transmito, pero no innovo; soy verdadero
en lo que digo y estoy dedicado a la Antigüedad” (shù ér bù zuò)»[^263].
Aunque esa afirmación pueda dar un cierto desánimo a la creatividad,
tenía como motivo enfatizar el papel de cada uno como portador de una
tradición, y no como fundador o creador de una nueva doctrina[^264]. En
otro lugar de las <cite>Analectas</cite>, se atribuye a Confucio la
frase: «Merece ser un profesor el hombre que descubre lo nuevo al
refrescar en su mente aquello que ya conoce (<i lang="zh">wēn gù ér
zhīxīn / kěy ǐ wéi shī yǐ</i>)»[^265]. Según Yu, el pensamiento de
Confucio manifiesta una visión de que «la capacidad de hacer un uso
transformador de obras prexistentes puede demostrar la comprensión y
devoción al núcleo de la cultura china, así como la capacidad de
distinguir el presente del pasado mediante pensamientos
originales»[^266].

[^263]: Confucio, <cite>Os analectos</cite>, libro VII, cap. 1, p. 62.
[^264]: Yu, <i lang="lt">op.&nbsp;cit.</i>, p. 7.
[^265]: Confucio, <i lang="lt">op.&nbsp;cit.</i>, p. 44.
[^266]: Yu, <i lang="lt">op.&nbsp;cit.</i>, p. 69.

Un último factor que habría influenciado la divergencia china en
relación a la propiedad intelectual tal como esta fue concebida en
Occidente es un cierto desdén de los confucionistas por el comercio y
por la creación de obras por puro lucro. Una vez más,
<cite>Analectas</cite>: «Las ocasiones en que el Maestro hablaba sobre
lucro, Destino y benevolencia eran raras (<i lang="zh">Zi hǎn yán
lì</i>)»[^267]. En su ampliamente usada traducción al inglés, Arthur
Waley esclareció la enseñanza del maestro añadiendo la nota a pie de
página «Podemos ampliar: raramente hablamos de asuntos desde el punto
de vista de lo que pagaría mejor, sino solo desde el punto de vista de
lo que era cierto»[^268]. Los comerciantes (<i lang="zh">shāng</i>) eran
considerados la más baja de las cuatro clases de la sociedad tradicional
china, detrás del oficial-estudioso (<i lang="zh">shì</i>), del
agricultor (<i lang="zh">nóng</i>) y del artesano
(<i lang="zh">gōng</i>). No sería sorpresa, por tanto, que los
confucionistas no hubieran dado énfasis, hasta el siglo XX, a la noción
de propiedad intelectual y a la idea relacionada de los derechos
comerciales exclusivos[^269].

[^267]: Confucio, <i lang="lt">op.&nbsp;cit.</i>, p. 69.
[^268]: Yu, <i lang="lt">op.&nbsp;cit.</i>, p. 8.
[^269]: Como afirma Yu, «La propiedad intelectual puede ser vista
  también en la expresión china del término «patente»,
  <i lang="zh">zhuānlì</i> (专利), que puede ser traducido literalmente
  como «beneficio exclusivo» o «lucro exclusivo». Curiosamente, el
  fallecido Dr. Arpad Bogsch, el director general desde hace mucho
  tiempo de la Organización Mundial de la Propiedad Intelectual,
  consideró el término tan problemático que «sugirió que se debería
  emplear alguna otra nomenclatura china para sustituir los dos
  caracteres chinos, a fin de evitar malentendidos» (Yu,
  <i lang="lt">op.&nbsp;cit.</i>, p. 8).

Por el lado del budismo y el sintoísmo, los otros dos conjuntos de ideas
más difundidos en Extremo Oriente, la influencia del confucianismo en la
cultura china hizo que la perspectiva del derecho de autor en la
religión se orientara, durante mucho tiempo, más a la defensa de una
base de información pública, de libre acceso y reutilización &mdash;lo
que en Occidente fue llamado dominio público&mdash;. La demora en China
en firmar tratados internacionales de propiedad intelectual (a partir de
la década de 1980, cuando el país también pasa a ser parte de la World
Intelectual Property Organization) tiene relación con una cultura
colectiva y de defensa del dominio público enraizada desde hace mucho
tiempo en su sociedad. Y también se asocia con la difusión de la cultura
<i lang="zh">shanzai</i> ya citada, que toma la copia como base para la
recreación de diferentes productos y marcas de partir de una práctica
creativa *compiladora* enraizada en el día a día del pueblo de la
región, incluso con la pérdida de influencia del confucianismo.

Sin embargo, en las últimas décadas del siglo XX y en las primeras del
siglo XXI, China no solo se ha adaptado a la visión occidental de la
propiedad intelectual, sino que, en 2020, se ha convertido en quien
lidera las solicitudes internacionales de patentes, por delante de
Estados Unidos[^270]. Por otro lado, también es el país al que más se
atribuyen infracciones de derechos de autor en el ámbito
musical; la International Federation of the Phonographic Industry (IFPI)
afirma que el 95&nbsp;% de las canciones que circulan por el país lo
hacen a partir de descargas sin autorización de los propietarios[^271].
Más allá del cuestionamiento a los datos de esas investigaciones,
podemos preguntarnos: ¿cómo sería una legislación que respetara el
pasado colectivista de la cultura confucionista del Extremo Oriente y
dialogara con una noción contemporánea de derecho de autor del
<i lang="lt">statu quo</i> del capitalismo? Yu apuesta por conceptos que
no aspiran a una legislación maximalista &mdash;es decir, que no tengan
*todos los derechos reservados* a los propietarios de un
<i lang="en">copyright</i>, por ejemplo&mdash;. Es el caso del Creative
Commons, citado por él como ejemplo de opción en que «los confucionistas
pueden desdeñar el comercio y aún así abrazar los derechos de propiedad
intelectual»[^272].

[^270]: Asunto divulgado por la AFP a partir de un comunicado de WIPO en
  2019 que decía que China «conquistó el título» por primera vez. «En
  1999, OMPI recibió 276 solicitudes de China, frente a 58&nbsp;990 en
  2019, 200 veces más hoy que hace 20 años», detalló el director general
  de la organización, Francis Gurry. Fuente: AFP, China se torna campeã
  de pedidos internacionais de patentes, <cite>UOL Notícias</cite>, 7
  abr. 2020, disponible en
  <https://noticias.uol.com.br/ultimas-noticias/afp/2020/04/07/china-se-torna-campea-de-pedidos-internacionais-de-patentes.htm>.
[^271]: A la IFPI en su lucha contra la llamada piratería en China le
  gusta mostrar esos datos, diciendo cómo y cuánto ganarían las
  industrias culturales si hubiera una adecuación a las normas
  occidentales de derecho de autor. Hay ejemplos disponibles en la
  página de la organización: <https://www.ifpi.org>.
[^272]: Yu, <i lang="lt">op.&nbsp;cit.</i>, p. 7.

### III.

En portugués, «<i lang="pt">presente de índio</i>» (<i lang="en">Indian giver</i>) es un
término que aparece en los idiomas occidentales a partir del contacto de
los colonizadores europeos con los pueblos originarios de continente que
sería entonces acuñado por uno de ellos como *América*. En inglés,
este está registrado por primera vez en 1765 para definir un tipo de
regalo por el cual se espera una retribución equivalente. Un siglo
después, <i lang="en">Indian giver</i> fue recogido en un diccionario de
americanismos como una frase común entre niños de Nueva York para
referirse a un regalo que alguien da y recibe de vuelta[^273], el mismo
significado que tomó en portugués y que, en concreto en Brasil, se suma
al sentido de un «presente no deseado» que la expresión obtuvo con su
uso cotidiano.

[^273]: Observado en 1765 por Thomas Hutchinson en <cite>History of
  Massachusetts: From the First Settlement thereof in 1628, until the
  Year 1750</cite> y después definido en el diccionario citado, escrito
  por John Russell Bartlett. Fuente:
  <https://en.wikipedia.org/wiki/Indian_giver>.

El uso común de la expresión parte de una acción que entiende como
«normal» que alguien reciba un regalo y lo consuma como cualquier otra
cosa recibida o adquirida. Occidental y de origen europeo, ese normal no
es la costumbre de muchos pueblos originarios del continente americano y
de otras regiones del planeta. Para estos, cualquier cosa que sea
regalada o donada debe tener alguna retribución, ser transmitido, a lo
sumo sustituido, no guardado para siempre o volver a emplearlo para el
aprovechamiento exclusivo de una persona. Un regalo es el comienzo de
una relación circular de ida y vuelta, que presupone responsabilidades
mutuas y no termina con el acto de recibir, guardar y usar en algún
momento, como en las costumbres de las sociedades occidentales donde el
capitalismo prevalece como modo principal de organizar la vida. Siendo
el inicio de una relación, no puede ser consumido y descartado como una
simple mercancía. En tal caso, podrá ser recuperado: «regalo de indio».

El siguiente caso contado por Viveiros de Castro es ilustrativo.

> Es muy común que un equipo de rodaje llegue a una región indígena y
> ofrezca treinta mil dólares para grabar, y que los indios conversen
> entre si y hagan una contraoferta, cuarenta mil dólares, y cierren la
> negociación. Se hace el trato. Entonces se hace la película, y el
> equipo piensa que solucionó el problema. Paga directamente y esas
> cosas. Cuando sale la película, el director recibe una llamada que
> dice lo siguiente: «¡Ya nos estás devolviendo el dinero, has robado a
> la gente!». Entonces él dice: «Espera, yo firmé un papel, ya di los
> cuarenta mil», y los indios: «No, pero no pagaste no sé qué», o
> entonces «no fue para todo el mundo». Entonces él se da cuenta de
> repente de que los indios tienen una concepción de transacción, de
> relación social en general, radicalmente opuesta a la nuestra. Cuando
> hacemos una transacción, entendemos que esta tiene comienzo, medio y
> fin, yo te di un chisme, tú me pagas, estamos en paz, tú vas para un
> lado, y yo voy para otro. Es decir, la transacción se hace a vistas de
> su término. Los indios, al contrario: la transacción no termina nunca,
> la relación no termina nunca, comenzó y no va a terminar nunca más, es
> para toda la vida. Al pedir más dinero, no es exactamente el dinero lo
> que los indios quieren, sino la relación. Ellos no aceptan que acabó
> la propuesta, no acabó nada, ahora es cuando va a comenzar. De donde
> vienen los famosos estereotipos: los indios piden todo el tiempo. Sí,
> piden. Y nos quejamos de que lo que ellos obtienen es desechado de
> repente: las aldeas se llenan de objetos descartados que los indios
> nos pidieron, insistieron hasta conseguirlos, y, cuando los consiguen,
> no los cuidan, los desechan, dejan que se pudran, que se oxiden. Y los
> blancos se quedan con aquella idea de que esos indios son salvajes de
> verdad, no saben cuidar de las cosas. Pero está claro, el problema de
> ellos no es el objeto, lo que ellos quieren es la relación.[^274]

[^274]: Viveiros de Castro, Economia da cultura digital, en Savazoni;
  Cohn (orgs,). <cite>Cultura digital.br</cite>, p. 90.

En la antropología, hay muchos estudios sobre el tipo de transacción que
ocurre en el intercambio (o donación) de regalos. Uno de los más
antiguos e influyentes es el del francés Marcel Mauss, el hoy clásico
«Essai sur le don», publicado en 1924 en Francia, traducido al español
como «Ensayo sobre el don»[^275], en el que este compara diferentes
sistemas de regalos entre sociedades de Polinesia, Melanesia y del
noroeste del continente americano para probar que el intercambio de
regalos es un fenómeno que presupone diversas transacciones
&mdash;jurídicas, morales, estéticas, religiosas, mitológicas&mdash;,
además de las económicas. Mauss afirma que el sistema de intercambio de
regalos en esas sociedades tiene un principio común regulador: la
obligación de dar, recibir y retribuir. En lugar de reducir esas
transacciones a simples intercambios de regalos, el francés muestra que
esos procesos llevan consigo una dimensión moral que le da un sentido a
las relaciones sociales, lo que las caracteriza como prestaciones de
servicios que tratan de establecer nuevas alianzas y a fortalecer las
antiguas[^276].

[^275]: La referencia original es Mauss, «Essai sur le don»,
  <cite>L’Année Sociologique I</cite>, pp. 30-186. El antropólogo
  Marshall Sahlins sitúa las ideas de regalo en la filosofía política a
  partir de Mauss en <cite>Stone Age Economics</cite> (Economía de la
  Edad de Piedra), publicado en 1972. Lewis Hyde habla sobre el tema en
  el ámbito de la cultura y del arte en <cite>The Gift</cite> (El
  regalo), en 1983; entre diversas otras obras densas que desarrollan,
  perfeccionan y recombinan las ideas de Mauss sobre regalo.
[^276]: Sertã; Almeida, Ensaio sobre a dádiva, en <cite>Enciclopédia de
  Antropologia</cite>. Disponible en: <http://ea.fflch.usp.br/obra/ensaio-sobre-dádiva>.

Mauss se da cuenta de que los bienes en circulación en esos sistemas son
inseparables de sus propietarios y poseen una sustancia moral propia
relacionada con la materia espiritual del dador de un objeto
determinado[^277]. Esa sustancia es dada cuando hace un intercambio de un
regalo y pasa a circular junto a ese objeto, que entonces nunca será
solo un «simple objeto», sea cual sea, sino algo que tiene *intención* y
que convive en igualdad con las personas. En ese sentido, el sistema de
mercancía conocido en Occidente se vuelve diferente para las
perspectivas de los pueblos tradicionales. En palabras de la antropóloga
Marilyn Strathern (1984), es lo contrario a la economía de la
<i lang="en">commodity</i>, en la que las personas y las cosas asumen la
forma social de las personas[^278]. Es en ese sentido que, en sociedades
originarias de diversos lugares del mundo, el modelo de propiedad (en
particular el de propiedad intelectual), calcado de la relación de la
obra de arte como mercancía de consumo, se vuelve insuficiente para
lidiar con una relación más duradera y compleja de la circulación de
objetos/bienes[^279]. En el sistema cultural de las sociedades
originarias, es perceptible, en primer lugar, la centralidad de los
valores colectivos, ligados a la pluralidad y a la supervivencia de la
comunidad, en relación a los valores individuales, de uso exclusivo y
elección individual. Lo que, a su vez, hace que los bienes
culturales y de conocimiento en ese contexto sean más difíciles de
transformar solo *en un producto más* vendido como mercancía, pues hay
principios y responsabilidades de reciprocidad y solidaridad que buscan
darle valor a la propia sustancia moral &mdash;que podríamos también
llamar «alma»&mdash; de los objetos en sus relaciones con las personas y
el mundo. Para citar un ejemplo, los derechos del pueblo guaraní, uno de
los más presentes en Brasil y Sudamérica, son guiados por los principios
de valoración de derechos colectivos en detrimento de los individuales,
lo que genera normas más flexibles, discutidas de vez en cuando en
comunidad en las <i lang="gn">Aty Guassu</i> (gran asamblea), basadas en
sus prácticas culturales y que tratan de mantener el equilibrio de la
convivencia y el respeto a las tradiciones[^280].

[^277]: <i lang="lt">Ibidem</i>.
[^278]: Strathern, <cite>The Gender of the Gift: Problems with Women and
  Problems with Society in Melanesi</cite>. Citado, en esos términos,
  por Coelho de Souza, The Forgotten Pattern and the Stolen Design:
  Contract, Exchange and Creativity among the Kĩsêdjê, en Brightman;
  Fausto; Grotti, <cite>Ownership and Nurture: Studies in Native
  Amazonian Property Relations</cite>.
[^279]: Coelho de Souza, <i lang="lt">op.&nbsp;cit.</i>, p. 183.
[^280]: «El Derecho indígena es una praxis nacida del consenso social,
  modificándose en la propia praxis. Son esos principios los que guían
  la moral comunitaria, tabúes y mitos, que cercenan y redirigen la
  convivencia social al plano del equilibrio, y cuando entonces ocurre un
  desequilibrio, esos principios se dirigen a sanar la ruptura
  producida». El artículo merece ser examinado: Machado; Ortiz, Direito
  e cosmologia Guarani: um diálogo impreterível, <cite>Revista de
  Direito: trabalho, sociedade e cidadania</cite>.

En segundo lugar, hay que resaltar que, en el sistema cultural de las
sociedades originarias, la noción de colectividad es todavía más
compleja de lo que parece. La antropóloga Marcela S. Coelho de Souza
(2016) dice que, para pueblos amerindios como el kĩsêdjê, de la región
próxima al parque del Xingu, norte del estado de Mato Grosso, en la
Amazonia brasileña, ni sujeto ni objeto, ni creador ni creación se
comportan de acuerdo a las expectativas occidentales contenidas en esas
acepciones. El colectivo aquí no implica solo a personas, sino también
objetos y las diferentes relaciones mutuas entre estos, lo que vuelve el
vocabulario propuesto por la noción de propiedad intelectual, basado en
la separación clara entre sujeto y objeto, creador y creación, muy pobre
para ser usado en esos casos.

No es fácil armonizar la lógica de derechos colectivos de un determinado
sujeto sobre su objeto de creación también porque, para muchos pueblos
amerindios, casi todo lo que define la cultura humana viene de fuera, o
es obtenido de fuerzas externas. En el caso del pueblo kĩsêdjê, por
ejemplo, el maíz viene del ratón; el fuego, del jaguar; nombres y
adornos corporales, de una raza de enanos caníbales; canciones, de las
abejas, buitres, árboles y tortugas acuáticas, entre otros casos que
permiten afirmar «si la cultura de los kĩsêdjê pertenece a los kĩsêdjê,
es justamente porque no son ellos los creadores»[^281]. Coelho de Souza
afirma que, cuando los derechos que implican bienes culturales y de
conocimiento están en juego, estos nunca surgen como derechos colectivos
que pueden ser inequivocamente atribuidos a personas o grupos, sino
antes a una vasta red de prerrogativas heterogéneas, derechos y
obligaciones que no encajan fácilmente en los moldes de la
representación legal requerida en las formas de un contrato
jurídico[^282].

[^281]: Coelho de Souza, <i lang="lt">op.&nbsp;cit.</i>, p. 183.
[^282]: Coelho de Souza, <i lang="lt">op.&nbsp;cit.</i>, p. 181, citando otro
  texto conocido de la antropología en Brasil: Carneiro da Cunha,
  «Cultura» e cultura: conhecimentos tradicionais e direitos
  intelectuais, en <cite>Cultura com aspas e outros ensaios</cite>, pp.
  331-373.

Creación y propiedad intelectual en el pensamiento occidental,
especialmente a partir de la Ilustración y de John Locke en el siglo
XVII, son nociones ligadas a la idea de que los objetos (historias,
narraciones) son hechos a partir del intelecto humano, fruto de nuestra
subjetividad y como algo que es una extensión de nuestra identidad.
Según esa idea, la creatividad se concretiza con la producción de un
determinado objeto, pero no emana de este, que permanece inerte y sin
vida &mdash;por lo menos en su concepción legal y tradicional&mdash;. La
propiedad intelectual, en este sentido, está anclada como una la relación
entre personas en relación con (y mediada por) cosas[^283], con una
clara separación entre lo que es materia y lo que es espíritu, sujeto y
objeto.

[^283]: Coelho de Souza, <i lang="lt">op.&nbsp;cit.</i>, p. 182. Traducción
  mía de «<i lang="en">Property is anchored as a relation among people
  with respect to (mediated by) things</i>».

La visión amerindia es bastante diferente. Trata, por ejemplo, los
objetos como registros «menos pasivos de las capacidades de un sujeto
que las cosificaciones personificadas de esas relaciones»[^284]. De modo
que la creación se produce *distribuida* en la relación entre los
múltiples objetos y personas, sin esa separación entre sujeto y objeto,
intelecto y materia, que estamos acostumbrados a hacer en Occidente. La
subjetividad también existe en los objetos y forma un paisaje animado
compuesto de diferentes tipos de niveles de acciones humanas[^285]. Para
los amerindios, cada objeto, así como cada animal, es potencialmente un
sujeto, lo que hace que nos remontemos al *perspectivismo amerindio*
propuesto por Eduardo Viveiros de Castro y Tânia Stolze Lima, concepto
ampliamente difundido en la antropología y que podemos definir como la
idea de que «seres provistos de alma se reconocen a sí mismos y a
aquellos a quienes están emparentados como humanos, pero son percibidos
por otros seres en forma de animales, espíritus o modalidades de no
humanos»[^286].

[^284]: <i lang="lt">Ibidem</i>, p. 182.
[^285]: Adaptación mía, en el original: «<i lang="en">In this
  distributed mode, recombinations are not the work of an intellect
  separated from matter; here, subjectivity exists distributed in
  objects, forming “an animated landscape composed of different kinds
  of bodies in which change and effect are events with meaning on the
  same level as human actions” (Leach, 2004, p. 169). […] “People” and
  “things” appear then as indexes of capacities and powers the
  apprehension of which becomes the focus of the explicit practice of
  subjects</i>» (Coelho de Souza, <i lang="lt">op.&nbsp;cit.</i>, p. 183).
[^286]: Esa definición aquí viene de la ayuda de Macial, «Perspectivismo
  amerindio», en <cite>Enciclopédia de antropologia</cite>. Disponible
  en <https://ea.fflch.usp.br/conceito/perspectivismo-amer%C3%ADndio>.
  El concepto se detalla en, entre otras obras, <cite>Um peixe olhou
  para mim: o povo Yudjá e a perspectiva</cite>, de Tânia Stolze Lima; y
  <cite>A inconstância da alma selvagem e outros ensaios de antropologia
  e Metafísicas canibais: elementos para uma antropologia
  pós-estrutural</cite>, de Eduardo Viveiros Castro.

Aunque esta concepción sea específica del perspectivismo amerindio,
también puede ser abordada desde una noción más amplia, compartida por
otros pueblos originarios de América Latina y de otros lugares del
mundo, de no separación entre naturaleza y sociedad. Central para lo que
se suele llamar modernidad, esa separación fue cuestionada en la
antropología hace muchas décadas[^287], con todavía más fuerza a partir
de los fenómenos ligados al calentamiento global, a partir de los años
90, en los que decisiones políticas tomadas por gobiernos, personas y
empresas («sociedad») han provocado alteraciones irreversibles en el
clima del planeta («naturaleza»). Para los pueblos originarios, como los
kĩsêdjê y los guaraní aquí citados, esa división nunca ha existido; la
misma concepción de mundo que no separa sujeto y objeto y que los
incluye en lo que se llama colectivo tampoco ve diferencia entre
naturaleza y sociedad.

[^287]: Entre otros autores, Bruno Latour, en <cite>Jamais fomos
  modernos</cite>, discute la idea de que la modernidad comienza con la
  escisión entre naturaleza y sociedad en el mundo occidental, lo que
  abrió camino para la destrucción de la naturaleza. «La naturaleza y la
  sociedad no son dos polos opuestos, sino más bien una misma producción
  de sociedades-naturalezas, de colectivos». (p. 138).

No es por casualidad, por tanto, que la influencia indígena en países
latinoamericanos haya provocado el debate en torno a los derechos de la
naturaleza[^288], una perspectiva que cuestiona esa separación e intenta
incluir árboles, ríos, montañas y bosques como seres de derecho dentro
de un sistema legal vigente occidental. La Constitución de Montecristi
de la República de Ecuador, por ejemplo, promulgada en 2008, afirma en
el artículo 71 del capítulo VII:

> La naturaleza o Pacha Mama, donde se reproduce y realiza la vida,
> tiene derecho a que se respete integralmente su existencia y el
> mantenimiento y regeneración de sus ciclos vitales, estructura,
> funciones y procesos evolutivos.

> Toda persona, comunidad, pueblo o nacionalidad podrá exigir a la
> autoridad pública el cumplimiento de los derechos de la naturaleza.
> [...]

> El Estado incentivará a las personas naturales y jurídicas, y a los
> colectivos, para que protejan la naturaleza, y promoverá el respeto a
> todos los elementos que forman un ecosistema.[^289]

[^288]: O «derechos no humanos». Véase Gudynas, <cite>Direitos da
  natureza: ética biocêntrica e políticas ambientais</cite>.
[^289]: La Asamblea Constituyente fue presidida por Alberto Acosta, e
  incluyó además 99 artículos que abordan expresamente la cuestión. Ese
  trabajo forma parte de un movimiento de diferenciación latinoamericano
  de la tradicional teoría constitucional europea, llamado en ese ámbito
  «neoconstitucionalismo latinoamericano», que incluye también las
  constituciones de Colombia (1994) y de Venezuela (1999). Para más
  detalles véase: Shiraishi Neto; Tapajós Araújo, «“Buen vivir”: notas
  de un concepto constitucional en disputa», <cite>Pensar</cite>, pp.
  379-403, mayo-ago. 2015. Disponible en
  <https://periodicos.unifor.br/rpen/article/viewFile/2886/pdf>

La inclusión (o intento) de seres vivos no humanos en legislaciones y
constituciones, como en el caso de Ecuador, refleja también la idea del
buen vivir, una noción común hace mucho tiempo entre pueblos originarios
de América Latina[^290] y que ha sido retomada en las últimas décadas
como una crítica al desarrollismo y a la necesidad de crecimiento y
acumulación de riquezas a costa tanto de la naturaleza como de muchas de
las poblaciones que de ella viven directamente. Para el buen vivir
tampoco debe haber separación entre naturaleza y sociedad, lo que, a su
vez, no debe ser confundido con una «vuelta al pasado», sino que es la
búsqueda, en las raíces ancestrales de los pueblos originarios, de una
convivencia más armoniosa entre hombre y naturaleza que nos de salidas,
particulares a cada comunidad, a la crisis ambiental y también social
que hoy vivimos.

[^290]: El término remite a idiomas originarios: <i lang="qu">sumak
  kawsay</i> en quechua; <i lang="ay">suma qamaña</i> en aimara, además
  de aparecer también como <i lang="gn">handereko e teko porã</i> en
  guaraní. Hay nociones similares aún entre los pueblos mapuche en
  Chile, los gunas en Panamá, los shuar y los achuar de la Amazonia
  ecuatoriana, así como en las tradiciones mayas de Guatemala y de
  Chiapas en Méjico. Véase Acosta, <cite>O bem viver: uma oportunidade
  para imaginar outros mundos</cite>.

En pueblos en los que no hay separación entre naturaleza y cultura,
sujeto y objeto, y en que el colectivo, en toda la complejidad que ese
término puede tener para esos pueblos, es prioritario en relación a lo
individual, es más difícil hablar de nociones como propiedad
intelectual, derechos de autor y cultura libre tal como comentamos hasta
este capítulo[^291]. Las ideas de copia, plagio, apropiación y remezcla
fueron presentadas dentro de una concepción *propietaria* del mundo,
vigente en Occidente desde el principio del capitalismo, pero que para
esos pueblos citados no es lo normal. Aún así, no hay manera de negar
que el sistema capitalista trata, con frecuencia, de apropiarse de los
conocimientos y de los bienes culturales de esos pueblos originarios
para extraer de ellos valor y después venderlos como mercancía
&mdash;como es el caso desde productos del bosque usados como remedios
ancestrales a estándares de diseños reconocidos como
<i lang="en">design</i>&mdash;. ¿Cómo crear entonces mecanismos que fomenten el
pensamiento colectivo y comunitario inspirado en esos pueblos, respeten
su cosmovisión y, al mismo tiempo, eviten que su cultura genere
mercancías que sean puestas a la venta en un mercado donde la mayor
parte del valor obtenido no irá para ellos?

[^291]: Además de Brightman; Fausto; Grotti (orgs.), <cite>Ownership and
  Nurture: Studies in Native Amazonian Property Relations</cite>, de
  2016, colección que tiene el artículo de Marcela Coelho de Souza aquí
  citado, hay por lo menos dos obras importantes para quien quiere
  profundizar más en el tema: Strathern, <cite>Property, Substance, and
  Effect: Anthropological Essays on Persons and Things</cite>, y Hirsch;
  Strathern, <cite>Transactions and Creations: Property Debates and the
  Stimulus of Melanesia</cite>. Agradezco a Eduardo Viveiros de Castro
  estas tres recomendaciones.

Hay dificultad de dar una respuesta única a esta cuestión. Si, como ya
vimos, un <i lang="en">software</i> tiene condiciones de producción
diferentes a las de una canción o de un diseño, también los contratos de esos
bienes deben ser diferentes, de acuerdo con los contextos y actores
involucrados. La protección de un dominio público amplio no es
excluyente de la remuneración de quien (re)crea, como demuestran las
licencias de Arte Libre, Creative Commons y Copyfarleft citadas en el
capítulo anterior. La equiparación de objetos y personas y el
establecimiento de una relación duradera de regalos que no termina sin
guerra pueden ser contemplados por negociaciones más complejas, que
inventen otros términos, y no los comunmente usados en el ámbito de la
propiedad intelectual. Algunas de las palabras de ese nuevo vocabulario
no tienen nada de nuevo; el <i lang="en">copyleft</i> de los ochenta y
el milenario bien común procedente del <i lang="lt">res commune</i>, por
ejemplo, pueden ser usados y recreados a partir de las perspectivas
amerindias citadas para establecer nuevos y proteger viejos bienes
comunes mediante prácticas cotidianas de cuidado y resistencia
&mdash;incluso en el aspecto legal, esa conexión de mediación que muchas
veces es necesaria para la garantía de un buen vivir para todos&mdash;.

### IV.

La producción de bienes culturales libres al margen de la motivación
individualista y propietaria de la cultura predominante en Occidente
requiere resistencia y creatividad. Resistir es el procedimiento
necesario para la preservación de una amplia base de datos colectiva de
creación, al tiempo que crear es necesidad básica para reinventar
conceptos y prácticas para construir vías alternativas de producción,
difusión y remuneración de la cultura menos restrictivas y más
autónomas. En este sentido, hay propuestas diferentes. Para Creative
Commons, reformar las leyes de derecho de autor concediendo el derecho
de elección de las libertades de uso, difusión y producción para los
identificados como autores es un camino para la construcción de un
dominio público robusto y accesible. Para el <i lang="en">copyleft</i>
propuesto por Stallman, liberar programas y algunos bienes culturales es
una salida para luchar contra monopolios que arrebatan la libertad de
creación y de elección autónoma de los usos de una determinada obra.

Otras personas, como la activista del conocimiento libre Evelin Heidel
(Scann), dicen que el feminismo debería oponerse al carácter patriarcal
del derecho de autor. Propone, así pues, modificar la legislación no de
manera punitiva para dar más protección a creaciones de mujeres que
quedarían fuera de esas legislaciones, sino para generar un paradigma
que de valor a la creación como práctica social y comunitaria.

«Intentar cambiar las leyes que hoy criminalizan o prohíben prácticas
fundamentales para la libertad de expresión, para el intercambio, la
distribución y la reapropiación de la cultura», de manera que se callen
las musas que inspiran a los genios para que se pueda, finalmente, hablar
de las mujeres[^292].

[^292]: Heidel (Scann), <i lang="lt">op.&nbsp;cit.</i>. Disponible en
  <https://www.genderit.org/es/feminist-talk/columna-que-se-callen-las-musas-por-que-el-feminismo-debe-oponerse-al-copyright>.

Para algunos también aquí citados, como Anna Nimus, abolir el
<i lang="en">copyright</i> también puede ser la salida. Joost Smiers y
Marieke van Schijndel imaginaron un mundo sin <i lang="en">copyright</i>
que tiene como idea principal el hecho de que la protección ofrecida por
los derechos de autor no es necesaria para el proceso de expansión de la
creación artística. Ellos citan varios argumentos que hacen que sea
ilógico apostar por el derecho de autor como modelo de regulación de
producción cultural: el hecho de ser un derecho exclusivo y monopolista
de una obra privatiza una parte esencial de nuestra comunicación y
perjudica a la democracia, por ejemplo[^293]; la cuestión de que si este
es realmente un incentivo para el creador, motivo alegado desde el
principio del derecho de autor, pero que algunos estudios económicos
citados demuestran que, de los ingresos obtenidos de copias vendidas,
un 10&nbsp;% va para el 90&nbsp;% de los artistas, y 90&nbsp;% va para
el 10&nbsp;%[^294]; la falsa idea de originalidad como expresión
individual y exclusiva de algún creador; el fracaso de la lucha contra
la llamada piratería de archivos digitales de obras culturales en la
red. Como solución, Smiers y Van Schijndel señalan a algo cercano al
procomún: «Creemos que es posible crear mercados culturales de forma que
la propiedad de los recursos de producción y distribución estén en manos
de mucha gente. En esas condiciones, nosotros pensamos, nadie podrá
controlar el contenido o la utilización de las formas de expresión
cultural a través de la posesión exclusiva y monopolista de derechos de
propiedad»[^295].

[^293]: Smiers; Van Schijndel, <cite>Imagine um mundo sem direitos do
  autor nem monopólios</cite>, p. 10.
[^294]: <i lang="lt">Ibidem</i>, p. 13.
[^295]: <i lang="lt">Ibidem</i>, p. 6.

En el Encontro de Cultura Livre do Sul, realizado por colectivos
culturales de Iberoamérica los días 21, 22 y 23 de noviembre de
2018[^296], una serie de activistas e investigadores y yo discutimos y
buscamos respuestas a algunas de las cuestiones abordadas en este libro.
Durante las seis mesas de debate del encuentro hablamos sobre políticas
públicas y marcos legales de derechos de autor; digitalización de
acervos y acceso al patrimonio cultural en repositorios libres; de
laboratorios, productoras colaborativas, <i lang="en">hackerspaces</i>,
<i lang="en">hacklabs</i> y otras formas de organización que defienden y
práctican en el día a día la cultura libre; de cómo nos integramos en
una red internacional que también defienda los bienes comunes; de muchas
formas de producción cultural &mdash;editorial, musical, audiovisual,
fotográfica&mdash; que se están realizando en el ámbito de las licencias
y de la cultura libre; y de las plataformas, contenidos y prácticas
educativas que tienen lo libre como paradigma de acción y propagación.

[^296]: Todos los debates pueden verse aquí:
  <http://baixacultura.org/encontro-de-cultura-livre-do-sul-todos-os-videos-y-relatos>

Junto a los más de doscientos participantes, reflexionamos sobre las
especificidades de la cultura libre en el sur global en relación al
norte. Como uno de los resultados, escribimos el Manifiesto de Cultura
Libre del Sur Global[^297], que propone algunos principios, conceptuales
y prácticos, que consideramos importantes para la propagación y el
cuidado de una cultura libre en este sur que no es solamente geográfico.
El fragmento del manifiesto a continuación fue un desenlace para el
encuentro &mdash;y también encaja que esté aquí, adaptado, no para
encerrar, sino para mantener activa la larga y continua discusión sobre
la cultura libre a través de los tiempos&mdash;.

[^297]: Disponible en <https://www.articaonline.com/2018/12/cultura-libre-del-sur-global-un-manifiesto/>.

> La discusión sobre la libertad de utilización y la producción de
> tecnologías libres ha sido fundamental para la cultura libre desde el
> principio, pero creemos que en el sur tenemos la urgencia más grande de
> preguntarnos para qué y para quién sirven nuestras tecnologías libres.
> No basta discutir si vamos a usar herramientas producidas en software
> libre o si vamos a optar por licencias libres en nuestras producciones
> culturales: necesitamos pensar en tecnologías, herramientas y procesos
> libres que sean usados para dar espacio, autonomía y respeto a los
> menos favorecidos, económica y tecnológicamente, de nuestros
> continentes, y para disminuir las desigualdades sociales en nuestras
> regiones, desigualdades que son aún más visibles en el contexto del
> ascenso fascista global que vivimos en el 2018.

> Desde el sur, tenemos que pensar en la cultura libre como un movimiento
> y una práctica cultural que propone dialogar intensamente con las
> culturas populares de nuestros continentes; que respeta y conversa con
> los pueblos originarios de América, que están aquí en nuestro continente
> viviendo en una cultura libre mucho antes de la llegada de los
> «latinos»; que defiende el feminismo y los derechos iguales para todes,
> sin distinción de género, raza, color u orientación sexual, identidad y
> expresión de género, discapacidad, apariencia física, tamaño corporal,
> edad o religión; que propone diálogo con la creatividad recombinante de
> las periferias de nuestros continentes, que están acostumbradas a
> compartir en sus comunidades y que son un blanco principal del
> exterminio practicado por nuestras policías regionales; que busca
> resguardar nuestra privacidad a partir de tácticas antivigilancia y de
> la defensa del derecho al anonimato y a la criptografía; y que lucha por
> la propagación de las fisuras en el sistema capitalista, buscando, a
> partir de una práctica cultural y tecnológica <i lang="en">anticopyright</i>, formas
> alternativas y solidarias de vivir en armonía con la Pachamama sin
> agotar los recursos ya escasos de nuestro planeta.
>
> Pensar y hacer la cultura libre desde el sur requiere pensar en la
> urgencia de las necesidades de supervivencia de nuestros pueblos. Es
> necesario acercarnos a la discusión sobre el procomún, concepto clave
> que nos une en la lucha contra la privatización de los recursos
> naturales, como los océanos y el aire, pero también contra la
> privatización del <i lang="en">software</i> libre y de los protocolos abiertos y
> gratuitos bajo los cuales se organiza Internet. Acercándonos al procomún
> se amplía nuestro campo de disputa en el sur global y nos acercamos al
> cotidiano de comunidades, centrales y periféricas, que luchan en el día
> a día por la preservación de los bienes comunes.
>
> Es importante recordar que el concepto “procomún” al que buscamos
> acercarnos debe ser pensado como algo en proceso, como un hacer común
> («commoning» en inglés). Es decir, no tener en vista solamente el
> producto en sí &mdash;libros, videos, música, hardware o software
> libres&mdash;, sino también nuestras propias prácticas y dinámicas a
> través de las cuales creamos juntos nuevas formas de vivir, convivir y
> también producir. Este es el hacer común. Por eso, es tan importante
> mantener vivas las redes que han recorrido todas las palabras,
> enlaces, referencias y personas citadas, debatidas, registradas e
> involucradas en las páginas de este libro que aquí continúa.


<div class="right-align">
Internet, Iberoamérica,

~~sur global, 23 de noviembre de 2018~~

remezclado en inverno de 2020
</div>

