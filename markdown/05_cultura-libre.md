<section class="citas">
<i>Los vendedores de</i> <span lang="en">software</span> <i>quieren dividir a los usuarios y
dominarlos para llevarlos a aceptar no compartir su software con los
demás. Me rehúso a romper la solidaridad con otros usuarios de esta
manera. Mi conciencia me impide firmar un acuerdo de confidencialidad o
un acuerdo de licencia de un programa. Durante años he trabajado en el
Laboratorio de Inteligencia Artificial oponiéndome a estas tendencias y
otras descortesías, pero al final fueron demasiado lejos: no podía
permanecer en una institución donde tales cosas se hicieran en mi nombre
en contra de mi voluntad. Para poder seguir utilizando los ordenadores
sin deshonra, he decidido agrupar un conjunto suficiente de programas
libres para poder vivir sin usar ningún programa que no sea libre.</i>

<div class="right-align">
Richard Stallman, <cite>El manifiesto de GNU</cite>, 1985
</div>

<br>

<i>Por un lado, estos artesanos de tecnología punta no solo suelen estar
bien pagados, sino que también tienen una considerable autonomía para
determinar su ritmo y su lugar de trabajo. En consecuencia, la división
cultural entre el jipi y el «hombre de organización» se ha vuelto
bastante borrosa. Pero, por otro lado, estos trabajadores están
vinculados por los términos de sus contratos y no tienen garantía alguna
de un trabajo continuado. Al carecer del tiempo libre de los jipis, el
trabajo se ha convertido en la principal vía para la autorrealización
para buena parte de la «clase virtual».</i>

<div class="right-align">
Richard Barbrook; Andy Cameron, <cite>La ideología californiana</cite>,
1995
</div>

<br>

<i>Gobiernos del Mundo Industrial, vosotros, cansados gigantes de carne
y acero, vengo del Ciberespacio, el nuevo hogar de la Mente. En nombre
del futuro, os pido en el pasado que nos dejéis en paz. No sois
bienvenidos entre nosotros. No ejercéis ninguna soberanía sobre el lugar
donde nos reunimos. Estamos creando nuestro propio Contrato Social. Esta
autoridad se creará según las condiciones de nuestro mundo, no del
vuestro. Nuestro mundo es diferente. Vuestros conceptos legales sobre
propiedad, expresión, identidad, movimiento y contexto no se aplican a
nosotros. Se basan en la materia. Aquí no hay materia.</i>

<div class="right-align">
John Perry Barlow, <cite>Declaración de independencia del
ciberespacio</cite>, 1996
</div>

<br>

<i>Cuando te saltas a los intermediarios, puede ser así de fácil.</i>

<div class="right-align">
Creative Commons, <cite>Sé Creativo</cite>, 2000
</div>
<br>

<i>Cuando descargas archivos MP3, también estás descargando el
comunismo.</i>

<div class="right-align">
Record Industry Association of America, <cite>Campaña antipiratería</cite>,
años 2000
</div>


<br>

<i>El código abierto y el</i>
<span lang="en" style="font-style: normal;">copyleft</span> <i>se
extienden actualmente mucho más allá de la programación del</i>
<span lang="en">software</span><i>: las «licencias abiertas» están en
todas partes y en tendencia pueden convertirse en el paradigma de un
nuevo modo de producción que libere finalmente la cooperación social (ya
existente y visiblemente desplegada) del control parasitario, la
expropiación y la «renta» a favor de grandes potentados industriales y
corporativos.</i>

<div class="right-align">
Wu Ming, <cite>Copyright y maremoto</cite>, 2002
</div>

<br>

<i>La idea es que el</i>
<span lang="en">copyright</span> <i>significa «<span lang="en">all
rights reserved</span>» y el Creative Commons significa «<span lang="en">some rights reserved</span>».
Y tú dices cuáles son estos. Existen varias fórmulas, varios tipos de
licencias abiertas. Se trata de intentar crear un modo de convivencia en
el ámbito de la información que sea tolerable, y que evite lo que está
sucediendo, que es el control de la información por las grandes
empresas. Ahora todo eso es, en cierta forma, un paliativo. El Creative
Commons puede ser visto, como lo es efectivamente por los más,
digamos, radicales, como una estrategia capitalista. El verdadero
anarquista no quiere saber nada de Creative Commons ni de</i>
<span lang="en">copyleft</span><i>, es totalmente
radical. En principio estoy con ellos, considero la propiedad privada
una monstruosidad, sea esta intelectual o no, pero sé también que
tampoco se avanza dándose contra una pared, tapando el sol con las
manos. Pienso que tienes que transigir, tienes que hacer algún tipo de
negociación.</i>

<div class="right-align">
Eduardo Viveiros de Castro, Economia da cultura digital, en Savazoni;
Cohn, <cite>Cultura digital.br</cite>, 2009
</div>
</section>

### I.

«¡La impresora está atascada de nuevo!»

<br>

Richard M. Stallman, programador de <i lang="en">software</i> en el
laboratorio de Inteligencia Artificial del Massachusetts Institute of
Technology (MIT), en la Costa Este de Estados Unidos, cuenta que
descubrió el problema una hora después de enviar desde su ordenador un
archivo de cincuenta página para imprimir y se dio cuenta de que la
máquina había puesto tinta en cuatro páginas de otro trabajo, y no del
suyo. No era realmente una novedad, ya que había vivido otra situación
parecida, en la cual el recién graduado físico de Harvard había usado
sus habilidades de programación para evitarlo, creando una pequeña
modificación en el código del programa de la impresora que permitía
avisar, a distancia, cuando esta estaba atascada, mediante la frase
«<i lang="en">The printer is jammed, please fix it</i>»[^135]. Pero esa
vez la impresora era nueva, uno de los últimos lanzamientos de Xerox
(modelo 9700), que imprimía trescientos puntos por pulgada en hojas
sueltas de papel a una velocidad de hasta dos páginas por segundo (pps),
por uno o dos lados, formato paisaje o retrato. Fue donada al
laboratorio de modo de prueba, hábito de la empresa y de otras basadas
en aparatos tecnológicos con lugares donde se reunían normalmente jáqueres
&mdash;si ellos conseguían <i lang="en">hackear</i>, muchas veces eran
llamados para trabajar en esas empresas&mdash;. En los años 60 y 70, el
MIT era uno de los primeros lugares donde esa comunidad de programadores
de <i lang="en">software</i> y <i lang="en">hardware</i> defensores de
la idea de que «toda la información debe ser libre», que remite a Thomas
Jefferson, al marqués de Condorcet y al nacimiento del liberalismo, se
reunían para inventar y compartir códigos para los cada vez más potentes
y más pequeños ordenadores que había en los centros de investigación.
Entre una <i lang="it">pizza</i> de madrugada y un interés friki y
de aficionado que ahondaba en cualquier asunto aparentemente banal, como
el formato de una zanahoria, o algo complejo, como formas de llamadas
telefónicas de broma, intentaban crear soluciones creativas para
problemas complejos. En resumen, <i>hackeaban</i>.

[^135]: En una traducción libre, «La impresora está atascada, por favor,
  arréglala». Esta historia se cuenta aquí a partir del capítulo 1, «<span lang="en">For
  Want a Printer</span>», de <cite>Free as in Freedom: Richard Stallman and the
  Free Software Revolution</cite>, biografía de Richard Stallman escrita
  por Sam Williams.

Cuando Stallman vio el problema con la nueva impresora de Xerox, pensó
en aplicar la corrección antigua y <i>hackearla</i> nuevamente. Sin
embargo, al buscar el <i lang="en">software</i> de la máquina de Xerox
que le permitiría corregir o modificar la impresora, descubrió que la
empresa no había enviado, como era costumbre hasta entonces, un programa
de cortesía para que los programadores pudieran leer el código, sino
solo un archivo casi infinito de ceros y unos llamado *binario*. Él
podría convertir los ceros y unos en instrucciones para máquinas de bajo
nivel con programas llamados *desensambladores* y, entonces, intentar
que se ejecuten en las *tripas* de la impresora, pero sería una tarea
lenta y difícil, que podría causar años de impresiones congestionadas y
molestias diversas.

Lo que Stallman hizo entonces fue ir a por el programa. Descubrió que
otro programador, en la Universidad Carnegie Mellon, también en la Costa
Este de Estados Unidos, tenía el <i lang="en">software</i>. Al visitarlo
con el credencial de «investigador del MIT», conversó de modo cordial
también con otros ingenieros involucrados en la producción de Xerox e
hizo una petición de acceso al código del <i lang="en">software</i> de
la impresora. Fue entonces informado de que el código era una novedad
considerada de vanguardia; por tanto, debía permanecer secreto y no ser
compartido. Stallman salió de la universidad sin decir nada, con rabia y
sin la copia, con la sensación de que lo que era antes libre y
compartible se estaba, al final de la década de 1970, volviendo
confidencial. No por alguna censura legal del gobierno, sino por
intereses de mercado; hasta entonces no existía el acuerdo de
confidencialidad (en inglés, <i lang="en">nondisclosure agreement</i>,
NDA) en la industria del <i lang="en">software</i>, lo que hacía que
todo el <i lang="en">software</i> fuera libre, con su código fuente
disponible para cualquiera que lo quisiera leer y modificar.

Un programa de ordenador &mdash;o de una impresora&mdash; funciona como
un conjunto de instrucciones para que la máquina ejecute funciones. Está
escrito en un lenguaje que esos inventos técnicos sabían leer y
procesar; cuanto más en las tripas, de más *bajo nivel* es el lenguaje;
cuanto más próximo a la interfaz con el humano, de más *alto nivel*. Al
conjunto finito de procedimientos que son ejecutados por una máquina se
le llama *algoritmo*, una palabra árabe (لخوارزمية) latinizada en el
contexto de las matemáticas en el siglo VII, pero cuya primera
utilización destinada a un ordenador fue por la condesa Ada
Lovelace[^136] para la máquina analítica de Charles Babbage &mdash;un
gigantesco aparato básicamente para resolver logaritmos y funciones
trigonométricas&mdash; a final del siglo XIX. Como todo lo que se basa
en instrucciones, las presentes en un algoritmo funcionan mediante la
circulación de información, en este caso entre máquina y humanos
mediados por el lenguaje; no tener acceso al código que controla la
circulación de información entre estas partes es no saber lo que está
siendo intercambiado; por tanto, tampoco saber cómo se está ejecutando
un procedimiento, no tener posibilidad de modificarlo, sea para arreglar
un error o proponer una mejora, y, finalmente, no poder pasárselo a
otros &mdash;que, sin tener la llave para abrir la *caja negra* del
algoritmo, poco pueden hacer con él&mdash;.

[^136]: La inglesa Augusta Ada King, condesa de Lovelace (1815-1852),
  hija del poeta conocido como Lord Byron (con quien convivió poco hasta
  sus ocho años, cuando Byron murió), fue la primera en darse cuenta de
  que la máquina analítica de Babbage poseía aplicaciones más allá del
  cálculo y, entonces, público el primer algoritmo, en 1843, destinado a
  ser ejecutado por esa máquina. Como resultado, ella es considerada una
  de las primeras programadoras. Es, asimismo, una de las raras mujeres
  en la historia de las tecnologías cuya historia fue contada,
  entre muchas otras que, habiendo desempeñado papeles importantes,
  fueron borradas de las narrativas que hoy son las más adoptadas en la
  documentación de la historia de la tecnología. Sobre Ada Lovelace,
  véase <https://en.wikipedia.org/wiki/Ada_Lovelace>.

De la misma forma que un bien cultural, un programa tiene en su origen el intercambio
de información y la recombinación de ideas. Cuando Stallman, al final de
la década de 1970, se da cuenta de que la información de un programa
empieza a cerrarse por motivos de confidencialidad, y a ser solo
accesible mediante un pago, surge un movimiento en algunos aspectos
semejante a lo que ocurrió con la consolidación del
<i lang="en">copyright</i> y el derecho de autor en la Europa del siglo
XVIII: el cierre privado de lo que antes era común y de libre acceso.
Como empieza a tener cada vez más valor en su circulación en el mercado
capitalista, el <i lang="en">software</i> pasa a tener un propietario;
su código, ahora cerrado, es la clave del valor del producto, el secreto
más bien guardado que determina su exclusividad.

Al contrario, sin embargo, que un bien cultural, un programa es un
conjunto de instrucciones para una máquina. ¿Cómo nos vamos a comunicar
con una máquina si no conocemos su código y su idioma? No vamos a
hacerlo. O mejor dicho, quien va a tener la exclusividad de comunicarse será
quien posea la propiedad del código del programa. Un problema de
comunicación se resuelve con la garantía del privilegio del emisor: solo
quien produjo, a partir de información común, tiene ese derecho. El
caso de la Xerox 9700 empuja a Stallman a preguntarse: ¿pero el derecho
de acceso, uso y reutilización de la información necesaria para que un
aparato técnico funcione no es también importante? Para él, negarse a
ofrecer el código fuente del <i lang="en">software</i> no era solo la
discontinuidad de una regla establecida a partir del final de la Segunda
Guerra Mundial, cuando, después de Alan Turing y otros, los programas
comenzaron a ser importantes, sino también una violación de la Regla de Oro, la
máxima moral que decía «actúa con los demás como te gustaría que
actuaran contigo»[^137].

[^137]: Williams, <cite>Free as in Freedom</cite>, p. 11.

De su insatisfacción personal y deseo de tratar de mantener la
información abierta y libre, Stallman crea, a finales de la década de
1970, la idea de <i lang="en">software</i> libre como un programa que
daría libertad a su usuario, justo como en los primeros años de los
programas de ordenador, *(0) de ejecutar el programa, para
cualquier propósito; (1) de estudiar el programa y adaptarlo a sus
necesidades; (2) de redistribuir copias del programa; (3) de modificar
(perfeccionar) el programa y distribuir esas modificaciones*[^138]. De
manera silenciosa pero esencial, el <i lang="en">software</i> libre se
extendería con Internet y la popularización de los ordenadores en los
años 80 y 90, y se llevaría a otros terrenos, como la cultura, en la
cual encontraría un caldo de cultivo para expandirse. A partir del
<i lang="en">software</i> libre nace el <i lang="en">copyleft</i>, en
los años 80, que después va a hacer que la cultura libre se propague en
los primeros años del Internet comercial como una idea, un movimiento de
personas y una práctica aliada con el intercambio de todo tipo de
archivos en Internet (o descarga), la libre recombinación de ideas para
crear bienes culturales y un desafío a los cambios en la legislación del
derecho de autor a partir de las transformaciones producidas por
Internet.

[^138]: En su primera definición, en inglés, en el sitio web de la Free
  Software Foundation, disponible en
  <https://www.gnu.org/philosophy/free-sw.html>.

El avance del <i lang="en">streaming</i> y la popularización de las
redes sociales en Internet, ya a finales de los años 2000, vuelven las
diferencias significativas que caracterizan a un programa y un bien
cultural, como un libro o una canción, más visibles que en los primeros
años de Internet. Detrás de la tecnología y del libre intercambio
está la energía &mdash;la energía viva[^139] de un trabajo inmaterial a
la que en muchos casos la cultura libre de Internet no prestó
atención&mdash;. «El abuso precede al uso», dice el francés Michel
Serres[^140], una buena frase para explicar el sentimiento de *resaca*
que el Internet pos-2016 trajo a todos los que nos embriagamos con la
«liberación del polo emisor de la información» de los primeros años de
la red y no conseguimos considerar alternativas económicas y políticas
a la construcción de una red que, en los años finales de la década de
2010, ayudó a difundir una venganza fascista formada por iniciativas
políticas de colonización de la red y propagación de odio presente en
varios rincones del planeta.

[^139]: Pasquinelli, A ideologia da cultura livre e a gramática da
  sabotagem, en Belisário; Tarim (orgs.). <cite>Copyflight</cite>, p.
  52.
[^140]: <i lang="lt">Ibidem</i>.

### II.

El 27 de septiembre de 1983, Stallman envió un correo electrónico por la
entonces Arpanet, red precursora de Internet, que unía principalmente
centros de investigación en universidades de Estados Unidos:

> A partir del próximo Día de Acción de Gracias comenzaré a
> escribir un sistema de <i lang="en">software</i> completo compatible con
> Unix llamado GNU (que significa «Gnu No es Unix»), y lo distribuiré
> libremente para que todos puedan usarlo. [...] Considero que la regla de
> oro exige que si a mí me gusta un programa, debo compartirlo con otras
> personas a quienes también les gusta. Mi conciencia no me permite firmar
> un acuerdo de confidencialidad o un acuerdo de licencia de
> <i lang="en">software</i>. Para poder seguir utilizando ordenadores
> sin violar mis principios, he decidido reunir suficiente
> <i lang="en">software</i> libre para no tener que usar ningún programa
> que no sea libre.[^141]

[^141]: Fragmento de <cite>Initial Announcement</cite>. La historia del
Proyecto GNU difiere de este plan inicial &mdash;el inicio, por ejemplo,
fue pospuesto hasta enero de 1984. Muchos de los conceptos filosóficos
de <i lang="en">software</i> libre no fueron detallados hasta algunos
años después, como afirma el texto que contextualiza el manifiesto,
disponible, así como el manifiesto, también en español en el sitio web
oficial del proyecto, disponible en
<https://www.gnu.org/gnu/initial-announcement.es.html>.

El correo terminaba con la firma que Stallman solía usar en Arpanet
(*RMS*) y el apartado postal para comunicación, en Cambridge. Sería el
paso inicial del Proyecto GNU, iniciativa que inaugura la idea de un
<i lang="en">software</i> que, en sentido opuesto a los cada vez más
cerrados programas publicados al inicio de los años 80, sería libre para
diferentes tipos de uso y modificación, con su código disponible para
que cualquiera pudiera acceder. Era un proyecto en que el programador
estaba trabajando desde hace algunos años, inspirado por una ética
jáquer que lo había influido en el MIT, basada en el acceso y en la
compartición total de información y en la colaboración en vez de la
competición; y que tenía por principios: 1) el acceso a ordenadores
&mdash;y a cualquier otro medio que sea capaz de enseñar algo sobre cómo
funciona el mundo&mdash; debe ser ilimitado y total; 2) toda la
información debe ser libre; 3) no confíes en la autoridad y promueve la
descentralización; 4) los jáqueres deben ser juzgados según su
<i lang="en">hacking</i>, y no según criterios sujetos a sesgos tales
como títulos académicos, raza, color, religión, posición o edad; 5)
puedes crear arte y belleza en el ordenador; 6) los ordenadores pueden
cambiar tu vida a mejor[^142].

[^142]: Fuente: <https://pt.wikipedia.org/wiki/%C3%89tica_hacker>.
  Existen muchas definiciones de «jáquer»; una de las más precisas es la
  de Gabriella Coleman en <cite>Coding Freedom: The Ethics and
  Aesthetics of Hacking</cite>, libro producido a partir de una
  etnografía en comunidades de jáqueres, que así los presenta:
  «Aficionados al ordenador movidos por una pasión curiosa por
  cacharrear y aprender de sistemas técnicos, y frecuentemente
  comprometidos con una versión ética de la libertad de la información»
  (traducción libre basada en el original «<i lang="en">computer
  aficionados driven by an inquisitive passion for tinkering and
  learning technical systems, and frequently committed to an ethical
  version of information freedom</i>»). Para un enfoque de la ética
  jáquer en contraposición a la ética protestante, véase Himanen,
  <cite>La ética del hacker y el espíritu de la era de la
  información</cite>.

Presente como <i lang="lt">modus operandi</i> en las comunidades de
programadores de los años 60 y 70 en que Stallman se crió, esa ética
jáquer comenzaba, según él, a cambiar cuando muchos miembros se fueron a
empresas privadas de tecnología, que comenzaban a surgir a montones en el
final de la década de 1970 y el inicio de 1980 para comercializar
ordenadores personales, <i lang="en">software</i> y
<i lang="en">hardware</i> diversos.

La desbandada de la comunidad jáquer en el laboratorio donde Stallman
trabajaba[^143] representaba bien ese movimiento: al inicio de 1980,
buena parte de los integrantes del AI Lab (Laboratorio de Inteligencia
Artificial) fueron contratados por la empresa Symbolics, creada por Russ
Noftsker, un integrante del laboratorio que lideraba el grupo que estaba
abandonando algunos principios jáqueres, como dejar abierto y compartir
el código fuente, para comercializar sus productos. La disputa de
Noftsker era, especialmente, contra el grupo liderado por Richard
Greenblatt, también del MIT, que había creado en 1979 el proyecto LISP
Machine, una empresa que fabricaba ordenadores basados en el lenguaje de
inteligencia artificial LISP y que trataba de permanecer fiel al
espíritu jáquer, sin renunciar al código abierto. Greenblatt creía que
los recursos que venían de la construcción y venta de algunas máquinas
podrían ser reinvertidos en la financiación de la empresa, mientras que
Noftsker apostaba por un camino, tradicional en el capitalismo y
convertido en regla en el mundo de las <i lang="en">startups</i> de
tecnología a partir de entonces, de buscar inversores y apoyo en fondos
de inversión. La opción de Noftsker atrajo a más personas, resultando en
la creación de Symbolics y en la salida de muchos integrantes del AI
Lab, una historia que Steven Levy cuenta en su libro <cite>Hackers:
Heroes of the Computer Revolution</cite>, en el cual nombró a Stallman,
que en la disputa permaneció en el bando de Greenblatt, como «El último
de los verdaderos jáqueres», también título del capítulo que detalla el
caso.

[^143]: Stallman da más detallas sobre esos cambios en «The Project
  GNU», uno de los textos presentes en la colección publicada con el
  título de <cite>Free Software, Free Society</cite> en 2002, por la GNU
  Press. La citada aquí es la versión en español Stallman,
  <cite>Software libre para una sociedad libre</cite>, publicada por la
  editorial española Traficantes de Sueños en 2004.

La propuesta de Stallman para el Proyecto GNU era dar a los usuarios la
libertad que Unix, sistema operativo robusto y el más usado en la época,
creado en 1969, no daba. Para eso, aprovechó las posibilidades que Unix
todavía permitía en la época, como el acceso a su código fuente, y
empezó a crear su propio sistema operativo, que tendría que ser
compatible con el más usado (Unix) en la época, pero, a diferencia de
este, debería ser «100&nbsp;% <i lang="en">software</i> libre». No
95&nbsp;% libre, no 99,5&nbsp;%, sino 100&nbsp;% &mdash;«para que los
usuarios sean libres de redistribuir el sistema completo y libres de
alterar y contribuir a cualquier parte del mismo»[^144]&mdash;. De ahí
que el nombre sea un acrónimo que rinde homenaje a Unix, pero al mismo
tiempo se diferencia: <i lang="en">Gnu is not Unix</i>. En aquel
momento, Stallman ya había creado uno de sus trabajos más conocidos, un
programa editor de textos llamado Emacs (abreviatura de «edición de
macros»), que daba una muestra de lo que haría más tarde con el Proyecto
GNU y que «fue libremente compartido con quien aceptara una única
condición impuesta: todas las modificaciones y mejoras hechas por los
usuarios en el programa deberían ser también compartidas»[^145].

[^144]: Stallman, en un texto de celebración de los quince años de GNU.
  Disponible en
  <https://www.gnu.org/philosophy/15-years-of-free-software.html>.
[^145]: En Torres, <cite>A tecnoutopia do software livre: uma história
  do projeto técnico e político do GNU</cite>, p. 128.

A comienzos de 1984, meses después de anunciar la creación del Proyecto
GNU, ya sin el ambiente fértil y colaborativo en el que había convivido
durante muchos años, Stallman salió del MIT y pasó a dedicarse por
completo al desarrollo de su sistema operativo. Para él, salir del
instituto era imprescindible si quería que nada interfiriese en la
distribución de GNU como <i lang="en">software</i> libre: «El MIT podría
haberse apropiado de mi trabajo y haber impuesto sus propios términos de
distribución, o incluso convertir el trabajo en un paquete de
<i lang="en">software</i> propietario»[^146]. El mismo año, dio comienzo
al desarrollo de Emacs para GNU, el *GNU Macs*, el primer programa del
nuevo sistema operativo, al cual seguirían varios otros en los años
siguientes, como compiladores de código de diversos lenguajes de
programación (GCC), depuradores (*GNU Debugger*), entre otros.

[^146]: Stallman, <cite>Software libre para una sociedad libre</cite>,
  pp. 250-251.

En octubre de 1985, Stallman funda la Free Software Foundation (FSF),
fundación sin ánimo de lucro que hasta la fecha es responsable del
Proyecto GNU. En ese mismo año publica <cite>El manifiesto de
GNU</cite>, que presenta las ideas relacionadas con su proyecto y llama
a programadores a ayudarlo en el desarrollo del sistema. Con frases del
primer anuncio de dos años antes y con constantes modificaciones hasta
1987, es hasta la fecha un documento central en la filosofía del
<i lang="en">software</i> libre. Algunos fragmentos:

> Los vendedores de software quieren dividir a los usuarios y
> dominarlos para llevarlos a aceptar no compartir su software con los
> demás. Me rehúso a romper la solidaridad con otros usuarios de esta
> manera. Mi conciencia me impide firmar un acuerdo de confidencialidad o
> un acuerdo de licencia de un programa. Durante años he trabajado en el
> Laboratorio de Inteligencia Artificial del MIT oponiéndome a estas
> tendencias y otras descortesías, pero al final fueron demasiado lejos:
> no podía permanecer en una institución donde tales cosas se hicieran en
> mi nombre en contra de mi voluntad. Para poder seguir utilizando los
> ordenadores sin deshonra, he decidido agrupar un conjunto suficiente de
> programas libres para poder vivir sin usar ningún programa que no sea
> libre. [...]
>
> Muchos programadores están descontentos con la comercialización del
> software de sistema. Puede permitirles ganar más dinero, pero los hace
> sentirse en conflicto con otros programadores en lugar de sentirse como
> compañeros. El fundamento de la amistad entre programadores es el
> compartir programas, pero los acuerdos de mercadotecnia que los
> programadores suelen utilizar básicamente prohíben tratar a los demás
> como amigos. El comprador de software debe escoger entre la amistad y la
> obediencia a la ley. Naturalmente, muchos deciden que la amistad es más
> importante. Pero aquellos que creen en la ley a menudo no se sienten a
> gusto con ninguna de las opciones. Se vuelven cínicos y piensan que la
> programación es sólo una manera de ganar dinero. [...]
>
> Una vez que GNU esté terminado, todo el mundo podrá obtener un buen
> sistema de software tan libre como el aire.
>
> Esto significa mucho más que ahorrarse el dinero para pagar una
> licencia Unix. Significa evitar el derroche inútil de la duplicación de
> esfuerzos en la programación de sistemas. Este esfuerzo se puede
> invertir en cambio en el avance de la tecnología.
>
> El código fuente del sistema completo estará disponible para todos. Como
> resultado, un usuario que necesite cambios en el sistema siempre será
> libre de hacerlo él mismo, o contratar a cualquier programador o empresa
> disponible para que los haga. Los usuarios ya no estarán a merced de un
> programador o empresa propietaria de las fuentes y que sea la única que
> puede realizar modificaciones.[^147]

[^147]: Stallman, <cite>El manifiesto de GNU</cite>. Disponible en
<https://www.gnu.org/gnu/manifesto.es.html>.

El proceso de desarrollo de GNU a partir de 1985 le aportó a Stallman
varias enseñanzas. La principal de ellas es el hecho de que no bastaba
crear un proyecto que tuviera como principio la libertad y el libre uso
y la libre compartición si no había alguna forma de proteger y
garantizar esa libertad también de forma jurídica. Así, en 1989, fue
publicada la General Public License (GPL), una licencia genérica que
cubría todos los códigos del proyecto GNU y que aspiraba a otorgar
libertades de uso que el <i lang="en">copyright</i> en boga en Estados
Unidos no permitía. Stallman precisaba «garantizar a los usuarios de GNU
los derechos básicos de acceso, copia, modificación y redistribución de
los programas, y para eso era preciso restringir las restricciones a
esos derechos. Él creó entonces, con la ayuda del
<i lang="en">copyright</i>, un sistema que otorgaba a todos el derecho
de que accedieran a sus programas y a nadie el derecho de restringir ese
acceso»[^148]. Registró el <i lang="en">copyright</i> del programa para,
después, liberarlo, creando un tipo de proceso viral en que todos los
usos solo son posibles si se transfieren a otros. Garantizaba así que
nadie se apropiase del <i lang="en">software</i>.

[^148]: Torres, <i lang="lt">op.&nbsp;cit.</i>, p. 133.

En el texto original de la GPL, constan las libertades que
caracterizarían, a partir de entonces, lo que es un
<i lang="en">software</i> libre, y también la justificación para usar el
sistema de <i lang="en">copyright</i> para protegerlo de este mismo:

> Para proteger tus derechos necesitamos evitar que otros te nieguen
> estos derechos o te pidan que renuncies a los derechos. Por lo tanto,
> tienes ciertas responsabilidades si distribuyes copias del programa o
> si lo modificas: responsabilidades de respetar la libertad de los
> demás.
>
> Por ejemplo, si distribuyes copias de tal programa, ya sea gratis o
> por una tasa, debes dar a los receptores las mismas libertades que
> recibiste. Debes garantizar que ellos también reciban o puedan recibir
> el código fuente. Y debes mostrarles estos términos para que conozcan
> sus derechos.[^149]

[^149]: GNU General Public License, disponible en
  <https://www.gnu.org/licenses/gpl-3.0.html>.

El <i lang="en">hack</i> en el sistema jurídico para garantizar las
libertades del <i lang="en">software</i> libre que dio origen a la GPL
tomó el nombre de <i lang="en">copyleft</i>. Fue un juego de palabras
con la palabra <i lang="en">copyright</i> propuesto, según cuenta
Stallman[^150], por su amigo Don Hopkins en una carta a él enviada en
1984 (o 1985), en la que Hopkins escribía la siguiente frase al final
del mensaje: «<i lang="en">Copyleft – all rights reversed</i>»
(<i lang="en">Copyleft</i> – todos los derechos invertidos), claramente
relacionada con los avisos de <i lang="en">copyright</i> que incluían la
frase «<i lang="en">All rights reserved</i>» (Todos los derechos
reservados). A lo largo de los años, se crearon diversas posibilidades
de interpretación del juego de palabras aparte de esta, entre ellas la
de que <i lang="en">copyleft</i> sería «copia de izquierda» en paralelo
al <i lang="en">copyright</i>, «copia de derecha».

[^150]: Stallman, <cite>Free Software, Free Society</cite>; Gay,
  <i lang="lt">op.&nbsp;cit.</i>; Stallman, <cite>Software libre para una
  sociedad libre</cite>, p. 293.

Con juego de palabras o de forma literal, el <i lang="en">copyleft</i>
fue el concepto, expresado en la licencia GPL y otras ligadas al
Proyecto GNU que la siguen hasta la fecha, de requerir la propiedad
legal para, en la práctica, renunciar a esta al autorizar que todos
hagan el uso que deseen de la obra, mientras que transmitan sus mismas
libertades a otros. La exigencia formal de la propiedad significa que
ninguna otra persona podrá poner un <i lang="en">copyright</i> sobre una
obra <i lang="en">copyleft</i> e intentar limitar su uso. Stallman ya
afirmó que su objetivo inicial era idealista: difundir la libertad y la
cooperación, promoviendo el <i lang="en">software</i> libre, y sustituir
el <i lang="en">software</i> propietario que prohíbe la colaboración. Su intento fue el
de intentar reconciliar el mantenimiento de la libertad de uso y
modificación del <i lang="en">software</i> con una protección para que
ninguna persona se apropiara de esta. Como él mismo afirmó:

> La manera más fácil de liberar un programa es ponerlo en dominio
> público, sin derechos reservados. Esto permite a la personas compartir
> el programa y sus mejoras, si así lo desean. Pero también permite que
> quienes no creen en la cooperación conviertan el programa en
> <i lang="en">software</i> propietario. Pueden hacer cambios,
> muchos o pocos, y distribuir su resultado como un producto
> propietario. Las personas que reciben el programa con esas
> modificaciones no disfrutan de la libertad que les dio el autor
> original; el intermediario les ha despojado de ella.[^151]

[^151]: Stallman, <cite>Software libre para una sociedad libre</cite>,
  p. 125.

A partir de la GPL y del <i lang="en">copyleft</i> se creó un aparato
legal que, en los años siguientes, demostraría ser una idea posible de
poner en práctica no solo en el mundo de la informática, sino también en
otras áreas del conocimiento y de la cultura, aglutinando a otros grupos
distintos en torno a un deseo antiguo manifestado en la sociedad de
democratización de los bienes culturales[^152]. Volver el derecho al
acceso mayor que el derecho a la restricción fue algo que, hasta
ese momento, solía manifestarse de diferentes formas: con la negación de
la propiedad intelectual, las prácticas <i>anticopyright</i> que
criticaban la posición de ver los bienes culturales solamente como
mercancías, el uso indiscriminado de fragmentos de otros obras sin
realizar un pago o incluso sin reconocimiento de fuente (como en los
diferentes usos de plagio creativo) y el rechazo a la autoría mediante
el anonimato o la identificación colectiva. La idea de usar el propio
sistema de propiedad intelectual para burlarlo mostró ser una novedad
que, con la popularización de Internet, se extendería después a diversos
lugares y áreas muy alejados de su origen.

[^152]: Torres, <i lang="lt">op.&nbsp;cit.</i>, p. 131.

### III.

A finales de los años 90, el <i lang="en">copyleft</i> se difunde por lo
menos de dos formas diferentes. La primera como una idea y una práctica
de enfrentamiento con el <i lang="en">statu quo</i> del derecho de
autor y del conocimiento considerado como mercancía, camino adoptado por
movimientos activistas de áreas como el medio ambiente; anarquistas,
autonomistas e integrantes de iniciativas relacionadas con una izquierda
antineoliberal; y artistas que se adhieren a una contracultura de
cuestionamiento de la autoridad en diversas áreas, como muchos de los
citados en el capítulo anterior. El segundo camino de propagación del
<i lang="en">copyleft</i> se da como un discurso aglutinante de
prácticas a favor de la defensa de la libertad de información y acceso a
partir de la digitalización y de Internet, es el caso de muchos jáqueres
relacionados con el <i lang="en">software</i> libre y el código abierto
y de ciberactivistas que, en esa época, se orientan a áreas como el
intercambio libre de archivos en la red y la defensa de medios de
comunicación libres que busquen perspectivas diferentes al periodismo de
las grandes redes.

En algunos casos, las dos formas se mezclan, como veremos más adelante.
Pero, en primer lugar, es importante contar como, diez años después de
la creación de la GPL, en 1999 el <i lang="en">copyleft</i> se convierte
en la inspiración principal para la creación de un movimiento en torno a
una cultura libre (<i lang="en">free culture</i>), sobre todo
proveniente de Estados Unidos y de Europa. Proyectos que surgen en esa
época, como el Science Commons, Open Access y Open Educational Resources
(OER) &mdash;en español, recursos educativos abiertos (REA)[^153]&mdash;
propagarán el acceso, el libre uso e intercambio de recursos en
diferentes áreas de la misma forma que la establecida a partir de las
libertades del <i lang="en">software</i> libre propuestas por Stallman.
En una sociedad donde información, código y ley empiezan a formar un
trío cada vez más poderoso, ideas como la libertad, los
<i lang="en">commons</i> y la apertura se desarrollan de forma clave en
un movimiento de cultura libre que aspira a ofrecer alternativas al
cercamiento y al control progresivos de la cultura en la época[^154].

[^153]: Más información sobre recursos educativos abiertos en
  <https://es.wikipedia.org/wiki/Recursos_educativos_abiertos>.
[^154]: Mansoux, Livre como queijo: confusão artística acerca da
  abertura, en Belisário; Tarim (orgs.), <cite>Copyfight</cite>, p. 195.

Artistas relacionados con la contracultura y la libertad del
conocimiento empiezan a mirar la idea del <i lang="en">copyleft</i> y
verla como táctica, apropiándose de ella y desarrollándola para diversos
fines, inclusive jurídicos. Es el caso del nacimiento de la primera
licencia libre fuera del ámbito del <i lang="en">software</i>, la
Licencia de Arte Libre[^155], creada a principios del 2000 por un grupo
de artistas franceses en el foro de Internet llamado Ataque Copyleft.
Publicada en julio del 2000, esta se basa en los mismos principios del
<i lang="en">copyleft</i> original y surge por el deseo de desencadenar
procesos creativos, y no por cuestiones ligadas a los derechos de autor
o al uso de aplicaciones[^156]. Bajo el punto de vista de los que
propusieron la licencia, el <i lang="en">software</i> libre abrió el
camino real para la expansión de técnicas creativas a partir de los
medios de comunicación digitales, y el arte libre (la licencia) ayudaría
a evitar la apropiación exclusiva del arte libre (como práctica):
«Si definimos en el <i lang="en">copyleft</i> como un principio
orientador, el Arte Libre se conecta con lo que el arte siempre fue,
desde tiempos remotos, incluso antes de que reconocieran que este posee
una historia: una creación de la mente, en rebelión contra una cultura a
la que le gustaría dominarlo y entenderlo»[^157].

[^155]: Disponible en <https://artlibre.org/licence/lal/es/>.
[^156]: Moreau, Sobre arte livre e cultura livre, em Belisário; Tarim,
  <i lang="lt">op.&nbsp;cit.</i>, p. 159.
[^157]: <i lang="lt">Ibidem</i>, p. 162.

De tradición <i>anticopyright</i> y de nombres colectivos de los años 80
y 90, el colectivo italiano Wu Ming se mostraría identificado con el
<i lang="en">copyleft</i> para usarlo como bastión para su defensa
contra la propiedad intelectual. Los primeros textos y entrevistas del
colectivo concedidas a periodistas que mencionan la cuestión datan de
2002 y 2003; en especial, <cite>Copyright y maremoto</cite>[^158], texto
publicado por uno de los integrantes del colectivo (Wu Ming 1), trata de
defender el código abierto y el <i lang="en">copyleft</i> como
estrategias aliadas de la libre compartición en contra la privatización
de la cultura &mdash;y que podrían superar la legislación de propiedad
intelectual de la época&mdash;. La fuerza del <i lang="en">copyleft</i>
derivada del hecho de ser una innovación jurídica surgida de abajo que
supera la mera «piratería», enfatizando la <i lang="lt">pars
construens</i>[^159] del movimiento real[^160].

[^158]: Wu Ming, <cite>Copyright y maremoto</cite>.
[^159]: <i lang="lt">Pars construens</i> es una expresión que designa un
  «argumento constructivo» en algún debate, en oposición a
  «<i lang="lt">pars destruens</i>». La distinción fue realizada por
  Francis Bacon, en 1620. Nota del texto de Wu Ming, <cite>Copyright y
  maremoto</cite>.
[^160]: <i lang="lt">Ibidem</i>.

> El código abierto y el <i lang="en">copyleft</i> se extienden
> actualmente mucho más allá de la programación del software: las
> «licencias abiertas» están en todas partes y en tendencia pueden
> convertirse en el paradigma de un nuevo modo de producción que libere
> finalmente la cooperación social (ya existente y visiblemente
> desplegada) del control parasitario, la expropiación y la «renta» a
> favor de grandes potentados industriales y corporativos.[^161]

[^161]: <i lang="lt">Ibidem</i>.

En 2005, el texto <cite>Notas inéditas sobre copyright y copyleft</cite>
actualiza el tema y señala el <i lang="en">copyleft</i> no como un
movimiento o ideología, sino como un término que «engloba una serie de
prácticas, escenarios y licencias comerciales, y que encarna todas las
exigencias de reforma y adecuación de las leyes de derecho de autor
hacia el «desarrollo sostenible»[^162].

[^162]: Wu Ming, Notas inéditas sobre copyright e copyleft, en <cite>La
  Remezcla</cite>.

También a comienzos de los años 2000, una parte del activismo digital y
de la academia jurídica empieza a observar movimientos en torno a la
cultura libre y a unirse en oposición al cada vez mayor endurecimiento
de las leyes de derecho de autor, principalmente en Estados Unidos,
lugar de origen de los primeros ordenadores personales, de los programas
para esos ordenadores y de otros inventos tecnológicos realizados en
Silicon Valley. Algunas de esas actualizaciones en la ley fueron el
Digital Millennium Copyright Act (DMCA) y el Sonny Bono Copyright Act
(también conocido como el ya citado Mickey Mouse Protection Act). En ese
mismo año, Brasil aprobó su última ley de derechos de autor, que, aún
vigente hasta la fecha de publicación de este libre, amplió de 60 a 70
años el plazo de protección de derechos del autor después de su
muerte[^163].

[^163]: Valente, <cite>Implicações jurídicas e políticas do direito
  autoral na internet</cite>, p. 150.

Una de las principales voces del activismo y del derecho que empieza a
organizarse en torno a la noción de cultura libre es Lawrence Lessig,
abogado y profesor de derecho en Harvard. Miembro del Berkman Center for
Internet & Society, Lessig acababa de publicar <cite>Code and Other Laws
of Cyberspace</cite> (1999), libro que lo convertiría en una referencia
en derecho y gobernanza en Internet, cuando se involucró en la defensa
de Eric Eldred, creador de una página de Internet que daba acceso a
libros en dominio público y que había quitado su sitio web de Internet
en protesta contra el aumento de veinte años del plazo de validez del
derecho de autor propuesto en el Sonny Bono Copyright Act. Conocido como
Eldred vs. Ascroft, el caso, de 1999, se popularizó en Internet en
función del alcance del sitio web, que en la época tenía más de veinte
mil accesos al día, y con la articulación en su defensa propuesta por
Lessig, que juntó a varios organizaciones de defensa del interés
público, como Electronic Frontier Foundation (EFF), la Free Software
Foundation (FSF), la Public Knowledge, entre autores, abogados,
economistas e incluso empresas tecnológicas, como Intel[^164].

[^164]: <i lang="lt">Ibidem</i>, p. 151.

Lessig argumentaba que la extensión del plazo de los derechos de autor
violaba la Constitución de los Estados Unidos, que determinaba, como
Thomas Jefferson y otros liberales habían defendido a final del siglo
XVIII, que la protección de derechos de autor tendría un plazo
limitado[^165]. Incluso apelando al máximo documento del país, la acción
de Lessig fue negada en todas las instancias, incluso en la Corte
Suprema. Sirvió, sin embargo, para mostrar tanto a Lessig como a otros
activistas que los caminos políticos y jurídicos tradicionales estaban
cerrados a la negociación sobre la flexibilización de los derechos de
autor y «que los derechos de acceso y protección del dominio público, en
las esferas oficiales, eran vistos como impedimentos perjudiciales al
comercio electrónico»[^166]. A finales de los años 90, las legislaciones
de Internet se adaptaban a partir de las leyes de derecho de autor
usadas en el entretenimiento y en la cultura, creadas a partir de
acuerdos como los de Berna y de París, en el siglo XIX, en aquel momento
ya también incorporados a la Organización Mundial del Comercio (OMC).

[^165]: En la cláusula de derechos de autor y de patentes de la
  Constitución de Estados Unidos, citada en la nota 55.
[^166]: Valente, <i lang="lt">op.&nbsp;cit.</i>, p. 154.

El cambio buscado tras las derrotas jurídicas fue crear un nuevo tema
para presentar otros caminos, jurídicos y políticos, en defensa del
conocimiento y de la cultura libre. De ese movimiento nacía, en 2001, la
Creative Commons (CC), una organización sin ánimo de lucro que aspiraba
a crear licencias alternativas al restrictivo «Todos los derechos
reservados» del <i lang="en">copyright</i>. Ofrecía como opción «algunos
derechos reservados», en la que cada creador podría escoger lo que le
gustaría liberar, yendo de lo más restrictivo &mdash;que era igual al
<i lang="en">copyright</i> ya existente&mdash; a lo menos, como el
dominio público[^167]. El proyecto comenzó con Lessig, Hal Abelson y
Eric Aldred a la cabeza, con apoyo financiero del Center for the Public
Domain, centro de investigación conectado a la Universidad de Harvard,
donde Lessig trabajaba, teniendo como objetivo «expandir el reducido
dominio público, fortalecer los valores sociales de compartir, de
apertura y del avance del conocimiento y de la creatividad
individual»[^168]. Intentaba ser una alternativa pragmática al sistema
de <i lang="en">copyright</i> vigente y se inspiraba abiertamente en el
movimiento del <i lang="en">software</i> libre y en el
<i lang="en">copyleft</i>, aunque tuviera características más amplias,
con licencias que servirían para diversos tipos de obras culturales y no
solo para un tipo (el <i lang="en">software</i>), como la GPL.

[^167]: Las licencias CC existentes en 2022 se muestran en el sitio web
  <https://creativecommons.org/licenses/>.
[^168]: En Bollier, <cite>How the Commoners Built a Digital Republic of
  their Own</cite>, traducido por Valente,
  <i lang="pt">op.&nbsp;cit.</i>, p. 156.

Como muchas de las propuestas que buscan ampliar el alcance de un
determinado conocimiento, Creative Commons tuvo que simplificar algunos
procedimientos, lo que llevó a muchos críticas sobre una despolitización
de la iniciativa y de la propia idea de <i lang="en">copyleft</i>. En la
construcción de sus conjuntos de licencias, por ejemplo, CC amplió las
posibilidades de elección del <i lang="en">copyleft</i> original
propuesto en la GPL sin establecer libertades, derechos ni cualidades
fijas &mdash;o sin distinguir lo que sería una licencia libre de una
licencia propietaria, ambas posibles dentro de las seis licencias a
elección en el proyección&mdash;. Así, Benjamin Mako Hill, Florian
Kramer, Dimitry Kleiner, Anna Nimus, entre otros en la época,
señalarían que CC no establecía una posición ética como el
<i lang="en">software</i> libre, o incluso como el movimiento de código
abierto[^169] &mdash;disidencia más flexible comercialmente en sus
principios que el <i lang="en">software</i> libre, pero que también
tendría, como este, ideas políticas definidas sobre lo que defenderían y
lo que no&mdash;.

[^169]: <i lang="en">Software</i> de código abierto
(<i lang="en">free/libre/open source software</i>, acrónimo FLOSS
adoptado por primera vez en 2001) es un nombre usado para un tipo de
<i lang="en">software</i> que surgió a partir de la llamada Open Source
Initiative (OSI), creada en 1998 (<https://opensource.org/>) como una
disidencia con principios un poco más flexibles que los del
<i lang="en">software</i> libre (<https://opensource.org/osd>), lo que
propició una expansión considerable tanto del término código abierto
(<i lang="en">open source</i>) como de proyectos y empresas que tienen
el <i lang="en">software</i> como producto y motor de sus negocios. La
OSI tiene como texto filosófico central <cite>La catedral y el
bazar</cite>, de Eric Raymond, publicado en 1999. En él, Raymond trabaja
con la idea de que *«con suficientes miradas, todos los errores saltan a
la vista»*, para decir que, si el código fuente está disponible para
probarlo, analizarlo y experimentar públicamente, los errores serán
descubiertos más rápidamente. El ensayo original puede ser leído,
entero, en inglés en
<http://www.catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar>, y
traducido al español en
<https://biblioweb.sindominio.net/telematica/catedral.html>.

Según esa perspectiva crítica, Creative Commons daría demasiada libertad
a la elección de los creadores (o consumidores), lo que serviría más
para reservar los derechos a los *usuarios* que a los propietarios de
derechos de autor[^170]. En la crítica de Nimus: «El Creative Commons
sirve para ayudar al creador a mantener el control sobre “su” obra, lo
que legitima el control ejercido por el productor antes de rechazarlo e
imponerle la distinción entre productor y consumidor antes de
revocarlo»[^171]. Según esa impresión, que resuena en muchas de las
prácticas antiarte y contrarias al derecho de autor de las vanguardias
artísticas del siglo XX, el CC sería como una versión rebuscada del
<i lang="en">copyright</i>, que «no contesta al régimen de
<i lang="en">copyright</i> como un todo ni conserva su estatuto legal
para dar marcha atrás en la práctica del <i lang="en">copyright</i>,
como hace el <i lang="en">copyleft</i>»[^172].

[^170]: Kramer, O mal-entendido do Creative Commons, en Belisário;
  Tarim, <i lang="lt">op.&nbsp;cit.</i>, pp. 180-181.
[^171]: Nimus, <i lang="lt">op.&nbsp;cit.</i>, p. 52.
[^172]: <i lang="lt">Ibidem</i>.

No es una sorpresa ni un demérito el camino pragmático adoptado por
Creative Commons. De influencia marcadamente liberal, de la tradición de
John Locke, Condorcet y Thomas Jefferson, Lessig no quería abolir el
<i lang="en">copyright</i>, sino reformarlo. Su propuesta, presentada
por la Creative Commons, defendía abiertamente la libertad de los
creadores, que estaba siendo atacada por el constante aumento del plazo
de extensión de los derechos de autor, lo que también amenazaba el
mantenimiento de un dominio público común. Así, su posición fue la de
«lograr más apoyo en torno a los objetivos que restablecen el paisaje social
de la creatividad»[^173], lo que despojó la iniciativa, por lo menos
en los primeros años, de todos los principios políticos y éticos
contrarios al <i lang="en">copyright</i> que buena parte de los
defensores del <i lang="en">software</i> libre, del
<i lang="en">copyleft</i> y de una cultura libre de tradición
<i>anticopyright</i> llevaban consigo.

[^173]: <i lang="lt">Ibidem</i>.

En varias ocasiones, Stallman salió públicamente a hablar de que, con la
difusión del CC en los años 2000, mucha gente empezó a preguntarse la
diferencia entre <i lang="en">copyleft</i> y el Creative Commons. En los
términos propuestos para el <i lang="en">hack</i> jurídico del
<i lang="en">copyleft</i>, solo una de las licencias de CC estaría
contemplada: la CC BY-SA
&mdash;*Compartición bajo la misma licencia*[^174]&mdash;, que permite
la reutilización y la compartición de la obra, incluso para fines
comerciales, siempre que preserve en el futuro las libertades obtenidas
para otros usos, de modo que «contagie» a las otras obras y garantice
que estas no se cierren con <i lang="en">copyright</i>. Otra licencia,
la CC BY[^175], que da las mismas libertades que el dominio público,
también es una licencia libre en las términos de la GPL y de las cuatro
libertades del <i lang="en">software</i> libre, mientras que las otras
cuatro principales licencias Creative Commons &mdash;que pueden no
permitir la modificación de la obra y prohibir el uso para fines
comerciales, por ejemplo&mdash; no serían libres.

[^174]: Texto completo de la licencia disponible en
  <https://creativecommons.org/licenses/by-sa/2.5/es/>.
[^175]: Disponible entera en <https://creativecommons.org/licenses/by/3.0/es/>.

Incluso con las críticas, la dimensión del CC, la practicidad de su
conjunto de licencias y su intención de intentar defender, aunque de
forma genérica, la compartición y el dominio público facilitaron su
difusión por diversos países y más allá del mundo de la tecnología. Las
disputas en torno al intercambio de archivos digitales en los años
2000 ayudaron también a popularizar el Creative Commons como una
alternativa viable para el combate contra el discurso de la
criminalización de la piratería de quien se bajaba archivos protegidos
con <i lang="en">copyright</i> en la Red. «Cuando te saltas a los
intermediarios, puede ser así de fácil» era una frase escuchada en un
vídeo de divulgación del CC[^176] de la época que resaltaba la
practicidad para los creadores que escogieran, de manera anticipada, qué
derechos querían preservar (además del crédito como autor, establecido
como estándar para todas las obras y reconocido en cualquier tipo de
legislación de propiedad intelectual) y cuáles querían liberar. El
derecho de adaptación o compartición libre de una canción, por ejemplo,
facilitaría su difusión en diferentes versiones remezcladas &mdash;un
caso ejemplar en ese aspecto es el del disco citado en ese mismo vídeo
de presentación del CC, llamado «Redd Blood Cells», en el que el bajista
Steven McDonald, del grupo Redd Kross, volvió a grabar con una versión
con bajo todas las canciones del disco <cite>White Blood Cells</cite>,
de White Stripes, grupo solo de guitarra, voz y batería.

[^176]: <cite>Sé Creativo</cite>, el vídeo, aún puede verse, doblado al
  español, en el enlace <https://www.youtube.com/watch?v=SUblaElbybE>.

A partir de 2003 y 2004, la difusión del CC hizo surgir grupos que
tradujeron y adaptaron sus licencias a las realidades locales en países
como Japón, Corea del Sur, Méjico, Croacia, Portugal, España, Alemania,
Argentina, Uruguay, generalmente organizados desde instituciones de
investigación y universidades o de grupos autónomos. En Brasil, los
primeros años de la década coincidieron con el ascenso de Lula a la
Presidencia del país, en 2002, y de Gilberto Gil como ministro de
cultura, en 2003. Figura central de la música brasileña, Gil se reunió
con Lessig junto al antropólogo Hermano Vianna y, según consta,
«comprendió rápidamente el proyecto y apoyó la causa»[^177]. En el
análisis de Hermano Vianna, amigo y colaborador con el músico brasileño,
«la cultura de compartir y principalmente la del
<i lang="en">sampling</i> estarían tan ligadas al tropicalismo que la
comprensión de la necesidad de pensar la cultura libre fue inmediata
para Gil»[^178]. ¿Cómo de recombinado no era ya el tropicalismo cuando
<cite>Tropicália ou panis et circensis</cite>, álbum modelo del movimiento de
1968, combinaba a Vicente Celestino, John Cage, cultura popular y
erudita pasando estratégicamente por la cultura pop y la fuertemente
influenciada por la antropofagia propuesta por Oswald de
Andrade[^179]?

[^177]: Valente, <i lang="lt">op.&nbsp;cit.</i>, p. 156.
[^178]: Bollier, <i lang="lt">op.&nbsp;cit.</i>, traducido por Valente,
  <i lang="lt">op.&nbsp;cit.</i>, p. 157.
[^179]: En Viveiros de Castro, <i lang="lt">op.&nbsp;cit.</i>, p. 81.

La adhesión del Ministerio de Cultura (MinC) liderado por Gil al
Creative Commons se produjo a partir de acciones como el desarrollo de
la licencia CC-GPL, en 2003, que tradujo el texto inicial de la GPL al
portugués, y de la adopción de las licencias en los materiales
producidos por el MinC. Marcó también un momento de compromiso del
ministerio con el <i lang="en">software</i> libre, lo que dio como
resultado proyectos como Pontos de Cultura, que, a partir del 2004,
distribuyó kits de ordenadores con sistemas operativos libres para
pequeños productores culturales por todo Brasil. Una extraña política
pública que combinó tecnología libre y cultura popular, el [proyecto]
Cultura Viva[^180], como se hizo conocido el proyecto, potenció la
difusión del <i lang="en">software</i> y de la cultura libres en el país
y convirtió a Brasil, en aquella época, en uno de los principales polos
desarrolladores y consumidores de tecnologías y cultura libres del
mundo. Gil, a su vez, se volvió cercano a Lessig y defensor público del
CC; lo divulgó como herramienta democratizadora y socializadora
&mdash;el único ministro de Cultura de un país en hacerlo, lo que
también contribuyó a dar visibilidad mundial al proyecto[^181]&mdash;.

[^180]: El [proyecto] Cultura Viva es «una política cultural orientada
  al reconocimiento y el apoyo a actividades y procesos culturales ya
  desarrollados, promoviendo la participación social, la colaboración y
  la gestión compartida de políticas públicas en el ámbito de la
  cultura». A pesar de que, en el momento de este texto, el proyecto
  esté parado y ni exista Ministerio de Cultura en Brasil, este
  puede conocerse en detalle en el portal
  <https://web.archive.org/web/20210302234024/http://culturaviva.gov.br/>.
[^181]: Valente, <i lang="lt">op.&nbsp;cit.</i>, p. 157.

Una posición conciliadora, propuesta por el antropólogo Eduardo Viveiros
de Castro, sintetiza el impacto de Creative Commons en Brasil y en el
mundo desde un punto de vista tanto conceptual como pragmático.

> Es una iniciativa, a mi parecer, muy meritoria. Ellos están intentando
> evitar que se encierre el mundo virtual, así como lo fue el mundo
> geográfico. Que este sea privatizado. Es un intento de mantener la
> información como un bien de dominio público. El punto más importante
> para Creative Commons es que la información no sigue el régimen de la
> suma cero, que esta puede avanzar hacia delante y no disminuir con
> ello. Eso no quiere decir que un autor deba ser plagiado; el objetivo
> es facilitar la circulación. [...] La idea es que el
> <i lang="en">copyright</i> significa
> «<i lang="en">all rights reserved</i>» y el Creative Commons significa
> «<i lang="en">some rights reserved</i>». Y tú decides cuáles. Existen
> varias fórmulas, varios tipos de licencias abiertas. Se trata de
> intentar crear un modo de convivencia en el ámbito de la información
> que sea tolerable y que evite lo que está sucediendo, que es el
> control de la información por las grandes empresas. Ahora todo eso aún
> es, de cierta forma, un paliativo. El Creative Commons puede verse,
> como lo es efectivamente por los más, digamos, radicales, como una
> estrategia capitalista. El verdadero anarquista no quiere saber nada
> de Creative Commons ni de <i lang="en">copyleft</i>, es totalmente
> radical. En principio estoy con ellos, considero la propiedad privada
> una monstruosidad, sea esta intelectual o no, pero sé también que
> tampoco se avanza dándose contra una pared, tapando el sol con las
> manos. Pienso que tienes que transigir, tienes que hacer algún tipo
> de negociación.[^182]

[^182]: Viveiros de Castro, <i lang="lt">op.&nbsp;cit.</i>, pp. 93-94.

Para la propagación de la cultura libre en los años 2000 hubo, además del
<i lang="en">copyleft</i> y de las licencias Creative Commons, otro
factor importante: la publicación de <cite>Free Culture</cite>
(<cite>Cultura libre</cite>), de Lawrence Lessig, en 2004. El libro
rescata la historia de la propiedad intelectual a partir de casos
emblemáticos, algunos de ellos ya comentados aquí &mdash;como las
batallas en los tribunales ingleses del siglo XVII que originaron el
<i lang="en">copyright</i> y el uso de historias de dominio público por
Disney. Inspirada en el <i lang="en">software</i> libre, la obra
defiende un concepto de cultura libre como aquella que debe restringirse
lo mínimo posible, de manera que haga posible su compartición,
distribución, copia y uso sin que eso afecte a la propiedad intelectual
de los bienes culturales. Con esto ayuda a difundir una visión de
la cultura que organiza un movimiento a favor de modificaciones en las
leyes de derecho de autor actuales, las cuales, según Lessig y otros
activistas, dificultan la creatividad y propagan una «cultura del
permiso», en la que todo creador debe pedir permiso si quiere usar una
determinada obra, sea cual sea la finalidad. Un movimiento por la
cultura libre, como empieza a ser identificado en esa época, lucharía
para mantener un dominio público robusto y accesible a todos, creando,
además de las leyes, también tecnologías, estrategias y tácticas para
mantener las creaciones libres, no necesariamente «gratis»,
parafraseando la conocida frase de Stallman usada en el contexto de la
libertad del <i lang="en">software</i> libre: «<i lang="en">Think free
as in free speech, not free beer</i>»[^183].

[^183]: «Piensa libre como en libertad de expresión, no como en cerveza
  gratis». Muy conocida en el mundo del <i lang="en">software</i> libre,
  se atribuye la frase a Stallman por Lessig por primera vez en 2006,
  disponible en <https://www.wired.com/2006/09/free-as-in-beer>.

<cite>Cultura libre</cite>, el libro, también presenta propuestas
prácticas de defensa del dominio público. Algunas de ellas han llegado a
ser discutidas y todavía hoy son consideradas por reformistas, aunque se
tenga la noción de que, para el interés de los conglomerados de
protección de los derechos de autor en todo el mundo, estas aún son
vistas como excesivamente radicales. La disminución del plazo de
extensión del <i lang="en">copyright</i>, por ejemplo, es una propuesta
que siempre ha existido y que Lessig retoma en el libro, con el objetivo
de considerarla a partir de la idea de que ese plazo «debería ser tan
largo como sea preciso para proporcionar incentivos a la hora de crear,
pero no más»[^184]. Lo que, además de favorecer el acceso y mantener las
obras durante más tiempo en dominio público, evitaría también la
necesidad de crear continuamente excepciones jurídicas que dificultan la
comprensión, para el público general que no es abogado, de lo que está
protegido y de lo que está abierto. Lessig afirma que, hasta 1976, el
período medio de duración de un <i lang="en">copyright</i> en Estados
Unidos era de 32,2 años, y que quizás ese período medio fuera adecuado.

[^184]: Lessig, <i lang="en">op.&nbsp;cit.</i>, p. 235.

> Sin duda, los extremistas llamarán a estas ideas «radicales». (Después
> de todo, yo los llamo «extremistas»). Pero, de nuevo, el plazo que
> yo había recomendado es más largo que el plazo bajo Richard Nixon.
> ¿Cuán «radical» puede ser pedir una ley de <i lang="en">copyright</i>
> más generosa que la existente cuando Nixon era presidente?[^185]

[^185]: <i lang="lt">Ibidem</i>, p. 236.

Otras ideas presentadas por Lessig en la publicación de 2004 sonarían
como premoniciones para las décadas siguientes, como la relacionada con
el intercambio de archivos en la red.

> Cuando sea extremadamente fácil conectarse a servicios que den acceso a
> contenidos, será más fácil descargar y almacenar contenidos en los
> muchos dispositivos que tendrás a tu disposición para reproducir
> contenidos. Será más fácil, en otras palabras, suscribirse que
> convertirse en el administrador de una base de datos, que es
> precisamente lo que es cualquiera que opere en el mundo del intercambio
> de archivos por medio de tecnologías inspiradas en Napster. Los
> servicios de contenidos competirán con el intercambio de contenidos,
> incluso si estos servicios cobran por el contenido al que dan acceso.[^186]

[^186]: <i lang="lt">Ibidem</i>, p. 239.

### IV.

El Internet de los años 90 y 2000, período en el que la cultura libre se
extendió, fue un momento de extrema libertad e imaginación, manifestada
por el optimismo reinante en torno a las posibilidades que la Red
ofrecía y por la libertad de compartir permitida en los diversos sitios
web que ofrecían los archivos de bienes culturales más variados del
planeta. Como una red basada en el intercambio de información, Internet
desde sus inicios permitió y facilitó el libre intercambio de archivos.
Cuando todavía era un sitio minoritario usado principalmente por
científicos, militares y representantes de la contracultura, en los años
1980 y en los primeros años de la década de 1990, la libre circulación
de información no llegó a incomodar de modo significativo a las
industrias basadas en la propiedad intelectual &mdash;después de todo,
en la época, solo era posible enviar archivos pequeños,
<i lang="en">bytes</i> de información que circulaban entre pocas
personas. Los formatos de codificación de un archivo de audio, por
ejemplo, solo transformarían una canción en datos que pueden ser
enviados por Internet libremente a partir de 1993, con la publicación
del MP3[^187], uno de los primeros tipos de compresión audio con
una pérdida de información casi imperceptible para el oído humano. Aún
así, pasarían algunos años hasta que el formato se popularizara y la
capacidad de transmisión de datos en Internet consiguiera enviar una
canción sin sobrecargar la Red.

[^187]: Su <i lang="en">bitrate</i> (tasa de bits) es del orden de *kbps*
(kilobits por segundo), siendo 128 kbps la tasa predeterminada, en la
que la reducción del tamaño del archivo es cercana al 90&nbsp;%
&mdash;el tamaño del archivo pasa a ser 1/10 del tamaño original.
Fuente: <https://en.wikipedia.org/wiki/MP3>. Como todas las tecnologías
citadas en este libro, es fruto de muchas experiencias y largos años de
investigaciones científicas, que remiten a formas de transmitir sonidos
con alta calidad y a maneras de codificar audios, que dieron como
resultado el formato MPEG y, luego, el MP3 (MPEG3), historia contada en
detalle en el artículo «Genesis of the MP3 Audio Coding Standard» de H.
G. Musmann, de la Universidad de Hannover, en Alemania, disponible en
<https://ieeexplore.ieee.org/document/1706505>.

Con el inicio del Internet comercial en el mundo a partir de 1994 (en
Brasil en 1995), miles de personas empezaron a poder subir y bajar
archivos libremente, protegidos o no por <i lang="en">copyright</i>,
mediante prácticas de par a par (<i lang="en">peer to peer</i>, también
abreviado <i lang="en">p2p</i>), como el *torrent*, proceso
descentralizado de compartición que facilita la descarga de forma que
cada usuario pueda descargar partes de un archivo a partir de otras
partes distribuidas en varios ordenadores &mdash;cuantos más
dispositivos, más rápido el proceso&mdash;. La facilidad de circulación
de información proporcionada por Internet aumentó exponencialmente con
el aumento de la velocidad de las conexiones; las redes por línea
conmutada de 56 kbps comunes en 1995[^188] en pocos años serían de
1&nbsp;000 kbps con la popularización del servicio conocido como ADSL
(<i lang="en">Assymmetric Digital Subscriber Line</i>, Línea de Abonado
Digital Asimétrica[^189]), responsable de la mayor parte del acceso a
Internet de ordenadores personales ya a comienzos de los años 2000. Con
más velocidad para bajar archivos mayores en la red, una práctica temida
y combatida desde el principio del derecho de autor volvería a estar en
el punto de mira: la piratería.

[^188]: Con esa velocidad, un archivo de música (3,5
<i lang="en">megabytes</i>) en MP3, por ejemplo, tardaría de media entre
15 y 30 minutos en ser descargado por un ordenador personal, mientras
que un vídeo de baja calidad (700 <i lang="en">megabytes</i>), de 28 a
42 horas. Como los datos por Internet y la voz por el teléfono se
transmitían por el mismo canal, solamente podía realizarse una operación
al mismo tiempo: descargar un archivo en MP3 ocuparía la línea
telefónica durante 30 minutos por lo menos, lo que representaba, con
fines de cobro, una conexión local de dos horas. Una operación que,
dependiendo del valor del pulso o del minuto, podría aumentar el valor
de la cuenta telefónica en cientos de reales en el Brasil de los
primeros años del Internet comercial (Foletto, <cite>Um mosaico de
parcialidades na nuvem coletiva</cite>, pp. 117-118).
[^189]: De modo general, el ADSL funciona también mediante las líneas y
  cables telefónicos, pero con la diferencia de que los datos se dividen
  en tres a la hora del envío: los datos de descarga, es decir, de los
  cables que llevan la información de Internet a las centrales, y de
  estas al ordenador; datos de subida, del ordenador a los cables, a
  las centrales y a Internet; y la voz mediante el teléfono, que se
  separa de la otra información mediante un aparato llamado
  <i lang="en">splitter</i>, instalado tanto en la línea del usuario
  como en la central telefónica. La transmisión simultánea de esos tres
  tipos de datos ocurre en frecuencias diferentes, pero en los mismos
  cables: la línea telefónica sirve como «camino» para la circulación
  de los tres tipos de datos. Ya no hay una llamada a un número
  específico para establecer una conexión, como en la <i lang="en">dial
  up</i>, lo que deja libre el teléfono y no implica un cobro de pulsos
  cuando se accede a Internet, abaratando el servicio. (Foletto,
  <i lang="pt">op.&nbsp;cit.</i>, p. 121).

Para las industrias basadas en la propiedad intelectual, los problemas
con la piratería comenzaron a importar con Napster,
<i lang="en">software</i> creado en 1999 &mdash;año en que también el
formato de distribución de música MP3 se estaba volviendo popular&mdash;
por un joven jáquer llamado Shawn Fanning. Funcionaba de la siguiente
forma: un usuario bajaba el programa, accedía a una interfaz de
búsqueda, buscaba una canción y, si encontraba disponible un archivo con
la canción (o disco) proporcionado por uno o más ordenadores también con
el programa, lo seleccionaba para descarga y esperaba. Las redes de
Internet domésticas en 1999 y en el 2000 eran lentas, con una velocidad
equivalente a entre 1/10 y 1/300 de la velocidad de dos décadas más
tarde; entonces, la espera para la descarga de una canción podría ser
a veces de horas; la de un libro, de algunas decenas de minutos; y la de
una película, de días o semanas. A cualquiera de las velocidades, la
posibilidad de elección era gigantesca y el archivo llegaba gratis.

La idea de Fanning y la de su confundador Sean Parker (que después sería
uno de los primeros accionistas de Facebook) fue crear un programa de
interfaz gráfica amigable, fácilmente descargable en los ordenadores de
la época, para que cualquier persona pudiera buscar sus canciones, en
MP3, por nombre de artista, disco, canciones e incluso géneros enteros,
y realizar la descarga de una copia a su máquina[^190]. Era el hábito de
compartir canciones, popularizado con las grabaciones en cintas de
casete de los años 70 en adelante, llevado a escala global y facilitado
por un formato que permitía al mismo tiempo compartir la música y
mantenerla en los discos duros, CDs y disquetes de la época. Una canción
en MP3 descargada por Napster traía también como novedad el hecho de ser
un «bien no rival»[^191], lo que quiere decir que podría coexistir en
diferentes copias y ser transportada a cualquier aparato que consiguiera
leer (y, por tanto, tocar) las combinaciones de ceros y unos que
comprimían una canción, por más compleja que fuese, en un pequeño
archivo que proporcionaba cerca de 4 MB de información. En ese momento,
no solo el ordenador personal reproducía el formato, sino también un
conjunto de aparatos de sonidos digitales y dispositivos más pequeños,
llamados genéricamente «reproductores de MP3», difundidos a partir del
iPod, de Apple, lanzado en 2001. Por no hablar de los discos compactos
a láser (CD-RW), que &mdash;como las cintas antes, pero con capacidad de
almacenar cerca de 10 horas de cientos de canciones y no solo 60
minutos&mdash; se popularizaron como un medio barato de distribución
física de archivos (capacidad: 700 MB) en esa época, después sustituidos
por el Digital Video Disc (DVD), con un poco más de seis veces la
capacidad del CD (4,7 GB), y los lápices USB, con todavía más espacio
(de 5, 10, 15 GB en adelante).

[^190]: Deak; Foletto, Ambiente digital de difusão: por onde circula a
cultura online?, <cite>BaixaCultura</cite>, 14 jun. 2019. Disponible en
<https://baixacultura.org/ambiente-digital-de-difusao-por-onde-circula-a-cultura-online/>.
[^191]: Otro concepto, creado en Brasil por el profesor de informática
  de la USP Imre Simon y por el investigador Miguel Said Vieira, hablaba
  de «<i lang="pt">rossio não rival</i>» &mdash;véase Simon; Vieira, O
  rossio não-rival (The Non-Rival Commons), <cite>Revista da
  USP</cite>&mdash;.  Ellos argumentaban que la mejor traducción de
  <i lang="en">commons</i> sería <i lang="pt">rossio</i>, que, de
  acuerdo con el diccionario Houaiss, es un «terreno segado y
  aprovechado en común». Según Savazoni, «el objetivo del esfuerzo
  emprendido por los autores era encontrar una forma de traducir un
  término que no encuentra en portugués un equivalente ideal, lo que lo
  hace realmente difícil de asimilar. Finalmente, la idea no ganó muchos
  adeptos. Tanto que varios autores optaron por mantener la expresión en
  el original del inglés, «commons», generando un anglicismo que, en mi
  opinión, mantuvo el concepto al margen en los debates
  político-culturales en portugués (Savazoni; <cite>O comum entre
  nós: da cultura digital à democracia do século XXI</cite>).

La descarga de MP3s gratuita fue la primera gran posibilidad de quiebra,
en Internet, del sistema basado en la venta de bienes culturales
puesto en pie por la explotación de la propiedad intelectual en el siglo
XIX. Sin remunerar a los autores por la descarga, ese sistema, siendo
Napster el primer caso, fue rápidamente atacado: ya al final de 1999, la
Record Industries Association of America (RIAA) abrió un proceso contra
el <i lang="en">software</i> de Fanning y Parker, que en su primer año
de funcionamiento tuvo que responder en los tribunales a la acusación de
piratería y defenderse contra una reclamación de indemnización de cien
mil dólares por música descargada. Incluso con todo el apoyo obtenido en
la época, Napster perdió el proceso y, el año siguiente, tuvo que
descontinuar la compartición de obras registradas con
<i lang="en">copyright</i>, lo que no significaba que necesitara cerrar
sus servicios por completo. Pero no encontró una solución que hiciera un
filtro entre las obras con y sin <i lang="en">copyright</i> &mdash;lo
que sería muy difícil sin interferir en la autonomía y en los datos de
cada una de las personas que publicaban contenido en el
<i lang="en">software</i>&mdash; y, en julio de 2001, terminó su
actividad, para al año siguiente reabrir como un servicio de firmas de
descargas de música y así permanecer hasta hoy[^192].

[^192]: Sin el alcance que obtuvo en sus primeros dos años, el sitio web
  fue comprado por un servicio llamado Rhapsody en 2011 y funciona por
  firma de pago en la dirección: <https://us.napster.com/home>.

La repercusión que el caso tuvo entre artistas[^193] y ciberactivistas;
los más de cien mil usuarios activos que Napster tenía registrados en
1999, en su mayoría jóvenes de todo el mundo que daban sus primeros
pasos en Internet; la portada de la revista <cite>Time</cite> de octubre
del 2000 con la frase «<i lang="en">What’s Next for Napster?</i>» («¿qué
es lo siguiente para Naspter?») y una foto del joven (19 años) Fanning
con gorra y unos cascos enormes: todos indicios de que el proceso no
acabaría ahí. Programas que funcionaban de forma similar, basados en el
intercambio *p2p*, se extendieron por la red, fue el caso de Gnutella,
Grokster, Kazaa, FreeNet, Morpheus, Soulseek, entre otros, que llevaron
adelante los mismos procedimientos de libre intercambio de archivos,
mientras la RIAA siguió e intensificó los impopulares procesos contra
usuarios que compartían archivos con esos programas[^194].

[^193]: Uno de los casos más emblemáticos de esa época fue la denuncia
  que Metallica, teniendo como portavoz a su batería y compositor Lars
  Ulrich, le puso en 2000 a Napster buscando no solo el dinero de
  Fanning y su <i lang="en">software</i>, sino también el de sus
  usuarios que descargaban las canciones del grupo, admiradores del
  sonido del grupo. Si no era algo inédito, era como mínimo raro ver a
  un artista denunciar a sus propios fanes. Sobre ese caso, véase
  <https://en.wikipedia.org/wiki/Metallica_v._Napster,_Inc>.
[^194]: Solo en 2004 hubo 264 denuncias interpuestas por la RIAA,
  denuncias que, según Valente, <i lang="lt">op.&nbsp;cit.</i>, fueron
  escogidas de forma ejemplar: «La ley norteamericana, dirigida
  originalmente a personas jurídicas, y no físicas, determinaba una
  indemnización de entre 750 a 30 mil dólares, aumentando a 150 mil en
  caso de conductas con dolo, por cada obra cuyos derechos de autor
  hubieran sido infringidos. Las acciones causaron gran conmoción
  pública, en especial por las grandes cifras que implicaban, pero
  también porque la identificación de usuarios mediante dirección IP
  llevaba a errores e hizo que la RIAA denunciara a individuos
  equivocados, como fue el caso de una denuncia a un muerto y una abuela
  que no sabía descargar música. El resultado, para la industria del
  contenido, fue una antipatía y una dificultad de posicionamiento
  resultante en el espacio público».

En los años siguientes, la reducción del tamaño de los dispositivos
digitales, y consecuentemente su abaratamiento, dio aún más espacio de
almacenamiento de archivos a CD-ROMs, DVDs, discos duros y lápices USB.
La popularización de nuevas tecnologías de transmisión de datos, como
el ya citado ADSL (que popularizó el concepto de *banda ancha*[^195]),
con la televisión por cable, ondas de radio y satélites, y después los
sistemas 2, 3 y 4G también para los móviles inteligentes, triplicarían
la velocidad y reducirían el tiempo de descarga y subida de contenidos,
mientras el acceso a la red se volvía más barato y sencillo en todo el
planeta &mdash;sobretodo en el norte global&mdash;. En ese escenario, la
batalla por el libre intercambio de archivos se convirtió en una
discusión ineludible. La cultura libre se extendía como un símbolo de la
libertad de acceso y circulación de información y encontraba espacio
para fortalecerse en los servicios de intercambio de archivos y entre
personas que descargaban contenido (con o sin
<i lang="en">copyright</i>, muchos no sabían o no veían diferencia)
libremente y querían preservar esa práctica. En aquella época, Lessig
dijo que «mientras que en el mundo analógico la vida prescinde del
<i lang="en">copyright</i>, en el mundo digital la vida está sujeta a la
ley de <i lang="en">copyright</i>»[^196], una frase que demuestra un
cierto espíritu en esos años de que la principal cuestión política y
legal en la red giró en torno a la descarga: su legalidad o no, su
impacto en la construcción del conocimiento, en el acceso a la
información, en la cadena de producción de las artes, en la
sustentabilidad de proyectos culturales, en la necesidad de una reforma
de las leyes de derecho de autor para que estas dejaran de criminalizar
una práctica habitual de millones de personas.

[^195]: Banda ancha es un concepto utilizado para, de modo general,
  definir conexiones más rápidas que las de línea conmutada mediante
  módems analógicos de 56 kbps. La recomendación de la Unión
  Internacional de Telecomunicaciones define banda ancha como la
  capacidad de transmisión que es superior a 2 o 5 megabits por segundo
  (mbps). La variación de lo que se considera banda ancha alrededor del
  planeta, sin embargo, es diversa; Colombia estableció una velocidad
  mínima de 1024 kbps y Estados Unidos de 25 mbps, por ejemplo. En
  Brasil todavía no hay un consenso que indique cuál es la velocidad
  mínima para considerar una conexión de banda ancha. Fuente:
  <https://pt.wikipedia.org/wiki/Banda_larga>.
[^196]: Lessig, <cite>Code and Other Laws of Cyberspace</cite>, p. 192.

Los grandes intermediarios ya citados, representados por organizaciones
que tenían dinero suficiente para contratar a varios abogados y llegar
hasta el final en cualquier proceso judicial, hicieron surgir en la
Justicia algunas figuras de la libre compartición en la red, es el caso
del sitio web de <i>torrents</i> The Pirate Bay (TPB). Presionados por
empresas relacionadas con la Motion Pictures Association (MPAA),
promotores suecos, país de origen de The Pirate Bay, presentaron
acusaciones el 31 de enero de 2008 contra Fredrik Neij, Gottfrid
Svartholm y Peter Sunde, que administraban el sitio web, y Carl
Lundström, empresario sueco que había financiado desde el principio a
TPB, por ayudar a publicar contenidos con derechos de autor. Fueron
condenados el 17 de abril de 2009 a una pena de prisión de un año y al
pago de 2,7 millones de euros a las empresas representadas por la MPAA,
como 20th Century Fox, Columbia Pictures, Warner Bros, EMI, entre otras.
El caso fue apelado en 2010, lo que redujo la pena de prisión de todos
los acusados (4 a 10 meses). Después de algunos años fugitivos,
cumplieron sus penas y desde 2015 están libres. El sitio web, agregaba
enlaces, pero no alojaba los contenidos protegidos por derechos de autor
reclamados, se mantiene en funcionamiento mediante varios espejos[^197].

[^197]: Sobre el caso, véase el documental <cite>The Pirate Bay: Away
  from the Keyboard</cite>, dirigido por Simon Klose y publicado en
  2013, disponible entero en
  <https://www.youtube.com/watch?v=eTOKXCEwo_8>, y la página en
  Wikipedia <https://en.wikipedia.org/wiki/The_Pirate_Bay_trial>.

En la década del 2000, las organizaciones relacionadas con la industria
de los intermediarios volvieron también comunes las campañas
antipiratería en las que insistían en la comparación de un archivo, copiable
o no rival, con un bien físico rival como un CD o DVD; que una película
descargada era un DVD vendido menos y, con ello, se ayudaba a «matar» a
los artistas de hambre[^198]; que la piratería «acababa con la emoción»,
pues el archivo descargado no tenía la misma calidad del visto en DVD o
en el cine[^199]; que, «cuando descargas archivos MP3, también estás
descargando el comunismo», en una imagen hoy histórica en la que un
Lenin vestido con uniforme militar y cabeza de diablo aparece al lado de
un joven blanco con auriculares frente a un ordenador. Hubo otros lemas
parecidos en campañas, pero ninguna llegó a acabar con el intercambio
de archivos; un sitio web cerrado era como cortar una cabeza de la Hidra
de Lerna, otra crecía en su lugar. Pero, aún así, sirvieron para
provocar la destrucción de un gran número de copias, el cierre de sitios
web, denuncias a personas y, principalmente, para mostrar a la industria
de los intermediarios culturales &mdash;principalmente
estudios y distribuidoras de cine y vídeo para la televisión,
discográficas y distribuidoras de música y editoriales de libros&mdash;
que no sería de esa forma como acabarían con el intercambio de archivos.

[^198]: Un ejemplo está disponible en
  <https://baixacultura.org/propagandas-antipirataria-3>.
[^199]: Lema de una campaña de la Honour Intellectual Property (HPI) que
  presentaba superhéroes como Superman, el Hombre de Hierro y otros
  salvando el mundo con las palabras, en inglés, «<i lang="en">Piracy
  kill the real thrill</i>». Más detalles en
  <https://baixacultura.org/propagandas-antipirataria-o-retorno-2>.

En los años 2000 también se difundió la idea de liberar bienes
culturales y educativos ya existentes para su uso, compartición y
reapropiación. En la educación, a partir de 2002, la ya citada comunidad
internacional REA surgió con el objetivo de promover el acceso, uso, y
reutilización de bienes educativos. En los museos, bibliotecas e
instituciones de memoria, hubo un movimiento parecido al de adopción de
las licencias de Creative Commons, en particular, como lema para hacer
las colecciones de esas instituciones más accesibles, conectadas y
disponibles para que los usuarios pudieran contribuir, participar y
compartirlas[^200], en el movimiento llamado Open GLAM (Gallery,
Library, Archive, Museum). Arraigadas en los principios éticos del
<i lang="en">software</i> libre y los recombinantes de la cultura libre,
ambas iniciativas se hicieron espacio en varias instituciones y
gobiernos de diferentes lugares del planeta y de las más variadas
ideologías. Fueron, en 2020, después de mucha organización, derrotas y
aprendizajes por el camino, los ámbitos donde más obras libres se
encontraban legalmente.

[^200]: En «Os 5 princípios do Open Glam», <cite>Creative Commons
  br</cite>, 24 sep. 2019. Disponible en
  <https://br.creativecommons.org/os-5-principios-do-open-glam>.

### V.

El movimiento del intercambio libre de archivos en la red criminalizado
como piratería solo disminuiría en fuerza en la década siguiente, con la entrada
de dos grandes actores que, juntos, transformarían Internet en algo
bastante diferente del de los primeros años. El primero fueron los
servicios de <i lang="en">streaming</i> que, poco comunes en los años
2000, se convirtieron en un gasto básico mensual, como agua y luz, para
millones de familias de clase media en varios lugares del mundo a partir
de los años 2010; y que contó con, es importante destacar, el
considerable aumento de la velocidad en la red en ese período, con
fibras ópticas que permiten una velocidad por lo menos cien veces mayor
que al inicio de los años 2000.

La misma industria que promovía campañas antipiratería supo escuchar una
demanda reclamada por algunos de quienes usaban los <i>torrents</i> para
tener acceso a diversas producciones culturales mundiales: *hazlo mejor
y pago*[^201]. Crearon (o se aliaron para crear) plataformas con mucha
música, películas y series disponibles de forma sencilla, barata, en una
interfaz amigable, ya subtituladas en muchos idiomas (es el caso de las
películas y las series), con cada vez algoritmos más potentes que
aprendían los gustos de la gente y mostraban otros productos que el
suscriptor podría querer de forma cada vez más precisa. Funcionaba,
además, en los ya diversos dispositivos (móviles inteligentes,
tabletas) que empezarían a hacerse cada vez más pequeños, potentes y
populares, y con ello consiguieron tanto ganarse a aquellos que
consideraban difícil bajarse una película (o una canción) como legalizar
el consumo cultural en línea, ya que todo lo que está en Netflix,
Spotify, Amazon Prime y Deezer, algunos de los más populares de esos
servicios en 2020, se publica cumpliendo la ley[^202]. No acabaron con
la descarga de par a par, con <i>torrent</i>, pero volvieron esa opción
más trabajosa, restringida a grupos menores &mdash;a comienzo de la
década de 2020, todavía un número considerable (y difícil de medir) de
personas, pero considerablemente menor que en las décadas
anteriores&mdash;.

[^201]: Sobre esa idea, léase «Faça melhor que eu pago: desafio à
  indústria», <cite>Leo Germani</cite>, 10 ene. 2010. Disponible en
  <https://web.archive.org/web/20210226091135/https://leogermani.com.br/2010/01/10/faca-melhor-que-eu-pago-desafio-a-industria/>.
[^202]: Deak; Foletto, <i lang="lt">op.&nbsp;cit.</i>

El segundo actor que entró en escena y disminuyó el movimiento del
intercambio libre en Internet fueron las redes sociales, primero Orkut
(año de lanzamiento: 2004), después MySpace (entre 2005 y 2008, la red
social más popular del planeta) y finalmente, y a mucho mayor escala,
Facebook (100 millones de usuarios en 2008, 2,5 billones en 2020).
Navegar en Internet era una frase común en los años 1990 y 2000 para
designar el hábito cotidiano de entrar en un sitio web y, de este,
ir a otro, hasta perderse, horas después, en una página a la que no se
sabía bien cómo se había llegado. <i lang="fr">Flanêur digital</i> era
otra expresión utilizada para identificar a ese caminante sin rumbo por
la red, que se perdía en las esquinas de los blogs como un peatón por
las calles de las grandes ciudades. Facebook, en particular, cambió ese
movimiento; trajo la ciudad entera para que el caminante andara sin
salir del lugar. Una ciudad construida por una única empresa privada
que, con cada movimiento hecho por sus habitantes, producía un dato, el
cual, recombinado con otros miles, se volvía muy rentable para que la
empresa lo comercializara &mdash;el «petróleo del siglo XXI, en la
expresión que se volvió un cliché en boca de gobernantes y futurólogos
junto a otra también generalizada a partir de los años 2010:
<i lang="en">big data</i>&mdash;.

Hablar con personas, escribir, publicar, hacer fotos, ver vídeos y
trabajar, actividades que antes se hacían en lugares diferentes de
Internet, empezaron a poder realizarse en un único lugar, en Facebook
&mdash;que después, con planes cada vez más ambiciosos de crear un
Internet paralelo en sus dominios, se transformó en dos, con la compra
de Instagram (en 2012, por 1 billón de dólares), y en tres, con WhatsApp
(en 2014, por 16 billones de dólares[^203]). Junto a las otras de las
llamadas <i lang="en">big techs</i> (Google, Amazon, Apple y Microsoft),
la empresa creada por Mark Zuckerberg cambió la manera de que las
personas produjeran y consumieran información. Pasó a decir dónde, cómo
y con qué forma la información empezaría a circular por la red &mdash;y
no eran más los sitios web, <i>torrents</i> y blogs creados para el
libre intercambio de archivos, sino un único espacio cerrado, vigilado y
monopolizado, una herramienta de *modulación* de opiniones y
comportamientos según los caminos ofrecidos por los cada vez más
complejos (y secretos) algoritmos[^204]&mdash;.

[^203]: Fuente:
  <https://tecnoblog.net/151547/facebook-compra-whatsapp-16-bilhoes-de-dolares>.
[^204]: Véase Souza; Avelino; Amadeu; <cite>A sociedade de controle:
  manipulação e modulação nas redes digitais</cite>.

Fue el fin del corto verano del Internet libre[^205] y el comienzo de
una cierta *resaca de Internet*[^206], en que críticas a ciertos
comportamientos ingenuos adoptados en las dos primeras décadas de
Internet empezaron a hacerse frecuentes &mdash;entre estas a la cultura
libre y, particularmente, al <i lang="en">copyleft</i>&mdash;. El
sociólogo español César Rendueles, en un libro que es todo un análisis
de la creencia ciberfetichista de que Internet resolvería todos nuestros
problemas sociales, económicos y políticos (<cite>Sociofobia</cite>,
2016), rescata un aspecto importante en esa crítica posresaca: la libre
circulación de información y el intercambio de archivos puede verse
también como una desregulación completa, cercana a la que ocurre en el
libre mercado &mdash;que estaba en el origen de la creación del
<i lang="en">copyright</i> y de la propuesta inicial de Lessig de la
cultura libre. Guarda una relación, por tanto, con la universalización
del mercado capitalista desarrollado a partir del siglo XIX, y difunde
el «dogma de que la coordinación social surge espontáneamente de la
interacción individual egoísta, sin necesidad ninguna de mediación
institucional»[^207].

[^205]: Esta expresión viene del <i lang="en">remix</i> de <cite>O curto
  verão da anarquia</cite>, de Hans Magnus Enzensberger, adaptada por
  Paulo José Lara (vulgo Pajeh) en una conversación de bar en 2019 en
  São Paulo, con el nombre de *algún proyecto que vendrá*.
[^206]: En 2018, sinteticé esa idea en un texto llamado «Ressaca da
  internet, espírito do tempo», escrito en BaixaCultura. Un fragmento:
  «No sabía, o no quería creer, o no quería escribir ni decir
  públicamente que no creía, que los grandes actores de Internet
  transformarían Internet en lo que es hoy, un espacio cerrado donde
  nosotros estamos presos en burbujas algorítmicas privadas de cuyo
  funcionamiento poco o nada sabemos &mdash;y solo en un año, con Trump
  y el Brexit, comenzamos a ver las posibilidades nefastas para la
  política de esa disposición entre personas y sistemas técnicos como
  Facebook&mdash;». Disponible en
  <http://baixacultura.org/ressaca-da-internet-espirito-do-tempo>.
[^207]: Rendueles, <cite>Sociofobia: mudança política na era da utopia
  digital</cite>, p. 94.

La creencia ciberfetichista criticada por Rendueles fue muy popular en
los primeros años de Internet y moldeó una forma de pensar hasta hoy
dominante en las noticias de tecnologías y en el discurso de las
<i lang="en">startups</i> digitales. Un texto conocido de esa época la
muestra de manera más nítida: la «Declaración de Independencia del
ciberespacio»[^208], publicada el 8 de febrero de 1996, escrito por John
Perry Barlow &mdash;uno de los creadores de la EFF e impulsor de
Creative Commons&mdash; en respuesta a un hecho que regularía las
telecomunicaciones en Estados Unidos y por primera vez incluía a
Internet[^209]. «Gobiernos del Mundo Industrial, vosotros, cansados
gigantes de carne y acero, vengo del Ciberespacio, el nuevo hogar de la
Mente. En nombre del futuro, os pido en el pasado que nos dejéis en paz.
No sois bienvenidos entre nosotros. No ejercéis ninguna soberanía sobre
el lugar donde nos reunimos»[^210].

[^208]: Barlow, «Declaración de independencia del ciberespacio», en el
  Foro Económico Mundial, Davos, Suiza, 8 feb. 1996. Disponible en
  <https://es.wikisource.org/wiki/Declaraci%C3%B3n_de_independencia_del_ciberespacio>.
[^209]: Habiendo sido uno de los creadores de la Electronic Frontier
  Foundation (EFF) en 1990, Barlow acompañaba y escribía sobre aspectos
  económicos, políticos y tecnológicos de Internet en esa época. La
  declaración es un texto que hace eco de otra reflexión llamada «La
  economía de las ideas», publicada en enero de 1994 en la revista
  <cite>Wired</cite> (<https://www.wired.com/1994/03/economy-ideas>), y
  fue encomendado para un proyecto llamado 24 Hours in Cyberspace, un
  evento que tuvo como objetivo reunir a fotógrafos, periodistas,
  editores, programadores y diseñadores para crear, el día 8 de febrero
  de 1996, una «cápsula del tiempo» colaborativa de la vida en línea de
  la época. Fuente: <https://en.wikipedia.org/wiki/24_Hours_in_Cyberspace#cite_note-4>.
[^210]: <i lang="lt">Ibidem</i>.

Sus primeras palabras ya acercan, como en un manifiesto, una visión
utópica e idealizada de que Internet sería algo externo a la sociedad,
expresada de forma más evidente en otro fragmento del texto: «Estamos
creando nuestro propio Contrato Social. Esta autoridad se creará según
las condiciones de nuestro mundo, no del vuestro. [...] Nuestro mundo es
diferente. Vuestros conceptos legales sobre propiedad, expresión,
identidad, movimiento y contexto no se aplican a nosotros. Se basan en
la materia. Aquí no hay materia»[^211].

[^211]: <i lang="lt">Ibidem</i>.

Barlow, también poeta y cantante de uno de los grupos más conocidos de
la contracultura jipi de la Costa Oeste de los Estados Unidos, Grateful
Dead, supo interpretar la novedad que Internet representó en la historia
de la humanidad y se alegró con la promesa de que esa libertad
transformaría para mejor toda la sociedad.

> Estamos creando un mundo en el que todos pueden entrar, sin
> privilegios o prejuicios debidos a la raza, el poder económico, la
> fuerza militar o el lugar de nacimiento. Estamos creando un mundo donde
> cualquiera, en cualquier sitio, puede expresar sus creencias, sin
> importar lo singulares que sean, sin miedo a ser coaccionado al silencio
> o al conformismo.
>
> [...]
>
> Nuestras identidades no tienen cuerpo, así que, a diferencia de
> vosotros, no podemos obtener orden por coacción física.
>
> Creemos que nuestra autoridad emanará de la moral, de un progresista
> interés propio, y del bien común. Nuestras identidades pueden
> distribuirse a través de muchas jurisdicciones.[^212]

[^212]: <i lang="lt">Ibidem</i>

Otro texto de esa época, escrito por una revista digital inglesa llamada
<cite>Mute Magazine</cite>[^213] en 1994, se volvería conocido al
analizar esa idea tecnoutópica: <cite>La ideología californiana</cite>,
de Richard Barbrook y Andy Cameron. La ideología en cuestión sería una
mezcla de las actitudes bohemias y antiautoritarias de la contracultura
jipi de la Costa Oeste de los Estados Unidos con el utopismo tecnológico
(otro nombre para el ciberfetichismo) y el (neo)liberalismo económico.
Una mezcla de ideas un tanto inusual &mdash;«¿quién pensaría que una
mezcla tan contradictoria de determinismo tecnológico e individualismo
libertario se convertiría en la híbrida ortodoxia de la era de la
información?»[^214]&mdash; caracteriza a las <i lang="en">big techs</i>
de los años 1990 en adelante y alimenta la creencia de que todos pueden
ser «<i lang="en">hip and rich</i>». Para ello bastaría creen en tu
propio trabajo y tener fe en que las nuevas tecnologías informáticas van
a emancipar al ser humano al ampliar la libertad de cada uno y reducir
el poder el Estado burocrático[^215].

[^213]: Barbrook; Cameron, The Californian Ideology. <cite>Mute</cite>,
v. 1, n. 3, 1 sep. 1995. Disponible en
<https://www.metamute.org/editorial/articles/californian-ideology>.
[^214]: Barbrook; Cameron, <cite>A ideologia californiana</cite>, p. 10.
[^215]: <i lang="lt">Ibidem</i>.

Las palabras de Barbrook y Cameron en 1995 son, casi dos décadas
después, precisas y premonitorias:

> Por un lado, estos artesanos de tecnología punta no solo suelen estar
> bien pagados, sino que también tienen una considerable autonomía para
> determinar su ritmo y su lugar de trabajo. En consecuencia, la división
> cultural entre el jipi y el «hombre de organización» se ha vuelto
> bastante borrosa. Pero, por otro lado, estos trabajadores están
> vinculados por los términos de sus contratos y no tienen garantía alguna
> de un trabajo continuado. Al carecer del tiempo libre de los jipis, el
> trabajo se ha convertido en la principal vía para la autorrealización
> para buena parte de la «clase virtual».[^216]

[^216]: <i lang="lt">Ibidem</i>.

La ideología californiana refleja tanto las disciplinas de la economía
de mercado como las libertades del «artesano jipi», un híbrido unido por
la fe, a veces ciega, de que la tecnología digital resolverá los
problemas y creará una sociedad igualitaria y sin privilegios o
prejuicios donde, como muy bien representa «Declaración de
independencia del ciberespacio», de Barlow, todos puedan expresar sus
opiniones sin importar cómo o cuán singulares y diferentes sean estas.

Con el avance del <i lang="en">streaming</i> y de las redes sociales
se vería más claramente que una sociedad donde las tecnologías de la
información conectadas en red lo deciden todo no es necesariamente
mejor, y puede ser mucho peor. Un sistema algorítmico fuerte al que,
como un <i lang="lt">deus ex machina</i>, se recurra para resolver todo
al final defiende la creencia también conocida como solucionismo
tecnológico &mdash;la idea de que basta un programa, un algoritmo,
*más tecnología*, para resolver y arreglar todos los problemas del
mundo&mdash;. Es la búsqueda de una salida mágica, rápida y supuestamente
indolora que descarta las alternativas institucionales o construidas por
la organización de la sociedad civil, más lentas y complejas, y puede
ser comprada terminada, ofrecida por empresas creadas o de alguna forma
relacionadas con los servicios proporcionados por las
<i lang="en">big techs</i>. Una vía que, durante la pandemia del nuevo
coronavirus en 2020, pasó por una especie de túnel de aceleración
ultraveloz, con la proliferación de aplicaciones que evaluaban, por
ejemplo, los desplazamientos de las personas en cuarentena, o rastreaban
y determinaban quién podría o no salir de casa a partir de un conjunto
de datos obtenidos y procesados por algoritmos privados[^217]. Los
mismos datos usados para algo considerado positivo porque atañe a la
salud de toda la sociedad &mdash;el control del movimiento de personas
que podrían transmitir un virus&mdash; pueden también ser usados para
una intrusión todavía mayor de la publicidad de productos
personalizados. Lo que genera aún una mayor clasificación &mdash;y
consecuente exclusión&mdash; de las personas según sus hábitos de
consumo en Internet e impulsa la vigilancia de todos los hábitos de una
persona en la red.

[^217]: Reportaje de Sam Biddle, «Coronavírus traz novos riscos de abuso
  de vigilância digital sobre a população». <cite>The Intercept</cite>,
  6 abr. 2020, disponible en
  <https://theintercept.com/2020/04/06/coronavirus-covid-19-vigilancia-privacidade>,
  dice que en Corea del Sur, en Taiwán y en Israel, las autoridades
  usaron los datos de localización de móviles inteligentes para imponer
  cuarentenas individuales. Palantir, empresa contratada por la NSA de
  los Estados Unidos, ayudó al Servicio Nacional de Salud de Gran
  Bretaña a rastrear infecciones. Aplicaciones que hacen uso de la alta
  precisión de los sensores presentes en los móviles inteligentes para
  imponer distancia social o mapear los movimientos de los infectados
  fueron implantadas en Singapur, en Polonia y en Kenia. En Brasil, uno
  de los principales operadores de Internet, Vivo, dice que anonimiza
  los datos de localización de sus usuarios, pero, en la práctica, es
  posible localizarlos, lo que viola los derechos individuales por
  afectar a la privacidad de cada persona sin autorización y
  conocimiento, como muestra un reportaje de Tatiana Dias en <cite>The
  Intercept Brasil</cite>: «Vigiar e lucrar»
  (<https://theintercept.com/2020/04/13/vivo-venda-localizacao-anonima>).
  Esos mismos datos de localización fueron entregados a empresas y
  gobiernos para la monitorización de la pandemia en Brasil.

Además de poner aún más en riesgo la privacidad de los usuarios,
soluciones tecnológicas terminadas, producidas por empresas privadas y
compradas como salvadoras por gobiernos, no tocan lo que se constituyó
como la institución central de la vida moderna: el mercado[^218]. La
crítica de Rendueles al <i lang="en">copyright</i> se basa también en el
hecho de que el problema principal a resolver con este es la ruptura de
las barreras a la libre circulación de información y al acceso a bienes
culturales, sin, no obstante, y en la mayoría de los casos, tocar las
condiciones sociales de ese mercado. La manera en que la ya citada
energía viva de las personas pasa a ser explotada por empresas que no
las tratan más como trabajadores, sino como colaboradores,
cambiando derechos laborales históricamente conquistados por una
supuesta libertad de escoger sus horarios de trabajo, tendió a ser,
durante buena parte de la existencia del <i lang="en">copyleft</i> hasta
entonces, un problema secundario. La fuente de los problemas escogida no
sería el mercado de la información ni el mercado de trabajo, sino las
barreras a la circulación y al uso de la información[^219]. Otro aspecto
de la crítica al <i lang="en">copyleft</i> es que más acceso a
información o más obras descargadas no necesariamente significa
conciencia crítica. La desinformación y el crecimiento de un mercado de
noticias falsas (<i lang="en">fake news</i>) fue, tanto como la
proliferación del mediactivismo[^220], uno de los resultados de la
«liberación del polo emisor de información» para que cualquiera
&mdash;con acceso a Internet&mdash; pudiese hablar en un blog, en sitio
web o en perfil en redes sociales. Una consecuencia que tiene como
resultado que el mercado (de <i lang="en">software</i> y productos
tecnológicos en particular) se mantenga intocable, sin regulación
estatal o auditoría externa, lo que en los últimos años ha traído
consecuencias como la proliferación de las noticias falsas o el uso de
estas para la manipulación de multitud de personas para fines
político-electorales, es el caso de las elecciones tanto de Donald Trump
en Estados Unidos en 2016 como de Jair Bolsonaro en Brasil en 2018.

[^218]: Morozov, Solucionismo, nova aposta das elites globais,
  <cite>Outras Palavras</cite>, 23 abr. 2020. Disponible en
  <https://outraspalavras.net/tecnologiaemdisputa/solucionismo-nova-aposta-das-elites-globais>.
[^219]: Rendueles, <i lang="lt">op.&nbsp;cit.</i>, p. 87.
[^220]: Mediactivismo es una identificación que, popularizada en los
  años 90 y 2000, se relaciona con movimientos de contestación que
  «aumentan la conciencia pública sobre la influencia de los medios de
  comunicación y fomentaron las demandas de democratización y acceso
  público a los medios», como dice la investigadora Stepania Milan,
  «When Algorithms Shape Collective Action: Social Media and the
  Dynamics of Cloud Protesting», en <cite>Social Media + Society</cite>.
  En algunos casos, es usado como sinónimo de medios alternativos y
  medios ciudadanos, entre otros términos. Sobre las definiciones
  posibles de mediactivismo escribí un texto (Foletto, «Midiativismo,
  mídia alternativa, radical, livre, tática: um inventário de
  conceitos semelhantes»).

La ruptura de todas las barreras de acceso a la palabra en la red
alimentó &mdash;y fue alimentada&mdash; por lo que el científico y
escritor Jaron Lanier llama <i lang="en">Bummer</i>
(<i lang="en">Behaviors of User Modified Made into an Empire for Rent</i>[^221]), una
máquina estadística (presente en las redes sociales y en los algoritmos
de plataformas de <i lang="en">streaming</i>, por ejemplo) que vive en
las nubes informáticas y, con el pretexto de organizar la información
del mundo, ha modificado el comportamiento de miles de personas.
La <i lang="en">Bummer</i>, dice Lanier, trata de «optimizar la vida», y
al hacer eso iguala cualquier tipo de información: lo que importa es la
circulación de datos, sean los que sean. Es en ese contexto en el que la
proliferación de noticias falsas se consagra al adquirir mayor valor de
cambio que las verídicas, siendo más baratas de producir y
potencialmente más fáciles de difundir, dirigidas de acuerdo con los
intereses de grupos específicos para reforzar sus perspectivas previas
sobre la realidad[^222]. En este escenario, «más cercano a una pesadilla
reaccionaria que a un comunitarismo», como dice Rendueles[^223], no es
casualidad que se hayan discutido nuevamente las formas de regulación
estatal de las <i lang="en">big techs</i>, especialmente en la Unión
Europea y los Estados Unidos, a partir de leyes de protección de datos
personales, de propuestas de moderación de la difusión de noticias falsas
en las redes sociales y de impuestos a los beneficios de las
<i lang="en">big techs</i>[^224]. Es curioso observar que estas
propuestas de regulación ya estaban, entre otros lugares, en varios
fragmentos del ya citado ensayo de Barbrook y Cameron, de 1995, que
apunta a un futuro digital como una mezcla de «intervención estatal,
emprendimiento capitalista y cultura hazlo tú mismo»[^225].

[^221]: Lanier lo define en <cite>Diez razones para borrar tus redes
sociales de inmediato</cite>, libro que hace una crítica feroz a las
redes sociales, los principales ejemplos de actuación de esa máquina
estadística. En la p. 24, usa una fórmula curiosa para memorizar los
seis componentes de <i lang="en">Bummer</i>, desarrollando cada uno en
las páginas siguientes de su libro: «A es de Adquisición de la Atención
que lleva al dominio de los idiotas; B es de Buitrear en la vida de todo el mundo;
C es de Colmar de contenido la mente de las personas; D es de Dirigir el
comportamiento de las personas de la manera más sibilina posible; E es
de Embolsarse dinero por dejar que los peores idiotas Engañen
disimuladamente a todo el mundo; F es de Falsas muchedumbres y una
sociedad Falsaria».
[^222]: El análisis del valor de la información periodística verdadera y
  de la proliferación de noticias falsas a partir de los oligopolios de
  tecnologías informáticas es uno de los enfoques que el profesor e
  investigador Elias Machado ha trabajado para su tesis de libre docencia
  en la UFSC. Él la ha discutido públicamente en las propias redes
  sociales y me la anticipó, en algunas conversaciones virtuales, de
  donde he formulado este fragmento.
[^223]: Rendueles, <i lang="lt">op.&nbsp;cit.</i>, p. 102.
[^224]: Sobre impuestos: en 2020, Europa estableció un paquete
  tributario que tuvo como medidas garantizar que los países del bloque
  intercambien los ingresos generados por ventas en plataformas
  <i lang="en">online</i>, que será implementado en los años siguientes.
  Fuentes: <https://www1.folha.uol.com.br/mercado/2020/07/europa-lanca-pacote-tributario-para-apertar-cerco-a-gigantes-digitais.shtml>.
[^225]: Barbrook; Cameron, <i lang="lt">op.&nbsp;cit.</i>, p. 37.

### VI.

La elección del movimiento de la cultura libre de tratar la barrera al
acceso de información y al conocimiento como su principal cuestión tiene
varias justificaciones, algunas ya presentadas aquí, buena parte de
ellas relacionada con la cuestión del campo de origen de su término, la
producción de <i lang="en">software</i>. Para Rendueles, y también
Dimitry Kleiner en <cite>The Telekommunist Manifesto</cite> (2010), el
<i lang="en">copyleft</i> fallaría al no comprender las diferencias
implícitas entre el <i lang="en">software</i> y la cultura libre. En el
primero, las condiciones sociales de remuneración de los programadores
de <i lang="en">software</i>, por ejemplo, no suelen ser dependientes de
la venta por unidad de producto (programa), sino por servicio mantenido,
dessarrollo, personalización y mantenimiento, entre otras formas aún más
complejas que implican la venta ligada a otros productos. Es una
práctica en el ámbito del desarrollo de <i lang="en">software</i>
liberar un código y vender servicios relacionados con este también
porque ese procedimiento, además de ser parte del modo colaborativo y
fragmentado en que el <i lang="en">software</i> se produce desde el
principio[^226], no impide la remuneración de sus desarrolladores. No
solo es posible liberar un código y vender servicios relacionados con él
en paralelo, sino que también existe un mercado de tecnologías abiertas,
inspirado en la idea del movimiento del código abierto, en el que uno de
los principales partícipes es Microsoft, histórico opositor al
<i lang="en">software</i> libre[^227].

[^226]: «El desarrollo de <i lang="en">software</i> puede y debe ser
  fragmentado. Hay todo un mito sobre programadores independientes
  trabajando en su garaje de madrugada, pero la verdad es que la
  división de un gran proyecto en un conjunto de problemas a resolver
  colectivamente en una especie de cadena de montaje no es una opción, y
  sí una necesidad técnica». Rendueles, <i lang="lt">op.&nbsp;cit.</i>, p.
  90.
[^227]: Un texto de la Linux Foundation explica cómo funciona el trabajo
código abierto de Microsoft: Baker, The Open Source Programa at
Microsoft: How Open Source Thrives, <cite>The Linux Foundation</cite>, 2
mar. 2018, disponible en
<https://www.linuxfoundation.org/blog/2018/03/open-source-program-microsoft-open-source-thrives>.

En este aspecto, la situación de la cultura es un poco diferente.
Trabajadores del sector, como músicos, en su mayoría autónomos no
asalariados (al contrario que programadores de
<i lang="en">software</i>), tienen como principal fuente de ingresos la
ejecución individual en directo de sus obras (<i lang="en">shows</i>) y
un porcentaje por obra comercializada[^228]. La liberación de una
canción y la venta de servicios relacionados con esta, como en el
<i lang="en">software</i>, sigue siendo posible &mdash;y práctica de
muchos artistas, sea por principios éticos o, principalmente, como forma
de «cebo» para la venta de <i lang="en">shows</i>&mdash;. Pero, no siendo
asalariados como los desarrolladores de <i lang="en">software</i>, es
más difícil que exista un servicio agregado que ofrecer que compense su
coste. Para Rendueles y Kleiner, liberar gratuitamente la información
usada para la producción de un programa &mdash;el código&mdash; no
altera la remuneración (en la mayoría de los casos, ya garantizada) de
sus productores, mientras que publicar de forma completa y gratuita una
información musical &mdash;una canción o un disco&mdash; modificaría las
ganancias de un músico autónomo. Ese argumento, dice Rendueles, fue uno
de los que habría limitado el alcance de las licencias libres basadas en
el <i lang="en">copyleft</i> para el ámbito cultural.

[^228]: Rendueles, <i lang="lt">op.&nbsp;cit.</i>, p. 86.

Hay también en esa posición una excepción importante: licenciar una obra
cultural para modificación y también para uso comercial, como el
<i lang="en">copyleft</i> propone en primer lugar para el
<i lang="en">software</i>, puede volverse, en la práctica, poco
razonable para músicos y otros artistas independientes. Por más
creatividad que haya en la escritura de líneas de código, este es un
conjunto de instrucciones que serán ejecutadas por una máquina, un tipo
de producto intelectual que tiene una función específica que depende de
otro objeto para tener lugar. No cabe duda de que una obra de arte
como una canción o una película tiene como objetivo una apreciación
estética o de entretenimiento que no suele ser solo funcional. Un
programa y una obra cultural son objetos de naturalezas diferentes,
producidos de manera y para fines distintos, lo que significaría que
tampoco sus productores deberían ser tratados de la misma forma.

El mismo Stallman comenta la cuestión: «para las novelas, y en general
para las obras que se utilizan como entretenimiento, la redistribución
textual no comercial podría ser una libertad suficiente para los
lectores»[^229]. El impulsor del <i lang="en">copyleft</i> argumenta que
ese tipo de obra, así como trabajos que informan de la opinión de una
persona (memorias, artículos de opinión y científicos), deberían tener
posibilitadas limitadas de uso, pues son diferentes de las obras que él
categoriza como «funcionales», en las que se incluyen recetas, obras
educativas y los programas. Él entonces defiende que, en los casos de
obras estéticas y que informan de la opinión de alguien, tener la
libertad de hacer copias ya sería suficiente para que cualquier persona
pudiera compartir cómo y dónde quisiera, prohibiendo el uso comercial y
ciertas posibilidades de modificación de la obra que pudieran alterar o
falsear la visión propuesta por su autor. Esta perspectiva de Stallman
presenta el <i lang="en">copyleft</i> como una idea que no quiere
destruir el <i lang="en">copyright</i>, sino reformarlo, incluso con
algunos puntos que retoman su inicio en el siglo XVIII &mdash;es el caso
de la propuesta, defendida por el creador del <i lang="en">software</i>
libre en el mismo texto, de modificación a diez años del período de
duración del <i lang="en">copyright</i>[^230]&mdash;. En ese sentido,
los autores tendrían, en teoría, formas de garantizar que sus ideas no
serían falseadas y que sus ganancias no se verían tan afectadas.

[^229]: En «Malinterpretar el copyright: una sucesión de errores»,
  Stallman, <cite>Software libre para una sociedad libre</cite>, p. 119.
[^230]: <i lang="lt">Ibidem</i>. Y como detalla Aracele Torres: «Su
  justificación para reducir el monopolio sobre la copia a un plazo de
  diez años es la de que esa reducción tendría poco impacto sobre la
  edición de obras de hoy, pues él considera ese tiempo suficiente para
  que una obra de éxito sea rentable. Además, de acuerdo con él, las
  obras generalmente suelen estar fuera de circulación bastante antes de
  ese plazo» (Torres, <i lang="lt">op.&nbsp;cit.</i>, p. 168).

Otro modo, más práctico, de equilibrar la ecuación remuneración de los
autores vs. respeto a las formas «originales» de las ideas vs. acceso
público a los bienes creativos de la humanidad es la visión que
Kleiner[^231] presenta con el concepto de licencias
<i lang="en">copyfarleft</i>[^232], que tienen una regla de uso en la
producción colectiva y otra de uso para quien emplee trabajo asalariado
en su producción. A los trabajadores y trabajadoras, por ejemplo, se les
permitiría el uso, incluso comercial, de la obra cultural, pero no a
aquellos que exploten el trabajo asalariado, que serían obligados a
negociar el acceso[^233]. De acuerdo con su propuesta, «sería posible
preservar un inventario común de bienes culturales disponible para
productores independientes de las grandes industrias intermediarias ya
citadas, pero al mismo tiempo impedir su explotación por agentes
privados»[^234].

[^231]: Kleiner, <cite>The Telekommunist Manifesto</cite>.
[^232]: Una versión actualizada de la misma idea tomó el nombre de
<i lang="en">copyfair</i> y fue definida por Michel Bauwens, fundador de
la P2P Foundation, como «un principio que aspira a reintroducir
requisitos de reciprocidad en las actividades de mercado». Hace eso al
preservar el derecho a compartir el conocimiento sin objeciones, pero
aspira a supeditar la comercialización de tales bienes comunes a algún
tipo de contribución para esos bienes comunes
(<https://wiki.p2pfoundation.net/Copyfair>). Una de las licencias
creadas como ejemplo de ese concepto y con las mismas limitaciones de
venta que la <i lang="en">copyfarleft</i> es la Peer Production License
(<https://wiki.p2pfoundation.net/Peer_Production_License>), usada en el
ámbito de lo común &mdash;en Brasil es la utilizada en el proyecto
Biblioteca do Comum (<http://bibliotecadocomum.org>)&mdash;.
[^233]: Foletto; Martins, Luna, Encontro on-line cultura livre do sul: a
  produção cultural comunitária para a construção do comum,
  <cite>Contratexto</cite>, p. 114.
[^234]: <i lang="lt">Ibidem</i>.

Inventario común de bienes culturales. Dominio público. Aquí llegamos a
un ámbito de discusión mayor en que, desde mediados de los años 2000, la
cultura libre ha desembocado como un afluente caudaloso: el procomún
(<i lang="en">commons</i>, en inglés[^235]). Concepto amplio, de larga
tradición histórica que remite a los griegos[^236], el procomún,
historicamente, ha definido tanto un conjunto de recursos (bosques,
agua, aire, campos) y de cosas (una herramienta, una máquina) como un
producto social y una práctica. En <cite>O comum entre nós</cite>,
Rodrigo Savazoni usa las palabras de Massimo de Angelis,
«<i lang="en">there is no commons without commoning</i>»[^237], y las
del investigador brasileño Miguel Said Vieira para designar el procomún
como un «sustantivo» (el conjunto de bienes compartidos) y un «verbo»
(la acción de compartir; el <i lang="en">commoning</i>, el
«convertir en común»)[^238].

[^235]: Savazoni, <i lang="lt">op.&nbsp;cit.</i>, pp. 29-30.
[^236]: Savazoni, <i lang="lt">op.&nbsp;cit.</i>, p. 45.
[^237]: De Angelis, Introduction. <cite>The Commoner</cite>, n. 11, p.
  1.
[^238]: Savazoni, <i lang="lt">op.&nbsp;cit.</i>, p. 39.

Son muchos los investigadores y las investigadoras que trabajan con la
idea de procomún. Comenzando con «La tragedia de los comunes», publicado
en 1968 por el ecologista Garret Hardin, que, al analizar el uso común
de un pasto abierto por diferentes rebaños, argumentaba que una gestión
común de ese y otros bienes comunes libres llevarían a su destrucción
&mdash;la solución sería la privatización o la nacionalización&mdash;.
Algunas décadas más tarde, Elinor Ostrom hace frente a esa idea y, con
estudios sistemáticos de los modelos de gestión autónoma de bienes
comunes como alternativa a la gestión privada o exclusivamente estatal
de los bienes naturales, gana el Premio Nobel de Economía en 2009[^239].
También a partir de los años 90 y 2000, el procomún se vuelve a acercar
a la lucha política del final del siglo XIX, en espacial en la izquierda
de origen autonomista y a partir de obras (como <cite>Multitud</cite>,
de 2004) de Michel Hardt y Antonio Negri, que desarrollan un concepto de
procomún «como producto de la práctica biopolítica de la multitud, que
se constituye como una red “abierta y en expansión”, múltiple y deforme,
amplia y plural, que actúa para que podamos “trabajar y vivir en
común”»[^240]. Está también la obra de Silvia Federici, que, sobre todo
en <cite>Calibán y la bruja</cite> (2004) y <cite>Revolución en punto
cero</cite> (2013), evoca la importancia del trabajo femenino para
preservar el procomún: «Las mujeres supusieron la primera línea de
defensa contra los cercamientos tanto en Inglaterra como en el «Nuevo
Mundo», y fueron las defensoras más aguerridas de las culturas comunales
que amenazaba con destruir la colonización europea»[^241].

[^239]: Después crea la Asociación Internacional para el Estudio del
  Procomún (IASC), que hoy reúne investigadores y activistas del
  procomún de diferentes países. Sitio web: <https://iasc-commons.org>.
[^240]: Savazoni, <cite>O comum entre nós: da cultura digital à
  democracia do século XXI</cite>, p. 46.
[^241]: En Federici, <cite>Revolución en punto cero</cite>, p. 248.
  Hasta hoy, son grupos de mujeres los que preservan la propiedad común
  de los modos de vida colectivos en las montaña de Perú y de
  agriculturas de subsistencia africanas (que, según Federici, producen
  el 80&nbsp;% de los alimentos que la población del continente
  consume), por citar dos ejemplos. El trabajo de la historiadora da un
  acercamiento a los estudios feministas del procomún y sitúa los medios
  materiales de reproducción como mecanismo primario para el «convertir
  en común».

El procomún empieza a relacionarse con más frecuencia con bienes como
<i lang="en">software</i>, conocimiento y con los archivos de bienes
culturales en la red, así como con los modos autónomos de gestión de esos
bienes por las comunidades, a mediados de la década del 2000, con la
difusión de las tecnologías digitales y de Internet. Al transformar el
<i lang="en">software</i> en un conocimiento de uso, producción y
gestión común, el <i lang="en">copyleft</i> convirtió el
<i lang="en">software</i> libre en un <i lang="en">commons</i>
intelectual, dice Benkler en <cite>The Wealth of Networks</cite> (2006),
uno de los primeros en dar una introducción al procomún de las
tecnologías digitales en la red. Los <i lang="en">commons</i>
intelectuales estarían basados en la información digital puesta en
circulación en Internet, bienes no rivales que, al ser consumidos o
usados por una persona, no se vuelven inaccesibles para el consumo o uso
de otras[^242]. Debido a esa característica, formarían una nueva
modalidad de producción del conocimiento colaborativa basada en bienes
comunes (<i lang="en">commons-based peer production</i>, CBPP), que, en
opinión del autor, generaría una nueva economía más democrática y
distributiva que la del período industrial.

[^242]: Torres, <i lang="lt">op.&nbsp;cit.</i>, p. 137.

La formulación de Benkler fue usada en los años siguientes por
algunos «economistas de lo común», entre ellos Michel Bauwens, creador
de la P2P Foundation, quien «indica que la economía de los pares da
origen a un tercer modo de producción, de gobernanza y de propiedad, que
sigue el proverbio “de cada cual según sus capacidades, a cada cual
según sus necesidades”»[^243]. La cultura libre, en ese sentido, sería
la representante de los bienes culturales de ese tercer modo de
producción (los otros dos serían, <i lang="lt">grosso modo</i>, el
capitalismo y el socialismo), que «reorganizaría el sistema productivo
en torno al cuidado y la solidaridad, al intercambio equitativo entre
pares y basado en la actuación de ciudadanos emprendedores cuyo objetivo
final no es maximizar el beneficio, sino la mejora de las condiciones
sociales de todas y todos»[^244].

[^243]: Savazoni, <i lang="lt">op.&nbsp;cit.</i>, p. 49.
[^244]: <i lang="lt">Ibidem</i>, p. 49.

