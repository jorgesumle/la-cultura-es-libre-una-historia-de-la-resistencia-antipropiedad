<section class="citas">
<i>Todo lo que me han escrito acerca de ese hombre maravilloso visto en
Fráncfort es cierto. No he visto Biblias completas, sino solo una serie
de cuadernos de diversos libros de la Biblia. La tipografía era muy
elegante y legible, no es en absoluto difícil de seguir &mdash;vuestra
merced sería capaz de leerla sin esfuerzo, y de hecho sin
gafas&mdash;.</i>

<div class="right-align">
Enea Silvio Bartolomeo Piccolomini, futuro papa Pío II, en carta al
cardenal Carvajal, 1455
</div>

<br>

<i>Considerando que impresores, bibliotecarios y otras personas,
reciente y frecuentemente se han tomado la libertad de imprimir,
reimprimir y publicar, o hacer que se imprima y reimprima, y han
publicado libros y otros escritos, sin el consentimiento de los
autores o propietarios de esos libros y escritos, en gran detrimento
de estos, y demasiado a menudo causando la ruina de estos y de sus familias;
para la prevención de estas prácticas en el futuro, y para alentar al
hombre culto a componer y escribir libros útiles, que, por favor, por
Su Majestad pueda ser aprobado este estatuto.</i>

<div class="right-align">
Estatuto de Anne, Inglaterra, 1710
</div>

<br>

<i>No veo razón para conceder ahora un nuevo plazo, lo cual no
impedirá que se conceda una y otra vez, con tanta frecuencia como expire
el antiguo; así que si esta ley se aprueba, establecerá de hecho un
monopolio a perpetuidad, una cosa que con razón es odiosa a los ojos de
la ley; será una gran traba al comercio, un gran obstáculo al
conocimiento, no supondrá ningún beneficio para los autores, pero sí una
gran carga para el público; y todo esto solo para incrementar las
ganancias privadas de los libreros.</i>

<div class="right-align">
Parlamento inglés, negando la petición de los libreros de aumentar la
extensión del plazo de los derechos de autoría
</div>

<br>

<i>El</i> <span lang="en">copyright</span>
<i>pertenece al autor; el autor, no obstante, no posee máquinas de
impresión; tales máquinas las posee el editor; así pues, el autor
necesita al editor. ¿Cómo regular esta necesidad? Simple: el autor,
interesado en que su obra sea publicada, cede los derechos al editor
durante un período determinado. La justificación ideológica ya no se
basa en la censura, sino en las necesidades del mercado.</i>

<div class="right-align">
Wu Ming, <cite>Notas inéditas sobre <span lang="en">copyright</span> y
<span lang="en">copyleft</span></cite>, 2005
</div>
</section>

### I.

Hacia el 1455, el futuro papa Pío II, Enea Silvio Bartolomeo
Piccolomini, andaba por las calles de la región de Fráncfort cuando vio
una vitrina con varios cuadernos impresos de un texto que conocía muy
bien. En la carta al cardenal Carvajal, relató así el episodio: «Todo lo
que me han escrito acerca de ese hombre maravilloso visto en Fráncfort
[<i lang="lt">sic</i>] es cierto. No he visto Biblias completas, sino
solo una serie de cuadernos de diversos libros de la Biblia. La
tipografía era muy elegante y legible, no es en absoluto difícil de
seguir &mdash;vuestra merced sería capaz de leerla sin esfuerzo, y de
hecho sin gafas&mdash;»[^33]. La llamada «Biblia de Gutenberg», también
conocida como «Biblia de 42 líneas», se imprimió por primera vez en 1455
y tuvo entre 158 y 180 copias. Constaba del Antiguo Testamento Hebreo y
del Nuevo Griego, tal como se conoce hoy la Biblia cristiana, escritos
en latín, con 42 (en algunas, 40) líneas, impresa parte en pergamino,
parte en papel común de tamaño *doble folio*, con dos páginas a cada
lado del papel (cuatro páginas por hoja).

[^33]: «<i lang="en">All that has been written to me about that marvelous man seen at
Frankfurt is true. I have not seen complete Bibles but only a number
of quires of various books of the Bible. The script was very neat and
legible, not at all difficult to follow—your grace would be able to
read it without effort, and indeed without glasses</i>». Disponible en
<https://pablozeta.com/posts/the-birth-of-movable-type/>.

Nacido en Mainz, sudeste de Alemania, en 1398, el hombre llamado
Johannes Gensfleisch zur Laden zum Gutenberg era un joyero y un hábil
comerciante antes de trabajar en la impresión y desarrollar el proceso
que facilitaría la expansión de las ideas de manera mucho más rápida que
la que existía hasta entonces. El sistema de tipos móviles con el que
Gutenberg imprimió la Biblia, y el que se popularizó a partir de ese
período, no fue &mdash;como nunca lo es&mdash; una invención a partir de la
nada. El proceso de producción manual de libros comenzaba, desde el
siglo XII, a experimentar modificaciones consistentes; los papeles
volvieron a circular por Europa, y las prensas, grandes bloques de
madera que tomaban tinta e imprimían en una superficie, empezaron a
hacer de la impresión un proceso industrial más rápido que las manos de
los monjes copistas y a volver los libros más baratos que los antiguos
libros de papiro de la Antigüedad[^34]. Las contribuciones de Gutenberg
al sistema ya utilizado en la época para la impresión fueron
principalmente la invención de un proceso de producción en masa de tipo
móvil, hecho a partir de una aleación que incluía plomo, estaño
y cobre y que podía reutilizarse; el uso de tinta a base de aceite, que
se adaptaba mejor a un papel más suave y absorbente probado por
él; y el uso de un modelo de prensa que era similar al de tornillo
usado en la agricultura de la época, y, por tanto, un objeto que
era más familiar a la vida cotidiana agraria de la región.

[^34]: En China hay registros de usos de formas de impresión semejantes
  a las de Gutenberg desde el siglo XI; Bi Sheng, en 1040, había usado
  los tipos móviles para imprimir en arcilla, material poco resistente y
  fácilmente quebrable; Wang Zhen, en 1298, trabajó con un sistema
  móvil, todavía esculpido en madera, un poco más resistente que la
  arcilla. La impresión en madera, técnica conocida como xilograbado,
  era ampliamente utilizada en la China de ese período y hay constancia de
  que, dada la peculiaridad de los caracteres chinos, continuaba siendo
  la forma más eficiente y barata de imprimir. Véase Briggs; Burke,
  <cite>Uma história social da mídia: de Gutenberg a Diderot</cite>; y
  Tsien Tsuen-Hsuin; Joseph Needham. <cite>Paper and Printing: Science
  and Civilisation in China</cite>, v. 5, p. 158.

El surgimiento del proceso de producción en masa de publicaciones, que
hoy llamamos impresión, aceleró un proceso de popularización de la
cultura escrita[^35]. Los avances propiciados por la impresión
facilitaron la difusión de ideas de todo tipo, no solo las de carácter
litúrgico y religioso que predominaban en la época. La creciente
difusión de publicaciones potenció la creación de lectores y cambió los
hábitos de disfrute de bienes culturales. Fue un inicio, poco a poco,
del cambio de la experiencia colectiva oral, basada en la representación
de quien presentaba el contenido que se quería transmitir, a la
experiencia individual, silenciosa y aislada, impresa en papel de forma
más duradera y fija que aquella al aire libre, acostumbrada a la
libertad de adiciones, apropiaciones e improvisaciones diversas de quien
la representaba. «La tendencia a actitudes más individualistas fue
fomentada por la posibilidad de imprimir, que ayudó al mismo tiempo a
fijar y difundir textos»[^36].

[^35]: Es interesante observar aquí, como hacen Eisenstein, en
  <cite>The Printing Revolution in Early Modern Europe</cite>, y
  Martins, en <cite>Autoria em rede</cite>, que la transición del libro
  manuscrito al libro impreso no se dio de forma inmediata; «al
  contrario, fue un proceso de negociación y mezcla entre dos lenguajes,
  el que refuerza la teoría de que la creación de un nuevo medio se da
  mediante la adaptación de un medio anterior» (Martins,
  <i lang="lt">op.&nbsp;cit.</i>, p. 68).
[^36]: Briggs; Burke, <i lang="lt">op.&nbsp;cit.</i>, p. 140.

La expansión de la difusión de publicaciones impresas y el
estímulo a un individualismo propiciado por la posibilidad de lectura
solitaria se aglutinaron a un humanismo renacentista para modificar
también la idea de autoría de entonces. Si, durante buena parte de la
Edad Media, la cultura era oral y la autoría era colectiva y difusa,
expresión de un deseo divino o arraigado en una cultura popular
determinada, y los libros tenían restringida su difusión a la producción
artesanal de las iglesias, ahora había elementos para la transformación
de la concepción de lo que sería el autor de una obra. Al trasladar al
hombre al centro (antropocentrismo) del mundo, el humanismo empezaba a
valorar la noción de originalidad e individualidad, que se expresó en la
apreciación del *estilo* y el reconocimiento del enfoque innovador de
cada autor, frente a la fuerte dependencia textual de la tradición
típica de las obras de la Edad Media[^37]. Teniendo identificado un
autor individual, las publicaciones también empezaban a volverse más
cerradas, con menor apertura a adiciones o comentarios, como hasta
entonces solía ocurrir en las *notas de los márgenes* de los libros
medievales.

[^37]: <i lang="lt">Ibidem</i>, p. 116.

Antes de la popularización de la impresión de tipos móviles, la
producción de un libro era una tarea difícil, cara y artesanal,
prácticamente restringida al ámbito de la Iglesia Católica y sus monjes
copistas. Después de Gutenberg, el libro podía ser impreso a escala
industrial, por comerciantes y empresarios que tuvieran dinero para
comprar las máquinas necesarias y organizar sus modelos de producción,
lo que ya era un cambio considerable en el sistema de circulación de
conocimiento de la época: posibilitaba la difusión de ideas, bienes
culturales e información más allá del control de la Iglesia. No por
casualidad, es en ese período cuando ocurre la Reforma Protestante,
movimiento religioso que cuestionó los dogmas del catolicismo de la
época, inaugurado a partir de las famosas noventa y cinco tesis escritas
por Martín Lutero en Wittenberg, Alemania, en octubre de 1517. Las
pequeñas imprentas, muchas veces clandestinas, fueron los canales de
difusión de las ideas reformistas por toda Europa.

El *universo* (o *revolución*) inaugurado por Gutenberg afianzó también
la idea de un mercado para bienes culturales y le dio a estos
características determinadas según las condiciones de producción en
masa. Imprimir un libro aún era un proceso caro, que requería una
considerable suma de dinero para realizarse. Pero, con la novedad de la
prensa de tipos móviles, se volvió también un negocio lucrativo; un solo
libro, que tardaría meses en ser producido artesanalmente en los
monasterios, se convirtió en 500, 1&nbsp;000 o más ejemplares impresos
en pocos días y distribuidos en las principales ciudades de la época, lo
que generó una red potente que atrajo a banqueros para financiar a los
impresores, vendedores para comercializar las obras, vendedores
ambulantes para transportarlas y nuevos lectores, muchas veces
alfabetizados a partir de las publicaciones que comenzaron a circular en
la época.

Como la Iglesia y las monarquías europeas no querían perder el control
de la propagación de ideas, los conflictos fueron inevitables. En el
primer caso, el miedo a la difusión de los principios de la Reforma
Protestante dio lugar a persecuciones de varios impresores de la época,
lo que años después dio lugar a la creación, en 1559, del *Index*, lista
de publicaciones consideradas heréticas y prohibidas por la Iglesia
Católica, con sus editores inhabilitados[^38]. La creación de un mercado
de publicación, a su vez, hizo que los gobiernos monárquicos de la época
establecieran reglas para controlar las relaciones entre quien escribía
un libro, quien vendía y quien lo leía. Hasta entonces, cualquier
persona que tuviera acceso a una imprenta o alguien que la tuviera
podía imprimir copias de lo que quisiera sin que nadie reclamara
legalmente la exclusividad de la producción y circulación de las obras
que iban a imprimirse. Además, era común que una obra, bien vendida en
una determinada región, fuera publicada como novedad en otra a partir de
diversas traducciones y adaptaciones sin ningún tipo de control. En una
época en la que, en Europa, Portugal, España, Inglaterra y Francia
empezaban a organizarse como Estados nación y las actuales Alemania,
Italia, Bélgica, Austria, Polonia, entre otras, se dividían en cientos
de ciudades-Estado independientes, no había legislaciones que regularan
la circulación de las obras en todas esas regiones; a lo sumo cada
ciudad o región contaba con sus propias reglas, que no valían para
otras. No existía ninguna distinción entre lo que sería una obra
«oficial» y una «pirata».

[^38]: Promulgada por el papa Paulo IV, Gian Pietro Carafa, sería una
  lista mantenida hasta el siglo XX, suspendiéndose en 1966. Sobre las
  acciones de censura y persecución a los editores y pastores radicales
  de la Reforma Protestante por Carafa, el grandioso romance <cite>Q: o
  caçador de hereges</cite>, de Luther Blisset (1999), publicado en
  Brasil en 2002 por Conrad, es una fuente excelente.

### II.

Correspondieron a la República de Venecia y a Inglaterra las primeras
labores más consolidadas de ofrecer licencias exclusivas a algunos
editores para la publicación de determinados libros. En la ciudad
italiana, conocida por su activo comercio marítimo con asiáticos,
árabes, bizantinos, africanos y por el heterogéneo tráfico de nobles,
banqueros, marineros, delincuentes y vendedores de los sitios más
variados, en 1486 se estableció el primer privilegio para la publicación
exclusiva de un libro. La obra escogida, <cite>Rerum venetarum ab urbe
condita opus</cite>, es un resumen de la historia de la *Sereníssima*,
como era conocida Venecia, escrita por Marcus Antonius Coccius
Sabellicus, historiador italiano, a quien el consejo que administraba la
ciudad concedió un permiso especial para escoger un único editor del
libro en el territorio veneciano[^39]. Algunos años después, los
privilegios reales para determinados impresores se consolidaron para más
obras en Venecia (1498) y se propagaron también a otras ciudades
italianas, como Florencia y Roma, así como a Francia y otras
ciudades-Estado alemanas, con un mismo objetivo: garantizar a ciertos
impresores la exclusividad de publicación de determinados libros, con el
fin de que solamente ellos se pudieran lucrar con su
comercialización[^40].

[^39]: En Armstrong, <em>Before Copyright: The French Book-Privilege
  System 1498-1526</em>
[^40]: En Martins, <i lang="lt">op.&nbsp;cit.</i>, p. 38, y también
  Woodmansee, <cite>The Author, Art, and the Market: Rereading the History of Aesthetics</cite>.

En Inglaterra, 1557 es el año de las primeras licencias dadas a
impresores, concedidas por la reina Mary a un grupo de Londres conocido
como Stationers Company, formado en 1403[^41] por artesanos relacionados
con la circulación y venta de libros y otros materiales de impresión. Al
ser uno de los primeros grupos organizados que trabajaban en el nuevo
negocio, presionaron a la monarquía inglesa para tener exclusividad de
producción y venta de publicaciones, y consiguieron un privilegio que,
en la práctica, dio a Stationers Company el monopolio de la copia y
difusión de libros. A partir de entonces, solamente podían imprimirse
legalmente en Inglaterra obras que tuvieran autorización real y que
estuvieran listadas en el registro oficial en nombre de un editor
relacionado con la empresa. Era un derecho a copiar (<i lang="en">right
to copy</i>) otorgado a algunos impresores, que, con eso, se convertían
en los únicos con privilegios sobre determinadas obras. No había mención
a derechos patrimoniales, morales o estéticos de los autores de una
determinada obra.

[^41]: Como consta en la página oficial de la organización, todavía
  activa hoy, disponible en
  <https://www.stationers.org/company/history-and-heritage>.

Después de un siglo y medio de monopolio, Stationers Company estaba cada
vez más amenazada por los libreros de provincias alejadas de Londres
&mdash;escoceses e irlandeses principalmente&mdash;. La empresa pidió
entonces al Parlamento inglés una nueva ley para alargar su derecho
exclusivo sobre la copia de libros. La respuesta fue la creación del
Estatuto de Anne, aprobado en 1710 por el Parlamento británico y
considerado la primera ley de <i lang="en">copyright</i> del mundo y
base para una parte de las legislaciones hasta hoy, más de tres siglos
después. Fue un duro golpe contra el privilegio de Stationers Company,
porque la ley designó a los autores (y no más a los editores) como los
propietarios de sus obras. El texto de la ley comenzaba así:

> Considerando que impresores, bibliotecarios y otras personas,
> reciente y frecuentemente se han tomado la libertad de imprimir,
> reimprimir y publicar, o hacer que se imprima y reimprima, y han
> publicado libros y otros escritos, sin el consentimiento de los
> autores o propietarios de esos libros y escritos, en gran detrimento
> de estos, y demasiado a menudo causando la ruina de estos y de sus familias;
> para la prevención de estas prácticas en el futuro, y para alentar al
> hombre culto a componer y escribir libros útiles, que, por favor, por
> Su Majestad pueda ser aprobado este estatuto.[^42]

[^42]: En el original en inglés: «Whereas Printers, Booksellers, and
  other Persons, have of late frequently taken the Liberty of Printing,
  Reprinting, and Publishing, or causing to be Printed, Reprinted, and
  Published Books, and other Writings, without the Consent of the
  Authors or Proprietors of such Books and Writings, to their very
  great Detriment, and too often to the Ruin of them and their Families:
  For Preventing therefore such Practices for the future, and for the
  Encouragement of Learned Men to Compose and Write useful Books; May it
  please Your Majesty, that it may be Enacted this Statute». Disponible
  en <https://en.wikipedia.org/wiki/Statute_of_Anne>.

Antes exclusivos de los miembros de Stationers Company, los derechos
sobre la impresión y reimpresión de libros pasaron a ser del autor
&mdash;o de otra persona a la que este le diera autorización&mdash; tan
pronto como fueran publicados. Una limitación importante era que la ley
otorgaba ese derecho solo durante un período determinado: 14 años,
renovable solo mientras el autor estuviera vivo, y 21 años para obras
publicadas antes de aquel momento. Al final de ese período, el
<i lang="en">copyright</i> expiraba y la obra era entonces libre de ser
publicada por cualquiera. La pena para quien no cumpliera el estatuto
era la destrucción de las copias y el pago de multas al propietario de
los derechos.

Para algunos investigadores e historiadores del derecho de autor, la
intención de la ley era acabar con el monopolio de Stationers Company
&mdash;y no dar los derechos de copia e impresión al autor&mdash;. Hubo
presiones de varios bandos para acabar con el monopolio de la empresa,
acusada de «vender la libertad de Inglaterra para
garantizarse los beneficios de un monopolio»[^43]. El escritor inglés
John Milton, autor de <cite>Paraíso perdido</cite> (1667), decía en esa
época que los impresores de Stationers Company eran «monopolizadores del
negocio de los libros, hombres que por tanto no trabajan en una
profesión honrada a la cual se debe el conocimiento»[^44]. El notorio
poder que los libreros ejercían sobre la difusión del conocimiento a
través de los monopolios estaría perjudicando su libre propagación.


[^43]: Como cuenta el abogado y profesor Lawrence Lessig en
  <cite>Cultura libre: Cómo los grandes medios usan la tecnología y las
  leyes para encerrar la cultura y controlar la creatividad</cite>, p.
  81.
[^44]: Wittenberg, <cite>The Protection and Marketing of Literary
  Property</cite>, citado en Lessig, <i lang="lt">op.&nbsp;cit.</i>, p. 81.

Al aprobar el Estatuto de Anne, el Parlamento británico también buscaba
aumentar la competición entre los libreros y, con eso, en teoría,
fomentar la mayor circulación de publicaciones. Con esa perspectiva,
limitar el período del <i lang="en">copyright</i> fue necesario para
garantizar que las publicaciones se volverían abiertas para que
cualquier distribuidor las publicara después de cierto tiempo.
«Determinar que el plazo para las obras ya existentes [fue]era de solo
veintiún años fue un compromiso para luchar contra el poder de los
libreros. La limitación en los plazos fue una forma indirecta de
asegurar la competencia entre libreros, y de este modo la producción y
difusión de cultura»[^45].

[^45]: Lessig, <i lang="lt">op.&nbsp;cit.</i>, p 81.

### III.

Promulgado en Inglaterra, lo cierto es que el Estatuto de Anne no fue
obedecido inmediatamente. Nació como una ley que, tal vez por la novedad
de la concepción que introdujo, disputó sus interpretaciones en los
tribunales durante muchas décadas, lo que da muestra, relevante todavía
hoy, de qué intereses están en juego cuando se habla de los
enfrentamientos entre productores, intermediarios y público. Stationers
Company y otros libreros surgidos después ignoraron la legislación y
continuaron insistiendo en el derecho perpetuo de controlar sus
publicaciones a su voluntad durante décadas.

En 1735, ya transcurridos los primeros 21 años de expiración de obras
siguiendo el Estatuto (1710 + 21), los libreros trataban de persuadir al
Parlamento para extender los períodos, para legalizar la explotación
comercial de las obras durante más tiempo. El texto en el que el
Parlamento comunicó su decisión &mdash;negativa&mdash; aporta un
determinado <i lang="de">zeitgeist</i> (espíritu del tiempo) de crítica
a los monopolios, sobre todo los de la Corona inglesa, bastante presente
en el país durante los siglos XVII y XVIII. Conviene recordar que la
llamada Guerra Civil Inglesa (1642-1651), raro período en que Inglaterra
no tuvo un monarca como su principal gobernante, fue en parte provocada
por las prácticas de la Corona de mantener monopolios:

> No veo razón para conceder ahora un nuevo plazo, lo cual no
> impedirá que se conceda una y otra vez, con tanta frecuencia como expire
> el antiguo; así que si esta ley se aprueba, establecerá de hecho un
> monopolio a perpetuidad, una cosa que con razón es odiosa a los ojos de
> la ley; será una gran traba al comercio, un gran obstáculo al
> conocimiento, no supondrá ningún beneficio para los autores, pero sí una
> gran carga para el público; y todo esto solo para incrementar las
> ganancias privadas de los libreros.[^46]

[^46]: Citado en Lessig, <i lang="lt">op.&nbsp;cit.</i>, p 82.

Sin haber conseguido la extensión del período inicial de
<i lang="en">copyright</i>, los editores todavía siguieron con disputas en
los tribunales ingleses durante algunas décadas. En la defensa de las
demandas contra unos y otros, empezaron también a evocar los derechos
que los autores tendrían sobre las obras como estrategia de
argumentación para garantizar la explotación comercial de sus
publicaciones durante más tiempo. Usaban artimañas jurídicas para eso,
como muestra uno de los casos más famosos de la época, Millar vs.
Taylor, en 1769. Millar era un librero activo en Londres asociado a
Stationers Company que, en 1729, compró los derechos de copia del poema
del escritor James Thomson <cite>Las estaciones</cite>, pagando entonces
105 £. Tras terminar el período de <i lang="en">copyright</i>, 14
años según el Estatuto de Anne, Robert Taylor, otro editor inglés,
comenzó a vender una edición de los poemas en los mercados londinenses
que competía con la de Millar &mdash;a quien no le gustó y que, con el
apoyo de la empresa de la que formaba parte, denunció a Taylor&mdash;. La
argumentación jurídica usada en el proceso fue que Millar, habiendo
pagado al autor, tenía el derecho perpetuo sobre la obra.

Cinco años después, sin embargo, la decisión fue revocada en otro caso
famoso de la época, Donaldson vs. Beckett[^47]. Millar murió poco
después de su victoria y vendió su patrimonio a un sindicato de
distribuidores de libros, del que formaba parte un individuo llamado
Thomas Beckett. Por otro lado, Alexander Donaldson era un librero
escocés que publicaba ediciones baratas de obras cuyo período de
<i lang="en">copyright</i> hubiera expirado, lo que hacía que fuera
considerado un editor «pirata» por los ingleses de Londres. Tras la
muerte de Millar, el escocés publicó una edición no autorizada de los
trabajos del poeta Thomson; Beckett, basándose en la decisión anterior
favorable a Millar, obtuvo una orden judicial contra él. Donaldson
entonces apeló a la Cámara de los Lores, una especie de Tribunal Supremo
de la época que tomaba decisiones que no pocas veces movilizaban a
«hinchas» en ambos lados. Por una mayoría de dos a uno, la Cámara de
los Lores decidió a favor de Donaldson contra el argumento de los
<i lang="en">copyrights</i> perpetuos &mdash;que, cinco años antes, el juez
Mansfield había acatado en pro de Millar&mdash;. Los lores ahora
aceptaron la alegación de los abogados del librero escocés: cualquier
derecho que hubiera existido antes, basado en el *derecho común*, había
terminado con el Estatuto de Anne, que pasaba entonces a ser la única
regulación jurídica para el derecho de copia de publicaciones impresas.
Después de que el período definido por el estatuto (14 o 21 años,
dependiendo del caso) hubiera expirado, los trabajos que estaban
originalmente protegidos &mdash;los de autores como William Shakespeare
y John Milton, por ejemplo&mdash; perdían tal protección y podían ser
usados, adaptados y comercializados libremente, pues pasaban a
*dominio público* &mdash;una noción que, aunque existe desde los griegos
y romanos[^48], pasó en ese momento a ser validada por primera vez en la
historia del sistema jurídico anglosajón&mdash;.

[^47]: Detallado en Lessig, <i lang="lt">op.&nbsp;cit.</i>, p 84.
[^48]: Hay diferentes versiones del origen de la idea de dominio público
  en Occidente. Una de las más aceptadas remite a los derechos de
  propiedad en Roma, donde había definiciones de <i lang="lt">res
  nullius</i> («cosas que no pueden ser apropiadas»), <i lang="lt">res
  communes</i> («cosas que podrían ser comúnmente apreciadas por la
  humanidad, como el aire, la luz solar y el mar), <i lang="lt">res
  publicae</i> («cosas que habían sido compartidas por todos los
  ciudadanos») y <i lang="lt">res universitatis</i> («cosas que eran
  propiedad de los municipios de Roma»). El término tiene origen en esos
  conceptos y fue disputado con otros semejantes, como
  <i lang="lt">publici juris</i> o <i lang="fr">propriété publique</i>,
  en el siglo XVIII, hasta difundirse y ser adoptado legalmente a partir
  de la Convención de Berna (ver próximo capítulo). Sobre el origen del
  dominio público, véase Huang, <cite>On Public Domain in Copyright Law,
  Frontiers of Law in China</cite>, v. 4, pp. 178-195, y Torremans,
  <cite>Copyright Law: a Handbook of Contemporary Research</cite>.

### IV.

La noción de <i lang="en">copyright</i> que surgió en la época del
Estatuto de Anne era específica: prohibía a otros reeditar un libro
impreso. Era un derecho ligado a un bien que, a su vez, se relacionaba
directamente a una tecnología que lo producía &mdash;en la época,
máquinas de impresión de tipos móviles&mdash;. En la Inglaterra del
siglo XVIII, el <i lang="en">copyright</i> todavía se limitaba a
determinar quién, y durante cuánto tiempo, podría copiar y distribuir un
bien cultural en formato impreso. No mencionaba derechos para los
autores, como la remuneración por la obra o la posibilidad de adaptación
de esta, ni citaba otras artes o soportes. Aunque los libreros ingleses
evocaran la protección del autor en sus defensas jurídicas, se trataba
más de una artimaña para proteger intereses de ciertos grupos que
comenzaban a industrializarse que de un sistema jurídico de protección
para quien creaba[^49].

[^49]: Wu Ming, <cite>Notas inéditas sobre copyright e copyleft</cite>,
  <i lang="lt">op.&nbsp;cit.</i>

Para el colectivo italiano Wu Ming, la ley de <i lang="en">copyright</i>
del Estatuto de Anne surgió de la necesidad de censura preventiva y de
restricción al acceso a los medios de producción cultural &mdash;de la
limitación, por tanto, a la difusión de ideas&mdash;. La intención de
los impresores al buscar la creación del Estatuto de Anne por el
Parlamento inglés sería la de reconocer la legitimidad de sus intereses
y crear una legislación que trabajara a su favor. El argumento aquí es
«el <i lang="en">copyright</i> pertenece al autor; el autor, no
obstante, no posee máquinas de impresión; tales máquinas las posee el
editor; así pues, el autor necesita al editor. ¿Cómo regular esta
necesidad? Simple: el autor, interesado en que su obra sea publicada,
cede los derechos al editor durante un período determinado. La
justificación ideológica ya no se basa en la censura, sino en las
necesidades del mercado»[^50].

[^50]: Nimus, <cite>Copyright, copyleft e os creative
  anti-commons</cite>, p. 42.

La creación de un sistema que regulara no solo los derechos exclusivos
de copiar, imprimir y vender una determinada obra, sino también la
*propiedad* de la ideas, surgiría prácticamente en la misma época, pero
del otro lado del canal de la Mancha.

