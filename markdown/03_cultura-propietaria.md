<section class="citas">
<i>Se considera que no puede haber relación entre la propiedad de una
obra y la de un campo, que puede ser cultivado por un solo hombre, o de
un mueble que solo sirve a un hombre; por consiguiente, la propiedad
exclusiva se basa en la naturaleza de la cosa. Así, la propiedad
literaria no se deriva del orden natural, ni se defiende por la fuerza
social, sino que es una propiedad fundada por la propia sociedad. No es
un verdadero derecho, es un privilegio.</i>

<div class="right-align">
Marqués de Condorcet, <cite>Fragmentos sobre la libertad de
prensa</cite>, 1776
</div>
<br>

<i>Si la naturaleza ha producido una cosa menos susceptible de propiedad
exclusiva que todas las demás, esa cosa es la acción del poder de pensar
que llamamos «idea», que un individuo puede poseer con exclusividad mientras que la guarde para sí; pero, en el momento en que se divulga, esta es
forzosamente poseída por todo el mundo, y el receptor no se puede
desembarazarse de ella. Su carácter peculiar también es que nadie la
posee menos, porque todos los demás la poseen íntegramente. Aquel que
recibe una idea de mí recibe la instrucción para sí, sin disminuir la
mía; como quien enciende su vela en la mía, recibe luz sin oscurecer la
mía.</i>

<div class="right-align">
Thomas Jefferson, en la carta a Isaac McPherson, 1813
</div>
</section>

### I.

La noción de que alguien tenga la propiedad de una idea, que se volvió
común en la sociedad occidental en los siglos siguientes, tanto entonces
como ahora, sigue teniendo algo de extraño: ¿cómo puedes ser *dueño* de
algo que yo continúo teniendo? ¿Eso por qué es un robo? Entendemos más
fácilmente la idea de robo cuando, por ejemplo, cojo un pimentero de la
cocina de tu casa. Estoy cogiendo algo, un vidrio que contiene pimienta,
y, tras ese acto, no vas a tener más ese pimentero en tu cocina. ¿Pero
qué estoy robando si, después de probar tu pimienta en un plato que
cocinaste, tomo la *idea* de usar esa pimienta en un plato y voy al
mercado a comprar un vidrio de pimienta igual al que tienes? ¿Qué
estaría robando en este caso[^51]?

[^51]: Esa comparación fue creada a partir de Lessig,
  <i lang="lt">op.&nbsp;cit.</i>, p. 94.

Sabemos que ideas, historias, canciones, poemas, obras de teatro no
tienen la misma naturaleza que objetos materiales como tierras, casas,
vehículos, molinos, arados, joyas. Podemos, por ejemplo, escuchar una
grabación de una canción mediante algún aparato en un determinado lugar
al mismo tiempo que el compositor de la música la toca en otro &mdash;y
esto no priva ni obstaculiza la escucha de ambos&mdash;. «Aquel que
recibe una idea de mí recibe la instrucción para sí, sin disminuir la
mía; como quien enciende su vela en la mía, recibe luz sin oscurecer la
mía», dijo Thomas Jefferson, considerado uno de los padres fundadores de
los Estados Unidos, presidente del país entre 1801 y 1809, en una carta
de 1813[^52]. Si las ideas son libres, no competidoras, virales,
asociadas y combinadas unas con otras sean cuales sean sus territorios u
orígenes, modificándose de acuerdo al uso y la creatividad de cada uno
como el fuego, ¿por qué transformarlas en *propiedad intelectual*[^53]?

[^52]: En la versión original, en inglés: «<i lang="en">He who recieves
  an idea from me, recieves instruction himself, without lessening mine;
  as he who lights his taper at mine, recieves light without darkening
  me</i>». Carta de Thomas Jefferson a Isaac McPherson, 1813. Disponible
  en <https://founders.archives.gov/documents/Jefferson/03-06-02-0322>.
  «Este pasaje se cita mucho como argumento contrario a la propiedad
  intelectual, pero la intención de Jefferson es solo mostrar que la
  propiedad intelectual no es natural &mdash;lo que no impide [y el es
  un defensor de esto] que esta sea creada por la sociedad&mdash;»
  (Ortellado, <cite>Porque somos contra a propriedade intelectua</cite>,
  p. 29).
[^53]: Las disputas en los tribunales ingleses en torno al
  <i lang="en">copyright</i> en el siglo XVII, citadas en el capítulo
  anterior, usaban la expresión *propiedad literaria*. La expresión en
  inglés <i lang="en">intellectual property</i> empieza a usarse algo
  más tarde; según el <cite>Oxford English Dictionary</cite>, su primer
  registro es de un artículo de 1769 en un conocido periódico de la
  época, <cite>Monthly Review</cite>, mientras que el uso con el
  significado que conocemos hoy data de 1808, como título de una
  colección de ensayos: <cite>New-England Association in favour of
  Inventors and Discoverers, and Particularly for the Protection of
  Intellectual Property</cite>. Fuente: <cite>Oxford English
  Dictionary</cite>.

El mismo Jefferson respondió en su época: «para que los creadores de
ideas no pierdan la motivación de crear y expresar sus ideas, es
necesario un estímulo material para quien “crea” o “expresa las ideas”.
Para que sean asimiladas por todos los que las reciben, las ideas deben
ser especialmente protegidas, para que cada vez que alguien las usa, el
“creador” tenga su recompensa»[^54]. Siendo Jefferson uno de sus
artífices, la Constitución de los Estados Unidos, promulgada en 1789, 79
años después del Estatuto de Anne y el mismo año que las primeras leyes
de derecho de autor en Fracia, ya citaba en una de sus cláusulas «el
congreso debe tener el poder de promover el progreso de las ciencias y
de las artes útiles asegurando a los autores e inventores, por un
período limitado, el derecho exclusivo a sus escritos y
descubrimientos»[^55].

[^54]: Thomas Jefferson, citado por Ortellado,
  <i lang="lt">op.&nbsp;cit.</i>, p. 29.
[^55]: Cláusula de derechos de autor y de patentes de la Constitución
  de los Estados Unidos, art. I, § 8, cl. 8.

Las primeras legislaciones que tratan de regular la propiedad
intelectual establecen legalmente aquello que todavía hoy es el
principal conflicto: conciliar la remuneración de los creadores con el
derecho de acceso a las creaciones artísticas. Al implantar el producto
de una determinada creación intelectual como una mercancía con valor
financiero de cambio, el pago material por una idea concreta va, en
muchos casos del siglo XIX en adelante, a entrar en conflicto con el
mantenimiento de un amplio dominio público de ideas común a la
humanidad. La cuestión planteada en esa época se repite todavía hoy:
¿hasta qué punto la introducción del derecho a la propiedad intelectual,
en vez de promover, restringe el progreso del conocimiento, de la
cultura y de la tecnología?

### II.

Hay diferencias sustanciales entre las características de la propiedad
intelectual y las de la propiedad material. Muchas de ellas se
establecieron en el período lleno de revoluciones, difusión de ideas y
creaciones tecnológicas que va desde mediados del siglo XVIII hasta el
final del XIX, momento en que la discusión en torno a la propiedad
pasaba por un período de transformaciones en Europa. La decadencia del
sistema feudal, el florecimiento de la burguesía comercial, la
proliferación de ideas a partir de publicaciones impresas, el
crecimiento del individualismo, las navegaciones que dieron lugar a la
invasión de América, entre otras cuestiones relacionadas, fueron
importantes para que se discutiera el estatus de la propiedad que hasta
entonces, literalmente, reinaba en los países europeos. La mayor parte
de las tierras y de bienes materiales hasta el siglo XVIII pertenecía a
las muchas monarquías que gobernaban el continente europeo, a la Iglesia
católica, a los nobles de cada región y, en menor escala, a las
comunidades que administraban de forma colectiva sus tierras y otros
recursos naturales, como bosques y lagos. Las diversas guerras en la
Inglaterra del siglo XVII y la Revolución francesa a final del siglo
XVIII tuvieron como un de sus motivos principales la quiebra de la
relación señor-vasallo que había en la gestión de las propiedades
materiales hasta entonces y, en consecuencia, el establecimiento de
nuevas leyes que frenaran el control real de las tierras y que regularan
la propiedad.

Una de las formas de legitimar intelectualmente la propiedad privada se
dio con las ideas liberales, que defendían el individualismo y la
limitación del poder del Estado absolutista de la época. Uno de los
divulgadores más importantes de esas ideas, el inglés John Locke
(1632-1704) decía que la propiedad, así como el derecho a la vida y a la
libertad, era un *derecho natural*, es decir, inherente al hombre[^56],
creado por Dios en el momento de la creación del mundo. Locke decía que,
como fruto legítimo de su trabajo, cada hombre tendría derecho a una
propiedad; «cualquier cosa que él [el hombre] no saque del estado con
que la naturaleza la promovió y dejó, la mezcla él con su trabajo y le
junta algo que es suyo, transformándola en su propiedad»[^57]. Como
límite a esa propiedad, señaló la necesidad de que las cosas en ese
«estado con el que la naturaleza las creó» permanecieran de manera que
fueran «suficientes para los demás, en cantidad y cualidad». Aquí ya
está el embrión de los debates modernos que se harán entre lo público y
lo privado en el derecho de propiedad y en torno al concepto del
procomún[^58].

[^56]: Locke hablaba de ese derecho como exclusivo de una persona de
  género masculino, ignorando a las mujeres, como sus antepasados de
  la Antigüedad y la Edad Media, y como continuaría ocurriendo hasta,
  por lo menos, la conquista de los primeros derechos civiles por las
  mujeres, en el siglo XIX.
[^57]: Locke, <cite>Dois tratados sobre o governo</cite>, p. 409.
[^58]: Locke usa aquí la palabra «procomún» (<i lang="en">commons</i>)
  de manera similar a la <i lang="lt">res comunes</i> romana, en el que
  es uno de los primeros registros cercanos a la idea de procomún que
  conocemos hoy, como «cosas que podrían ser comunmente apreciadas y
  cuidadas por la humanidad». Será el mismo procomún que, menos de dos
  siglos después, Karl Marx discutirá en <cite>Os despossuídos</cite>,
  una antología de textos de 1842 que trata sobre el derecho del uso de
  la tierra a partir del robo de la madera, una cuestión importante en
  la Alemania de la época. Se relacionará con los bienes intelectuales y
  digitales, como veremos en el capítulo 5, [«Cultura
  libre»](#cultura-libre), de este libro.

Al defender la propiedad como un derecho natural, principalmente en
<cite>Dois tratados sobre o governo</cite> (1689), el filósofo inglés
definía la noción de propiedad esencial para el desarrollo de la
libertad individual, una idea que sería clave para que la burguesía en
ascenso se librara de las limitaciones sociales impuestas por las
monarquías absolutistas, que dificultaban la movilidad social y el libre
comercio. La noción de propiedad privada divulgada por Locke ganó
influencia y se extendió como aquella que sustituiría, en el pensamiento
occidental de la época, a la concepción feudal de propiedad, real,
hereditaria e inmutable. Sería usada también como base ideológica para
la construcción de una forma de entender la propiedad privada material
como fruto del trabajo y un derecho del hombre que se difundiría en las
siguientes décadas y siglos, manteniéndose hasta el día de hoy.

Durante los siglos XVII y XVIII, la discusión sobre la propiedad
afloraba también en Francia, a partir de las ideas liberales y en
debates que involucraban a filósofos ilustrados de la época, como
Rousseau, Diderot y Voltaire. Como Inglaterra, España y otros países
gobernados por monarquías en la Europa de la época, Francia tenía su
sistema de privilegios, otorgado a determinados grupos profesionales por
los reyes &mdash;entre ellos el de los impresores, implantado desde
mediados del siglo XVI&mdash;. En 1777, la monarquía francesa concedió
los llamados «privilegios de autor» (<i lang="fr">privilèges
d’auteur</i>), que, distintos a los «privilegios de los editores»
(<i lang="fr">privilèges en librairie</i>), ya existentes, no trataba solo
del período y de la forma de comercialización de las obras (como el
<i lang="en">copyright</i> inglés establecido por el Estatuto de Anne),
sino del reconocimiento perpetuo de propiedad de las ideas. Se
considera un primer &mdash;aunque todavía incipiente&mdash; derecho
concedido a los autores, fruto de la aplicación de la noción de
propiedad privada como derecho natural también para las ideas.

Entre 1763 y 1764, por encargo de la comunidad de los editores
parisinos, entonces preocupada con la posible supresión de los derechos
editoriales que les garantizaban la exclusividad sobre las obras, el
francés Denis Diderot (1713-1784) escribe la llamada <cite>Carta sobre
el comercio de libros</cite>. El texto busca acercar la *propiedad
literaria* (como todavía era llamada en esa época también en Francia) a
la de bienes materiales y defender la propiedad perpetua de los autores y,
por extensión, de los editores, sobre las creaciones «del espíritu
humano». Dice:

> ¿Una obra no pertenece tanto a su autor como su casa o sus
> tierras? ¿No puede este alienar para siempre su propiedad? ¿Se
> permitiría, por cualquier razón o pretexto, expoliar a aquel que
> libremente lo sustituyó en sus derechos? ¿Ese sustituto no merece tener
> para ese derecho toda la protección que el gobierno concede a los
> propietarios contra los otros tipos de usurpadores?[^59]

[^59]: Diderot, <cite>Carta sobre o comércio do livro</cite>, p. 52.

Diderot, que había editado junto a D’Alembert la primera enciclopedia
entre 1751 y 1772, también defendía la extensión el derecho de autor a
sus «sustitutos», los editores, que, en su formulación, compraban
legítimamente las obras de sus creadores, teniendo así los derechos
sobre estas. Era un discurso que tomaba prestada de Locke la noción de
derecho a la propiedad como natural y buscaba aplicarla también a los
bienes intelectuales, lo que otorgaba al autor una propiedad absoluta e
inviolable sobre su obra, por tiempo indefinido. También era un
pensamiento con el que estaba de acuerdo la burguesía comercial e
industrial de la época, que buscaba cambiar el control real ejercido a
través de la concesión de privilegios por otro, basado en el derecho
natural y ejercido por el mercado.

Aunque encontraran acogida en la sociedad francesa de la época, las
ideas de Diderot sobre los derechos de autor tenían oposición incluso
dentro del liberalismo predominante en el ambiente intelectual. Marie
Jean Antoine Nicolas de Caritat, conocido como marqués de Condorcet
(1743-1794), no estaba de acuerdo con la idea de que el autor sea el
legítimo propietario de sus obras por tiempo indeterminado. En un libro
llamado <cite>Fragmentos sobre la libertad de prensa</cite>, Condorcet
resalta la importancia del interés público, critica la idea del
monopolio comercial de los editores y rechaza la idea de equiparar la
propiedad literaria a las demás formas de propiedad material.

> Se considera que no puede haber relación entre la propiedad
> de una obra y la de un campo, que puede ser cultivado por un solo
> hombre, o de un mueble que solo sirve a un hombre; por consiguiente, la
> propiedad exclusiva se basa en la naturaleza de la cosa. Así, la
> propiedad literaria no se deriva del orden natural, ni se defiende por
> la fuerza social, sino que es una propiedad fundada por la propia
> sociedad. No es un verdadero derecho [<i lang="en">véritable droit</i>],
> es un privilegio [<i lang="en">privilège</i>].[^60]

[^60]: Condorcet, <cite>Fragments sur la liberté de la presse</cite>, en
<cite>Œuvres de Condorcet</cite>, tomo 11, pp. 253-314, citado en
Machado Pontes; Sousa Alves, <cite>O direito de autor como um direito de
propriedade: um estudo histórico da origem do copyright e do droit
d’auteur</cite>.

En nombre de un ideal social también presente en la Ilustración, el de
la universalización del conocimiento, Condorcet y otros de la época
defendían la libre circulación de los textos y el fin de la apropiación
privada de una idea &mdash;todo privilegio sería un restricción al
derecho de acceso de otros ciudadanos, siendo, por tanto, nocivo a la
libertad&mdash;. También en <cite>Fragmentos sobre la libertad de
prensa</cite>, Condorcet se pregunta si los privilegios son necesarios,
útiles o nocivos al progreso de «las Luces» &mdash;como solía llamarse
al conocimiento en esa época&mdash;. Él mismo responde que no; la
propiedad literaria es «innecesaria, inútil e incluso injusta»[^61]. A
partir de ahí, defiende que una legislación que concede a los autores el
derecho de propiedad sobre sus obras no influencia positivamente el
descubrimiento de verdades útiles, «sino que comprende de manera nefasta
la manera en que esas verdades se difunden, siendo una de las
principales causas de diferencia en la sociedad entre los hombres
ilustrados o cultos y la masa inculta, para quien la mayor parte de las
verdades permanece desconocida»[^62]. Condorcet pensaba que un mundo en
que las ideas pudieran circular libremente sería aquel en que debería
haber libertad de creación, reproducción y difusión del conocimiento y
del arte, lo que volvería inapropiada cualquier apropiación individual
de los bienes culturales &mdash;un principio que volverá a oírse en
las ideas de cultura libre del siglo XX&mdash;.

[^61]: <i lang="lt">Ibidem</i>.
[^62]: <i lang="lt">Ibidem</i>.

El choque de ideas entre Diderot y Condorcet, entre otros, fomentaría la
creación de leyes durante un evento fundamental para la disminución de
los privilegios reales y de la propia monarquía en Europa, la Revolución
Francesa (1789-1799). En sus primeros años, los revolucionarios
establecieron la abolición de los privilegios comerciales (como diversos
otros) otorgados por el gobierno del rey Luis XVI &mdash;entre ellos,
los «privilegios de los editores»&mdash; y crearon leyes que formarían
las bases del sistema que, a partir de entonces, sería conocido como
<i lang="fr">droit d’auteur</i> (derecho de autor). La ley «Sobre el
trabajo del congreso sobre la propiedad literaria y artística»[^63], de
1791, concede un monopolio de explotación de artistas del teatro sobre
la representación de sus obras durante toda su vida y hasta cinco años
después de su muerte. Dos años después, otra ley aumenta el beneficio
para artistas de otras áreas y hasta diez años tras la muerte de los
autores. Inspiradas por los discursos tanto de Diderot como de
Condorcet, influenciadas también por Locke, Rosseau y otros, las leyes
trataban de conciliar los diversos intereses enfrentados involucrados.
Por un lado, consagraron la idea de Diderot sobre el carácter sagrado de
la creatividad individual y la inviolabilidad del derecho de autor; por
otro, hubo también lugar para la noción de Condorcet de que, tras cierto
tiempo (en un primer momento cinco, después diez años tras la muerte del
autor), la obra debería pertenecer al dominio público, para el progreso
de «las Luces» y del conocimiento universal.

[^63]: Disponible íntegramente en
<https://fr.wikisource.org/wiki/Compte_rendu_des_travaux_du_congr%C3%A8s_de_la_propri%C3%A9t%C3%A9_litt%C3%A9raire_et_artistique/Loi_du_19_juillet_1791>.

De esta época en adelante se consolidaron el <i lang="en">copyright</i>
inglés y el derecho de autor francés como los principales sistemas
de leyes, los cuales regulan hasta hoy la creación de bienes culturales (e
intelectuales) en Occidente. Una de las diferencias entre los dos
sistemas era la cuestión del soporte: el <i lang="en">copyright</i>
era válido inicialmente para una obra solo cuando esta se materializaba
en un soporte físico, como un libro impreso. Sin embargo, en el
<i lang="fr">droit d’auteur</i>, ese prerrequisito del soporte no existía:
las leyes pasarían a proteger la autoría y la integridad de la obra (los
derechos morales) incluso cuando esta aún fuera una idea y no estuviera
materializada en algún formato. Otras diferencias entre los dos sistemas
todavía convivirían y se volverían más complejas a lo largo de disputas
teóricas y filosóficas durante el siglo XIX, período en que varios
países empezaron a adoptar por primera vez legislaciones que regulaban
la propiedad intelectual, entre ellos Brasil[^64]. En la teoría
jurídica, se acordó relacionar el <i lang="en">copyright</i> como una
opción *utilitarista*, una licencia dada a los propietarios de una obra
para su explotación comercial durante un tiempo determinado, con el
objetivo de recuperar los costes empleados en la producción y obtener
nuevas inversiones durante el período, al tiempo que el
<i lang="fr">droit d’auteur</i>, por lo menos en su inicio, sería una
opción marcada por la influencia del *derecho natural*, que, si
tuviera éxito, tal como Diderot y otros defendían, convertiría el
derecho de autor en permanente y hereditario, lo que podría haber
llevado a la comercialización y privatización de todos los bienes
culturales y a la ausencia de un dominio público. La reglamentación
creada en Francia durante la época de la Revolución Francesa restringió
ese derecho a un determinado período, lo que, de cierta forma, mezcló
las dos influencias, utilitarista y del derecho natural, tanto en la
legislación francesa como en la de países que adoptaron el
<i lang="en">copyright</i>, como Inglaterra y Estados Unidos[^65].

[^64]: Según Paranaguá y Branco en <cite>Direitos autorais</cite>, las
  primeras referencias a los derechos de autor en Brasil datan de 1830,
  con un Código Criminal que considera como crimen la violación de
  derechos de autor. La primera ley, sin embargo, sería la n. 496/1898,
  también llamada Lei Medeiros e Albuquerque, en homenaje a su autor,
  que a su vez fue revocada por el Código Civil de 1916, que clasificó
  el derecho de autor como bien móvil, fijó el plazo de prescripción de
  una ofensa a los derechos de autor en cinco años. Solamente en 1973
  fue cuando Brasil vio publicada una ley única y completa que
  regulaba el derecho de autor.
[^65]: Y unió las nociones de <i lang="en">copyright</i> y derecho de
autor, algo que hasta hoy permanece en las legislaciones de muchos
países. Sobre esa discusión, véase en especial en un artículo de Paulo
Rená, <cite>Droit d’autor vs. copyright: diferenças conceituais entre
direito de autor e direito de cópia</cite>, Hiperfície, 28 mar. 2012.
Disponible en
<https://hiperficie.wordpress.com/2012/03/28/droit-dautor-vs-copyright-diferencas-conceituais-entre-direito-de-autor-e-direito-de-copia>.

Después de esta primera consolidación jurídica de la propiedad
intelectual, algunos tratados de las décadas siguientes serían
responsables de la determinación de estándares internacionales que
buscaban acordar algunos puntos comunes entre los países que estaban más
influidos por el sistema del <i lang="en">copyright</i> (Inglaterra,
Estados Unidos y buena parte de las excolonias anglosajonas) y los de
mayor incidencia de los derechos de autor (Francia, Alemania, España y
la mayor parte de América Latina, incluido Brasil). La Convención de
Berna, firmada durante la década de 1880, fue el principal de esos
tratados, promovida por la Asociación Literaria y Artística
Internacional, grupo creado en 1878 por influencia del escritor francés
Victor Hugo. La propuesta era definir estándares jurídicos que sirvieran
para varios países y, así, evitar que una determinada obra protegida por
<i lang="en">copyright</i> en Inglaterra, por ejemplo, pudiera ser
copiada y vendida por cualquiera en Francia, actuación que era común en
la época y no gustaba a muchos escritores, caso del mismo Victor Hugo
(autor de, entre otros, <cite>Los miserables</cite>, de 1862) y también
de Charles Dickens, a quien, para su ira[^66], republicaron varias obras
que escribió, publicadas originalmente en Inglaterra, en grandes tiradas
sin su autorización en Estados Unidos.

[^66]: Como, entre otros, contó el escritor Ruy Castro, en «Dickens y
  los piratas», <cite>Folha de S.Paulo</cite>, 8 febr. 2012, disponible
  en <https://www1.folha.uol.com.br/fsp/opiniao/24603-dickens-e-os-piratas.shtml>.

La Convención de Berna fue firmada en 1886 por países como Francia,
Bélgica, España, Suiza, Alemania, Haití, Túnez e Italia, y tuvo como
resultado la definición de derechos exclusivos &mdash;que a partir de
entonces necesitaban autorización legal&mdash; para la traducción de
obras, la adaptación y reorganización, la lectura y representación en
lugares públicos, teatros y salas de concierto, la reproducción de
copias impresas, entre otros usos. Algunos países que adoptaban el
sistema influenciado por el <i lang="en">copyright</i> se opusieron a
algunas definiciones, es el caso de Inglaterra, que firmaría la
convención el año siguiente, pero no seguiría gran parte de las
disposiciones hasta un siglo después, en 1988[^67], y de Estados Unidos,
que se negaron a firmar con el pretexto de que el acuerdo establecido en
Berna cambiaría de forma significativa su legislación de derecho de
autor &mdash;y solo hicieron efectivas todas las reglas del acuerdo
internacional en 1989[^68]&mdash;. A pesar de las oposiciones, la
Convención de Berna se consolidó como el tratado de propiedad
intelectual más aceptado del mundo; dio origen a entidades
internacionales[^69] de administración de esos derechos y pasó también a
orientar los cambios que las nuevas tecnologías desarrolladas en las
décadas siguientes y en el siglo XX causarían en la producción y
difusión de bienes culturales.

[^67]: A partir de <cite>Copyright, Designs and Patents Act 1988</cite>,
  que reformaría la ley de <i lang="en">copyright</i> del país.
  Legislación completa disponible en
  <https://www.legislation.gov.uk/ukpga/1988/48/contents>.
[^68]: Según la lista de países firmantes de la World Intellectual
  Property Organization (WIPO). Fuente:
  <https://www.wipo.int/treaties/en/ShowResults.jsp?lang=en&treaty_id=15>.
[^69]: Primeramente, la United International Bureaux for the Protection
  of Intellectual Property (BIRPI), creada en 1893 para organizar la
  Convención de Berna y la de París, que dio origen a la noción
  internacional de propiedad industrial. A partir de 1970, cambia su
  nombre al que todavía hoy mantiene: World Intellectual Property
  Organization (WIPO).

### III.

La creación de una noción de propiedad intelectual en el siglo XIX está
también ligada a las nuevas tecnologías de reproducción y expresión
desarrolladas en ese período. Así como, en el siglo XVI, los primeros
privilegios para los impresores y el <i lang="en">copyright</i>
surgieron después de la invención y la propagación de la máquina de
impresión de tipos móviles en Europa, también las nuevas formas de
reglamentar la creación y la reproducción de bienes culturales se dan
con la introducción de nuevas tecnologías. No obstante, al contrario que
las máquinas de impresión, que hicieron circular ideas en diferentes
formatos, pero solamente en un tipo de soporte, las tecnologías del
siglo XIX amplían los soportes de transmisión de ideas al audio y las
imágenes, lo que también aumenta la velocidad de circulación de la
información y empieza a dar fin al impreso como principal soporte de
disfrute y consumo de bienes culturales.

Las maneras en que las invenciones tecnológicas del siglo XIX se
relacionan e influencian unas a otras son diversas y complejas. Para
facilitar y analizar ciertos impactos, podemos dividir esas tecnologías
en dos grupos: las *tecnologías de comunicación*, que, al acortar
distancias y conectar de forma más rápida a personas en diferentes
lugares, aumentaron el intercambio de nueva información, es el caso del
telégrafo, del teléfono y de la radio &mdash;todas, a su vez, muy
relacionadas con la expansión de los medios de transporte, como el tren,
el barco de vapor y el automóvil&mdash;; y las *tecnologías de
grabación y reproducción*, aquí consideradas tanto las de sonido, como
el gramófono y el fonógrafo, como las de imagen, que combinaron
tradiciones antiguas hechas de trucos físicos y mezclas químicas de
sustancias con nuevas técnicas e invenciones provenientes de la
expansión de la ciencia &mdash;y también de la industria&mdash; de la
época, es el caso principalmente de la fotografía y del cine.

En el grupo de *tecnologías de comunicación*, el telégrafo inauguró, en
la primera mitad del siglo XIX, una nueva era de difusión de información
al transmitir mensajes mediante impulsos eléctricos a regiones
separadas por miles de kilómetros. Su creación estaba asociada al
desarrollo del ferrocarril, que exigía métodos instantáneos de
señalización por seguridad, «aunque hubiera algunos hilos telegráficos
que seguían las vías, no de los ferrocarriles, sino de los
canales»[^70]. Se atribuye a los ingleses William Fothergill Cooke y
Charles Wheatstone el origen de un primer sistema de uso comercial del
telégrafo, en 1837, con el objetivo de acompañar la construcción del
ferrocarril entre Londres y Birmingham, en Inglaterra[^71]. En las
décadas siguientes se popularizaría como un servicio proporcionado por
el Estado en la mayor parte de Occidente, lo que aumentó a niveles
entonces desconocidos la velocidad de transmisión de información,
pública y privada, local y regional, nacional e imperial.

[^70]: Briggs; Burke, <i lang="lt">op.&nbsp;cit.</i>, p. 140.
[^71]: <i lang="lt">Ibidem</i>.

El año que se conoció como el de la primera transmisión telegráfica es
recordado por nosotros hasta hoy por ser también el de la publicación de
la *patente* de invención, por los ya citados Cooke y Wheatstone. Aquí
vale recordar que, además del derecho de autor y el
<i lang="en">copyright</i>, los siglos XVIII y XIX también vieron
surgir, conosolidarse en minucias legales y propagarse como una de las
bases del modo de producción capitalista otra noción jurídica de
apropiación de las ideas: la patente, que a partir de entonces sería
definida como un registro de una concesión, pública y limitada, para la
explotación privada y comercial de una idea. A diferencia de los bienes
culturales, las patentes se aplican a bienes considerados utilitarios
&mdash;más tarde, la noción incluiría el <i lang="en">software</i> e
incluso una fórmula matemática, como un algoritmo&mdash;, que, en ese
momento, lograron la reproducción en masa a partir de los parques
industriales en expansión. Durante otro de los tratados internacionales
reguladores de la propiedad intelectual de ese período, la Convención de
París de 1883, la patente da origen a una ramificación en los estudios y
regulaciones jurídicas sobre la propiedad intelectual, que pasa a ser
llamada entonces *propiedad industrial*, brazo jurídico que va a regular
mundialmente invenciones como el telégrafo, además de registros de
diseño industrial y marcas (nombres comerciales), diseño de productos y
envoltorios, entre otros diversos objetos de una lista que solo
aumentaría con las nuevas tecnologías desarrolladas en el siglo XX.

El telégrafo propició por lo menos dos inventos más que ayudaron a
acelerar la difusión de ideas por todo el mundo en el siglo XIX. El
primero fue el teléfono, presentado por Alexander Graham Bell en la
Oficina de Patentes de los Estados Unidos en 1876 como «la manera de, y
el instrumento para, transmitir sonidos vocales u otros
telegráficamente, causando ondulaciones eléctricas, similares a las
vibraciones del aire que acompañan al sonido vocal»[^72]. Se servía de
los canales de transmisión de mensajes del telégrafo para transformar
energía acústica &mdash;la voz&mdash; en energía eléctrica, lo que
permitiría el intercambio de información a través del habla entre dos (o
más) puntos conectados por una red. El segundo fue la radio, en 1895,
año en que el italiano Guglielmo Marconi, entonces con 21 años, realizó
su primera transmisión en un sistema de propagación de señales en ondas
sonoras a partir de una antena hacia lugares a poco más de tres
kilómetros de distancia del origen. Era una especie de «telégrafo
inalámbrico«, con información sonora codificada en una señal
electromagnética que propaga mediante ondas, medidas en hertz, en el
espacio físico. Un año después, entonces viviendo en Inglaterra, Marconi
registró su patente como «mejoras en la transmisión de impulsos y
señales eléctricos y en los respectivos aparatos»[^73], la primera
emitida para un sistema de telégrafo sin filo a base de ondas
hertzianas.

[^72]: Fuente: <http://www2.iath.virginia.edu/albell/bpat.1.html>. Para
  más información véase
  <https://pt.wikipedia.org/wiki/Alexander_Graham_Bell>.
[^73]: «Improvements in Transmitting Electrical Impulses and Signals and in
Apparatus there-for». Fuente: Hong, <cite>Wireless: From Marconi’s
Black-box to the Audion</cite>. Se puede encontrar más información al
respecto en el artículo de Wikipedia sobre la historia de la radio:
<https://en.wikipedia.org/wiki/History_of_radio#cite_note-34>.

Hay varios inventos cercanos y competidores surgidos en esa época que
pueden identificarse aquí como *tecnologías de grabación y
reproducción*. Son, todos ellos, puntos culminantes de numerosas
intentos a lo largo de la historia de grabar, reproducir y almacenar
sonidos e imágenes que, cuando empiezan a circular en la sociedad,
alteran la dependencia de una medición simbólica mediante el alfabeto,
predominante hasta entonces, para la comprensión de la realidad. Son
métodos que empiezan a almacenar y transmitir, en forma de ondas de luz
y sonido, efectos visuales y acústicos de lo real, volviendo autónomos
los oídos y los ojos[^74] &mdash;lo que produce una serie de
transformaciones en la forma de producir, circular, consumir y
regular los bienes culturales a partir de entonces&mdash;.

[^74]: Kittler, <cite>Gramofone filme typewriter</cite>, p. 24.

El primero de estos inventos es el fonógrafo, cuya presentación pública
fue el 6 de diciembre de 1877 en Estados Unidos por Thomas Edison, señor
del entonces primer laboratorio de investigación en la historia de la
tecnología, en Menlo Park, Nueva Jersey[^75]. El aparato transformaba,
a partir del giro de una manivela, sonidos emitidos en una boquilla en
trazos en un cilindro pequeño con surcos que después podían ser
reproducidos y amplificados a partir de un cono acoplado al aparato. El
gramófono, creado y patentado por el alemán Emil Berliner en 1888, ya
hacía lo mismo, pero usando un disco plano (de cera, goma laca, cobre,
después vinilo) en vez del cilindro. La tecnología detrás de los dos
productos era un poco diferente, así como las intenciones de los
inventores; pero interesado en la calidad de grabación de música
clásica, Berliner optó por el uso de una matriz para duplicar las
grabaciones sonoras, ya que, para él, la capacidad de repetición
importaba más que para Edison y también que para Graham Bell &mdash;que
inventó otro aparato parecido entonces, el *grafófono*&mdash;, quienes
concebían el uso de sus inventos para registros familiares o en
oficinas[^76]. En las primeras décadas del siglo XX, el disco plano de
Berliner ganó la disputa con los cilindros de Edison y se consolidó como
el formato más usado para ese tipo de aparato de grabación y
reproducción sonora, sobre todo porque era más fácil de producirse de
forma industrial que los cilindros y podía incluir capas, sellos y otros
accesorios.

[^75]: <i lang="lt">Ibidem</i>.
[^76]: Briggs; Burke, <i lang="lt">op.&nbsp;cit.</i>, pp. 181-182.

Un poco antes, aún en la primera mitad del siglo XIX, el daguerrotipo,
presentado públicamente por el francés Louis Daguerre en 1839, fue el
primer proceso de producción de imágenes en extenderse ampliamente por
Occidente. Consistía en una placa de cobre, u otro metal más barato, que
con un baño de plata formaba una superficie reflectante que, al ser
colocada en una caja oscura y expuesta a una determinada situación
durante algún tiempo (que podría ser de hasta diez minutos en ese primer
momento), formaba un «retrato» de esa situación, siendo mostrado
públicamente después del revelado en un proceso químico. No era un
procedimiento fácil, pero se difundió por Occidente en la décadas de
1840 y 1850 especialmente por ser más práctico y barato que los retratos
pintados, muy comunes en la época en las familias burguesas e
industriales. Junto al *calotipo* (proceso que usaba nitrato de plata y
producía «negativos» sorbre papel, desarrollado por el inglés William
Heny Fox Talbot un año después), el daguerrotipo sería el más común de
los diversos procedimientos fotográficos existentes en la época hasta la
consolidación del método de fotografía instantánea con rollos de
película, a final del siglo XIX. Patentado por George Eastman, un
banquero transformado en empresario en Estados Unidos, ese método sería
la base para la creación y comercialización de las cámaras con
películas de carrete, principal producto de una empresa que Eastman
fundaría en 1882, la Kodak, casi convertida en sinónimo de la fotografía
en el siglo XX.

La introducción de la «imagen en movimiento» con el cine tal vez haya
sido el mayor cambio tecnológico de aquel momento. Nació a partir de
varias innovaciones que van desde la consolidación del dominio
fotográfico hasta la síntesis del movimiento; durante todo el siglo hubo
experimentos que, a partir de principios más antiguos, como la *cámara
oscura*[^77], trataban de producir y reproducir imágenes en movimiento,
es el caso de algunos experimentos ópticos como el *zoótropo* (en
1828-1832 por William George Horner) y el *praxinoscopio*[^78] (1877 por
Émile Reynaud). El ya citado Thomas Edison trabajó en el tema y en 1891
saldría, del laboratorio de tecnología que dirigía, la patente del
*cinetógrafo*, una máquina que registraba imágenes en movimiento y las
exhibía en un catalejo dentro de un cajón de madera. Dos años después,
vendría del ingeniero jefe de los Edison Laboratories, William Kennedy
Laurie Dickson, la patente del *quinetoscopio*, un instrumento de
proyección interna de películas con un visor individual por el cual se
podía asistir, mediante la inserción de una moneda, a una pequeña tira
de película en <i lang="en">looping</i>. Los lugares con quinetoscopios
se volverían populares en las décadas siguientes en Estados Unidos y
serían llamados de <i lang="en">nickelodeons</i>; exhibían imágenes en
movimiento de números cómicos con animales adiestrados, ejercicios de
circo y bailarinas bailando, y obtuvieron un éxito comercial
considerable.

[^77]: Con referencias primarias que remiten a los griegos, la cámara
  oscura es un tipo de aparato óptico basado en el principio del mismo
  nombre que consiste en una caja (que puede tener algunos centímetros o
  alcanzar las dimensiones de una sala) con un orificio en una de sus
  caras. La luz, reflejada por algún objeto externo, pasa por ese
  orificio, atraviesa la caja y llega a la superficie interna opuesta,
  donde se forma una imagen invertida de aquel objeto. Fuente:
  <https://pt.wikipedia.org/wiki/C%C3%A2mera_escura>.
[^78]: Entra en esta lista de juegos ópticos también el estroboscopio.
  Véase más en <https://pt.wikipedia.org/wiki/Hist%C3%B3ria_do_cinema>.

Dos años después del registro de la patente del quinetoscopio, ocurrió
lo que pasó a la historia del cine como la primera proyección de pago de
una película de corta duración, en el Salón Grand Café, en París, el 28
de diciembre de 1895. Fue una presentación pública de un aparato
inventado &mdash;y patentado el mismo año en Francia&mdash; por los
hermanos Lumière (Auguste y Louis) llamado *cinematógrafo*, que, basado
en el cinetógrafo de los laboratorios Edison, funcionaba como una
máquina tres en uno: grababa, revelaba y proyectaba películas. Con gran
cobertura de la prensa de la época, la considerada como primera sesión
de cine proyectó diez cortos de los dos hermanos, todos de menos de un
minuto, mudos (las películas sonoras solo aparecerían después de 1927) y
que hoy serían considerados documentales. Entre los mostrados estaba
<cite>La Sortie de l’Usine Lumière à Lyon</cite>, el primero de la
sesión y también la primera película de la historia del cine, que
mostraba escenas de personas saliendo de la fábrica de los Lumière en
Lyon.

### IV.

Al observar esa época y las diferentes historias sobre dónde, cómo y
quién había creado esos inventos tecnológicos, son importantes algunas
consideraciones sobre patentes y propiedad intelectual. La primera de
ellas es que el teléfono, la radio, el gramófono, la fotografía y el
cine habían sido creadas «a hombros de gigantes», como dice la expresión
atribuida al francés Bernardo de Chartres en el siglo XII y popularizada
por Isaac Newton en 1675. Decir esto revela que fueron invenciones que
en gran medida fueron posibles a partir de otras creaciones
&mdash;aparatos técnicos, ideas y mecanismos que no llegaron a nosotros
porque se perdieron debido a la escasez de recursos de esos inventores
para hacer un registro que perdurara&mdash;, o entonces fueron
incorporadas a otras ideas de quien, con más posibilidades técnicas y
financieras, pondría esos inventos en circulación a una escala
industrial.

La segunda consideración es que, especialmente en esa época, había
muchas discrepancias sobre quién realmente había inventado las
tecnologías de comunicación, grabación y reproducción aquí
identificadas. El teléfono, por ejemplo, ya tenía un antepasado muy
cercano alrededor del 1860, dieciséis años antes del registro de patente
de Graham Bell; era una especia de «teléfono parlante» desarrollado por
el italiano residente en Estados Unidos Antonio Meucci, que llegó a
trabajar con Bell y a registrar su invención en 1871, al tiempo que el
alemán Johan Philipp Reis, en 1861, y el estadounidense Elisha Grey, en
el mismo 1876 de la patente de Bell, también trabajaron con prototipos
parecidos. Dos años antes de la primera transmisión mediante ondas
hertzianas del italiano Marconi, en 1893, un cura brasileño llamado
Roberto Landell de Moura hacía, en Puerto Alegre, experimentos
semejantes de transmisión de voz mediante ondas, lo que solo se
confirmaría y documentaría oficialmente en 1900, ya después de la
patente de Marconi. De los varios antecesores del fonógrafo y del
gramófono, hay uno muy cercano en particular, llamado
<i lang="fr">paleofone</i>, que fue registrado por el francés Charles Cros
en su país el mismo año del registro realizado por Edison en Estados
Unidos. La disputa por la invención del cine entre los Lumière, hijos de
un pequeño empresario francés de Lyon, y Edison causó y todavía hoy
causa discrepancias, ya que ambos hacían, en el mismo período,
películas diferentes.

Estas y muchas historias semejantes de la época nos muestras que Graham
Bell, Thomas Edison y Guglielmo Marconi fueron principalmente
empresarios y rápidos patentadores, que supieron anticipar posibilidades
de negocios lucrativos a partir de los inventos que registraron. Con sus
patentes, trataron de garantizar jurídicamente la exclusividad de
producción y uso de productos que no necesariamente inventaron, sino
que, a partir de estructuras (o de los contactos) de producción entonces
bien establecidas, aumentaron su difusión a partir de la producción a
escala industrial y la distribución masiva como mercancía. Buscaban la
recuperación de sus inversiones en administración de investigación y
desarrollo, es cierto, pero también la garantía del mantenimiento de sus
beneficios durante mucho tiempo &mdash;lo que, a partir del registro de
patentes, de hecho ocurrió&mdash;.

Por ejemplo, Graham Bell. De familia escocesa que trabajaba en el otrora
prometedor negocio de la oratoria, Alexander migró a Canadá al inicio de
su vida adulta e hizo carrera en Estados Unidos como inventor y
empresario; fue uno de los fundadores de American Telephone and
Telegraph Company (AT&T), una de las mayores empresas de telefonía
(luego de Internet y también de televisión por cable) de Estados Unidos
del siglo XX. Thomas Edison, que trabajó con Graham Bell, era un
empresario de tecnología, financiado por figuras como Henry Ford y
Harvey Firestone, creador de un laboratorio de producción de inventos
que se convertiría en General Electric, uno de los más grandes
conglomerados industriales del planeta todavía a día de hoy. A partir
del registro de patente de la radio en Inglaterra en 1896, Marconi
crearía la Wireless Telegraph & Signal Company en el país, después
transformada en Marconi Co., empresa que sería una de las más
importantes de las telecomunicaciones británicas en las primeras décadas
del siglo XX.

A diferencia de Graham Bell, Edison, Marconi y también de los Lumière,
que ya en ese época tenían una estructura para patentar y empezar a
producir sus inventos a mayor escala, Meucci, Landell de Moura, Cros y
otras figuras no tan recordadas hoy eran inventores que, sin muchos
recursos para producir y litigar en el entonces fuerte *mercado* de
patentes, no transformaron sus ideas en productos vendibles. Meucci, por
ejemplo, era un inmigrante; nació en Italia, vivió quince años con su
esposa y familia en La Habana, Cuba, donde hay registros de que
ya en 1849 inventó el *telégrafo parlante* a partir de una máquina de
electrochoques[^79]. En 1850, con algunos ahorros, se mudó a Estados
Unidos con el objetivo de vivir de sus invenciones &mdash;en la época,
la joven nación se estaba consolidando como un gran lugar de
peregrinación para inventores y empresarios que querían trabajar en sus
inventos&mdash;. Meucci montó una fábrica de velas, empleó a otros
compatriotas exiliados, se involucró en la política de su país
&mdash;Giuseppe Garibaldi, líder de la unificación de Italia, trabajó en
su fábrica y estuvo alquilando su casa durante cuatro años&mdash;,
falló, montó otra empresa, ahora basada en su *telégrafo parlante*,
llamada Telettrofono Company, que llegó a registrar su invento en 1871,
cinco años antes del teléfono de Bell. Pero, sin tantos recursos y poder
político como Bell, perdió las disputas jurídicas contra él relacionadas
con las patentes y no consiguió desarrollar más su invento[^80].

[^79]: Más detalles sobre Meucci y las fuentes de información aportadas
  aquí en <https://en.wikipedia.org/wiki/Antonio_Meucci>.
[^80]: En un pequeño reconocimiento tardío, en 2002, la U. S. House of
  Representatives (equivalente a la Cámara de los Diputados brasileña)
  homenajeó a Meucci en una resolución
  (<https://www.congress.gov/bill/107th-congress/house-resolution/269>)
  por haber formado parte del desarrollo del teléfono, a pesar de que no
  especifique cuál y haya varias disputas sobre quién realmente habría
  inventado primero el teléfono.

En la periferia del mundo de las patentes de la época (y aún hoy), el
cura católico brasileño Landell de Moura hacía pruebas, muchas veces en
solitario, en sus iglesias en Puerto Alegre, São Paulo y Campinas, con
el telégrafo y lo que vendría a ser la radio en la misma época que
Marconi en Italia. Fue solamente en 1900, en São Paulo, cuando Landell
de Moura consiguió hacer un registro aceptado por los trámites de la
época, teniendo testigos y siendo documentado por el Jornal do
Commercio[^81]. El año siguiente, obtendría una primera patente
brasileña para lo que llamaba «aparato destinado a la transmisión
fonética a distancia, con hilo o sin hilo, a través del espacio, de la
tierra o del elemento acuoso». Con ella viajaría los años siguientes a
Europa y Estados Unidos, donde, en 1904, también dejaría patentes de
«transmisor de ondas», «telégrafo sin hilo» y «teléfono sin hilo», con
alguna repercusión. No obstante, vuelve a Brasil en 1905, donde continúa
sus experimentos, pero, sin el apoyo de la Iglesia, de los empresarios o
de los gobernantes locales, no desarrolla más sus investigaciones
autodidactas; Marconi, Bell y otros, en Europa y en Estados Unidos,
siguieron[^82].

[^81]: Jornal do Commercio, 10 jun. 1899, p. 3. Fuente:
<http://landelldemoura.com.br/>.
[^82]: Esta información sobre Landell de Moura está basada en
<https://pt.wikipedia.org/wiki/Roberto_Landell_de_Moura>.

El 30 de abril de 1877, ocho meses antes de que Thomas Edison registrara
la patente del fonógrafo en Estados Unidos, el escritor bohemio e
inventor francés Charles Cron depositó un sobre cerrado en la Academia
de Ciencias francesa con un artículo sobre un «procedimiento de
grabación y reproducción percibidos con el oído». Era el mismo modo de
funcionamiento del aparato de Edison, que conocía los rumores de la
invención de Cros[^83]. Pero el francés carecía de lo que, al otro lado
del atlántico, el laboratorio en Menlo Park tenía de sobra: condiciones
técnicas y financieras para la realización práctica de la idea. De ahí
también el hecho de que el fonógrafo, un mes después de la presentación
pública y el registro de Edison, comenzó a producirse en masa, mientras
que el paleófono, invento de Cros, fue olvidado. Sin posibilidad de
reclamar jurídicamente algún crédito por las ideas, el francés no llegó
a ver las transformaciones que la rica biblioteca de audios que él
anticipó producirían en el mundo; murió en 1888, a los 45 años.

[^83]: Kittler, <i lang="lt">op.&nbsp;cit.</i>, p. 47.

Entre todos estos hombres blancos y de las Américas y Europa &mdash;y
aquí también hay una distinción de género, color y origen de aquellos
que *pasaron a la historia* y los que fueron borrados o no citados en
esos registros&mdash;, Louis Daguerre tan vez haya sido un caso extraño
para la cuestión de la propiedad intelectual. Socio de Joseph Niépse, a
quien se le atribuye la primera «fotografía de la vida», llamada
*heliografía*[^84] y presentada por lo menos una década antes, Daguerre
mostró su invento a la Academia Francesa de Ciencias en 1839. El Estado
francés adquirió la patente del daguerrotipo y, justo después, la
convirtió en dominio público, «abierta para el mundo entero»[^85]. Ese
gesto, inusual entre las tecnologías de comunicación, grabación y
reproducción aquí citadas, facilitó que hubiera una verdadera
<i lang="fr">daguerréomanie</i> en Francia, con un gran número de
daquerrotipografos también en otros países; «había diez mil de ellos en
América en 1853, entre ellos Samuel Morse, y en Gran Bretaña había cerca
de dos mil fotógrafos registrados en el censo de 1861»[^86]. Otros
procedimientos de producción fotográfica más baratos y fáciles de
reproducirse, como el calotipo de Henry Fox Talbot, y después el rollo
de película de Eastman y de la Kodak (ambos registrados como patentes
privadas), volvieron el daguerrotipo un procedimiento obsoleto que no
llegó a desarrollarse a escala industrial después de 1870.

[^84]: Joseph recubrió una placa de estaño con betún blanco de Judea que
  tenía la propiedad de edurecerse al entrar en contacto con la luz. En
  las partes no afectadas, el betún era retirado con una solución de
  esencia de lavanda. En 1826, exponiendo una de esas placas durante
  ocho horas aproximadamente en su cámara oscura fabricada, consiguió
  una imagen del patio de su casa. «Heliografía» significa grabado con
  la luz solar. Fuente:
  <https://en.wikipedia.org/wiki/Nic%C3%A9phore_Ni%C3%A9pce>.
[^85]: Briggs; Burke, <i lang="lt">op.&nbsp;cit.</i>, p. 166.
[^86]: <i lang="lt">Ibidem</i>, p. 167.

### V.

Ante la consolidación de la propiedad intelectual en el siglo XIX,
conviene replantear la pregunta del inicio de este capítulo de otra
forma: ¿la introducción de los elementos jurídicos reguladores de las
propiedades de las ideas restringieron o promovieron el progreso del
conocimiento, de la cultura y de la tecnología? Una respuesta posible
sería decir que lo promovieron, basta ver la cantidad de inventos que se
hicieron populares en esa época y las enormes transformaciones que estos
trajeron a la sociedad. También es aceptable decir que los cambios
causados por las leyes de derechos de autor de esa época, por ejemplo,
permitieron que muchos artistas empezaran a poder vivir de sus trabajos
y no quedaran más a merced de monopolios e intereses de la Corona, lo
que les aseguró una serie de derechos y les otorgó garantías que les
pusieron al nivel, en algunos casos, de otros trabajadores profesionales
de la época, además de darles más &mdash;por lo menos en teoría&mdash;
posibilidades de libertad para la creación, sen el control religioso o
estatal.

Otra respuesta posible es decir que los mecanismos jurídicos reguladores
de la propiedad intelectual restringieron el progreso y el acceso al
conocimiento. Antes una base de datos casi infinita y de acceso libre,
el dominio público de ideas e información pasó a tener sus ideas
encerradas en pequeños feudos, mayores o menores de acuerdo a las
posibilidades ecónomicas y las disposiciones político-institucionales de
quienes las retenían. En un primer momento, el cierre de algunas ideas
del dominio público es por poco tiempo; las primeras leyes de
<i lang="en">copyright</i> establecieron 14 años después de la
publicación como el período de explotación comercial exclusivo de la
obra, con el fin de remunerar al autor (o a los intermediarios que
habían financiado su producción) por la inversión realizada. Pero, con cada
nuevo aparato tecnológico &mdash;y el mundo lucrativo abierto por
estos&mdash;, este período se vuelve mayor: 40, 50, 70, 120 tras la
publicación o 70 años después de la muerte del autor, como se consolidó
en las leyes de derecho de autor en Brasil y en muchos países del mundo
en el siglo XX[^87].

[^87]: Una lista del período tras el cual una obra entra en dominio
  público puede consultarse en Wikipedia:
  <https://pt.wikipedia.org/wiki/Dom%C3%ADnio_p%C3%BAblico>.

Usada como justificación ideológica por reyes, nobleza e Iglesia para
la regulación de la publicación de las ideas, la censura cede lugar, a
partir de los siglos XVIII y XIX, al mercado y a la libre competencia.
Ya no es por tratar temas prohibidos a los ojos de los censores que la
circulación de ideas necesita ser controlada; es para que una persona
pueda vivir de (y lucrarse con) sus invenciones, de modo exclusivo y sin
competir con otro individuo (o empresa). Para eso, las leyes; para
hacerlas cumplir, el Estado. En un contexto de aumento de la velocidad
de circulación de la información, y con la enorme posibilidad de
reproducción de ideas a partir de las tecnologías citadas, fue así cómo
la sociedad capitalista occidental se organizó a partir de entonces, y
hasta el día de hoy, en lo que se refiere a la producción y circulación
de ideas.

Pero la manera de gestionar la propiedad de ideas a partir de la noción
de propiedad intelectual y sus ramificaciones (derechos de autor y
propiedad industrial) no sería la única desde entonces. Corriente de
ideas también surgida en la segunda mitad del siglo XIX, el anarquismo
negaría desde su inicio los derechos de autor; la frase «¡la propiedad
es un robo!», sacada de un texto de Pierre-Joseph Proudhon de 1840
&mdash;un año después de que la patente del daguerrotipo fuera
convertida en dominio público en Francia&mdash;, sería aplicada desde el
principio a la propiedad material, pero no por eso dejaría de incluir
también a la propiedad industrial, como buena parte de las obras (sobre
todo impresas) anarquistas desde entonces dejarían claro en sus páginas
iniciales con mensajes como «sin derechos reservados», «todos los
derechos rechazados», entre otros mensajes explícitos negando la
existencia de derechos de autor. Hacían eso de forma clara y coherente
con sus principios: las ideas, como las tierras, deben ser libres,
circular libremente, sin restricciones tanto de monopolios reales o
religiosos como de regulaciones legales promovidas por el Estado para
controlar la competencia del mercado. La forma de analizar esos
principios filosóficos en la práctica de la supervivencia cotidiana en
un planeta cada vez más dominado por el capitalismo y su propiedad
privada suscitan matices y discusiones diversas hasta el día de hoy. Es
de notar que, considerada por muchos ingenua, la perspectiva de la
ausencia de propiedad que el anarquismo defendió, teniendo la autonomía
de la persona como eje central de sus preocupaciones, va a encontrar eco
en jáqueres e influenciar la construcción de Internet &mdash;y del
<i lang="en">software</i> libre&mdash; décadas después. Será una idea
astutamente presente e influyente en la sociedad hasta el día de
hoy, como se muestra en el siguiente capítulo.

También fruto del siglo XIX, el socialismo trataría los derechos de
autor de forma diferente. Tanto en la Unión Soviética (URSS) como en
Cuba estaban en vigor las leyes de derecho de autor acordadas en Berna
cuando, en 1917 y 1959, respectivamente, estallan las revoluciones
soviéticas y cubana. En 1928, la ley de derecho de autor en el país
europeo, de influencia romano-francesa, es alterada, y el período de
validez de los derechos (patrimoniales) se reduce a un intervalo más
cercano a las leyes iniciales del siglo XVIII: 25 años después de la
publicación de una obra o 15 años tras la muerte del autor. Así
permanece hasta 1973, cuando la URSS firma los tratados internacionales
de propiedad intelectual y adopta el plazo estándar de 70 años tras la
muerte del autor como el oficial para la vigencia de los derechos
patrimoniales; los morales, que hablan respecto al reconocimiento de
autoría, son perpetuos e inalienables. Ese plazo se aplica también hoy a
Rusia y a todos los Estados postsoviéticos como Ucrania, Georgia,
Estonia, Lituania, Moldavia, entre otros[^88]. En Cuba sucede algo
parecido: la ley es alterada en 1977 y disminuye el plazo de extensión
de los derechos de autor a 25 años después de la muerte del autor, lo
que permanece hasta 1994, cuando Cuba firma entonces tratados
internacionales y adopta el período de 50 años tras la muerte del autor,
que permanece en 2022. En China y en Corea del Norte, otros países
que adoptaron el régimen socialista en el siglo XX, hay una larga
tradición social colectivista que hace que las nociones de copia,
autoría y propiedad intelectual sean entendidas de maneras diferentes,
que serán tratadas en el capítulo 6 [«Cultura
colectiva»](#cultura-colectiva).

[^88]: Como muestra la lista de la nota anterior.

Es de imaginar, finalmente, que en un contexto de gran circulación de
aparatos tecnológicos de reproducción y comunicación, términos como
«plagio», «copia» y «creación» ganarían otros significados. Si el
romanticismo fija en el siglo XIX la percepción, hasta hoy predominante,
del autor como genio creativo solitario, un legítimo *propietario* de
bienes culturales, al inicio del siglo XX va a cuajar esa noción casi
sagrada de creación. Artistas y creadores en general van a torcer y a
darle la vuelta a la noción de plagio y usarlo como método de producción
artística y estrategia de confrontación a la propiedad intelectual
&mdash;y, por consiguiente, al propio capitalismo&mdash;. La copia de la
copia de la copia generaría otras formas de expresión, que a su vez
serían recombinadas y formarían la base de muchos bienes culturales
ampliamente conocidos en el siglo XX y hasta el día de hoy.

