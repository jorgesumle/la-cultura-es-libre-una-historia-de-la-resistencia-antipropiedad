La palabra «cultura» tiene tantos significados a lo largo de la historia
que vamos a empezar buscando una definición para poder añadirle el
*libre* que da nombre a este libro. El primer capítulo del libro
<cite>Micropolíticas: cartografias do desejo</cite> (1984), de Felix
Guattari y Suely Rolnik, tiene de título «Cultura: ¿un concepto
reaccionario?», un texto que aporta diferentes significados de cultura
que nos pueden ayudar: el *sentido A* es definido como *cultura-valor* y
corresponde a un juicio de valor que determina quién tiene y quién no
tiene cultura. Se manifiesta, por ejemplo, en ciertos diálogos
habituales en los que se dice que «tal individuo está bien educado,
estudió en colegios caros, viajó por todo el mundo, *tiene cultura*».

El *sentido B* es el de *cultura-alma colectiva*, algo que, a diferencia
del primero, todos tienen: hay cultura negra, cultura
<i lang="en">queer</i>, cultura <i lang="en">underground</i>. Sería el
conjunto de producciones, valores, modos de hacer y de vivir, una
«especie de alma un tanto difusa, difícil de captar, y que dio lugar a
todo tipo de ambigüedades a lo largo de la historia»[^2]. A cada alma
colectiva (los pueblos, las etnias, los grupos sociales) se le atribuye
una cultura: en muchos casos es también sinónimo de civilización, algo
que fue bastante problematizado en la antropología, disciplina en la
que la cultura es un tema principal y que, por eso mismo, cuenta con
innumerables conceptos y debates[^3]. El *sentido C* propuesto por
Guattari y Rolnik es el de *cultura-mercancía*, como un producto puesto
en mercado de circulación monetaria. Es un sentido más objetivo que los
otros dos, pues se refiere a algo que podemos ver y tocar: un libro, un
cuadro, por ejemplo. Podríamos usar este sentido para designar otra
noción, la de bienes culturales, que serían aquellos objetos puestos en
circulación en *un mercado* que incluye otras personas además de su
creador. Algunos ejemplos son un diseño publicado en un blog en
Internet, un vídeo producido por cuatro personas con un móvil
inteligente y publicado en una plataforma de emisión en continuo, textos
políticos maquetados en un <i lang="en">zine</i> para venderse o
distribuirse en un puestecito en la calle, un libro de poesía de una
editorial, un ensayo sobre arte en una revista mensual. Existen varios
otros; basta con satisfacer la necesidad de que sean organizados en
algún formato reconocido y de que se distribuyan a varias personas.

[^2]: Guattari; Rolnik, <cite>Micropolíticas: cartografias do desejo</cite>, p. 19.
[^3]: Como muchos otros antropólogos, Clifford Geertz, por ejemplo,
  habla de cultura como modos de vida diversos y distintas maneras de
  expresarse (<cite>La interpretación de las culturas</cite>); Marshall
  Sahlins habla de cultura como razón práctica (<cite>Cultura y razón
  práctica</cite>); y Roy Wagner habla de la capacidad inventiva en la
  alteridad (<cite>La invención de la cultura</cite>).

En el sentido A, no es como hablar de la libertad de una cultura que es
vista como un valor, pues, aunque sea posible escribir, no es lógico
hablar de «valor libre» en oposición a un «valor cerrado», por ejemplo.
Un individuo considerado como alguien que tiene cultura no se identifica
como *portador* de una cultura libre. En el sentido B, cultura como
«alma colectiva» esta ya es libre <i lang="lt">a priori</i>; no hay
<em>cultura <i lang="en">underground</i></em> que no sea libre, ni una
cultura como la samba o el hip-hop, por ejemplo, que sea completamente
cerrada y propiedad de una única empresa. Pero hay bienes culturales
producidos en el ámbito de esas culturas que no son libres, objetos que
beben de las almas colectivas nombradas y pasan a circular en un mercado
determinado, y se vuelven propiedad de algunos.

Es, en definitiva, en el sentido C de «cultura» con el que vamos a
hablar aquí de cultura libre: como una cultura que es puesta en
circulación a partir de ciertos bienes culturales en un mercado
determinado, bienes que son de libre acceso, difusión, adaptación y
valor &mdash;características todas que serán abordadas a lo largo de
este libro&mdash;. Aunque esta *cultura* sea una *mercancía*, tenida en
cuenta en conjunto como un *valor* distintivo y fruto de un *alma
colectiva* que lleva consigo sus políticas y relaciones sociales, esta
distinción nos sitúa por ahora en un concepto a lo largo de las próximas
páginas.

Definida una noción maleable de cultura y de cultura libre, podemos
pasar a otros conceptos que son importantes que aparezcan, aunque en
una explicación mínima, en este prólogo. La noción de que un texto, un
libro, una obra teatral pueda ser *vendido* por un valor determinado no
es algo que se ha dado desde siempre en la historia de la humanidad,
aunque sí una concepción establecida como sentido común a partir de los
siglos XVII y XVIII, con la aparición de los primeros monopolios dados a
los impresores, de la invención del <i lang="en">copyright</i>, de la
propiedad intelectual y de los derechos de autor. Antes de eso había,
claro, producción de libros, diseños, pinturas, esculturas, obras
teatrales hechos y puestos en circulación para diferentes públicos,
pero no había un consenso de que esas obras circularían a cambio de una
determinada cuantía, que sería pagada a su *dueño*, o a quien las
produjo. Y no lo había por varios motivos: primero porque la circulación
era restringida, debido a la dificultad de que se produjera (en el caso
de un libro, por ejemplo); segundo porque la forma de disfrute de esas
obras era comúnmente colectiva y oral, no individual; y tercero porque
no estaba bastante claro el sentido de que una obra dada tenía algún
dueño o incluso un *autor*, como se explica en el capítulo 1 [«Cultura
oral»](#cultura-oral).

Solo comienza a tener sentido la relación de los bienes culturales con
mercancías con un determinado precio y con autor cuando, en el siglo XV,
se crea una máquina de impresión que propaga ciertos tipos de bienes
culturales para públicos mucho mayores de los que existían hasta
entonces. A partir de entonces, se establecen formas de controlar la
circulación de esos bienes, como leyes, como el
<i lang="en">copyright</i>, un derecho concedido a alguien, de modo
exclusivo, para producir y reproducir una obra, como se describe en el
capítulo 2 [«Cultura impresa»](#cultura-impresa). Después surge la noción de propiedad
intelectual, que se consolidó en los siguientes siglos como una rama del
derecho civil, que trata de regular creaciones del intelecto humano,
como se muestra en el capítulo 3 [«Cultura propietaria»](#cultura-propietaria), a partir de una
relación, hasta hoy cuestionada, con la propiedad física.

A partir del siglo XIX, la propiedad intelectual se consolida dividida
en dos ramas. Una de ellas es el *derecho de autor*, establecido a raíz
del <i lang="en">copyright</i>, en el siglo XVIII, en la Francia de la
Ilustración, como un conjunto de prerrogativas dadas por ley a una
persona o una empresa a la que se le atribuye la creación de una obra
intelectual. Los derechos de autor van a ser, a su vez, divididos en
otras dos ramas: los *derechos morales*, referentes a las leyes que
rigen la autoría de una obra y su integridad, es decir, la posibilidad o no
de alterar una creación en particular; y los *derechos
patrimoniales*, que regulan la producción y reproducción comercial de
esa obra. En ese período ya se percibe que había una situación más
compleja en la difusión de una obra para muchas más personas, que con eso
se pasaba a un disfrute menos colectivo y cada vez más individual de
bienes culturales, y también que el autor de una determinada obra puede
ser identificado como aquel «que permite remontar las contradicciones
que pueden desplegarse en una serie de textos»[^4].

[^4]: Como Michel Foucault conceptualizó en su conocido ensayo «¿Qué es
  un autor?» el 1969 en <cite>Dichos y escritos</cite>, v. 3.

A finales del siglo XIX y durante el siglo XX, cuando esas nociones se
consolidan en el sentido común y en un sistema legal de propiedad
intelectual, son innumerables las formas, en especial en el arte y la
contracultura, de dar respuesta a lo establecido. «¿Necesito pagarle a
alguien para leer un libro?», «¿soy dueño de este texto?», «¿quién ha
dicho que no puedo usar un extracto de una obra para hacer otra, o para
inventar una nueva forma de arte, nuevos bienes culturales?». Algunos
movimientos vanguardistas, artistas y colectivos se enfrentan al
<i lang="lt">statu quo</i> del derecho de autor y de la autoría y, por eso,
se vuelven defensores de una cultura libre antes de que el término se
popularizara, así como hay otros que cuestionan la condición de
originalidad de una obra determinada en una época de propagación de las
máquinas de reproducción técnica, como se informa en el capítulo 4
[«Cultura recombinante»](#cultura-recombinante).

La otra rama en que se divide la propiedad intelectual es la llamada
propiedad industrial. Está ligada a la producción y al uso de
determinados bienes a escala industrial, lo cual amplía el control legal
de la creación a procesos, invenciones, modelos, diseños, identificados
como obras de utilidad &mdash;es decir, que son usadas para un
fin concreto en un mercado determinado, en oposición al derecho de
autor, que rige la creación artística, científica, musical, literaria, y
que, en esa concepción, no sería utilitaria&mdash;. Las propiedades
industriales tienen como principal elemento registrador la patente, una
concesión pública &mdash; por lo tanto, otorgada por algún órgano
estatal&mdash; para que un determinado titular explote comercialmente,
de manera exclusiva y limitada en el tiempo, una determinada creación.
De la lámpara incandescente a la cámara cinematográfica, del fonógrafo
de Thomas Edison hasta el <i lang="en">software</i>, las patentes son
monopolios de explotación comercial de una idea que generan mucho
dinero, también por eso muchas batallas y cuestionamientos críticos,
especialmente del siglo XIX en adelante.

La expansión de la tecnología digital y su casi omnipresencia en la vida
de buena parte de los más de siete billones de personas que habitan el
planeta Tierra en el siglo XXI dan lugar a condiciones aún más complejas
de producción, difusión y comercialización de bienes culturales. Con
esto, otra noción que trata esta obra se vuelve todavía más maleable:
¿al final qué es copia y qué es original? Si Internet solamente funciona
a partir de la copia de datos y archivos que son transferidos y
compartidos, ¿es posible controlar la reproducción de una canción
copiada millones de veces copiada y que, mientras tanto, sigue
existiendo *igualmente* en todos los millones de copias? La discusión en
torno a la compartición de archivos en la red y sus consecuencias da
lugar, finalmente, al capítulo 5, [«Cultura libre»](#cultura-libre), no
por casualidad el más grande de todos.

Tradiciones milenarias en Extremo Oriente y en algunos pueblos
originarios de América Latina nos muestran que el mundo no es solo lo
que llamamos occidental y que la perspectiva de lo que es copia,
original, libre y colectivo tiene diferencias significativas entre
culturas diferentes. Son ideas que nos incitan a descolonizar nuestra
visión occidental aplicada a las historias, filosofías y modos de pensar
las cosas y el mundo que conocemos, y a buscar modos diferentes de ver
esas cuestiones, como es el caso del concepto de <i lang="zh">shanzai</i>
en China, sinónimo de producto falso, <i lang="en">fake</i>, pero
también una forma de ver los bienes culturales
como elementos en constante transformación de acuerdo a cada contexto,
objetivo y fin, sin un único y sagrado origen. Y también la perspectiva
de algunos pueblos amerindios que, al no separar sujeto de objeto,
vuelven el vocabulario desarrollado sobre la propiedad y el derecho de
autor insuficiente para usarse con esos pueblos, como se muestra en el
capítulo 6 [«Cultura colectiva»](#cultura-colectiva). Es a partir de la mezcla de algunas de
esas experiencias no occidentales citadas y de una visión desde el
llamado sur global que, al final de ese capítulo, apuntamos algunas
alternativas para la propagación y la defensa de una cultura libre hoy.

