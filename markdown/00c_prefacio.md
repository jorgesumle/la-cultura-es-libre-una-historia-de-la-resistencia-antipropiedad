El abanico conceptual abierto por este libro que tienes en tus manos va
de Occidente a Oriente, tratando toda la trayectoria de las
especulaciones sobre las nociones contrastantes de propiedad intelectual
y dominio público, desde la Grecia y la Roma antiguas y la China
Imperial bajo la influencia del confucionismo, pasando por la Edad Media
y los mundos renacentista e ilustrado europeos, por la modernidad
globalizante de la expansión de los horizontes mundiales del período de
los descubrimientos &mdash;la expansión a las Américas, África y
Asia&mdash;, hasta nuestros días, con los extraordinarios impactos de
las tecnologías digitales modernas sobre la producción y circulación de
obras culturales en todo el planeta.

Este abanico conceptual se encuentra, en este libro, profusamente
ilustrado mediante varios pasajes históricos que sentaron las bases para
la creación de los llamados «derechos de propiedad intelectual». El
libro trata esta creación con múltiples observaciones sobre cómo
«personas, grupos y movimientos subvirtieron el <i lang="lt">status
quo</i> de sus épocas, de la creación y circulación de la cultura y del
arte». Contiene ilustraciones que van desde las descripciones sobre
técnicas de utilización del papiro para la confección de los primeros
libros en remotas épocas imperiales, pasando por la revolución de la
imprenta que abrió la época posmedieval (con Gutenberg), constatando las
nuevas imposiciones económico-político-sociales en Inglaterra y Francia
&mdash;del siglo  XVI hasta el XVIII&mdash; en su transición de
monarquías absolutistas a regímenes constitucionales (con el surgimiento
del <i lang="en">copyright</i> y del derecho de autor), observando la
llegada de la radio (con Guglielmo Marconi), hasta, finalmente,
desembocar en la era contemporánea del cine, la televisión e Internet
con todo lo que nos relaciona, hoy, con la llamada *cultura libre* (el
<i lang="en">software</i> libre, el <i lang="en">sampler</i>, las
diversas formas de compartir, etc.).

Mi relación con el amplio abanico de alcance conceptual del libro viene
del deseo de que este sea leído con la lente multifocal que requiere,
necesaria para cubrir la vasta diversidad de ambición del autor para
tratar una de las cuestiones más complejas de la historia cultural de la
humanidad. Las implicaciones de la existencia de una o de múltiples
nociones de un derecho de propiedad, con respecto a nuestras actividades
artísticas e intelectuales a lo largo de varios períodos de desarrollo
de nuestra civilización, es fundamental para la comprensión de cómo
llegamos hasta aquí y de hacia dónde estamos caminando como sociedad
humana. Este libro, a pesar de estar centrado en el derecho de autor en
oposición al dominio público de las ideas &mdash;cuestión en sí misma
suficiente para ocupar todo un universo especulativo&mdash;, nos informa
sobre conocimiento y razón, nos ayuda a medir nuestro horizonte de
desarrollo humano con la anchura de la pluralidad de perspectivas. El
libro se centra en la propiedad intelectual, pero revela mucho más: la
propia noción histórica de propiedad, todo un mundo de carencias y
riquezas de los poseedores y los desposeídos.

Un vasto libro sobre cultura, política, sociología, antropología e
historia. Un libro de una sobriedad elocuente sobre cuestiones casi
siempre poco sencillas sobre la dinámica de las disputas humanas. Un
libro para la actualidad, para la posmodernidad y para el futuro
civilizatorio. Para sacarle provecho leamos.

<br>
<p class="right-align">Gilberto Gil</p>

