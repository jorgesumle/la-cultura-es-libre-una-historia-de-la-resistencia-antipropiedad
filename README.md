# La cultura es libre: Una historia de la resistencia antipropiedad

Esto es una traducción del libro
<a href="https://baixacultura.org/download/13637/"><i lang="pt">A Cultura é
Livre: Uma história da resistência antipropriedade</i></a> de Leonardo
Foletto, quien lo publicó bajo la licencia <a
href="https://creativecommons.org/licenses/by-sa/4.0/deed.es"><abbr
title="Creative Commons Atribución-CompartirIgual 4.0 International">CC
BY-SA 4.0</abbr></a>.

## Generar libro

### HTML y EPUB

```
sudo apt install make pandoc
make html epub
```

### PDF

#### Debian GNU/Linux y derivados

Hay que instalar [estas fuentes chinas](https://tex.stackexchange.com/questions/168732/how-to-install-correctly-simhei-ttf-and-simsun-ttc-for-pdflatex-on-tex-live-2013) y ejecutar los siguientes comandos:

```
sudo apt install fonts-noto-core fonts-sil-gentium-basic make pandoc texlive texlive-lang-arabic texlive-lang-chinese texlive-lang-spanish texlive-xetex
make pdf
```

#### Otros sistemas operativos

Hay que instalar las siguientes fuentes:

- [Estas fuentes chinas](https://tex.stackexchange.com/questions/168732/how-to-install-correctly-simhei-ttf-and-simsun-ttc-for-pdflatex-on-tex-live-2013)
- Noto Sans Arabic
- Gentium Book Basic

Asimismo, hay que instalar TeX Live con soporte para árabe y chino,
XeTeX y pandoc.


## Colaborar

La traducción está escrita en Markdown para luego transformarla a varios
formatos con [pandoc](https://pandoc.org/). Cada sección del libro se
encuentra en un archivo `.md` diferente.

El archivo `Makefile` sirve para generar los archivos de salida.
`index.html` es la
[página](https://freakspot.net/libro/la-cultura-es-libre/) que enlaza a
los diferentes formatos del libro.

## Errores del original

- Es 'Teognis de [Mégara](https://pt.wikipedia.org/wiki/M%C3%A9gara)',
  no de *'Megarar'.
- En \*«<i lang="pt">não existe evidência a respeito da data de criação
  dessas duas obras nem que houve uma pessoa chamada Homero que a
  escreveu</i>», debería decir «[...] <i lang="pt">as escreveu</i>»,
  porque son dos (plural).
- En la página 66, en \*«Um dos mais importantes divulgadores dessa
  ideias» debería decir «Um dois mais importantes divulgadores dessas
  ideias» (concordancia).
- En la página 68, en «entre eles o de impressores, estabelecidos desde
  meados do século XVI» no se entiende bien la oración explicativa si no
  se refiere a un singular (el derecho establecido).
- En la página 68, en «os chamados “privilégios do autor” (*privilèges
  d’auteur*), que, diferente dos», debería decir «diferentes dos» para
  concordar.
- En la página 75, el título «III» debería tener un tamaño más grande.
- En la página 81, es «calótipo», no \*«celótipo».
- En la página 82, es «cinetoscópios», no \*«cinestocópios».
- En la página 89, es «calótipo», no \*«celótipo».
- En la página 107, falta un espacio después del punto en
  «poème.Découpez» (nota 103).
- En la página 109, en la nota 106, la URL a la que quiere hacer
  referencia el autor es
  <https://www.nealumphred.com/hugo-ball-sound-poetry-gadji-beri-bimba/>.
- En la página 126, en la enumeración de la cita
  («<i lang="pt">condições de elaborar a montagem de seus projetos,
  fundindo planos; linhas e sombras, sem qualquer instrumento auxiliar
  que a técnica do desenho exige</i>.»)no tiene sentido que haya un
  punto y coma.
- En la página 143 (nota 142), arregla el título del libro de Pekka
  Himanen. Es «espíritu», y no \*«espírito».
- En la página 143 (nota 143), arregla el nombre de la editorial (es
  «Traficantes de Sueños», no \*«Traficante de Sueños»).
- En la página 145 (nota 145), arregla el título del libro.
- En la página 145 (nota 146), arregla el título del libro, mostrando
  solo el citado en castellano.
- En la página 150 «do» no debería ir en cursiva ya que no forma parte
  del extranjerismo «<i lang="lt">status quo</i>».
- En la página 151, es «Open Access», no \*«Open Acess».
- En la página 153 (nota 159) sobran unas comillas dobles, antes de
  «<i lang="pt">A distinção</i>».
- En la página 154, es «Electronic Frontier Foundation», no \*«Eletronic
  Frontier Foundation».
- En la página 156 (nota 168), el título del libro es <cite>How the
  Commoners Built a Digital Republic of their Own,</cite> (no
  \*«Republico»).
- En la página 163 (nota 183), arregla la URL (es «wired.com», no
  \*«wired.C.om»).
- En la página 166, es «<i lang="pt">tipos de compressão de áudio</i>»,
  y no\*«<i lang="pt">tipos de compreensão de áudio</i>».
- En la página 169 (nota 191), sobran unas comillas de cierre después de
  «<i lang="pt">português</i>.
- En la página 172 (nota 195), es redundante decir «<i lang="pt">mbps
  por segundo</i>».
- En la página 184, la frase «<i lang="pt">impulsiona a vigilância
  on-line de todos os hábitos de uma pessoa na rede</i>» es redundante.
- En la página 186, los algoritmos de <i lang="en">streaming</i> solo se
  encargan del <i lang="en">streaming</i>, el autor debe de referirse a
  los de la plataforma de <i lang="en">streaming</i>.
- En la página 225 (nota 292), arregla en enlace.
- Las palabras estranjeras como <i lang="en">software</i> y
  <i lang="en">hardware</i> se escriben en cursiva.
- En la página 235 arregla el enlace de <cite>The Economy of
  Ideas</cite>.
- En la página 240 arregla el título del libro de Pekka Himanen. Es
  *espíritu*, y no \*espírito.
- En la página 245 no es \*'Madri', sino 'Madrid'.
- En la sección «Referências» la editorial no es \*'Traficante de Sueños', sino 'Traficantes de Sueños'.
- El artículo enlazado en la nota a pie de página 33 ya no existe, así
  que lo he reemplazado por otro que contiene el mismo texto.

## Modificaciones

- A diferencia del PDF original, los enlaces a Internet son interactivos.
- En el capítulo 5 no hay apartado IV, Pasa del III al V, así que lo he
  ajustado para que no haya tal salto.
- Cita del libro <cite>Cultura libre: Cómo los grandes medios
  usan la tecnología y las leyes para encerrar la cultura y controlar la
  creatividad</cite> (traducción española, en vez de traducción
  portuguesa, que me parece de peor calidad).
- En el enlace de la página 116 (nota 121), pone el enlace con HTTPS
  para evitar redirección
  (<https://enciclopedia.itaucultural.org.br/termo3654/situacionismo>).
- Actualiza el año al 2022 en «<i lang="pt">quando Cuba então assina tratados
  internacionais e passa a adotar o período de 50 anos após a morte do
  autor, o que permanece em 2020</i>».
- Actualiza el año al 2022 en «<i lang="pt">As licenças CC existentes em
  2020 estão detalhadas no site</i>» (pág. 156).
- Pone en pasado «<i lang="pt">São, em 2020, depois de muita
  organização</i>», porque ya no estamos en 2020.
- En la página 231 usa la nomenclatura oficial de Creative Commons
  «BY-SA».
