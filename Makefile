MARK_DIR=markdown/
SECTIONS=$(MARK_DIR)00a_portada.md $(MARK_DIR)00b_indice.md $(MARK_DIR)00c_prefacio_tit.md $(MARK_DIR)00c_prefacio.md $(MARK_DIR)00d_presentacion_tit.md  $(MARK_DIR)00d_presentacion.md $(MARK_DIR)00e_introduccion_tit.md $(MARK_DIR)00e_introduccion.md $(MARK_DIR)01_cultura-oral_tit.md $(MARK_DIR)01_cultura-oral.md $(MARK_DIR)02_cultura-impresa_tit.md $(MARK_DIR)02_cultura-impresa.md $(MARK_DIR)03_cultura-propietaria_tit.md $(MARK_DIR)03_cultura-propietaria.md $(MARK_DIR)04_cultura-recombinante_tit.md $(MARK_DIR)04_cultura-recombinante.md $(MARK_DIR)05_cultura-libre_tit.md $(MARK_DIR)05_cultura-libre.md $(MARK_DIR)06_cultura-colectiva_tit.md $(MARK_DIR)06_cultura-colectiva.md $(MARK_DIR)90_epilogo_tit.md $(MARK_DIR)90_epilogo.md $(MARK_DIR)91_referencias_tit.md $(MARK_DIR)91_referencias.md $(MARK_DIR)92_agradecimientos_tit.md $(MARK_DIR)92_agradecimientos.md $(MARK_DIR)93_sobre-el-autor_tit.md $(MARK_DIR)93_sobre-el-autor.md
SECTIONS_PDF=$(MARK_DIR)00c_prefacio.md $(MARK_DIR)00d_presentacion.md $(MARK_DIR)00e_introduccion.md $(MARK_DIR)01_cultura-oral.md $(MARK_DIR)02_cultura-impresa.md $(MARK_DIR)03_cultura-propietaria.md $(MARK_DIR)04_cultura-recombinante.md $(MARK_DIR)05_cultura-libre.md $(MARK_DIR)06_cultura-colectiva.md $(MARK_DIR)90_epilogo.md $(MARK_DIR)91_referencias.md $(MARK_DIR)92_agradecimientos.md $(MARK_DIR)93_sobre-el-autor.md
SECTIONS_EPUB=$(MARK_DIR)00c_prefacio_tit.md $(MARK_DIR)00c_prefacio.md $(MARK_DIR)00d_presentacion_tit.md  $(MARK_DIR)00d_presentacion.md $(MARK_DIR)00e_introduccion_tit.md $(MARK_DIR)00e_introduccion.md $(MARK_DIR)01_cultura-oral_tit.md $(MARK_DIR)01_cultura-oral.md $(MARK_DIR)02_cultura-impresa_tit.md $(MARK_DIR)02_cultura-impresa.md $(MARK_DIR)03_cultura-propietaria_tit.md $(MARK_DIR)03_cultura-propietaria.md $(MARK_DIR)04_cultura-recombinante_tit.md $(MARK_DIR)04_cultura-recombinante.md $(MARK_DIR)05_cultura-libre_tit.md $(MARK_DIR)05_cultura-libre.md $(MARK_DIR)06_cultura-colectiva_tit.md $(MARK_DIR)06_cultura-colectiva.md $(MARK_DIR)90_epilogo_tit.md $(MARK_DIR)90_epilogo.md $(MARK_DIR)91_referencias_tit.md $(MARK_DIR)91_referencias.md $(MARK_DIR)92_agradecimientos_tit.md $(MARK_DIR)92_agradecimientos.md $(MARK_DIR)93_sobre-el-autor_tit.md $(MARK_DIR)93_sobre-el-autor.md
LANG=es-ES
MARKDOWN_ONE_FILE=la-cultura-es-libre.md
HTML_OUTPUT=la-cultura-es-libre.html
TEX_OUTPUT=la-cultura-es-libre.tex
TITLE=La cultura es libre: una historia de la resistencia antipropiedad
STYLESHEET=style.css

epub:
	cat $(SECTIONS_EPUB) > $(MARKDOWN_ONE_FILE)
	sed -i "s/## /# /g" $(MARKDOWN_ONE_FILE)
	sed -i "s/### /## /g" $(MARKDOWN_ONE_FILE)
	pandoc $(MARKDOWN_ONE_FILE) -f markdown --metadata title="$(TITLE)" -t epub -s -o la-cultura-es-libre.epub metadata.yaml --toc -V lang=$(LANG)
	rm $(MARKDOWN_ONE_FILE)

latex:
	cat $(SECTIONS_PDF) > $(MARKDOWN_ONE_FILE)
	sed -i "s/<[\/]*\(i\|cite\)[^>]*>/*/g" $(MARKDOWN_ONE_FILE)
	pandoc $(MARKDOWN_ONE_FILE) -f markdown -t latex -s -o $(TEX_OUTPUT) -V lang=$(LANG)
	./latex_personalizado/ajustes_latex.sh
	rm $(MARKDOWN_ONE_FILE)

html:
	pandoc $(SECTIONS) -f markdown -H header.html -t html --css $(STYLESHEET) --metadata pagetitle="$(TITLE)" -s -o $(HTML_OUTPUT) -V lang=$(LANG)
	sed -i 's|<head|<head prefix="og: http://ogp.me/ns# book: http://ogp.me/ns/book#"|g' $(HTML_OUTPUT)

pdf: latex
	xelatex $(TEX_OUTPUT)

publish: epub html pdf
	scp -r index.html imagenes/ la-cultura-es-libre.* style.css root@185.112.144.134:/var/www/html/libro/la-cultura-es-libre/

