TEX_OUTPUT=la-cultura-es-libre.tex

sed -i '/{article}/r latex_personalizado/documentclass.tex' $TEX_OUTPUT
sed -zi 's/\\documentclass\[[[:space:]]*spanish,\n*\]{article}/\\documentclass\[\n  11pt,spanish,twoside\n]{article}/' $TEX_OUTPUT
sed -i 's/\\setotherlanguage\[\]{english}/\\setotherlanguages{arabic, english}\n\\newfontfamily\\arabicfont\[Script=Arabic,Scale=1.1\]{Noto Sans Arabic}/' $TEX_OUTPUT
sed -i 's/\\usepackage{hyperref}/\\usepackage[colorlinks,linktoc=all]{hyperref}\n\\hypersetup{linkcolor=black}/' $TEX_OUTPUT
sed -i '/begin{document}/r latex_personalizado/begin.tex' $TEX_OUTPUT
sed -i 's/El abanico conceptual abierto por este libro que tienes en tus manos va/\\noindent &/' $TEX_OUTPUT
sed -i '/^Gilberto Gil/r latex_personalizado/despues-de-prefacio.tex' $TEX_OUTPUT
sed -i 's/Este libro nace de un esfuerzo que data del 2008, año de creación de/\\noindent &/' $TEX_OUTPUT

sed -i '/São Paulo, invierno de 2020/r latex_personalizado/despues-de-presentacion.tex' $TEX_OUTPUT
sed -i 's/^Gilberto Gil/\\\hfill \\\break\\\begin{flushright}\n&\\\end{flushright}/' $TEX_OUTPUT
sed -i 's/Leonardo Foletto/\\\hfill \\\break\\\begin{flushright}\n&/' $TEX_OUTPUT
sed -i 's/São Paulo, invierno de 2020/&\\\end{flushright}/' $TEX_OUTPUT
sed -i '/cultura libre hoy/r latex_personalizado/antes-cap-1.tex' $TEX_OUTPUT
sed -i 's/Marco Valerio Marcial, \\emph{Epigramas}, s. I/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Alguien, s. I/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Séneca, s. II/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i '/El plagio en las literaturas hispánicas}, 2010/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Kevin Perromat, \\emph{El plagio en las literaturas hispánicas}, 2010/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/La palabra «cultura» tiene tantos significados a lo largo de la historia/\\noindent &/' $TEX_OUTPUT

sed -i '/italianas como Sicilia, Nápoles y Venecia/r latex_personalizado/antes-cap-2.tex' $TEX_OUTPUT
sed -i '/\\textenglish{copyleft}}, 2005/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Enea Silvio Bartolomeo Piccolomini, futuro papa Pío II, en carta al/\\\begin{flushright}\n&/' $TEX_OUTPUT
sed -i 's/cardenal Carvajal, 1455/&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Estatuto de Anne, Inglaterra, 1710/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Parlamento inglés, negando la petición de los libreros de aumentar la/\\\begin{flushright}\n&/' $TEX_OUTPUT
sed -i 's/extensión del plazo de los derechos de autoría/&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Wu Ming, \\emph{Notas inéditas sobre \\textenglish{copyright} y/\\\begin{flushright}\n&/' $TEX_OUTPUT
sed -i 's/\\textenglish{copyleft}}, 2005/&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT

sed -i '/pero del otro lado del canal de la Mancha/r latex_personalizado/antes-cap-3.tex' $TEX_OUTPUT
sed -i 's/Marqués de Condorcet, \\emph{Fragmentos sobre la libertad de prensa},/\\\begin{flushright}\n&/' $TEX_OUTPUT
sed -i 's/^1776/&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i '/Thomas Jefferson, en la carta a Isaac McPherson, 1813/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Thomas Jefferson, en la carta a Isaac McPherson, 1813/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT

sed -i '/conocidos en el siglo XX y hasta el día de hoy/r latex_personalizado/antes-cap-4.tex' $TEX_OUTPUT
sed -i 's/Conde de Lautréamont (Isidore Lucien Ducasse), \\emph{Poesias}, 1870/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/\\emph{Derecho a ser traducido, reproducido y deformado en todas las\nlenguas\.}/\\\begin{flushright}\n&\n\\\end{flushright}/' $TEX_OUTPUT
sed -i 's/Oswald de Andrade, \\emph{Serafim Ponte Grande}, 1933/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/Guy Debord; Gil Wolman, \\emph{Um guia para os usuários do detournamènt},\n1956/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/\\emph{El medio es el mensaje\.}/\\\begin{flushright}\n&\n\\\end{flushright}/' $TEX_OUTPUT
sed -i 's/Marshall McLuhan, \\emph{Understand Media}, 1964/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i '/Friedrich A. Kittler, \\emph{Gramofone, filme, typewritter}, 1986/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Friedrich A. Kittler, \\emph{Gramofone, filme, typewritter}, 1986/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT

sed -i '/libre a niveles no conocidos hasta entonces\./r latex_personalizado/antes-cap-5.tex' $TEX_OUTPUT
sed -i 's/Richard Stallman, \\emph{El manifiesto de GNU}, 1985/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Richard Barbrook; Andy Cameron, \\emph{La ideología californiana}, 1995/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/John Perry Barlow, \\emph{Declaración de independencia del ciberespacio},\n1996/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Creative Commons, \\emph{Sé Creativo}, 2000/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/Record Industry Association of America, \\emph{Campaña antipiratería},\naños 2000/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/Wu Ming, \\emph{Copyright y maremoto}, 2002/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i '/Cohn, \\emph{Cultura digital\.br}, 2009/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -zi 's/Eduardo Viveiros de Castro, Economia da cultura digital, en Savazoni;\nCohn, \\emph{Cultura digital\.br}, 2009/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT

# Con pdflatex
# sed -i 's/لخوارزمية/\\<&>/' $TEX_OUTPUT
sed -i 's/لخوارزمية/\\textarabic{&}/' $TEX_OUTPUT

sed -i '/^  p\.\~49\./r latex_personalizado/antes-cap-6.tex' $TEX_OUTPUT
sed -i 's/^\\emph{Yo soy porque nosotros somos\.}/\\\begin{flushright}\n&\n\\\end{flushright}/' $TEX_OUTPUT
sed -i 's/^Ubuntu/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i 's/^Confucio, las \\emph{Analectas}, aprox. s. IV-II a..C./\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -zi 's/Marcela Stockler Coelho de Souza, \\emph{The Forgotten Pattern and the\nStolen Design: Contract, Exchange and Creativity among the Kĩsêdjê},\n2016/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT
sed -i '/Evelin Heidel (Scann), \\emph{Que se callen las musas}, 2017/r latex_personalizado/antes-cap-cit.tex' $TEX_OUTPUT
sed -i 's/Evelin Heidel (Scann), \\emph{Que se callen las musas}, 2017/\\\begin{flushright}\n&\n\\\end{flushright}\\\hfill \\\break/' $TEX_OUTPUT

# Con pdflatex
# sed -i 's/儒學/\\begin{CJK*}{UTF8}{zhsong}&\\end{CJK*}/' $TEX_OUTPUT
# sed -i 's/魯鎮/\\begin{CJK*}{UTF8}{zhsong}&\\end{CJK*}/' $TEX_OUTPUT
# sed -i 's/論語/\\begin{CJK*}{UTF8}{zhsong}&\\end{CJK*}/' $TEX_OUTPUT
# sed -i 's/专利/\\begin{CJK*}{UTF8}{zhsong}&\\end{CJK*}/' $TEX_OUTPUT

sed -i '/remezclado en inverno de 2020/r latex_personalizado/antes-epilogo.tex' $TEX_OUTPUT
sed -zi 's/Internet, Iberoamérica,\n\n\\sout{sur global, 23 de noviembre de 2018}\n\nremezclado en inverno de 2020/\\\begin{flushright}\n&\n\\\end{flushright}/' $TEX_OUTPUT

sed -i 's/Después de hablar tanto de creación, reapropiación, propiedad, copia,/\\noindent &/' $TEX_OUTPUT
sed -i '/una experiencia única y singular de conocer\./r latex_personalizado/antes-referencias.tex' $TEX_OUTPUT
sed -i 's/Este libro fue escrito, en su redacción final, entre septiempre de 2019/\\noindent &/' $TEX_OUTPUT
sed -i '/{\[}1972{\]} 2000\./r latex_personalizado/antes-agradecimientos.tex' $TEX_OUTPUT

sed -zi 's/\(\\includegraphics{imagenes\/Leonardo-Feltrin-Foletto\.png}\)\n\n\(Foto: Sheila Uberti\)/\\hspace\*{-0.65cm}\\rotatebox{90}{\\footnotesize \2} \1/' $TEX_OUTPUT
sed -i 's/Leonardo Feltrin Foletto nació en Taquari, interior de Río Grande del/\\noindent &/' $TEX_OUTPUT
sed -i '/entender la cultura libre\.$/r latex_personalizado/antes-sobre-el-autor.tex' $TEX_OUTPUT

# Por alguna razón no funciona ~ en algunos sitios
sed -i 's/n\.\~/n\. /' $TEX_OUTPUT
sed -i 's/p\.\~/p\. /g' $TEX_OUTPUT
sed -i 's/ed\.\~bras\./ed\. bras\./' $TEX_OUTPUT
sed -i 's/vs\.\~/vs\. /g' $TEX_OUTPUT
sed -i 's/al\.\~/al\. /' $TEX_OUTPUT
sed -i 's/a\.\~C\./a\. C\./g' $TEX_OUTPUT
sed -i 's/d\.\~C\./d\. C\./' $TEX_OUTPUT
sed -i 's/Jr\.\~/Jr\. /' $TEX_OUTPUT
sed -i 's/1\~000/1 000/' $TEX_OUTPUT
sed -i 's/58\~990/58 990/' $TEX_OUTPUT
sed -i 's/Dr\.\~/Dr\. /' $TEX_OUTPUT

# Arreglo de página 125
sed -i '/treinta minutos en cada uno de sus lados./a \\\enlargethispage{\\baselineskip}' $TEX_OUTPUT

